#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Comparsion of OTU composiitons from several ways to compute them
Atlas Guyane of Laurales

Methods for building OTUS are:
-> from pairwise distance array 
- as cc of the graph built from a gap
- as islands in a MDS scatter plot
- as islands in a t-SNE scatter plot
-> with Swarm.

Composition is given by ascii file
atlas_guyane_laurales.sw.cc.ocf
atlas_guyane_laurales.sw.mds.ocf
atlas_guyane_laurales.sw.tsne.ocf
atlas_guyane_laurales.swarm.ocf

"ocf" stads for otu composition file

23.10.05
af
"""

import numpy as np
import matplotlib.pyplot as plt
import yap_classes as yap

# ----------------------------------------------------------------------
#
#					Preparation
#
# ----------------------------------------------------------------------


### --------------------------------------------------general parameters


basename		= "atlas_guyane_laurales"

## for plots
varname			= "species"
fmt				= "pdf"

## for graph
gap				= 7

## for tree
height			= 10
leaf_font_size	= 6

## for oTUs as islands
mds_bins 		= 32
tsne_bins 		= 32
mds_level		= .5
tsne_level		= .5
min_size		= 1


### ----------------------------------------------------------file names


fasfile				= basename + ".fas"
charfile			= basename + ".char"
disfile				= basename + ".sw"
coordfile_mds		= basename + ".sw.mds"
coordfile_tsne		= basename + ".sw.tsne"
#
hacfile				= basename + ".sw.hac"
#
clusfile_cc			= basename + ".sw.cc.clus"
clusfile_mds		= basename + ".sw.mds.ild.clus"
clusfile_tsne		= basename + ".sw.tsne.ild.clus"
clusfile_swarm		= basename + ".swarm.clus"
#
ocffile_hac			= basename + ".sw.hac.ocf"
ocffile_cc			= basename + ".sw.cc.ocf"
ocffile_mds			= basename + ".sw.mds.ild.ocf"
ocffile_tsne		= basename + ".sw.tsne.ild.ocf"
ocffile_swarm		= basename + ".swarm.ocf"


### --------------------------------------------designs colors for plots


char 			= yap.charmat(charfile=charfile)
v_tax			= char.get_tax(varname)
v_col, dicol	= yap.def_colors(v_tax)

### -------------------------------------------computes the SW distances

fas		= yap.fasta(fasfile=fasfile)
fas.computes_distances(disfile=disfile)
dis		= yap.distances(disfile=disfile)


# ----------------------------------------------------------------------
#
#					OTU as clusters
#
# ----------------------------------------------------------------------

tr				= dis.to_tree()
tr.draw(labels=v_tax, leaf_font_size=leaf_font_size, imfile=hacfile, fmt=fmt)
part_hac 		= tr.get_clusters_byheight(height)
part_hac.composition(var=v_tax, min_size=min_size, ocffile=ocffile_hac)


# ----------------------------------------------------------------------
#
#					OTU as cc
#
# ----------------------------------------------------------------------

gr			= dis.to_graph(gap=gap)
clus_cc 	= gr.build_cc()
part_cc 	= yap.partition(clus=clus_cc)
part_cc.write_clus(clusfile=clusfile_cc)
part_cc.composition(var=v_tax, min_size=min_size, ocffile=ocffile_cc)

# ----------------------------------------------------------------------
#
#					OTUs from MDS scatterplot
#
# ----------------------------------------------------------------------

# computes and the MDS

pc_mds	= dis.mds(coordfile=coordfile_mds)

# plots the MDS point cloud 

pc_mds.scatter(v_col=v_col, 
dot_size=20, legend=True, dicol=dicol, imfile=coordfile_mds, fmt=fmt)

# builds OTUs from  MDS islands, and their composition

pc_mds.heatmatrix(bins=mds_bins)
part_mds = pc_mds.label_otus(level=mds_level, seq_id=fas.seq_id)
part_mds.write_clus(clusfile=clusfile_mds)
part_mds.composition(var=v_tax, min_size=min_size, ocffile=ocffile_mds)

# ----------------------------------------------------------------------
#
#					OTUs from t-SNE scatterplot
#
# ----------------------------------------------------------------------

# computes and the TSNE

pc_tsne	= dis.tsne(coordfile=coordfile_tsne)

# plots the MDS point cloud 

pc_tsne.scatter(v_col=v_col, dot_size=20, legend=True, dicol=dicol, imfile=coordfile_tsne, fmt=fmt)

# builds OTUs from  tsne islands, and their composition

pc_tsne.heatmatrix(bins=tsne_bins)
part_tsne = pc_tsne.label_otus(level=tsne_level, seq_id=fas.seq_id)
part_tsne.write_clus(clusfile=clusfile_tsne)
part_tsne.composition(var=v_tax, min_size=min_size, ocffile=ocffile_tsne)

# ----------------------------------------------------------------------
#
#					OTUs with SWARM
#
# ----------------------------------------------------------------------


clus_swarm	= fas.swarm()
part_swarm	= yap.partition(clus=clus_swarm)
part_swarm.write_clus(clusfile=clusfile_swarm)
part_swarm.composition(var=v_tax, min_size=min_size, ocffile=ocffile_swarm)


y_hac	= np.log(part_hac.sizes)
y_cc	= np.log(part_cc.sizes)
y_mds	= np.log(part_mds.sizes)
y_tsne	= np.log(part_tsne.sizes)
y_swarm	= np.log(part_swarm.sizes)

plt.plot(y_swarm, c="red", label="swarm")
plt.plot(y_hac, c="blue", label="clustering")
plt.plot(y_tsne, c="green", label="islands, tsne")
plt.plot(y_cc, c="cyan", label="cc")
plt.plot(y_mds, c="orange", label="islands, mds")
plt.legend()
plt.title("Rank-size plot of the sets of OTUs for different methods")
plt.savefig("compare_rank_sizes." + fmt, format=fmt)
plt.show()


# ======================================================================
#
#					That's all, folks
#
# ======================================================================


