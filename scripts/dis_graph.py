#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import yap_classes as yap

### ----------- setting basename and parameters 

# basename and parameter

datarep = "../laurales/"
basename = "atlas_guyane_laurales"
gap = 7

# for I/O
dis_algo = "sw"
dis_itype = "dis"

# plotting the graph: how 

tool = "quick"
varname = "genus"
d_size = 20
legend = True

# plotting the graph: where

x11 = True
fmt = "png"
save = True

### ------------ building file names (I/O)

# charfile and characters

charfile = datarep + basename + ".char"
char = yap.charmat(charfile=charfile)
v_tax = char.get_tax(varname)
v_col, dicol = yap.def_colors(v_tax)

# distance file

disfile = datarep + basename + "." + dis_algo + "." + dis_itype 
dis = yap.distances(disfile=disfile)

# file where to save the plot

gr_basename = basename + "." + dis_algo + ".g" + str(gap)

# building the graph
gr = dis.to_graph(gap=gap)

# plotting the graph

imfile 	= datarep + "plots/" + gr_basename + ".graph"
gr.plot(tool=tool, col_node=v_col, col_edge="gray", dot_size=d_size, legend=legend, dicol=dicol, title=gr_basename, x11=x11, imfile=imfile, fmt=fmt)
