#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yap_classes as yap

# setting basename and parameters 

basename = "../malabar/190204_PM_PEL_Tey_f20_rbcL"
dis_algo = "sw"
dis_itype = "h5"
n_axis = 100
meth = "grp"

# building file names (I/O)

h5file = basename + "." + dis_algo + "." + dis_itype
coordfile = basename + "." + dis_algo + ".mds"

# loading file and computation

dis = yap.distances(h5file=h5file)
dis.mds(k=n_axis,meth=meth, pc=False, coordfile=coordfile)
