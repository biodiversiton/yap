#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import numpy as np
import yap_classes as yap

#basename	= "../atlas_guyane_laurales/atlas_guyane_laurales"
basename	= "../malabar/190204_PM_PEL_Tey_f20_rbcL"
dis_algo	= "sw"
dis_itype	= "h5"
linkage		= "single"
#
if dis_itype=="h5":
	h5file		= basename + "." + dis_algo + "." + dis_itype
	dis			= yap.distances(h5file=h5file)
tr			= dis.to_tree(meth=linkage)
tr.draw(truncate=20)
tr.Z_heights(Log=True)
