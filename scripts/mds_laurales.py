#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yap_classes as yap

# setting basename and parameters 

basename = "../laurales/atlas_guyane_laurales"
dis_algo = "sw"
dis_itype = "dis"
n_axis = 100
meth = "svd"

# building file names (I/O)

disfile = basename + "." + dis_algo + "." + dis_itype
coordfile = basename + "." + dis_algo + ".mds"

# loading file and computation

dis = yap.distances(disfile=disfile)
dis.mds(k=n_axis,meth=meth, pc=False, coordfile=coordfile)
