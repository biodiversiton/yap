#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import yap_classes as yap

# setting basename and parameters 

basename = "../laurales/atlas_guyane_laurales"
dis_algo = "k6"
dis_otype = "dis"

# building filenames (I/O)

fasfile = basename + ".fas"
disfile = basename + "." + dis_algo + "." + dis_otype

# building an instance of class fasta and computation of distance array

fas = yap.fasta(fasfile=fasfile)
fas.computes_distances(disfile=disfile)
