#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yap_classes as yap

### ------------ basename, parameters, input files 

datarep = "../laurales/"
basename = "atlas_guyane_laurales"
varname = "genus"
dis_algo = "k6"
coord = "mds"
n_axis_splines = 20

# building filenames (input)

charfile = datarep + basename + '.char'
coord_basename = basename + '.' + dis_algo + '.' + coord
coordfile = datarep + coord_basename

# setting character and colors

char = yap.charmat(charfile=charfile)
v_tax = char.get_tax(varname)
v_col, dicol = yap.def_colors(v_tax)

# for display

d_size = 20
legend = True
x11 = True
save = True
fmt = 'png'

# setting point cloud

pc = yap.point_cloud(coordfile=coordfile)

# setting axis

axis_i = 1  # first axis is 1, not 0
axis_j = 2  # second axis is 2, not 1


### ------------- scatter plot of axis 1 & 2 

imfile = datarep + "/plots/" + coord_basename + "." + str(axis_i) + "." + str(axis_j) 
pc.scatter(axis_i-1,axis_j-1, v_col=v_col, dot_size=d_size, legend=legend, dicol=dicol, x11=x11, imfile=imfile,fmt=fmt)

### -------------- parallel coordinates

imfile = datarep + "/plots/" + coord_basename + ".splines" 
pc.plot_splines(v_col, n_axis=n_axis_splines, legend=legend, dicol=dicol, x11=x11, imfile=imfile, fmt=fmt)	

### -------------- heatmap 

bins = 64
log = False
#
pc.heatmatrix(i=axis_i-1, j=axis_j-1, bins=bins, log=log)
imfile = datarep + "/plots/" + coord_basename + ".hm" 
pc.heatmap(x11=x11, imfile=imfile, fmt=fmt)

### -------------- spikes

imfile = datarep + "/plots/" + coord_basename + ".spikes" 
pc.spikeshow(i=axis_i-1, j=axis_j-1, bins=bins, log=log, title=None, x11=x11, imfile=imfile, fmt="png")


