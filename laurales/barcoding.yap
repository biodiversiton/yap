#yapdoc.pdf
#Chapter 2 p 15
#Barcoding project

do fas_len_hist
do fas_len_plot
#do fas_aligned

do char_show
do char_stats
do char_inventory

do disedi
do diskm

do dis_heatmap
set general dis_algo k6
do dis_heatmap


do mds pc_scatter
set general coord tsne
do tsne pc_scatter

print("Script terminated")
