#Following documentation/yapref.pdf
#
# Computes distances
do disedi
do diskm
# Visualize the distance array
do dis_graph
do dis_heatmap
do dis_hac
do dis_hist
do dis_mst
#Dimension reduction of distance based point clouds
do mds
do tsne
#Visualize point clouds
do pc_heatmap
do pc_scatter
do pc_splines
do pc_spikes
#Builds OTUs
do otu_build_cc
do otu_build_hac
do otu_build_islands
do otu_build_swarm
#Analyses OTUs
do otu_splitting
do otu_composition
#Shows an OTU
do otu_show_graph
do otu_show_heatmap
do otu_show_mst
do otu_show_phylotree
do otu_show_tree
#Maps reads on reference databases
do blast_queries
do blast_bes_thit
do blast_neighborhood
do blast_otu
