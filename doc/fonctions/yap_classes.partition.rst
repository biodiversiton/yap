﻿yap\_classes.partition
======================

.. currentmodule:: yap_classes

.. autoclass:: partition

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~partition.__init__
      ~partition.ami
      ~partition.composition
      ~partition.plot_rank_size
      ~partition.vec_of_classes
      ~partition.write_cc_char
      ~partition.write_cc_distances
      ~partition.write_cc_fastas
      ~partition.write_clus
      ~partition.write_clus_byseqid
      ~partition.write_clus_byseqid_old
      ~partition.write_otufile
      ~partition.write_sizes
   
   

   
   
   