﻿yap\_classes.graph
==================

.. currentmodule:: yap_classes

.. autoclass:: graph

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~graph.__init__
      ~graph.build_cc
      ~graph.color_by_degree
      ~graph.communities
      ~graph.communities_old
      ~graph.get_degrees
      ~graph.plot
      ~graph.to_partition
      ~graph.write_clus_deprecated
   
   

   
   
   