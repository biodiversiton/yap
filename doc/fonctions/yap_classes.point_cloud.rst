﻿yap\_classes.point\_cloud
=========================

.. currentmodule:: yap_classes

.. autoclass:: point_cloud

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~point_cloud.__init__
      ~point_cloud.def_colors_deprecated
      ~point_cloud.heatmap
      ~point_cloud.heatmap_deprecated
      ~point_cloud.heatmatrix
      ~point_cloud.hovering
      ~point_cloud.islands
      ~point_cloud.points_123
      ~point_cloud.scatter
      ~point_cloud.set_Y
      ~point_cloud.sns_plot
      ~point_cloud.spikeshow
      ~point_cloud.splines
   
   

   
   
   