﻿yap\_classes.fasta
==================

.. currentmodule:: yap_classes

.. autoclass:: fasta

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~fasta.__init__
      ~fasta.afc
      ~fasta.align
      ~fasta.coa_kmers
      ~fasta.dereplicate
      ~fasta.disedi
      ~fasta.get_histograms
      ~fasta.next_seq
      ~fasta.plot_length
      ~fasta.plot_length_histogram
      ~fasta.set_seq_id
      ~fasta.set_seq_len
      ~fasta.set_words
      ~fasta.swarm
      ~fasta.view
      ~fasta.write
      ~fasta.write_subfile
      ~fasta.xtract_by_seqid
   
   

   
   
   