﻿yap\_classes.tree
=================

.. currentmodule:: yap_classes

.. autoclass:: tree

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~tree.Z_heights
      ~tree.__init__
      ~tree.draw
      ~tree.draw_tree_coloredlabels
      ~tree.get_clusters_byheight
      ~tree.get_clusters_bynumber
      ~tree.set_height
   
   

   
   
   