﻿yap\_classes.otu\_builder
=========================

.. currentmodule:: yap_classes

.. autoclass:: otu_builder

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~otu_builder.__init__
      ~otu_builder.to_partition
      ~otu_builder.write_clus_deprecated
   
   

   
   
   