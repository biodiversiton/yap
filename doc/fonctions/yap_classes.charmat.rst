﻿yap\_classes.charmat
====================

.. currentmodule:: yap_classes

.. autoclass:: charmat

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~charmat.__init__
      ~charmat.add_varname
      ~charmat.build_colorfile
      ~charmat.change_colorfile
      ~charmat.get_colors
      ~charmat.get_inventory
      ~charmat.get_seq_id
      ~charmat.get_stats
      ~charmat.get_tax
      ~charmat.seq_by_taxa
      ~charmat.set_colors
      ~charmat.show
      ~charmat.write2tsv
      ~charmat.write_subfile
      ~charmat.xtract_by_name
   
   

   
   
   