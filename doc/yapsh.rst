=============================
yapsh speaks with yap_classes
=============================

----------------
What it is about
----------------

Here, the ``yapsh`` commands for all methods are deployed each with python code calling library ``yap_classes``, in an interactive python session. This shows that the purpose
of ``yapsh`` as DSL wrapper of ``yap_classes`` is threefolds:

* execute a calculation
* manage the names of output files
* simplify the burden of remembering all parameters, which are in a yaml file.

.. _orga:

------------
Organisation
------------

This documentation displays a series of three python consoles for each method.

* the first console provides the names of the files to be downloaded
* the second console provides the values of the parameters for the method
* the third consol provides the script for running the method

This shows how ``yap_classes`` is organised as follows: for each method,

* one or several input data files are required; the names are managed according to a naming convention, from the ``basename``, and automatically downloaded
* similarly, the names of the output files are automatically generated, with the same convention, and the output files saved (and can be re-used as input files of another method)
* one or several parameters are required; they are fiven in the ``yaml`` file, not required here, where they are given in the interactive python console (or the corresponding script)
* the last block is about running the method, knowing the input files, and the parameters.

-----------
To run them
----------- 

* be sure that the required data files are in the directory on which the terminal has been opened 
* start an interactive python session
* import yap_classes, as yap: 

>>> import yap_classes as yap 

* run the three blocks

A python script named <method>.py is provided for each method with the code in the consoles as a script.


.. _methods:

-------
Methods
-------

.. _fas_len_hist:

fas_len_hist
-------------

>>> basename = 'atlas_guyane_laurales'
>>> fasfile = basename + ".fas"

>>> bins = 50
>>> color = 'red'
>>> kde = True 
>>> title = 'histogram of sequences lengths'
>>> x11 = True
>>> save = True
>>> fmt = 'png'

>>> fas = yap.fasta(fasfile=fasfile)
>>> if save:
...    imfile = "plots/" + basename + '.hist'
>>> else:
...    imfile = None
>>> fas.plot_length_histogram(bins=bins, col=color, kde=kde, title=title, x11=x11, imfile=imfile, fmt=fmt)

.. _fas_len_plot:

fas_len_plot
-------------

>>> basename = 'atlas_guyane_laurales'
>>> fasfile = basename + ".fas"
		
>>> dots = True
>>> color = 'red'
>>> x11 = True
>>> save = True
>>> fmt = 'png'
		
>>> fas	= yap.fasta(fasfile=fasfile)
>>> imfile = "plots/" + basename + '.lenplot'
>>> fas.plot_length(dots=len_dots, col=color, x11=x11, imfile=imfile, fmt=fmt)

.. _fas_phyML:

fas_phyML
----------

>>> basename = 'atlas_guyane_laurales'
>>> fasfile = basename + ".fas"
>>> charfile = basename + ".char"

>>> varname = "species"

>>> fas = yap.fasta(fasfile=fasfile)
>>> fas.phylo_tree(charfile=charfile, varname=varname)

.. _char_counts:

char_counts
------------

>>> basename = 'atlas_guyane_laurales'
>>> charfile = basename + ".char"

>>> varname = 'genus'
>>> mode = '2cols'
>>> min_size = 3

>>> char = yap.charmat(charfile=charfile)
>>> res, count_df char.get_counts(varname)
>>> if mode=="2cols":
...    n = len(res)
...    for i in range(n):
...        item = res[i]
...        if item[1] >= min_size:
...            print(item[0], " -> ", item[1])
... if mode=="df":
...    print(count_df)		


.. _char_stats:

char_stats
-----------

>>> basename = 'atlas_guyane_laurales'
>>> charfile = basename + ".char"

>>> char = yap.charmat(charfile=charfile)
>>> char.get_stats()

.. _disseq:

disseq
-------

Warning: here, for running disseq with yap_classes in a script, ``os`` must be imported.

>>> import os

>>> basename = 'atlas_guyane_laurales'
>>> fasfile = basename + ".fas"

>>> otype = 'tsv'
>>> algo = 'sw' 


>>> dis_basename = basename + "." + algo
>>> if otype=="tsv":
...    if algo=="sw":
...        cmd = 'disseq -ref ' + fasfile + ' -out ' + dis_basename 
...    if algo=="nw":
...        cmd = 'disseq -ref ' + fasfile + ' -algo nw -out ' + dis_basename 
... if otype=="h5":
...    if algo=="sw":
...        cmd	= 'disseq -ref ' + fasfile + ' -out ' + dis_basename + " -h5"
...    if algo=="nw":
...        cmd	= 'disseq -ref ' + fasfile + ' -algo nw -out ' + dis_basename + " -h5"
>>> os.system(cmd)

.. _diskm:

diskm
------


Warning: here, for running diskm with yap_classes in a script, ``os`` and ``numpy`` must be imported.

>>> import os
>>> import numpy as np

>>> basename = 'atlas_guyane_laurales'
>>> fasfile = basename + ".fas"

>>> otype = 'tsv'
>>> kmer = 6


>>> dis_algo = 'k' + str(kmer) 
>>> dis_basename = basename + "." + dis_algo
>>> if otype=="tsv":
...    cmd = 'jelly_diskm.py ' + basename + " " + str(kmer) 
... if otype=="h5":
...    cmd = 'jelly_diskm.py ' + basename + " " + str(kmer) + ' h5'
>>> os.system(cmd)
>>> tjffile	= basename + ".k" + str(kmer) + ".tjf"
>>> Tjf = np.loadtxt(tjffile, delimiter="\t", dtype=object)
>>> (n,p) = Tjf.shape
>>> m = 4**kmer
>>> print("\nThere are potentially ", m, "different kmers")
>>> print(p, "have been realized")
>>> print("i.e a fraction of ", p/m)

.. _dis_hist:

dis_hist
---------

>>> basename = 'atlas_guyane_laurales'

>>> itype = 'tsv'
>>> bins = 20
>>> col = 'green'
>>> kde = True
>>> title = 'histogram of pairwise distances (sw)'
>>> x11 = True
>>> save = True
>>> fmt = 'png'
>>> dis_algo = 'sw'
		
>>> dhistbasename	= basename + "." + dis_algo
>>> dhistdisfile	= dhistbasename + ".dis" 
>>> if itype=="tsv":
...    dis = yap.distances(disfile=dhistdisfile)
...    seq_id	= dis.seq_id
... if itype=="h5":
...    dis = yap.distances(h5file=dhisth5file)
... if save:
...    imfile = "plots/" + dhistbasename + ".dhist"
... else:
...    imfile = None
>>> dis.histogram(bins=bins, col=col, kde=kde, title=title, x11=x11, imfile=imfile, fmt=fmt)

.. _heatmap:

heatmap
--------

*Required file: <basename>.<dis_algo>.dis or <basename>.<dis_algo>.h5, <basename>.char if a varname is selected*
		
>>> heatbasename = basename + "." + dis_algo
>>> if itype=="tsv":
...    heatdisfile = heatbasename + ".dis"
... if itype=="h5":
>>> if itype=="tsv":
...    dis	= yap.distances(disfile=heatdisfile)
...    self.seq_id	= dis.seq_id
... if itype=="h5":
...    dis = yap.distances(h5file=heatdisfile)
>>> if varname:
...    char = yap.charmat(charfile=self.charfile)
...    labels = char.get_tax(varname)
... else:
...    labels	= seq_id
>>> if seaborn:
...    dis.clus_heatmap(labels=labels)
...    plt.show()
>>> else:
...    tr = dis.to_tree()
...    tr.draw()
...    dis.heatmap(items_order=tr.nodes_order)

.. _hac:

hac
----

*Required file: <basename>.<dis_algo>.dis or <basename>.<dis_algo>.h5 or <basename>.char* 
		
>>> hacbasename = self.basename + "." + dis_algo
>>> hacdisfile = hacbasename + ".dis" 
>>> hach5file = hacbasename + ".h5"    # to be implemented
>>> if part2file:
...    ocffile = hacbasename + ".ocf"
>>> else:
...    ocffile = None
>>> if itype=="tsv":
...    dis	= yap.distances(disfile=hacdisfile)
...    seq_id	= dis.seq_id
>>> dis	= yap.distances(disfile=hacdisfile)
>>> tr = dis.to_tree()
>>> tr.draw()
>>> v_tax, _, _ = load_charfile(charfile=self.charfile, varname=varname)
>>> if varname:
...    char = yap.charmat(charfile=self.charfile)
...    v_tax = char.get_tax(varname)
>>> if partition:
...    height = input("\nenter the height for cutting the tree ... ") 
...    print("height is", height, "\n")
...    part = tr.get_clusters_byheight(height)
...    part.composition(var=v_tax, min_size=min_size, ocffile=ocffile)

.. _mst:

mst
----

*Required files: <basename>.<dis_algo>.dis or <basename>.<dis_algo>.h5; <basename>.char if varname given* 
		
>>> mst_basename = self.basename + "." + dis_algo	
>>> if itype=="tsv":
...    mst_disfile = mst_basename + ".dis" 
>>> if itype=="h5":
... mst_disfile = mst_basename + ".h5"
>>> if itype=="tsv":
...    dis = yap.distances(disfile=mst_disfile)
>>> if itype=="h5":
...    dis = yap_distances(h5file=mst_disfile)
>>> v_tax, v_col, dicol = load_charfile(charfile=self.charfile, varname=varname)
>>> A_tree, Gx	= dis.to_mst()
>>> yap.plot_mst(A_tree, Gx, v_tax=v_tax, legend=legend, dicol=dicol, v_col=v_col, dot_size=d_size)

.. _mds:

mds
----

>>> disfile = "atlas_guyane_laurales.sw.dis" 
>>> dis = yap.distances(disfile=disfile) 
>>> pc  = dis.mds() 

.. _cc_otu:

cc_otu
-------

>>> basename = 'atlas_guyane_laurales'
>>> fasfile = basename + ".fas"
>>> charfile = basename + ".char"


>>> distype = 'dis'
>>> gap = 7
>>> dis_algo = 'sw'
>>> meth = 'cc'
>>> x11 = True
>>> tool = 'quick'
>>> varname = 'genus'
>>> d_size = 20
>>> legend = True
>>> fmt = 'png'
>>> save = True
>>> min_size = 2



>>> cc_basename = basename + "." + dis_algo
>>> cc_disfile = cc_basename + "." + distype 
>>> if varname:
...    char = yap.charmat(charfile=charfile)
...    v_tax = char.get_tax(varname)
...    v_col, dicol = yap.def_colors(v_tax)
>>> ccgr_basename = basename + "." + dis_algo + ".g" + str(gap)
>>> cc_clusfile = ccgr_basename + ".clus"
>>> comment = ["sample is "+ basename, "with distance " + dis_algo, "with gap " + str(gap)]
>>> if distype=="h5":
...    dis = yap.distances(h5file=cc_disfile)
>>> if distype=="dis":
...    dis = yap.distances(disfile=cc_disfile)
>>> gr = dis.to_graph(gap=gap)
>>> if x11:
...    if save:
...        imfile = "plots/" + ccgr_basename + ".graph"
...        gr.plot(tool=tool, col_node=v_col, col_edge="gray", dot_size=d_size, legend=legend, dicol=dicol, title=ccgr_basename, x11=x11, imfile=imfile, fmt=fmt)
...    else:
...        print("\ngraph not plotted!\n")
>>> gr.build_cc()
>>> gr.write_clus(clusfile=cc_clusfile, comment=comment)


.. _otu_composition:

otu_composition
----------------

>>> basename = "atlas_guyane_laurales" 
>>> charfile = basename + '.char'

>>> meth = "cc"
>>> gap = 7
>>> dis_algo = "sw"
>>> varname = "species"
>>> min_size = 3
>>> comp2screen = True
>>> comp2file = True

>>> if meth=="cc": 
...    cls_basename = basename + "." + dis_algo + ".g" + str(gap)
...    comment	= ["# basename is " + basename, "# method for otus is cc", "# method for distances is " + dis_algo, "# gap is " + str(gap)]
>>> elif meth=="swarm":
...    cls_basename = self.basename + ".swarm"
>>> clusfile = cls_basename  + ".clus"
>>> char = yap.charmat(charfile=charfile)
>>> v_tax = char.get_tax(varname)
>>> seq_id = char.get_seq_id()		
>>> part = yap.partition(clusfile=clusfile, basename=self.basename, seq_id=seq_id)
>>> if comp2screen:
...    part.composition(var=v_tax, min_size=min_size)
>>> if comp2file:
...    ocffile 	= cls_basename + ".ocf"
...    part.composition(var=v_tax, min_size=min_size, x11=False, ocffile=ocffile, comment=comment)                     

.. _otu_splitting:

otu_splitting
--------------


>>> basename = "atlas_guyane_laurales"
>>> fasfile = basename + '.fas'
>>> charfile = basename + '.char'

>>> otu_distype = 'sw'
>>> gap = 7
>>> dis_algo = 'sw'
>>> meth = 'cc'
>>> rank_size = True
>>> min_size = 2
>>> split_fas = True
>>> split_char = True
>>> split_dis = True

>>> if meth=="cc": 
...    cls_basename	= basename + "." + dis_algo + ".g" + str(gap)
>>> elif meth=="swarm":
...    cls_basename	= basename + ".swarm"
...    clusfile = cls_basename  + ".clus"
>>> if dis_algo=="sw":
...    disfile = basename + '.sw.dis'
>>> char = yap.charmat(charfile=charfile)
>>> seq_id = char.get_seq_id()
>>> dis = yap.distances(disfile=disfile)
>>> part = yap.partition(clusfile=clusfile, basename=self.basename, seq_id=seq_id)
>>> if rank_size:
...    part.plot_rank_size()
>>> if split_fas:
...    part.write_cc_fastas(fasfile=fasfile, min_size=min_size)
>>> if split_char:
...    part.write_cc_char(charfile=charfile, min_size=min_size)
>>> if split_dis:
...    part.write_cc_distances(dis.Dis, seq_id=dis.seq_id, min_size=min_size)
