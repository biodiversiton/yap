﻿yap\_lib.yapotu\_dsl
====================

.. currentmodule:: yap_lib

.. autoclass:: yapotu_dsl

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~yapotu_dsl.__init__
      ~yapotu_dsl.cc_otu
      ~yapotu_dsl.char_counts
      ~yapotu_dsl.char_show
      ~yapotu_dsl.char_stats
      ~yapotu_dsl.dis_hist
      ~yapotu_dsl.diskm
      ~yapotu_dsl.disseq
      ~yapotu_dsl.fas_len_hist
      ~yapotu_dsl.fas_len_plot
      ~yapotu_dsl.fas_phyML
      ~yapotu_dsl.hac
      ~yapotu_dsl.heatmap
      ~yapotu_dsl.mds
      ~yapotu_dsl.mst
      ~yapotu_dsl.otu_composition
      ~yapotu_dsl.otu_show_graph
      ~yapotu_dsl.otu_show_heatmap
      ~yapotu_dsl.otu_show_mst
      ~yapotu_dsl.otu_show_phylotree
      ~yapotu_dsl.otu_show_tree
      ~yapotu_dsl.otu_splitting
      ~yapotu_dsl.pc_heatmap
      ~yapotu_dsl.pc_scatter
      ~yapotu_dsl.pc_splines
      ~yapotu_dsl.rsyst_extract
      ~yapotu_dsl.set
      ~yapotu_dsl.set_seqids
      ~yapotu_dsl.swarm_otu
      ~yapotu_dsl.tsne
   
   

   
   
   