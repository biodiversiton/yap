﻿yap\_classes.distances
======================

.. currentmodule:: yap_classes

.. autoclass:: distances

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~distances.__init__
      ~distances.clus_heatmap
      ~distances.dbscan
      ~distances.heatmap
      ~distances.histogram
      ~distances.mds
      ~distances.mds_diodon
      ~distances.plot_graph
      ~distances.set_Dis
      ~distances.set_seq_ids
      ~distances.tests
      ~distances.to_graph
      ~distances.to_mst
      ~distances.to_tree
      ~distances.tsne
      ~distances.write
      ~distances.write_components
      ~distances.write_components_old
      ~distances.write_fasta
      ~distances.xtract_random_submatrix
      ~distances.xtract_submatrix
   
   

   
   
   