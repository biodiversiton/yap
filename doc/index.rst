.. yap documentation master file, created by
   sphinx-quickstart on Thu Feb 18 10:44:39 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============================================
Welcome to yapsh and yap_classes documentation!
===============================================
.. toctree::
   :maxdepth: 4

  

yapsh 
------

:doc:`yapsh`

  - :ref:`orga`
  - :ref:`methods`

yapsh methods
-------------

:ref:`fas_len_hist`

:ref:`fas_len_plot`

:ref:`fas_phyML`

:ref:`char_stats`

:ref:`disseq`

:ref:`diskm`

:ref:`dis_hist`

:ref:`heatmap`

:ref:`hac`

:ref:`mst`

:ref:`mds`

:ref:`cc_otu`

:ref:`otu_composition`

:ref:`otu_splitting`

.. automodule:: yap_classes

----------------------
Yap_classes: functions
----------------------
.. autosummary::
  :toctree: fonctions
  :nosignatures:

  plot_mst
  plotsave
  count_and_order
  def_colors
  dot_size
  get_legend
  heatmap

Yap_classes: Fasta
-------------------
.. autosummary::
  :toctree: fonctions
  :nosignatures:

  fasta
  fasta.next_seq
  fasta.set_seq_id
  fasta.set_seq_len
  fasta.set_words
  fasta.xtract_by_seqid
  fasta.write
  fasta.plot_length
  fasta.plot_length_histogram
  fasta.align
  fasta.computes_distances
  fasta.phylo_tree

Yap_classes: Charmat
======================
.. autosummary::
  :toctree: fonctions
  :nosignatures:

  charmat
  charmat.get_stats
  charmat.get_tax
  charmat.get_seq_id
  charmat.get_counts
  charmat.build_colorfile
  charmat.get_colors
  charmat.change_colorfile
  charmat.show

Yap_classes: Distances
======================
.. autosummary::
  :toctree: fonctions
  :nosignatures:

  distances
  distances.set_Dis
  distances.tests
  distances.histogram
  distances.to_tree
  distances.to_graph
  distances.to_mst
  distances.mds
  distances.mds_diodon
  distances.tsne
  distances.write_components
  distances.heatmap
  distances.xtract_submatrix
  distances.write
  distances.write_fasta

Yap_classes: Graph
==========================
.. autosummary::
  :toctree: fonctions
  :nosignatures:

  graph
  graph.build_cc
  graph.plot
  graph.get_degrees
  graph.color_by_degree
  graph.communities
  graph.to_partition
  graph.write_clus

Yap_classes: Otu_builder
==========================
.. autosummary::
  :toctree: fonctions
  :nosignatures:

  otu_builder
  otu_builder.swarm
  otu_builder.to_partition
  otu_builder.write_clus

Yap_classes: Partition
==========================
.. autosummary::
  :toctree: fonctions
  :nosignatures:

  partition
  partition.vec_of_classes
  partition.write_otufile
  partition.composition
  partition.plot_rank_size
  partition.write_sizes
  partition.write_cc_fastas
  partition.write_cc_distances
  partition.write_cc_char
 
Yap_classes: Point clouds
==========================
.. autosummary::
  :toctree: fonctions
  :nosignatures:

  point_cloud
  point_cloud.set_Y
  point_cloud.scatter
  point_cloud.hovering
  point_cloud.points_123
  point_cloud.plot_splines
  point_cloud.heatmatrix
  point_cloud.spikeshow
  point_cloud.heatmap

Yap_classes: Tree
==========================
.. autosummary::
  :toctree: fonctions
  :nosignatures:

  tree
  tree.set_height
  tree.get_clusters_byheight
  tree.get_clusters_bynumber
  tree.draw
  tree.draw_tree_coloredlabels

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

