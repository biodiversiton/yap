Feature: mds
    Test mds (pydiodon)

    Scenario Outline: mds
        Given a yap distance class instanciated with <disfile>
        When I launch mds with default parameters
        Then I get Y with shape (110,44)

        When I launch mds with coordfile
        Then I get a coordfile identical to <expected>
        
    Examples: # bug in path / not allowed replaced with :
| disfile                                       | meth | expected |
| tests:data2tests:atlas_guyane_laurales.sw.dis | svd  | tests:data2tests:atlas_guyane_laurales.coord |
