"""mds feature tests."""
import os
import tempfile
import filecmp
import warnings
import pytest
from pytest_bdd import (
    given,
    scenario,
    scenarios,
    then,
    when,
    parsers
)
import yap_classes as yap

scenarios('features/mds.feature')

@given(parsers.parse('a yap distance class instanciated with {disfile:S}'), target_fixture='dis' )
def _(disfile):
    """a yap distance class instanciated with <disfile>"""
    disfile = disfile.replace(':', '/')
    #raise Exception(f'{disfile=} {os.getcwd()=}')
    return yap.distances(disfile=disfile)
  
@when('I launch mds with default parameters')
def _(dis):
    """I launch mds with default parameters"""
    dis.mds()

@then('I get Y with shape (110,44)')
def _(dis):
    """I get Y with shape (110,44)."""
    assert dis.Y.shape == (110,44)

@when('I launch mds with coordfile',target_fixture='coordfile')
def _(dis):
    """I launch mds with coordfile"""
    coordfile = tempfile.mkstemp()[1]
    dis.mds(coordfile=coordfile)
    return coordfile

@then(parsers.parse('I get a coordfile identical to {expected:S}'))
def _(expected,coordfile):
    """I get a coordfile identical to <expected>"""
    expected = expected.replace(':', '/')
    assert filecmp.cmp(coordfile, expected)
    os.remove(coordfile)
