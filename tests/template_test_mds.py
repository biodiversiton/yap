"""mds feature tests."""
import os
import tempfile
import filecmp
import warnings
import pytest
import yap_classes as yap

#  yap_classes default params: self, k=-1, meth="svd", loop=True, nb_eig=0, pc=True, coordfile=None
# assign self.Y
# default pydiodon:            dis,  k=-1, meth="svd", no_loop=True

@pytest.fixture
def dis():
  yield yap.distances(disfile='../laurales/atlas_guyane_laurales.sw.dis')


def test_mds_default(dis):
  dis.mds()
  assert dis.Y.shape == (110,44)

# add coordfile
def test_mds_pc_False_coordfile(dis):
  coordfile = tempfile.mkstemp()[1]
  dis.mds(pc=False, coordfile=coordfile)
  assert filecmp.cmp(coordfile, "data2tests/atlas_guyane_laurales.coord")

# Comportement aléatoire
def test_mds_pc_False_meth_grp(dis):
  coordfile = tempfile.mkstemp()[1]
  dis.mds(pc=False, meth='grp', k=30, coordfile=coordfile)
  assert dis.Y.shape[0] == 110
  assert os.stat(coordfile).st_size > 0

def test_evd(dis):
  coordfile = tempfile.mkstemp()[1]
  dis.mds(pc=False, meth='evd', coordfile=coordfile)
  assert dis.Y.shape == (110,49)

def test_svd_k30(dis):
  coordfile = tempfile.mkstemp()[1]
  dis.mds(pc=False, k=30, meth='svd', coordfile=coordfile)
  assert dis.Y.shape == (110,30)
  assert filecmp.cmp(coordfile, "data2tests/atlas_guyane_laurales.k30.coord")

 



