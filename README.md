# yapotu


Yet Another Pipeline for building OTUs

## Brief Description

`yapotu` is a three layers cake of programs written mainly in python, numpy, matplotlib, panda and scikit-learn to build, analyse and visualize OTUs in samples of sequences produced in barcoding or metabarcoding (`yap_classes`, `yap_lib` and `yapsh`).   `yapotu` can handle large datasets, like samples of $10^5$ reads in metabarcoding.   

The basic functions are in a librray called `yap_classes`. `yapsh` is a user friendly interface between the library and the user (who does not need to dig into the API if not familiar with python) with a second library `yap_lib` as a glue. It allows some assembly of elementary functions in `yap_classes` without programming, in a DSL (Domain Specific Language) style. The action to be run is indicated in a `yapsh` command, (see manual `yapotu.pdf`), and the parameters therefore are given in a `yaml` configuration file. This will be translated by `yapsh` "motor" into running `python` functions in a way transparent for the user. `yaml` is a language used for building configuration files. `yaml` files can be written with any notepad as plain text files. See manual for the way to build a `yaml` file. 


## Data sets as examples

Two data sets are made available with installation of `yapsh`:

* one for barcoding, called `atlas_guyane_laurales`
* one for metabarcoding, called `malabar`, from the name of a project the data come from.



## Installation

`yap_classes`, `yap_lib` and `yapsh` are written in `python`. Python 3.8 or above must be installed on your machine. 


### Required python libraries

For `yapsh` to run, and call `yap_lib` and `yap_classes`, the following python libraries must be installed as well:

* numpy
* pandas
* pydiodon (see below) 
* sklearn
* matplotlib
* mpl_toolkits.mplot3d 
* networkx 
* scipy
* seaborn
* h5py
* yaml

### Installation with `pip` 

`yap_classes` requires (for dimension reduction of point clouds) that pydiodon has been installed for running. Here is how to install pydiodon first.

```sh
# just one pip command to install pidiodon in a user's default (depends on the system) local directory
# this library is used for dimension reduction of distance arrays between sequences or reads.

pip3 install "git+https://gitlab.inria.fr/diodon/pydiodon.git"
```

And now, how to install yapsh!

```sh
# just one pip command to install yapsh in a user's default (depends on the system) local directory

pip3 install "git+https://gitlab.inria.fr/biodiversiton/yap.git"
```




## Manual

See the manual `yapotu.pdf` for a gentle tutorial. 


## Authors and acknowledegment

<strong>authors : </strong> 
* Alain Franc,     
* Jean-Marc Frigerio  

<strong>acknowledgement : </strong> We thank Valérie Laval for having tested most of the functions in `yap` on her own datasets, and having made many suggestions to improve the functionalities of `yap!`   

<strong>version</strong> 22.09.19, 23.08.24


## License

[to be done]




