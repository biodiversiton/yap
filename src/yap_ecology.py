#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
embryonic class for community ecology
handles sets of inventories

file names are not handled automatically, and must be specified by the user.
"""

import numpy as np
import matplotlib.pyplot as plt


class community():
	"""
	af, 24.02.23
	"""
	def __init__(self):
		"""
		af, 24.02.23
		"""
		pass
		
	# ------------------------------------------------------------------
	
	def load_invfile(self, invfile):
		""" Loads an inventiory file
		
		argument
		--------
		@ invfile		string		file name with the inventory
		
		returns
		-------
		@taxa			list of strings		taxa present
		@ counts		list of integers	number of occurence per taxon
		
		Notes
		-----
		
		If taxa[i] is a taxon in a sample, coounts[i] is the number of times
		it has been counted.
		
		af, 24.02.23
		"""
		Inv		= np.loadtxt(invfile, delimiter="\t", dtype=object)
		taxa	= Inv[1:,0].astype(str).tolist()
		counts	= Inv[1:,1].astype(int).tolist()
		#
		return taxa, counts
		
	# ------------------------------------------------------------------
	
	def compare_2_inventories(self, invfile_1, invfile_2, x11, save, plot):
		""" Compares two inventories
		
		argument
		--------
		invfile_1		string		filename of fisrt inventory
		invfile_2		string		filename of second inventory
		x11				boolean		whether to display the table of counts on screen
		save			boolean		whether to save the table of counts
		plot			boolean		whether to plot counts on inventiry 1 on x axis
										and of inventory 2 on y axis
		
		returns
		-------
		@ taxa			list of strings		taxa counted
		@ counts_12		numpy array			array with counts of taxa
		
		Notes
		-----
		inventories are given as files *.inv
		taxa is the set of taxa which have been observed in at least one inventory
		row i of counts_12 is the number of occurences of taxa[i] in respectively 
		inventory 1 and inventiry 2
		
		
		af, 24.02.23
		"""
		taxa_1, counts_1	= self.load_invfile(invfile_1)
		taxa_2, counts_2	= self.load_invfile(invfile_2)
		#
		taxa		= sorted(list(set(taxa_1 + taxa_2)))
		n_taxa		= len(taxa)
		counts_12	= np.zeros((n_taxa,2))
		# 
		for i1, taxon_1 in enumerate(taxa_1):
			k				= taxa.index(taxon_1)
			counts_12[k,0]	= counts_1[i1]
		for i2, taxon_2 in enumerate(taxa_2):
			k				= taxa.index(taxon_2)
			counts_12[k,1]	= int(counts_2[i2])
		#
		if x11:
			for i in range(n_taxa):
				print(taxa[i], "->", counts_12[i,:])
		#
		if plot:
			# erases counts of 'no_hits' and 'ambiguous'
			i_nohit	= taxa.index("no_hit")
			i_ambig	= taxa.index("ambiguous")
			C		= np.delete(counts_12, [i_nohit, i_ambig], 0)
			# prepares the pot
			x		= C[:,0]
			y		= C[:,1]
			m		= min(x)
			M		= max(x)
			# plots
			plt.plot([m,M],[m,M], c="chartreuse", zorder=1)
			plt.scatter(x, y, s=10, c="red", zorder=2)
			plt.xlabel("best hit")
			plt.ylabel("neighbors")
			plt.show()
		#
		return taxa, counts_12
