#!/usr/bin/env Rint

#https://thesquareplanet.com/blog/interactive-r-scripts/


#Emilie vendredi 31 mai 2024 13:34

# # 1er test dada2 sur 3 echantillons


# Installation de BiocManager et de dada2

#if (!require("BiocManager", quietly = TRUE))
#    install.packages("BiocManager")

#BiocManager::install("dada2")


library(grDevices)
library(utils)

# ##### Chargement de la librairie dada2
library(dada2)


# ##### Indication du chemin vers les données fastq décompréssées
path <- "."
#list.files(path)


# ## Avant de commencer : penser à enlever les amorces !!

# #### Utilisation de la commande trimLeft = c(FWD_PRIMER_LEN, REV_PRIMER_LEN) en indiquant la longueur des amorces forward et reverse. Commande à utiliser uniquement si les séquences commencent par les amorces et qu'il n'y pas d'adaptateurs ou d'héterogeneity spacer sinon utiliser cutadapt ou trimmomatic
trimLeft = c(27, 22)


# ##### Forward and reverse fastq filenames have format: SAMPLENAME_R1_001.fastq and SAMPLENAME_R2_001.fastq


fnFs <- sort(list.files(path, pattern="_R1_001.fastq", full.names = TRUE))
fnRs <- sort(list.files(path, pattern="_R2_001.fastq", full.names = TRUE))


# #### 1. Extract sample names, assuming filenames have format: SAMPLENAME_XXX.fastq


sample.names <- sapply(strsplit(basename(fnFs), "_"), `[`, 1)


#sample.names
# #### 2. Inspect read quality profiles
plotQualityProfile(c(fnFs,fnRs))

R1trunc = readline(prompt="R1 truncLen: ")
R1trunc = as.integer(R1trunc)
R2trunc = readline(prompt="R2 truncLen: ")
R2trunc = as.integer(R2trunc)
truncLen=c(R1trunc, R2trunc)
cat(sprintf(c("truncLen_R1: %s\n", "truncLen_R2: %s\n"), c(truncLen[1], truncLen[2])), sep="")
cat(sprintf(c("trimLeft_R1: %s\n", "trimLeft_R2: %s\n"), c(trimLeft[1],trimLeft[2])),  sep="")
q()
