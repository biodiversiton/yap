#!/usr/bin/env Rint

#https://thesquareplanet.com/blog/interactive-r-scripts/


#Emilie vendredi 31 mai 2024 13:34

# # 1er test dada2 sur 3 echantillons


# Installation de BiocManager et de dada2

#if (!require("BiocManager", quietly = TRUE))
#    install.packages("BiocManager")

#BiocManager::install("dada2")

library(grDevices)
library(utils)
library(optparse)
# ##### Chargement de la librairie dada2
library(dada2)

option_list = list(
	make_option("--trimLeft_forward", type="integer", help="Forward primer length"),
	make_option("--truncLen_r1", type="integer", help="R1 truncLen")
  )

opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

trimLeft_forward = opt$trimLeft_forward
truncLen_r1     = opt$truncLen_r1

#cat(c(trimLeft_forward,trimLeft_reverse,truncLen_r1,truncLen_r2))

# ##### Indication du chemin vers les données fastq décompressées
path <- "."
#list.files(path)


# ## Avant de commencer : penser à enlever les amorces !!

# #### Utilisation de la commande trimLeft = c(FWD_PRIMER_LEN, REV_PRIMER_LEN) en indiquant la longueur des amorces forward et reverse. Commande à utiliser uniquement si les séquences commencent par les amorces et qu'il n'y pas d'adaptateurs ou d'héterogeneity spacer sinon utiliser cutadapt ou trimmomatic
trimLeft = c(trimLeft_forward)
truncLen = c(truncLen_r1)

# ##### Forward and reverse fastq filenames have format: SAMPLENAME_R1_001.fastq and SAMPLENAME_R2_001.fastq


fnFs <- sort(list.files(path, pattern="_L001.fastq", full.names = TRUE))

# #### 1. Extract sample names, assuming filenames have format: SAMPLENAME_XXX.fastq


sample.names <- sapply(strsplit(basename(fnFs), "_"), `[`, 1)

# #### 3. Filter and trim 
# ##### 3.1 Place filtered files in filtered/ subdirectory


filtFs <- file.path(path, "filtered", paste0(sample.names, "_F_filt.fastq.gz"))
names(filtFs) <- sample.names

names(filtFs)


# ##### 3.2 Utilisation de filtres : 
# ##### maxN=0 (DADA2 requires no Ns), truncQ=2, rm.phix=TRUE and maxEE=2. Le paramètre maxEE indique le nombre maximum de “expected errors” autorisées dans un read, ce qui est un meilleur filtre que le score de qualité moyen.       
# ##### ATTENTION au paramètre truncLen qui coupe les reads, ce paramètre ne doit pas être trop stringent car il y a un risque que le contigage ne puisse plus se faire en fonction de la taille du marqueur étudié.

out <- filterAndTrim(fnFs, filtFs,
              truncLen=truncLen,
              trimLeft=trimLeft,
              maxN=0, maxEE=2, truncQ=2, rm.phix=TRUE,
              compress=TRUE, multithread=TRUE) # On Windows set multithread=FALSE



# #### 4. Apprentissage des erreurs


errF <- learnErrors(filtFs, multithread=TRUE)
#plotErrors(errF, nominalQ=TRUE)


# #### 5. Sample Inference

# ##### By default, the dada function processes each sample independently. However, pooling information across samples can increase sensitivity to sequence variants that may be present at very low frequencies in multiple samples. The dada2 package offers two types of pooling. dada(..., pool=TRUE) performs standard pooled processing, in which all samples are pooled together for sample inference. dada(..., pool="pseudo") performs pseudo-pooling, in which samples are processed independently after sharing information between samples, approximating pooled sample inference in linear time


dadaFs <- dada(filtFs, err=errF, multithread=TRUE)


#Inspecting the returned dada-class object:
#dadaFs[[1]]


# #### 6. Merge des séquences R1 et R2

# ##### ATTENTION: Non-overlapping reads are supported, but not recommended, with mergePairs(..., justConcatenate=TRUE)


#mergers <- mergePairs(dadaFs, filtFs, dadaRs, filtRs, verbose=TRUE)


# Inspect the merger data.frame from the first sample
#head(mergers[[1]])


# #### 7. Construction de la table des ASV


seqtab <- makeSequenceTable(dadaFs)
dim(seqtab)


# Inspect distribution of sequence lengths
table(nchar(getSequences(seqtab)))


# #### 8. Elimination des chimères


seqtab.nochim <- removeBimeraDenovo(seqtab, method="consensus", multithread=TRUE, verbose=TRUE)
dim(seqtab.nochim)


sum(seqtab.nochim)/sum(seqtab)

print("chimères terminées")

# #### 9. Comptage du nombre de reads conservés à chaque étape du pipeline


getN <- function(x) sum(getUniques(x))

#If processing a single sample, remove the sapply calls: e.g. replace sapply(dadaFs, getN) with getN(dadaFs)
#track <- cbind(out, sapply(dadaFs, getN), sapply(dadaRs, getN), sapply(mergers, getN), rowSums(seqtab.nochim))
track <- cbind(out, getN(dadaFs), rowSums(seqtab.nochim))

colnames(track) <- c("input", "filtered", "denoisedF", "nonchim")
rownames(track) <- sample.names
write.csv(track, file.path(path, "resume.csv"))


# #### 10. Assignation taxonomique par méthode bayésienne

# Base de données : https://benjjneb.github.io/dada2/training.html


# Here we download and use the Diat.barcode (last version) pre-processed for DADA2.
# You can use your own local database if needed.
#if (!requireNamespace("devtools", quietly = TRUE))
#    install.packages("devtools")
#devtools::install_github("fkeck/diatbarcode")

#tax_fas <- diatbarcode::download_diatbarcode(flavor = "rbcl312_dada2_tax")
#taxa <- assignTaxonomy(seqtab.nochim, tax_fas$path, minBoot = 75,
#                      taxLevels = c("Empire", "Kingdom", "Subkingdom", "Phylum", "Class", "Order", "Family", "Genus", "Species"),
#                      outputBootstraps = TRUE, verbose = TRUE, multithread = TRUE)
#
#spe_fas <- diatbarcode::download_diatbarcode(flavor = "rbcl312_dada2_spe")
#exact_sp <- assignSpecies(seqtab.nochim, spe_fas$path)
#
#write.csv(taxa, file.path(path, "taxa.csv"))
#write.csv(exact_sp, file.path(path, "species.csv"))

# giving our seq headers more manageable names (ASV_1, ASV_2...)
asv_seqs <- colnames(seqtab.nochim)
asv_headers <- vector(dim(seqtab.nochim)[2], mode="character")

for (i in 1:dim(seqtab.nochim)[2]) {
    asv_headers[i] <- paste(">ASV", i, sep="_")
}

    # making and writing out a fasta of our final ASV seqs:
asv_fasta <- c(rbind(asv_headers, asv_seqs))
write(asv_fasta, file.path(path, "ASVs.fas"))

    # count table:
asv_tab <- t(seqtab.nochim)
row.names(asv_tab) <- sub(">", "", asv_headers)
write.table(asv_tab, file.path(path, "ASVs_counts.tsv"), sep="\t", quote=F, col.names=NA)

q("no")
