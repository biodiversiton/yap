#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""
af, 23.06.01, 24.01.12, 24.01.16, 24.02.07
"""
import sys
import os
import traceback
import yaml
import glob
from datetime import datetime
from print_color import print as print_col
#
import numpy as np
import scipy.cluster.hierarchy as sch   # for handling linkage matrices
import matplotlib.pyplot as plt
#
import yap_classes as yap
import yap_mapping as ymap
import yap_ecology as yacol
#
from yapotu_lib import *

# display version upon opening

__version__ = str(datetime.fromtimestamp(os.stat(sys.argv[0]).st_mtime))
print_col('loading yap_lib    , version', __version__[:16], color="cyan")

# ======================================================================
#
#			some useful functions  
#
# ======================================================================

# These function are 'factorized' along different 'operational' functions 
# they act behind

"""
=========================================================================================================================
Function				: 	what it does
=========================================================================================================================
testif()				:	tests whether file file2test exists
check_parameters()		:	displays on the screen the selected parameters	---> pourquoi self ?
check_file_exists()		:	checks whether a file exists in the working directory
check_overwrite()		:	checks whether a file already exists before overwriting it
load_charfile()			:	loads charfile and returns a variable
load_disfile()			:	loading diustance file
print_parameters()		:
print_through_which()	:
=========================================================================================================================

"""

# ----------------------------------------------------------------------

def testif(file2test):
	""" tests whether file file2test exists
	
	go = True  if it exists
	go = false if it does not exist
	
	af, 22.08.18
	"""
	flag	= os.path.isfile(file2test)
	if flag:
		#
		print_col("\n!!! file ", file2test, "already exists !!!", color="magenta")
		print_col("do you want to overwrite it? (y/n)", color="magenta")
		ch 	= input()
		#
		if ch=="y":
			go = True
		else:
			go = False
	else:
		go = True
	#
	return go

# ----------------------------------------------------------------------

def check_parameters(self):
	"""displays on the screen the selected parameters
	
	Notes
	-----
	- permits to accept / decline the parameter choice
		if declined, the program is terminated
		
	af, ..., 23.04.29
	"""
	print_col("\nare you ok with these choices ? (y/n) ", color="green")
	ch	= input()
	#
	if ch in ["n", "no"]:
		print_col("\nok; program terminated, try again!", color="red")
		return False
	if ch in ["y", "yes"]:
		print_col("\nok; let us move on!!\n", color="green")
	return True
		
		
		
		
# ----------------------------------------------------------------------

def check_file_exists(filename):
	"""checks whether a file exists in the working directory
	
	@ filename	; string 	; filename to check
	
	af, ..., 23.04.29
	"""
	print("\nfile to read is", filename)
	#
	flag	= os.path.isfile(filename)
	if flag:
		print_col("file exists, ok!!\n", color="green")
	else:
		print_col("\n!!! warning !!!", color="red")
		print_col("file", filename, "does not exist", color="red")
	#
	return
	
# ----------------------------------------------------------------------

def check_overwrite(file_to_write):
	"""checks whether a file already exists before overwriting it
	
	@ file_to_write	; string ; name of the file to overwrite
	
	af, ..., 23.04.28
	"""
	flag	= os.path.isfile(file_to_write)
	if flag:
		print_col("\n!!! file ", file_to_write + " already exists !!!", color="red")
		print_col("do you want to overwrite it? (y/n)", color="red")
		ch = input()
		if ch=="y":
			go = True
		else:
			go = False
	else:
		go = True
	#
	return go

# ----------------------------------------------------------------------

def load_charfile(charfile, varname):
	"""loads charfile and returns a variable
	
	@ charfile	; string	; the file to download
	@ varname	; string	; a header of the charfile
	
	Returns
	-------
	- v_tax	; 1D numpy array	; the column of the varname
	- v_col	; 1D numpy array	; a color per item depending on the varname value
	- dicol	; dictionary		; a dictionary
									key:	a value of the varname
									value:	a corresponding color
	Notes
	-----
	- the colors are selected automatically
	
	af, ..., 23.04.29
	"""
	char 			= yap.charmat(charfile=charfile)
	v_tax 			= char.get_tax(varname)
	v_col, dicol	= yap.def_colors(v_tax)
	#
	return v_tax, v_col, dicol
	
# ----------------------------------------------------------------------

def load_disfile(itype, disfile=None, h5file=None):
	"""loading diustance file
	
	@ disfile	; string	; name of a disfile in tsv format
	@ h5file	; string	; name of a distance file in hdf5 format
	
	Returns
	-------
	
	dis	; an instance of the class distances
	
	
	Note
	----
	
	23.11.19, af : est-ce bien utile ?
	
	af, ..., 23.04.29
	"""
	if itype=="dis":
		dis	= yap.distances(disfile=disfile)
	if itype=="h5":
		dis		= yap.distances(h5file=h5file)
	#
	return dis
	
# ----------------------------------------------------------------------

def print_parameters(method, col="green"):
	"""
	"""
	item 	= "parameters for " + method
	n		= len(item)
	under	= ''.join(['-']*n) 
	print_col("\n" + item, color=col)
	print_col(under, color=col)
	
# ----------------------------------------------------------------------

def print_through_which(method, col="magenta"):
	"""
	"""
	item =  "\n-> [yap_lib]:[" + method + "()]\n"
	print_col(item, color=col)

# ======================================================================
#
#			class dsl  
#
# ======================================================================

class yapotu_dsl():
	"""Main class of yap
	
	"""
	
	# ------------------------------------------------------------------
	
	def __init__(self, basename):
		"""initialises a yapotu_dsl() class
		
		Parameters
		----------
		
		basename	string
					the basename of the project
		
		Notes
		-----
		- begins by loading the yaml file from the argument <basename>
		
		- initialize tells whether to initialise with
			a fastaq file
			a fasta file
			a h5 file
		
		- three possibilities are handled in the init()
		
		- if initialisation =="fastaq",
		 
		- if initialisation=="fastas", 
			-> starts with a fastafile and a character file
			-> checks that fasta and character file exist
			-> if not, returns an error
		
		- if initilisation=="h5", there are no fasta file nor character file
			-> loads an h5 distance file (the distance is given in key init_dis_algo)
				the name of the file is 
				<basename>.<init_dis_algo>.<h5>
			-> loads the h5file as a distance class
			-> puts it as an attribute is deferred
			-> writes a fasta file and a void character file from h5file
			
		af, 22.07.16, 22.11.12, 23.08.04, 23.08.21, 24.01.16, 24.04.03
		"""
		
		### -------------- basename and files as attributes
		
		self.basename		= basename
		self.configfile 	= basename + '.yaml'
		self.fasfile		= basename + '.fas'
		self.fastaqfile		= basename + '.fastq'
		self.charfile		= basename + '.char'
		
		### ------------ loading yaml file
		
		print("\n[yap_lib]:[init()]:loading yaml file ...")
		try:
			self.yam = yaml.load(open(self.configfile), Loader=yaml.SafeLoader)
			print("[yap!]:yaml file loaded")
		except Exception as e:
			traceback.print_exc()
			return
			
		### -------------- Initialisation
		
		print_col("\nInitialisation", color="green")
		print_col("--------------", color="green")
		
		initialisation		= self.yam['initialisation']		; print("\ninitialisation        ", initialisation)
		if_questions		= self.yam["if_questions"]			; print("if_questions          ", if_questions)
		
		self.questions		= if_questions
		if self.questions:
			if not check_parameters(self.yam): 
				return
		else:
			print_col("mode is non interactive", color="magenta")
		
		"""
		If this key is set to "yes", the program will ask some questions
		like displaying the parameters and asking whether they are correct.
		If this key is set to "no", no question is asked (and the implicit answer 
		to corretness is "yes".
		Questions are asked within an input() command, which cannot be 
		implemented on a Galaxy server. 
		"""
		
		### -------------- initialisation with a fastaq file
		
		if initialisation=="fastaq":
			
			print_col("\nInitialisation with a fastaqfile", color="green")
			print_col("--------------------------------", color="green")
			
			to_fasta		= self.yam['to_fasta']			; print("\nto_fasta        ", to_fasta)
			to_fasta_q  	= self.yam['to_fasta_q']		; print("\nto_fasta_q      ", to_fasta_q)

			to_fasta_noN	= self.yam['to_fasta_noN']		; print("to_fasta_noN    ", to_fasta_noN)
			orientate		= self.yam['orientate']			; print("orientate       ", orientate)
		
			if self.questions:
				if not check_parameters(self.yam): return
			
			flag	= os.path.isfile(self.fastaqfile)
			
			if flag==True:
				print("fastaq file", self.fastaqfile, "exists, loading fastaq file ...")
				#
				fq	= yap.fastaq(fastaqfile=self.fastaqfile)
				self.seq_id		= fq.seq_id
				self.n_seq		= fq.n_seq
				print_col("\nThere are", self.n_seq, "reads\n", color="cyan")

				#
				if to_fasta:
					#save fasta file
					fq.to_fasta(qual_min=to_fasta_q)
					self.yam['initialisation'] = 'fasta'
					#save yaml file
					os.rename(self.configfile, self.configfile + '_OLD')
					with open(self.configfile+ '_OLD', 'r') as y_in,\
						open(self.configfile, 'w')          as y_out:
						for l in y_in:
							l = l.rstrip('\n')
							if l =='initialisation: fastaq    # can be fasta, fastaq, h5':
								print('initialisation: fasta    # can be fasta, fastaq, h5', file=y_out)
								continue
							else:
								print(l, file=y_out)
					print(self.configfile,'modified')

				#
				if to_fasta_noN:
					fq.to_fasta(qual_min=-1)
				"""
				#
				if orientate:
					fastaq.xxx()
				"""
			else:
				print_col("fastaq file", self.fastaqfile, "does not exist", color="red")
				print_col("programme terminated", color="red")
				exit()
			
		### -------------- initialisation with a fasta file
		
		if initialisation=="fasta":
			#
			flag	= os.path.isfile(self.fasfile)
			#
			if flag==True:
				print("fasta file", self.fasfile, "exists, loading fasta file ...")
				#
				fas 			= yap.fasta(fasfile=self.fasfile)	# to get the seq_ids
				self.seq_id		= fas.seq_id
				self.n_seq		= fas.n_seq
				print_col("\nThere are", self.n_seq, "reads\n", color="cyan")
			else:
				print_col("fasta file", self.fasfile, "does not exist", color="red")
				print_col("programme terminated", color="red")
				exit()
		
		### ------------------- loads the h5file as an attribute
		
		#flag_load_dis	= False
		#
		if initialisation=="hdf5":
			#
			print_col("\nparameter if loading hdf5 file", color="green")
			print_col("------------------------------", color="green")
			
			h5_2_fas		= self.yam['h5_2_fas']			; print("h5_2_fas       ", h5_2_fas)
			#
			if self.questions:
				if not check_parameters(self.yam): return
			#
			print("-> loads h5file ...")
			dis_algo	= self.yam['dis_algo']
			h5file		= self.basename + "." + dis_algo + ".h5"
			flag		= os.path.isfile(h5file)
			if flag:
				print("h5 file", h5file, "exists, ok!")
				dis				= yap.distances(h5file=h5file)
				self.dis		= dis
				#flag_load_dis	= True
			else:
				print_col("h5 file", h5file, "does not exist!:", color="red")
				exit("program terminated")
				
				
			### ------------------ writes a fasta file from the h5 file
			
			if h5_2_fas==True:
				#
				print("-> writes fasta file from h5 file")
				with open(self.fasfile,"w") as f_fas:
					for i in range(dis.n):
						seqid	= ">" + dis.seq_id[i]
						f_fas.write(seqid + "\n")
						f_fas.write(dis.words[i] + "\n")
				#
				self.seq_id	= dis.seq_id
				self.n_seq	= dis.n
				#

		### --------------- creates a void charfile if charfile absent
		
		flag	= os.path.isfile(self.charfile)
		#
		if flag==True:
			print_col("\ncharacter file", self.charfile, "exists, ok!", color="green")
		else:
			print_col("charfile absent; writing a void charfile", color="magenta")
			with open(self.charfile, "w") as f_char:
				item = "SEQ_ID" + "\t" + "void"
				f_char.write(item + "\n")
				for read in self.seq_id:
					item 	= read + "\t" + "not_assigned"
					f_char.write(item + '\n')
		
		
		### --------------- dictionary ine to seq_id
		
		dic= {}
		for i in range(self.n_seq):
			dic[i] = self.seq_id[i]
		#
		self.dic_index2seqid	= dic
		
		
	# ==================================================================
	#
	#				genaral parameters
	#
	# ==================================================================
	
	def general(self):
		""" Specification of general parameters
		
		rajouter kmer ?
		
		af, 24.05.16
		"""
		
		
		coord		= self.yam['coord']			; print("\ncoord       ", coord)
		dis_algo	= self.yam['dis_algo']		; print("dis_algo    ", dis_algo)
		dis_type	= self.yam['dis_type']		; print("dis_type    ", dis_type)
		fmt			= self.yam['fmt']			; print("fmt         ", fmt)
		otype		= self.yam['otype']			; print("otype       ", otype)
		otu_meth	= self.yam['otu_meth']		; print("otu_meth    ", otu_meth)
		save		= self.yam['save']			; print("save        ", save)
		varname		= self.yam['varname']		; print("varname     ", varname)
		x11			= self.yam['x11']			; print("x11         ", x11)
		


	# ==================================================================
	#
	#					extraction from R-Syst
	#
	# ==================================================================
	
	def deprecated_rsyst_extract(self):
		"""under construction - will be deprecated
		af & jmf, 22.08.09
		"""
		
		database		= self.yam['rs_database']		; print("\ndabase    ", database)
		project			= self.yam['rs_project']		; print("project    ", project)
		taxon 			= self.yam['rs_taxon']			; print("taxon      ", taxon)
		amplicon		= self.yam['rs_amplifarea']		; print("amplicon   ", amplicon)
		min_length		= self.yam['rs_min_length']		; print("min_length ", min_length)
		max_length		= self.yam['rs_max_length']		; print("max_length ", max_length)
		#
		cmd		= "rsyst_extract.py -b " + self.basename
		os.system(cmd)
	

	# ==================================================================
	#
	#			mapping
	# 
	# ==================================================================
	 
	"""
	blast_queries(self)		:	Produces a file with quality of the local alignment of queries versus references
	map_dissec(self)		:	Computes the distance array between a sample and a R-Syst database
	
	"""
	
	def blast_queries(self):
		"""Produces a file with quality of the local alignment of queries versus references
		
		requires
		--------
		- a fasta file <basename>.fas of queries
		
		returns
		-------
		- writes a blastn file <basename>.blastn: one row per query, and 11 columns
		
		Notes
		-----
		
		af, 24.02.26, 24.04.22
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		
		# general
		
		charname		= self.yam[generale]['varname']				; print("charname        ", charname)
		
		# specific
		reference		= self.yam[f_name]['reference']		; print("reference       ", reference)
		num_threads		= self.yam[f_name]['num_threads']		; print("num_threads     ", num_threads)
		num_alignments	= self.yam[f_name]['num_alignments']	; print("num_alignments  ", num_alignments)
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ----------------- building files
		
		queries_fasfile	= self.fasfile
		print("query fasfile is", self.fasfile)
		#
		blastfile		= self.basename + ".blastn"
		print("blast outfile is", blastfile)
				 
		### ----------------- running
		
		blst	= ymap.mapping_blast(reference, charname)
		print_col("blasting ...", color="cyan")
		t_init	= time.time()
		blst.blast_queries(num_threads, queries_fasfile, num_alignments, blastfile)
		t_end	= time.time()
		print_col("...took", t_end - t_init, "seconds", color="cyan")
			
	# ------------------------------------------------------------------
	
	def map_dissec(self):
		"""Computes the distance array between a sample and a R-Syst database
		
		Requires
		--------
		- the fasta file of an OTU
		- the fasta file of a reference db
		
		returns
		-------
		- a distance file (computed with disseq or diskm) between 
			the references and the queries (all pairs)
		
		24.02.27, 24.03.06, 24.03.12, 24.04.22
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		charname	= self.yam['varname']			; print("charname    ", charname)
		map_meth	= self.yam['map_meth']			; print("map_meth    ", map_meth)
		dis_algo	= self.yam['dis_algo']			; print("dis_algo    ", dis_algo)
		dis_type	= self.yam['dis_type']			; print("dis_type    ", dis_type)
		reference	= self.yam['map_reference']		; print("reference   ", reference)
		x11			= self.yam['x11']				; print("x11         ", x11)
		save 		= self.yam['save']				; print("save        ", save)
		fmt			= self.yam['fmt']				; print("fmt         ", fmt)
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ---------------- building file names
		
		# fasta files
		query_fasfile	= self.fasfile
		ref_fasfile		= reference + ".fas"
		
		# seqid
		
		q_fas		= yap.fasta(fasfile=query_fasfile)
		queries		= q_fas.seq_id 
		
		# mapping distance and plot file
		mapping_disfile	= self.basename + "with_ref." + dis_algo + "." + dis_type
			
		### ----------- running
		
		mapotu	= ymap.mapping_withdis(reference, charname)
		
		if map_meth=="disseq":
			mapotu.map_dissec(query_fasfile, ref_fasfile, mapping_disfile, algo=dis_algo, dis_type=dis_type)
		
			
	# ------------------------------------------------------------------
	
	def deprecated_map_dis_otu(self):
		"""Mapping an OTU on a reference db with distance arrays
		
		!!!!! deprecated !!!!!
		
		requires
		--------
		- the fasta file of an OTU
		- the fasta file of a reference db
		
		returns
		-------
		- a distance file (computed with disseq or diskm) between 
			the references and the queries (all pairs)
		- an inventory of te OTU knowing these distances
		
		24.02.27, 24.03.06, 24.04.22
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		"""
		otu_num		= self.yam['otu_num']			; print("\notu_num     ", otu_num)
		map_meth	= self.yam['map_meth']			; print("map_meth    ", map_meth)
		map_inv		= self.yam['map_inv']			; print("map_inv     ", map_inv)
		reference	= self.yam['map_reference']		; print("reference   ", reference)
		charname	= self.yam['map_charname']		; print("charname    ", charname)
		gap_max		= self.yam['map_gap_max']		; print("gap_max     ", gap_max)
		x11			= self.yam['x11']				; print("x11         ", x11)
		save 		= self.yam['save']				; print("save        ", save)
		fmt			= self.yam['fmt']				; print("fmt         ", fmt)
		"""
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ---------------- building file names
		
		# fasta files
		query_fasfile	= self.fasfile
		ref_fasfile		= reference + ".fas"
		
		# seqid
		
		q_fas		= yap.fasta(fasfile=query_fasfile)
		queries		= q_fas.seq_id 
		
		# mapping distance and plot file
		mapping_disfile	= "mapping/otu_" + str(otu_num) + ".sw.dis"
		
			
		### ----------- running
		
		mapotu	= ymap.mapping_otu(reference, charname)
		
		if map_meth=="disseq":
			mapotu.map_dissec(query_fasfile, ref_fasfile, mapping_disfile)
		#
		if map_inv==True:
			set_taxa, Tab	= mapotu.annotates_queries(queries, mapping_disfile, gap_max)
			#
			if save:
				imfile	= "mapping/otu_" + str(otu_num) + ".inv"
			else:
				imfile	= None
			print(set_taxa)
			mapotu.plot_inventories(set_taxa, Tab, x11, imfile, fmt)
		
		
	# ------------------------------------------------------------------
	
	def map_sample_dis(self):
		"""(Descent with modifications of diagno-syst)
		
		af, 24.02.12
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
		charname	= self.yam[general]['varname']			; print("charname    ", charname)
		x11			= self.yam[general]['x11']				; print("x11         ", x11)
		save 		= self.yam[general]['save']				; print("save        ", save)
		fmt			= self.yam[general]['fmt']				; print("fmt         ", fmt)
		
		# specific
		
		reference	= self.yam[f_name]['reference']		; print("\nreference   ", reference)
		gap_max		= self.yam[f_name]['gap_max']		; print("gap_max     ", gap_max)
		new_char	= self.yam[f_name]['new_char']		; print("new_char    ", new_char)

		
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ---------------- building file names
		
		
		# fasta files
		
		query_fasfile	= self.fasfile
		ref_fasfile		= reference + ".fas"
		
		# 3 columns distance file
		
		sparse_disfile	= self.basename + ".sparse.sw.dis"
		print("sparse_disfile is", sparse_disfile)
		
		# character file
		
		charfile	= self.basename + ".char"
		
		# inventory
		
		#invfile			= self.basename + "." + charname + ".inv"
		
		### ----------------- running 
		
		mapsam					= ymap.mapping_dis_sample(reference, charname)
		queries, ref, dis		= mapsam.load_sparse_disfile(sparse_disfile=sparse_disfile)
		mapsam.check_refids(ref, mapsam.refid)
		dic						= mapsam.diagno_syst(gap_max, queries, ref, dis, mapsam.refid, mapsam.v_tax)
		annot					= ymap.dic_annot(dic)
		#
		#
		if new_char:
			header 		=  charname + "_withdis"
			annot.add2char(header, charfile)
		#
		if save:
			invfile		= self.basename + "." + charname + ".inv"
			annot.inventory(x11, invfile)
		
	# ------------------------------------------------------------------
	#
	#				 with blastn
	#
	# ------------------------------------------------------------------
	
	
	def blast_an_otu(self):
		"""to be deprecated
		
		maps queries of an OTU by parsing the blastfile 
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		!!!!!!!!!!! TO BE DEPRECATED ? !!!!!!!!!!!!!!!!!!!!!!
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		@ pl_hits	list of tuples		
		
		Notes
		-----
		
		If tup is a tuple in the list
			- tup[0]	a string,	is a taxon in reference database
			- tup[1]	a float,	is the pident of blast of the query on the subject
			
		
		af, 24.02.22, 24.02.27
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
<<<<<<<< HEAD:yap_lib_beforediving.py
		otu				= self.yam['otu_num']				; print("\notu             ", otu)
		charname		= self.yam['varname']				; print("charname        ", charname)
		x11				= self.yam['x11']					; print("x11             ", x11)
		save			= self.yam['save']					; print("save            ", save)
========
		otu				= self.yam[general]['otu_num']				; print("\notu             ", otu)
		charname		= self.yam[general]['varname']				; print("charname        ", charname)
		x11				= self.yam[general]['x11']					; print("x11             ", x11)
		save			= self.yam[general]['save']					; print("save            ", save)
		
		# specific
		reference		= self.yam[f_name]['reference']		; print("reference       ", reference)
		min_pident		= self.yam[f_name]['min_pident']		; print("min_pident      ", min_pident)

>>>>>>>> modules:yap_lib/yap_lib_2keys.py
		
		# specific
		reference		= self.yam['blast_reference']		; print("reference       ", reference)
		min_pident		= self.yam['blast_min_pident']		; print("min_pident      ", min_pident)

		
		### ----------- basic checks
		
		if self.questions:
			if not check_parameters(self.yam): return
			
		### ----------------- building files
		
		blastfile		= "blast/otu_" + str(otu) + ".blastn"
		print("blast outfile is", blastfile)
		
		if save:
			hitsfile	= "blast/otu_" + str(otu) + ".hits"
		else:
			hitsfile	= None
		
		### ------------- running
		
		blst_otu	= ymap.mapping_blast_otu(reference, charname)
		pl_hits		= blst_otu.parse_otu(blastfile, x11, hitsfile)
		blst_otu.parse_otu_display(pl_hits, min_pident)
		
	# ------------------------------------------------------------------
	
	
	def blast_set_of_otus(self):
		"""To be deprecated
		
		Provides an inventory of a list of OTUs
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		!!!!!!!!!!! TO BE DEPRECATED ? !!!!!!!!!!!!!!!!!!!!!!
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		Notes
		-----
		The list of OTUs is all OTUs from num=otu_first to num=otu_last (included)
		
		For each OTU in the list
			- computes the blastfile by calling blast_queries()
			- lists all tuples (taxon, pident) for each pair query x subject
			- filters the quality by pident
			- computes an inventory
			
		The inventory is a list of tuples, each tuple being (taxon, counts)
		where counts is the nb of times it is the taxon of a subject with acceptable pident
		
		af, 24.02.22, 24.02.27
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
		charname		= self.yam[general]['varname']				; print("charname        =", charname)
		
		# specific
		otu_first		= self.yam[f_name]['otu_first']		; print("\notu_first       =", otu_first)
		otu_last		= self.yam[f_name]['otu_last']		; print("otu_last        =", otu_last)
		reference		= self.yam[f_name]['reference']		; print("reference       =", reference)
		num_threads		= self.yam[f_name]['num_threads']		; print("\nnum_threads   ", num_threads)
		num_alignments	= self.yam[f_name]['num_alignments']	; print("num_alignments  ", num_alignments)
		min_pident		= self.yam[f_name]['min_pident']		; print("min_pident      =", min_pident)
		
		### ----------- basic checks
		
		if self.questions:
			if not check_parameters(self.yam): return
			
		### ----------------- running
		
		dic_inv	= {}
		for otu in list(range(otu_first, otu_last+1)):
			print_col("\notu ->", otu, color="cyan")
			queries_fasfile	= "fas/otu_" + str(otu) + ".fas"
			blastfile		= "blast/otu_" + str(otu) + ".blastn"
			#
			blst	= ymap.mapping_blast_otu(reference, charname, verbose=False)
			print("blasting ...")
			t_init	= time.time()
			blst.blast_queries(num_threads, queries_fasfile, num_alignments, blastfile, verbose=False)
			t_end	= time.time()
			print("... took", t_end - t_init, "seconds")
			#
			pl_inv			= blst.parse_otu(blastfile, verbose=False)
			otu_inv			= blst.parse_otu_display(pl_inv, min_pident, x11=False)
			key				= "otu_" + str(otu)
			dic_inv[key]	= otu_inv
		#
	
	# ------------------------------------------------------------------
	
	def blast_best_hit(self):
		"""Annotates queries from the subject with best hit after mapping with BLAST
		
		af, 24.02.23
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
		charname		= self.yam[general]['varname']				; print("charname        =", charname)
		x11				= self.yam[general]['blast_x11']				; print("x11             =", x11)
		
		# specific
		reference		= self.yam[f_name]['reference']		; print("reference       =", reference)
		min_pident		= self.yam[f_name]['min_pident']		; print("min_pident      =", min_pident)
		sort			= self.yam[f_name]['sort']			; print("sort            =", sort)
		new_char		= self.yam[f_name]['new_char']		; print("new_char        =", new_char)
		
		### ----------- file names and basic checks 
		
		blastfile	= self.basename + ".blastn"
		charfile	= self.basename + ".char"
		
		check_file_exists(blastfile)
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ------------ output new files
		
		
		invfile		= blastfile + '.besthit.' + charname + ".inv"
		
		
		### -------------- running
		
		map_blst					= ymap.mapping_blast_sample(reference, charname)
		dic_besthit, dic_annot		= map_blst.parse_besthit(blastfile, min_pident)
		self.dic_besthit			= dic_besthit
		map_blst.blast_inventory(dic_annot, sort, x11, invfile)
		if new_char:
			annot 		= ymap.dic_annot(dic_annot)
			header 		=  charname + "_blast_besthit"
			annot.add2char(header, charfile)
		
	# ------------------------------------------------------------------
	
	def blast_neighborhood(self):
		"""Annotates queries if subjects neighborhood is homogeneous after mapping with BLAST
		
		af, 24.02.23, 24.04.04
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
		charname		= self.yam[general]['varname']				; print("charname        =", charname)
		x11				= self.yam[general]['blast_x11']				; print("x11             =", x11)
		
		# specific
		reference		= self.yam[f_name]['reference']		; print("reference       =", reference)
		min_pident		= self.yam[f_name]['min_pident']		; print("min_pident      =", min_pident)
		sort			= self.yam[f_name]['sort']			; print("sort            =", sort)
		new_char		= self.yam[f_name]['new_char']		; print("new_char        =", new_char)
		
		### ----------- file names and basic checks
		
		charfile	= self.basename + ".char"
		blastfile	= self.basename + ".blastn"
		invfile		= blastfile + 'blastn.neighbor.' + charname + ".inv"
		
		check_file_exists(blastfile)
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ----------- running
		
		blst		= ymap.mapping_blast_sample(reference, charname)
		dic_annot	= blst.parse_neighborhood(blastfile, min_pident)
		blst.blast_inventory(dic_annot, sort, x11, invfile)
		#
		if new_char:
			annot 		= ymap.dic_annot(dic_annot)
			header 		=  charname + "_blast_neighbor"
			annot.add2char(header, charfile)
		
		
	# ==================================================================
	#
	#			methods for fasta file
	# 
	# ==================================================================
	
	
	def fas_len_hist(self):
		"""Displays the histogram of sequences lengths
		
		requires
		--------
		- a fasta file 
		
		returns
		-------
		- a file with the plot of sequence length histogram
			
		Notes
		-----
		
		- If save == True, the name of the file is built automatically
			as plots/<basename>.hist.<fmt>
		
		
		af, 22.04.12 ; 22.05.16 ; 23.04.28, 24.03.06
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general 
		x11		= self.yam[general]['x11']			; print("x11     ", x11)
		save	= self.yam[general]['save']			; print("save    ", save)
		fmt		= self.yam[general]['fmt']			; print("fmt     ", fmt)
		title	= self.yam[general]['title']		; print("title   ", title)
		
		# specific
		bins	= self.yam[f_name]['bins']		; print("bins    ", bins)
		kde		= self.yam[f_name]['kde']		; print("kde     ", kde)
		color	= self.yam[f_name]['color']		; print("color   ", color)
		

		
		#### --------------- basic checks
		
		check_file_exists(self.fasfile)
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ----------------- builds I/O filenames and lodas input files.
		
		fas	= yap.fasta(fasfile=self.fasfile)
		if save:
			imfile	= "plots/" + self.basename + '.hist'
		else:
			imfile = None
		
		### ----------------- running
						
		fas.plot_length_histogram(bins=bins, col=color, kde=kde, title=title, x11=x11, imfile=imfile, fmt=fmt)
		
	# ------------------------------------------------------------------

	def fas_len_plot(self):
		"""Displays the rank-size plot of the sequences lengths
		
		requires
		--------
		- a fasta file
		
		returns
		-------
		- a rank_size plot
		
		
		Notes
		-----
		
		- If save == True, the name of the file is built automatically
			as plots/<basename>.lenplot.<fmt>
		
		
		22.04.12 ; 22.05.16 ; 23.04.28 ; 23.05.06, 24.03.06
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
		x11			= self.yam[general]['x11']		; print("x11       ", x11)
		save		= self.yam[general]['save']		; print("save      ", save)
		fmt			= self.yam[general]['fmt']		; print("fmt       ", fmt)
		
		# specific
		color		= self.yam[f_name]['color']		; print("color     ", color)
		dots		= self.yam[f_name]['dots']		; print("dots      ", dots)

		
		#### --------------- basic checks
		
		check_file_exists(self.fasfile)
		if self.questions:
			if not check_parameters(self.yam): return
		
		
		### ------------------ running
		
		fas	= yap.fasta(fasfile=self.fasfile)
		if save:
			imfile	= "plots/" + self.basename + '.lenplot'
		else:
			imfile = None
		fas.plot_length(dots=dots, col=color, x11=x11, imfile=imfile, fmt=fmt)	

	# ==================================================================
	
	def phylo_phyML(self):
		"""Builds a phylogenetic tree with PhyML
		
		
		!!!! to be translated to block phyloTree
		
		@ Key block:	fas
		
		Parameters of the method
		------------------------
		varname	; string	; which taxon for labelling the leaves
		
		Parameters for I/O
		------------------
		
		I/O suffixes
		--------------
		infile	: fas
		
		Notes
		-----
		-> if varname is a taxonomic name, its values will be displayed as
			labels of the leaves
		-> if varname==None, the seq_ids will be used as labels
		
		Documentation to be continued
		
		af & jmf, ..., 23.04.29
		
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
		varname		= self.yam[general]['varname']		; print("varname     ", varname)
		
		# specific
		labels		= self.yam[f_name]['labels']	; print("\nlabels      ", labels)
		
		
		#### --------------- basic checks
		
		check_file_exists(self.fasfile)
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ----------------------
		
		
		if labels:
			#check_file_exists(self.charfile)
			charfile	= self.charfile
		else:
			charfile	= None
			varname		= None
		
		### ------------------ running		
		
		print(charfile)
		print(varname)
		phylo	= yap.phyloTree(fasfile=self.fasfile)
		phylo.phyML(charfile=charfile, varname=varname)
		
		
	# ------------------------------------------------------------------
	
	def phylo_distances(self):
		"""Computes pairwise distances between leaves on a phylogram
		
		
		to be trabslated to a block phyloTree
		
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# no parameters?
		
		nwfile		= self.basename + '.newick'
		disfile		= self.basename + '.evol.dis'
		#
		phylo		= yap.phyloTree(fasfile=self.fasfile)
		phylo.evol_distances(nwfile=nwfile, disfile=disfile)
		
	# ------------------------------------------------------------------
	
	def fas_aligned(self):
		"""Analyses an aligned fatsa file
		
		af, 23.12.23
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		######################################################
		# specific - revoir si besoin de otu ; faire sans ....
		######################################################
		
		
		otu			= self.yam[f_name]['otu']		; print("\notu   ", otu)
		nb_all		= self.yam[f_name]['nb_all']	; print("nb_all", nb_all)
		entropy		= self.yam[f_name]['entropy']	; print("entropy", entropy)
		het_pos		= self.yam[f_name]['het_pos']	; print("het_pos", het_pos)
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		mafft_dir 	= "mafft/"
		if otu>0:
			mafftfile	= mafft_dir + "otu_" + str(otu) + "_mafft.fas" 
		else:
			mafftfile	= self.basename + "_mafft.fas"
		check_file_exists(mafftfile)
		
		title = self.basename + "; otu_" + str(otu)
		
		
		if otu>0:
			het_mafftfile	= "mafft/analyses/otu_" + str(otu) + "_het_mafft.fas"
			nb_alleles_plot	= "mafft/analyses/otu_" + str(otu) + "_nb_alleles"
			entropy_plot	= "mafft/analyses/otu_" + str(otu) + "_entropy"
		else:
			het_mafftfile	= self.basename + "_het_mafft.fas"
			nb_alleles_plot	= self.basename + "_nb_alleles"
			entropy_plot	= self.basename + "_entropy"
		
		mft			= yap.aligned_fas(mafftfile=mafftfile)
		mft.alleles_by_position()
		#
		if nb_all:
			mft.plot_nb_alleles(title=title, imfile=nb_alleles_plot)
		#
		if entropy:
			mft.plot_alleles_entropy(title=title, imfile=entropy_plot)
		#
		if het_pos:
			mft.build_hetrogeneous_positions(sub_mafftfile = sub_mafftfile)
			
	# ------------------------------------------------------------------ù
	
	def histograms(self):
		"""Builds the histogram table of  kmers of a fasta file
		
		@ k		; integer;		the length of the kmer
		
		
		Notes
		-----
		
		Each row 'i' is a sequence or a read, each column 'j' is a kmer of given length,
		the value 'x' at cell (i,j) is the number of occurences of kmer 'j'
		in sequence 'i'.
		
		af, 24.01.08
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# specific 
		k	= self.yam[f_name]['k']		; print("\nk   ", k)
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		
		# reading fasta file
		fas	= yap.fasta(fasfile=self.fasfile)
		
		# building kmer table with jellyfish
		fas.get_histograms(k=k)
	
	# ==================================================================
	#
	#			methods for character file
	# 
	# ==================================================================	
	
	def char_inventory(self):
		"""Number of item for each value of a variable in charmat
				
		@ Key block:	char_
		
		Parameters of the function
		--------------------------
		@ varname	; string	; name of the variable 
		@ mode		; string	; mode for displaying results 
		@ min_size	; integer	; minimum size for the taxon to be displayed
		
		Parameters for I/O
		------------------
		
		None
		
		I/O suffixes
		------------
		infile	: char
		outfile	: none
		
		Notes
		-----
		-> the values of varname are ordered in decreasing order of counts
		-> mode is	2cols	if two columns
					df		if with a data frame
					
		af, 22.04.12 ; 22.05.16 ; 22.07.16 ; 23.04.28
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general 
		varname			= self.yam[general]['varname']	; print("\nvarname  : ", varname)
		
		# specific
		min_size		= self.yam[f_name]['min_size']	; print("min_size : ", min_size)
		mode			= self.yam[f_name]['mode']		; print("mode     : ", mode)
		
		#### --------------- basic checks
		
		check_file_exists(self.charfile)
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ----------------- running		
		
		char			= yap.charmat(charfile=self.charfile)
		res, count_df	= char.get_inventory(varname)
		#
		if mode=="2cols":
			n	= len(res)
			for i in range(n):
				item 	= res[i]
				if item[1] >= min_size:
					print(item[0], " -> ", item[1])
		#
		if mode=="df":
			print(count_df)		
		
	
	# ------------------------------------------------------------------
	
	def char_stats(self):
		"""Provides the number of different values for each varname
		
		Parameters
		----------
		no parameters
		
		af, 22.04.12; 22.05.16 ; 23.04.28
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		#### --------------- basic checks
		
		check_file_exists(self.charfile)
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ------------- running 
		
		char		= yap.charmat(charfile=self.charfile)
		char.get_stats()
		
	# ------------------------------------------------------------------
	
	def char_show(self):
		"""Displays the character file on the screen
		
		@ Key block:	char_
				
		Parameters
		----------
		no parameters
		
		I/O suffixes
		------------
		
		infile	: char
		
		Notes
		-----
		To be used for small character files only
		
		af, 22.07.16, 23.04.28
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		#### --------------- basic checks
		
		check_file_exists(self.fasfile)
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ------------- running 		
		
		char		= yap.charmat(charfile=self.seq)
		char.show()
		
	# ------------------------------------------------------------------
	
	def set_colors(self):
		"""
		af, 24.04.25
		""" 
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
<<<<<<<< HEAD:yap_lib_beforediving.py
		varname			= self.yam['varname']	; print("\nvarname  : ", varname)
========
		# general
		varname			= self.yam[general]['varname']	; print("\nvarname  : ", varname)
>>>>>>>> modules:yap_lib/yap_lib_2keys.py
		
		### ---------------- file names
		
		colorfile	= self.basename + "_" + varname + ".col"
				
		#### --------------- basic checks
		
		check_file_exists(self.charfile)
		if self.questions:
			if not check_parameters(self.yam): return
			
		### --------------- running
		
		char 	= yap.charmat(charfile=self.charfile)
		
		# writes colorfile
		char.build_colorfile(varname, default_col="grey", colorfile=colorfile)
		
		# interacts with colorfile
		
	# ------------------------------------------------------------------
	
	def sequences_per_taxon(self):
		"""Provides a file with the list of sequences labelled with taxa
		af, 24.03.12
		"""
		
		print_col("\nParameters for sequences per taxon", color="green")
		print_col("----------------------------------", color="green")
		varname			= self.yam['varname']	; print("\nvarname  : ", varname)
		
		
		sptfile		= self.basename + '.' + varname + '.spt'
		
		char 	= yap.charmat(charfile = self.charfile)
		char.seq_by_taxa(varname, sptfile) 
		
		
	# ==================================================================
	#
	#			methods for distances
	# 
	# ==================================================================	
	
	def disseq(self):
		"""Computes pairwise distances on a fasta file through pairwise alignment
		
		@ Key block:	dis_
		
		Parameters of the function
		--------------------------
		@ algo	; string	; which algorithm to use

		
		Parameters for I/O
		------------------
		@ otype	; string	; type of output file		
		
		I/O suffixes
		------------
		Infile			: fas
		Outfile			: <algo>.dis, <algo>.h5
		
		Notes
		-----
		
		-> otype: types of output file;  can be 'ascii' with tab as separator, or hdf5; 
			- otype = dis	for ascii file
			- otype = h5	for hdf5file
		
		-> algo:  can be
			- sw		for Smith-Waterman dissimilarity
			- nw		for Needleman-Wunsch dissimilarity
			
		-> kmer based distances can be computed with function diskm
		
		af & jmf, 22.08.12, 22.08.27, 23.05.02, 23.08.21, 23.11.19
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		#################################################
		###### Attention, à revoir (otype, dis_type)
		#################################################
		
		otype		= self.yam['dis_type']		; print("\notype     :", otype)
		dis_algo	= self.yam['dis_algo']		; print("dis_algo  :", dis_algo)
		
		
		#### --------------- basic checks
		
		
		check_file_exists(self.fasfile)
				
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ------------- running 				
		
		
		
		# extend the basename for specifying the distance
		
		dis_basename		= self.basename + "." + dis_algo
		print("\ndis_basename is", dis_basename)

		# checks whether the file <disbasename>.dis already exists
		# before overwriting it ...
		#
		### !!! what about h5 ??? (af, 23.11.16)
		### 23.11.19 => not necessary because it is not computed with yapotu ....
		#
		if self.questions:
			go = testif(dis_basename + ".dis")
		else:
			go = True
		
		# Computing distances
		
		if go:
			fas	= yap.fasta(fasfile=self.fasfile)
			fas.disseq(otype=otype, algo=dis_algo, dis_basename=dis_basename)
		else:
			return
	# ------------------------------------------------------------------	
	
	def diskm(self):
		"""Computes pairwise diddimilarities with kmers
		
		@ Key block:	dis_
		
		Parameters of the method
		------------------------
		@ kmer	; integer	; length of kmers 
		
		Parameters for I/O
		------------------
		@ otype	; string 	; type of distance file
		
		I/O suffixes
		------------
		infile	: .fas
		outfile	: k<x>.dis, k<x>.h5 
					(<x> is the length of the kmer)
					
		Notes
		-----
		
		-> the length of kmers is given as a plain integer, like 12, and not 'k12'
			this is due to a transparent call to jelly_diskm
			
		-> about jellyfish
			...
			
		af & jmf, 22.08.16, 22.08.27, 23.05.02, 23.11.24
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general 
		
		dis_type	= self.yam[general]['dis_type']		; print("dis_type", dis_type)
		
		###########################################################################
		# specific (!!!!! attention .... faut-il remonter kmer en <general> ? !!!!!
		###########################################################################
		
		kmer		= self.yam[f_name]['kmer']		; print("kmer", kmer)
		
		#### --------------- basic checks
		
		check_file_exists(self.fasfile)
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ------------- running 						

		# extend the basename for specifying the distance

		dis_algo			= 'k' + str(kmer) 
		dis_basename		= self.basename + "." + dis_algo
		print("\ndis_basename is", dis_basename)

		# checks whteher the file <sample>.kx.dis already exists
		# before overwriting it ...
		if self.questions:
			go = testif(dis_basename + ".dis")
		else:
			go = True
				
		# runs the computation of kmers with jellyfish
		
		if go:		
			if dis_type=="dis":
				cmd = 'diskm_jelly.py ' + self.basename + " " + str(kmer)

			#
			if dis_type=="h5":
				cmd = 'jelly_diskm.py ' + self.basename + " " + str(kmer) + ' h5'
			#
			print("\n" + "-> command line:  " + cmd)
			os.system(cmd)
		else:
			pass
			
	# ------------------------------------------------------------------
	
	def dis_hist(self):
		"""Histogram of distances
		
		@ Key block:	dhist_
				
		Parameters of the method
		------------------------

		@ bins		; integer	; number of bars in the histogram
		@ col		; string	; color of the histogram
		@ kde		; boolean	; whether to add kernel density estimation
		@ title		; string	; title of the plot
		
		Parameters for I/O
		------------------
		@ itype		; string	; type if distance file
		@ dis_algo	; string 	; method with which distances have been computed
		@ x11		; boolean	; whether to display the histogram on the screen
		@ imfile	; boolean 	; whether to save the histogram as a file
		@ fmt		; string	; format of the file		
		
		I/O suffixes
		------------
		infile	: .<dis_algo>.dis, <dis_algo>.h5	(e.g. sw.dis)
		outfile	: .<dis_algo>.dhist.<fmt> (e.g. .k8.dhist.eps) 
		
		Notes
		-----
		
		af, 22.08.19, 23.05.02, 23.08.21
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# genaral
		
		dis_type	= self.yam[general]['dis_type']	; print("\ndis_type    ", dis_type)
		dis_algo	= self.yam[general]['dis_algo']	; print("dis_algo     ", dis_algo)		
		x11			= self.yam[general]['x11']			; print("x11          ", x11)
		save 		= self.yam[general]['save']		; print("save         ", save)
		fmt			= self.yam[general]['fmt']			; print("fmt          ", fmt)
		
		# specific
		
		title		= self.yam[f_name]['title']		; print("title        ", title)
		bins		= self.yam[f_name]['bins']		; print("bins         ", bins)
		col			= self.yam[f_name]['col']			; print("col          ", col)
		kde			= self.yam[f_name]['kde']			; print("kde          ", kde)
		

		
		#### --------------- basic checks
		
		dhistbasename	= self.basename + "." + dis_algo
		dhistdisfile	= dhistbasename + "." + dis_type 
		check_file_exists(dhistdisfile)
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ----------------------- running
		
		# building file names
		
		dhisth5file		= dhistbasename + ".h5" 	# to be implemented
		
		
		print("loading distance file as an instance of class distances")
		if dis_type=="dis":
			dis	= yap.distances(disfile=dhistdisfile)
			self.seq_id	= dis.seq_id
		if dis_type=="h5":
			dis		= yap.distances(h5file=dhisth5file)
		# 
		if save:
			imfile 	= "plots/" + dhistbasename + ".dhist"
		else:
			imfile = None
		#
		dis.histogram(bins=bins, col=col, kde=kde, title=title, x11=x11, imfile=imfile, fmt=fmt)
	
	# ------------------------------------------------------------------
	
	def dis_heatmap(self):
		"""Displays a heatmap of the distance matrix with seaborn
		
		@ Key block:	dheat_
		
		Parameters of the function
		--------------------------
		
		@ varname	; string	; caracter for labeling rows and colulmns of the heatmap
		@ seaborn	; boolean	; whether to use seaborn
		
		Parameters for I/O
		------------------
		@ itype		; string 	; format of the distance file
		@ dis_algo	; string	; methof with which distances have been computed
		@ x11		; boolean	; whether to display the histogram on the screen
		@ save		; boolean 	; whether to save the histogram as a file
		@ fmt		; string	; format of the file				
		
		I/O suffixes
		------------
		infile	: <dis_algo>.dis or <dis_algo>.h5
		outfile	: [à faire]
		
		Notes
		-----
		
		-> if varname==no, the seqi_ids are used to label rows and columns
		
		af, ..., 23.05.02
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
		
		dis_type		= self.yam[general]['dis_type']			; print("\ndis_type       ", dis_type)
		dis_algo		= self.yam[general]['dis_algo']			; print("dis_algo       ", dis_algo)
		varname			= self.yam[general]['varname']			; print("varname        ", varname)
		save			= self.yam[general]['save']		; print("save           ", save)
		fmt				= self.yam[general]['fmt']			; print("fmt            ", fmt)		
		
		# specific
		seaborn			= self.yam[f_name]['seaborn']		; print("seaborn        ", seaborn)

		
		#### --------------- basic checks
		
		heatbasename		= self.basename + "." + dis_algo
		heatdisfile			= heatbasename + "." + dis_type
		#
		check_file_exists(heatdisfile)
		if varname:
			check_file_exists(self.charfile)
		#
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ----------------------- running
		
		# loading distance file 
		
		print("loading distance file as an instance of class distances")
		if dis_type=="dis":
			dis	= yap.distances(disfile=heatdisfile)
			self.seq_id	= dis.seq_id
		elif dis_type=="h5":
			dis		= self.Dis
		else:
			print("\ndis_type", dis_type, "not recognized\n")
			return
			
	
		# if labels with taxa
		
		if varname:
			char 		= yap.charmat(charfile=self.charfile)
			labels		= char.get_tax(varname)
		else:
			labels	= self.seq_id
							
		
		# drawing heatmap
		
		if seaborn:
			if save:
				imfile	= "plots/" + heatbasename + ".hm"
			else:
				imfile=None
			#			
			dis.clus_heatmap(labels=labels, imfile=imfile, fmt=fmt)
		else:
			tr				= dis.to_tree()
			dend			= sch.dendrogram(Z=tr.Z, no_plot=True)
			leaves_order	= dend["ivl"]
			items_order		= [int(s) for s in leaves_order]
			#
			dis.heatmap(items_order=items_order)

	# ------------------------------------------------------------------
	
	def deprecated_spikeshow(self):
		"""Displays a forest of spikes of the distance matrix
		
		Keys
		----
		
		@ itype		; string ; format of the distance file
		@ dis_algo	; string	; methof with which distances have been computed
		@ varname	; string	; caracter for labeling rows and colulmns of the heatmap
		@ save		; boolean	; whether to save the heatmap in a file
		
		Required file
		-------------
		<basename>.<dis_algo>.dis or <basename>.<dis_algo>.h5
		<basename>.char if a varname is selected
		
		Notes
		-----
		
		- if varname==no, the seqi_ids are used to label rows and columns
		
		af, ..., 23.05.02
		"""
		
		print("[yap!]:[dis_heatmap()]")
		#
		# --------------------- reading parameters 
		
		print("\nparameters for dis_heatmap")
		print("------------------")
		#
		itype			= self.yam['dheat_itype']		; print("\nitype      ", itype)
		dis_algo		= self.yam['dheat_dis_algo']		; print("dis_algo   ", dis_algo)
		varname			= self.yam['dheat_varname']		; print("varname    ", varname)
		seaborn			= self.yam['dheat_seaborn']		; print("seaborn    ", seaborn)
		
		#### --------------- basic checks
		
		heatbasename		= self.basename + "." + dis_algo
		if itype=="tsv":
			heatdisfile		= heatbasename + ".dis"
		if itype=="h5":
			heatdisfile		= heatbasename + ".h5" 	
		#
		check_file_exists(heatdisfile)
		if varname:
			check_file_exists(self.charfile)
		#
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ----------------------- running
		
		# loading distance file 
		
		print("loading distance file as an instance of class distances")
		if itype=="tsv":
			dis	= yap.distances(disfile=heatdisfile)
			self.seq_id	= dis.seq_id
		if itype=="h5":
			dis		= yap.distances(h5file=heatdisfile)
			
	
		# if labels with taxa
		
		if varname:
			char 		= yap.charmat(charfile=self.charfile)
			labels		= char.get_tax(varname)
		else:
			labels	= self.seq_id
							
		
		# drawing heatmap
		
		if seaborn:
			dis.clus_heatmap(labels=labels)
			plt.show()
		else:
			tr			= dis.to_tree()
			tr.draw()
			dis.heatmap(items_order=tr.nodes_order)

	# ------------------------------------------------------------------
	
	def dis_hac_old(self):
		"""Hierarchical aggregative clustering
		
		@ Key block:	hac_
		
		Parameters of the function
		--------------------------
		

		@ meth			; string	; method for linkage
		@ partition		; boolean	; whether to build a partition of taxa
		@ part_to_file	; boolean	; whether to write the partition in a file		
		@ height		; real		; the height at which to cut the tree for the partition
		@ varname		; string	; taxon with which to build the partition
		@ min_size		; integer	; minimum size of a cluster for being in partition
		
		Parameters for I/O
		------------------
		@ itype			; string 	; format of the input distance file
		@ dis_algo		; string 	; methof with which distances have been computed		
		
		I/O suffixes
		------------
		<basename>.<dis_algo>.dis or <basename>.<dis_algo>.h5 or
		self.charfile 
		
		
		Notes
		-----
		***********************************************************
		****** à revoir (couper l'arbre selon la hauteur ...) *****
		***********************************************************
		
		af, 22.11.12, 23.05.02, 23.11.24
		"""
		
		print("[yap!]:[hac()]")
		#
		# --------------------- reading parameters 
		
		print("\nparameters for hac")
		print("------------------")
		#
		dis_type		= self.yam['hac_dis_type']		; print("\ndis_type    ", dis_type)
		dis_algo		= self.yam['hac_dis_algo']		; print("dis_algo     ", dis_algo)
		meth			= self.yam['hac_meth']			; print("meth         ", meth)
		varname			= self.yam['hac_varname']		; print("varname      ", varname)
		partition		= self.yam['hac_partition']		; print("partition    ", partition)
		height			= self.yam['hac_height']		; print("height       ", height)
		min_size		= self.yam['hac_min_size']		; print("min_size     ", min_size)
		part2file		= self.yam['hac_part2file']		; print("part2file    ", part2file, "\n")
		
		
		# basic checks
		
		hacbasename	= self.basename + "." + dis_algo
		hacdisfile	= hacbasename + ".dis" 
		hach5file	= hacbasename + ".h5" 	# to be implemented
		
		check_file_exists(hacdisfile)
		if varname:
			check_file_exists(self.charfile)
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		
		### ------------------------------- running 
		
		# file with partition (clusfile)
		
		if part2file:
			clusfile		= hacbasename + "." + meth + ".clus"
			
		# labels with taxa
		
		if varname:
			char 		= yap.charmat(charfile=self.charfile)
			labels		= char.get_tax(varname)
		else:
			labels = None
		
		# --------------------- loading distance file 
		
		print("loading distance file as an instance of class distances")

		dis	= yap.distances(disfile=hacdisfile)
		
		# -------------------- to tree()
		
		tr	= dis.to_tree()
		tr.draw(labels=labels)
		
		# --------------------- to partition()
		
		# gets a partition from the tree (by height)
		
		if height > 0:
			print("height is", height, "\n")
			part 	= tr.get_clusters_byheight(height)
			part.write_clus(clusfile)
			part.plot_rank_size(log=False)
		
		
	# ------------------------------------------------------------------
	
	def dis_mst(self):
		"""Minimum spanning tree
		
		@ Key block:	mst_
		
		Parameters of the function
		--------------------------
		@ varname	; string 	; taxon selected for coloring nodes
		@ d_size	; integer	; dot size

		
		Parameters for I/O
		------------------
		@ itype		; string	; type of input distance file
		@ dis_algo	; string	; method for building distance matrix
		
		I/O suffixes
		------------
		infiles	: .dis or .h5
				  .char
		outfile	: 
				
		>>>>> [à compléter : sauvegarde du plot]
		
		<basename>.<dis_algo>.dis or <basename>.<dis_algo>.h5
		self.charfile	if varname given 
		
		
		af, 22.11.14, 23.05.02
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general 
		
		dis_type	= self.yam[general]['dis_type']		; print("\ndis_type     =", dis_type)
		dis_algo	= self.yam[general]['dis_algo']		; print("dis_algo      =", dis_algo)
		varname		= self.yam[general]["varname"]		; print("varname       =", varname)
		legend		= self.yam[general]["legend"]		; print("legend        =", legend)
		
		# specific
		
		dot_size	= self.yam[f_name]["dot_size"]		; print("dot_size      =", dot_size)
		
		### --------------------  basic checks
		
		# building names 
		mst_basename	= self.basename + "." + dis_algo	# extended basename
		mst_disfile		= mst_basename  + "." + dis_type 	# disfile to be read
			
		# checking necessary files exist
		
		check_file_exists(mst_disfile)
		if varname:
			check_file_exists(self.charfile)
		
		# check parameters
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ----------------- running
		
		# loading files
		
		if dis_type=="dis":
			dis				= yap.distances(disfile=mst_disfile)
		elif dis_type=="h5":
			dis				= self.dis
		else:
			print("type of distance file anadequate!")
		
		# loading charfile
		
		v_tax, v_col, dicol = load_charfile(charfile=self.charfile, varname=varname)
		
		# building and plots mst
		
		A_tree, Gx	= dis.to_mst()
		yap.plot_mst(A_tree, Gx, v_tax=v_tax, legend=legend, dicol=dicol, v_col=v_col, dot_size=dot_size)
		#
	
		
	# ==================================================================
	#
	#
	
	# ------------------------------------------------------------------

	def dis_graph(self):
		"""Builds the graph from distance array
		
		@ Key block:	graph_
				
		Parameter of the method
		-----------------------
		@ gap			; integer	; gap for building gap
		
		Parameters for displaying
		-------------------------
		@ tool			; string	; tool for displaying the graph
		@ varname		; string	; taxon according to which to color the nodes
		@ d_size		; integer	; dot size
		@ legend		; boolean	; whether to display the legen for colors		
		
		Parameter for I/O
		-----------------
		@ dis_type		; string	; type of distance file		
		@ dis_algo		; string 	; method with which distances have been computed
		@ x11			; boolean	; whether to display the graph on screen
		@ save			; boolean	; whether to save the plot of the graph
		@ fmt			; string	; format of the file 				
		
		I/O suffixes
		------------
		infile	: .<dis_algo>.<dis_type>, e.g. sw.dis
		outfile	: .<dis_algo>.<dis_type>.g<gap>.graph.<fmt>, e.g. sw.dis.g7.graph.eps
		
		
		Notes
		-----
		-> tool is the tool in networkxs used to display the graph
			default value is "quick", which works pretty well.
		
		-> dis_type is 'dis' or 'h5', and specifies which distance matrix to read.
		
		af, 22.03.22; 22.05.16; 22.08.27; 23.04.23 ; 23.05.04, 23.07.07
		23.08.04, 23.08.19
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
		dis_type	= self.yam[general]["dis_type"] 	; print("\ndis_type     ", dis_type)
		dis_algo	= self.yam[general]["dis_algo"]		; print("dis_algo     ", dis_algo) 
		varname		= self.yam[general]["varname"]		; print("varname      ", varname)
		x11			= self.yam[general]["x11"] 			; print("x11          ", x11)
		legend		= self.yam[general]["legend"]		; print("legend       ", legend)
		fmt			= self.yam[general]["fmt"]			; print("fmt          ", fmt)
		save		= self.yam[general]["save"]			; print("save         ", save, "\n")
		
		# specific
		gap			= self.yam[f_name]["gap"]			; print("gap          ", gap) 
		tool		= self.yam[f_name]["tool"] 			; print("tool         ", tool)
		d_size		= self.yam[f_name]["d_size"]		; print("d_size       ", d_size)

		
		### ------------- basic checks
		
		cc_basename		= self.basename + "." + dis_algo
		cc_disfile		= cc_basename + "." + dis_type 
		
		check_file_exists(cc_disfile)
		
		if self.questions:	
			if not check_parameters(self.yam): return
		
		### ------------ running
		
		# loads character file and selects v_tax: the column of header given by <varname>
		if varname:
			char 			= yap.charmat(charfile=self.charfile)
			v_tax 			= char.get_tax(varname)
			v_col, dicol	= yap.def_colors(v_tax)
		
		# extends the basename to keep track of the algo for distances
		
		ccgr_basename		= self.basename + "." + dis_algo + ".g" + str(gap)
				
		# instances a class <distances> as dis

		if dis_type=="h5":
			if self.h5file==cc_disfile:
				dis	= self.dis
			else:
				dis	= yap.distances(h5file=cc_disfile)
		#
		if dis_type=="dis":
			dis		= yap.distances(disfile=cc_disfile)

		### ------------ running
		
		gr		= dis.to_graph(gap=gap)
		
		# plotting the graph
		
		if dis_type=="h5":
			x11	= False
			print("x11 has been set to False, because of the number of reads")
			print("the graph has not been visualized because of its size.")
		else:
			if save:
				imfile 	= "plots/" + ccgr_basename + ".graph"
			else:
				imfile = None
			#
			gr.plot(tool=tool, col_node=v_col, col_edge="gray", dot_size=d_size, legend=legend, dicol=dicol, title=ccgr_basename, x11=x11, imfile=imfile, fmt=fmt)
			if imfile:
				print("graph has been saved in file named", imfile + '.' + fmt)
			else:
				print("\ngraph not plotted!\n")
			
	
	# ------------------------------------------------------------------
	
	
	def mds(self):
		"""Multidimensional Scaling
		
		@ Key block:	mds_
		
		Parameters of the method
		------------------------
		@ meth		; string	; method for MDS
		@ n_axis	; integer	; numner of axis to compute		
		
		Parameters for display
		----------------------
		None
		
		Parameters for I/O
		------------------
		@ itype		; string	; type of distance file
		@ otype		; string	; type of output coordonate file
		@ dis_algo	; string	; method with which distances have been computed
		
		I/O suffixes
		-------------
		infile	:.<dis_algo>.dis or <dis_algo>.h5
		outfile	: <dis_algo>.mds, e.g. sw.mds
		
		
		Notes
		-----
		
		-> itype must be 'tsv' or 'h5'
			- if itype=='dis', the distance array is in ascii
			- if iytpe=='h5', the distance dile is in hdf5
		-> otype usually is 'tsv', because coordinate files have a limited number of colulmns
		-> meth selects between 'evd', 'svd', 'grp'; 
			- svd is default value for medium sized arrays (like up to 10,000)
			- grp is recommended method beyond
		-> n_axis is the number of axis to be computed
			- if meth==svd, it is recommended to have something like 30 axis
			- if meth==grp, this figure should be much higher, like 1,000
		-> dis_algo must be 'sw' or 'nw' (see disseq)
		
		
		af, 22.05.16, 22.08.27, 23.05.03
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# paremeters
		print_parameters(f_name)
		
		# general
<<<<<<<< HEAD:yap_lib_beforediving.py
		dis_type	= self.yam['dis_type']			; print("\ndis_type     =", dis_type)
		dis_algo	= self.yam['dis_algo']			; print("dis_algo     =", dis_algo)
		#otype		= self.yam['otype']				; print("otype        =", otype)
		
		# specific
		n_axis		= self.yam['mds_n_axis']		; print("n_axis       =", n_axis)
		meth		= self.yam['mds_meth']			; print("method       =", meth)
		nb_eig		= self.yam['mds_nb_eig']		; print("nb_eig       =", nb_eig)
========
		dis_type	= self.yam[general]['dis_type']			; print("\ndis_type     =", dis_type)
		dis_algo	= self.yam[general]['dis_algo']			; print("dis_algo     =", dis_algo)
		#otype		= self.yam[general]['otype']				; print("otype        =", otype)
		
		# specific
		n_axis		= self.yam[f_name]['n_axis']		; print("n_axis       =", n_axis)
		meth		= self.yam[f_name]['meth']			; print("method       =", meth)
		nb_eig		= self.yam[f_name]['nb_eig']		; print("nb_eig       =", nb_eig)
>>>>>>>> modules:yap_lib/yap_lib_2keys.py
		

		### -------------------------------- basic checks
		
		# building file names
		
		mds_basename	= self.basename + "." + dis_algo	# extended basename
		mds_disfile		= mds_basename  + "." + dis_type

		# checks  
		
		check_file_exists(mds_disfile)
		if self.questions:
			if not check_parameters(self.yam): return
		
		### --------------- running 
		
		# names of files to be written
		
		mds_coordfile	= mds_basename + ".mds" 			# coordfile to be written
		
		# instance of class "distances"

		print("loading distance file as an instance of class distances")
		#
		if dis_type=="dis":
			dis		= yap.distances(disfile=mds_disfile, zero_diagonal=False, test_symmetry=False, set_diag_to_zero=False)
		elif dis_type=="h5":
			dis		= self.dis
		else:
			print_col("dis_type apparently inadequate ...", color="red")
			
		#  tests whether the file <sample>.<dis_algo>.mds already exists
		if self.questions:
			go = testif(mds_coordfile)
		else:
			go = True		
		
		# --------------- running mds 
		
		if go:		
			dis.mds(k=n_axis,meth=meth, no_loop=True, nb_eig=nb_eig, pc=False, coordfile=mds_coordfile)
			#self,k=-1, meth="svd", no_loop=True, nb_eig=0, pc=True, coordfile=None
		else:
			return
		
	# ------------------------------------------------------------------
	
	def coa_kmers(self):
		"""Correspondence Analysis on kmers contingency table
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		
		# specific
		k 			= self.yam[f_name]['k']				; print('\nk         ', k)

		#
		r			= self.yam[f_name]['r']				; print('r         ', r)
		transpose	= self.yam[f_name]['transpose']		; print('transpose ', transpose)
		meth		= self.yam[f_name]['meth']			; print('meth      ', meth)

		#
		plot_coa	= self.yam[f_name]['plot_coa']		; print('plot_coa  ', plot_coa)
		
		### -------------------------------- basic checks
		
		# building file names
		
		histfile	= self.basename + ".k" + str(k) + ".histo"
		
		# checks  
		
		check_file_exists(histfile)
		if self.questions:
			if not check_parameters(self.yam): return
		
		### --------------- running 
		
		hist 	= yap.histo_kmers(histfile=histfile)
		hist.coa(r=r, meth=meth, transpose=transpose)
		hist.write_components()
		
		if plot_coa:
			hist.plot_coa()
		

	# ------------------------------------------------------------------
	def tsne(self):
		"""t-SNE
		
		@ Key block:	tsne_
		
		Parameters of the method
		------------------------
		@ dim		; integer	; dimension ot point cloud
		@ p			; integer	; see notes (default: p = -1)
		
		Parameters for display
		----------------------
		None
		
		Parameters for I/O
		------------------
		@ itype		; string	; type of input distance file
		@ otype		; string	; type of output coordinates file
		@ dis_algo	; string	; method with which distances have been computed
		
		I/O suffixes
		------------
		infile	: <dis_algo>.dis or <dis_algo>.h5
		outfile	: <dis_algo>.tsne, like k12.tsne
		
		
		Notes
		-----
		
		-> itype must be 'tsv' or 'h5' (as for mds)
			- if itype=='dis', the distance array is in ascii
			- if iytpe=='h5', the distance dile is in hdf5
		-> otype usually is 'tsv', because coordinate files have a limited number of colulmns
		-> dim: can be 2 or 3; recommended to keep it at dim=2
		-> p: number of coordinates to keep in the mds as first step;
			- recommended to keep it at p=-1 (all coordinates computed)
		-> dis_algo: as for mds; can be sw or nw	
		
		af, 22.08.06, 22.08.27, 23.05.03
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		dis_algo	= self.yam['dis_algo']			; print("\ndis_algo    ", dis_algo)
		dis_type	= self.yam['dis_type']			; print("dis_type    ", dis_type)
		otype		= self.yam['tsne_otype']		; print("otype       ", otype)
		dim			= self.yam['tsne_dim']			; print("dim         ", dim)
		p			= self.yam['tsne_p']			; print("p           ", p)

		
		### -------------------------------- basic checks
		
		# building file names
		
		tsne_basename	= self.basename + "." + dis_algo	# extended basename
		tsne_disfile	= tsne_basename + "." + dis_type 
		
		# checks  
		
		check_file_exists(tsne_disfile)
		if self.questions:
			if not check_parameters(self.yam): return
		
		### --------------- running 
		
		# extending basename, and names of distance files to be used
		
		tsne_coordfile	= tsne_basename + ".tsne" 			# coordfile to be written
		
		# instance of class "distances"

		print("loading distance file as an instance of class distances")
		if dis_type=="dis":
			dis		= yap.distances(disfile=tsne_disfile)
		elif dis_type=="h5":
			dis		= self.dis
		else:
			print("dis_type apparently inadequate ...")
			
		#  tests whether the file <sample>.<dis_algo>.mds already exists
		if self.questions:
			go = testif(tsne_coordfile)
		else:
			go = True
					
		# --------------- tsne
		
		if go:		
			dis.tsne(n_tsne=dim, mode="comp", p=p, coordfile=tsne_coordfile)
		else:
			return
	
	# ------------------------------------------------------------------
	
	def otu_build_dbscan(self):
		"""(OTUs with DBScan - under construction)
		"""
		
		print("[yap!]:[dbscan()]")
		
		print("\nparameters for dbscan")
		print("---------------------------")
		
		
		# parameters for the method
		
		# general
		
		dis_algo		= self.yam[general]['dis_algo']	; print("dis_algo...", dis_algo)
		dis_type		= self.yam[general]['dis_type']	; print("dis_type..", dis_type)
		save 			= self.yam[general]['save']		; print("save.......", save)
		
		
		# specific
		
		gap				= self.yam[f_name]['gap']		; print("\ngap........", gap)
		min_sample		= self.yam[f_name]['min_sample']	; print("min_sample.", min_sample)
		
		# parameters for I/O
		

		#
		
		# checks parameters
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		
		dbscan_basename	= self.basename + "." + dis_algo	# extended basename
		dbscan_disfile	= dbscan_basename + "." + dis_type
		dbscan_clusfile = dbscan_basename + ".db.clus"
		
		if dis_type=="dis":
			dis		= yap.distances(disfile=dbscan_disfile)
		if dis_type=="h5":
			dis		= self.dis
		
		print("distance file loaded ...")
		print("running dbscan ...")
		part 	= dis.dbscan(gap=gap, min_sample=min_sample)
		if save:
			print("partition saved in file", dbscan_clusfile)
			part.write_clus(clusfile=dbscan_clusfile)
		
		 
	# ------------------------------------------------------------------
	
	def pc_scatter(self):
		"""Scatter plot after MDS or t-SNE
		
		@ Key block:	pc_
		
		Parameters of the method
		------------------------
		@ axis_i		; integer	; first axis (starting at 1)
		@ axis_j		; integer	; second axis
		@ varname		; string	; taxon for coloring dots
		
		Parameters for display
		----------------------
		@ color_range	; string	; discrete or continuous
		@ legend		; boolean	; whether to display the legend for colors
		@ d_size		; integer	; dot size
		@ d_size_small	; integer	; small dot size
		@ d_size_large	; integer	; large dot size
		@ x11			; boolean	; whether to display the plot on the screen
		@ save 			; string	; whether to save the plot as a file
		@ fmt			; string	; format of file with saved plot
		
		Parameters for I/O
		------------------
		@ coord			; string 	; origine of coordfile (mds or tsne)
		@ dis_algo		; string 	; method with which distances have been computed
		@ itype			; string	; format of coordfile		


		I/O suffixes
		------------
		infile	: .coord
		outfile	: 
		
		Notes
		-----
		
		There are several parameters which can be ordered into families:
		-> coord, dis_algo, itype: formats of I/O files
			- mds or tsne for 'coord' (which method has issued the coordinate file)
			- usually tsv for itype (type of the coordinate file, usually ascii)
			- sw, nw, kx for dis_algo
		-> axis_i, axis_j: which axis to draw: is to draw: axis_i (x axis) and axis_j (y axis)
			axis are counted naturally from 1 to r; yap translates into python (from 0 onwards)
		-> varname, legend : how to color dots if a charfile is available
		-> d_size, d_size_small, d_size_large: dot size
			large dots are those which are colored; small ones and those which are not colored
		-> x11, save, fmt: for displaying/saving the plot
		
		af, 22.05.16, 22.08.18, 23.05.03, 23.08.21
		""" 
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
<<<<<<<< HEAD:yap_lib_beforediving.py
		coord			= self.yam['coord']				; print("\ncoord        ", coord)
		color_range		= self.yam['pc_color_range']	; print("color_range  ", color_range)
		colorfile		= self.yam['pc_colorfile']		; print("colorfile    ", colorfile)
		axis_i			= self.yam['pc_axis_i']			; print("axis_i       ", axis_i)
		axis_j			= self.yam['pc_axis_j']			; print("axis_j       ", axis_j)
		varname			= self.yam['varname']			; print("varname      ", varname)
		legend			= self.yam['pc_legend']			; print("legend       ", legend)
		dot_size 		= self.yam['pc_d_size']			; print("dot_size     ", dot_size)
		d_size_small	= self.yam['pc_d_size_small']	; print("d_size_small ", d_size_small)
		d_size_large	= self.yam['pc_d_size_large']	; print("d_size_large ", d_size_large)
		x11				= self.yam['x11']				; print("x11          ", x11)
		save 			= self.yam['save']				; print("sav          ", save)
		fmt				= self.yam['fmt']				; print("fmt          ", fmt)
		dis_algo		= self.yam["dis_algo"]			; print("dis_algo     ", dis_algo)
========
		# general 
		
		dis_algo		= self.yam[general]["dis_algo"]			; print("\ndis_algo     ", dis_algo)
		coord			= self.yam[general]['coord']			; print("coord        ", coord)
		varname			= self.yam[general]['varname']			; print("varname      ", varname)
		legend			= self.yam[general]['legend']			; print("legend       ", legend)
		x11				= self.yam[general]['x11']				; print("x11          ", x11)
		save 			= self.yam[general]['save']				; print("sav          ", save)
		fmt				= self.yam[general]['fmt']				; print("fmt          ", fmt)
		
		
		# specific 
		
		color_range		= self.yam[f_name]['color_range']		; print("color_range  ", color_range)
		colorfile		= self.yam[f_name]['colorfile']			; print("colorfile    ", colorfile)
		axis_i			= self.yam[f_name]['axis_i']			; print("axis_i       ", axis_i)
		axis_j			= self.yam[f_name]['axis_j']			; print("axis_j       ", axis_j)

		d_size 			= self.yam[f_name]['d_size']			; print("d_size       ", d_size)
		d_size_small	= self.yam[f_name]['d_size_small']		; print("d_size_small ", d_size_small)
		d_size_large	= self.yam[f_name]['d_size_large']		; print("d_size_large ", d_size_large)

>>>>>>>> modules:yap_lib/yap_lib_2keys.py
		
		
		### ----------------- basic checks  
		
		# test whether coordfiles exist
		#
		# coordfiles are expected to be in tsv format
		
		mds_coordfile	= self.basename + "." + dis_algo + ".mds"
		tsne_coordfile	= self.basename + "." + dis_algo + ".tsne"
		coa_coordfile	= self.basename + "." + dis_algo + ".coa"
		
			
		if coord=="mds":
			check_file_exists(mds_coordfile)
		
		if coord=="tsne":
			check_file_exists(tsne_coordfile)
			
		if coord=="coa":
			check_file_exists(coa_coordfile)
		#
		if varname:
			check_file_exists(self.charfile)
			
		# checks parameters
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ------------------ running 
		
		# dot size if no colorfile
		
<<<<<<<< HEAD:yap_lib_beforediving.py
		#if colorfile==False:
		#	dot_size	= dot_size
========
		if colorfile==False:
			dot_size	= d_size
>>>>>>>> modules:yap_lib/yap_lib_2keys.py
			
		
		# color selection
		
		if colorfile:
			colorfile			= self.basename + "_" + varname + ".col"
			if varname:		
				char 			= yap.charmat(charfile=self.charfile)
				v_tax			= char.get_tax(varname)
				v_col, dicol	= yap.set_colors_from_file(v_tax, colorfile)
				#
				dot_size	= [-1]*len(v_col)
				for i, item in enumerate(v_col):
					if v_col[i] == "grey":
						dot_size[i]	= d_size_small
					else:
						dot_size[i]	= d_size_large
				
			else:
				print_col("you should select a varname", color="red")
		else:
			dicol			= {}
			#
			if varname:		
				char 			= yap.charmat(charfile=self.charfile)
				v_tax			= char.get_tax(varname)
				if color_range=="discrete":
					v_col, dicol	= yap.def_colors(v_tax)
				if color_range=="continuous":
					v_col			= v_tax
			else:
				v_col	= "blue"		

		# instance of class "point_cloud"
		

		os.makedirs("plots", exist_ok=True)
		#
		if coord=="tsne":
			pc			= yap.point_cloud(coordfile=tsne_coordfile)
			base4plot	= "plots/" + tsne_coordfile + "." + str(axis_i) + "." + str(axis_j)
			plotfile	=  base4plot + "." + fmt
			print("plotfile is:  ", plotfile)
		#
		if coord=="mds":
			pc			= yap.point_cloud(coordfile=mds_coordfile)
			base4plot	= "plots/" + mds_coordfile + "." + str(axis_i) + "." + str(axis_j) 
			plotfile	= base4plot + "." + fmt
			print("plotfile is:  ", plotfile)
		#
		if coord=="coa":
			pc			= yap.point_cloud(coordfile=coa_coordfile)
			base4plot	= "plots/" + coa_coordfile + "." + str(axis_i) + "." + str(axis_j) 
			plotfile	= base4plot + "." + fmt
			print("plotfile is:  ", plotfile)
		#
		if self.questions:
			go = testif(plotfile)
		else:
			go = True						
		
		if go:
			print("plotting axis", axis_i, "and axis", axis_j)
			if save:
				imfile 	= base4plot
			else:
				imfile	= None
			pc.scatter(axis_i-1,axis_j-1, v_col=v_col, dot_size=dot_size, legend=legend, dicol=dicol, color_range=color_range, x11=x11, imfile=imfile,fmt=fmt)
		else:
			return
		
	# ------------------------------------------------------------------
	
	
	def pc_splines(self):
		"""Displays parallel coordinates of a point cloud with splines
		
		@ Key block:	pc_
		
		Parameters of the method
		------------------------
		@ n_axis_splines	; integer	; number of coordinates to take into account
		
		Parameter for display
		---------------------
		@ varname			; string	; taxon for coloring splines
		@ legend			; boolean	; whether to display the color legend
		@ x11				; boolean	; whether to display the splines on the screen
		@ save				; boolean	; whether to save the plot
		@ fmt				; string	; format for the saved plot
		
		Parameters fo I/O
		-----------------
		@ coord				; string	; which method has issued the coordinare file
		@ dis_algo			; string	; method with which distances have been computed
		
		Suffixes for I/O
		----------------
		infile	: .coord, .char
		
		Notes
		-----
		-> coord			: mds or tsne (which method has issued the coordinate file)
		-> itype			: usually tsv (type of the coordinate file, usually ascii)
		-> dis_algo			: sw, nw, kx
		-> varname, legend : how to color dots if a charfile is available
		
		af, 22.05.16; 23.05.04, 23.08.21
		""" 
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general 
		dis_algo		= self.yam[general]["dis_algo"]		; print("dis_algo       ", dis_algo)
		coord			= self.yam[general]['coord']			; print("\ncoord          ", coord)
		varname			= self.yam[general]['varname']			; print("varname        ", varname)
		legend			= self.yam[general]['legend']			; print("legend         ", legend)
		save			= self.yam[general]['save']			; print("save           ", save)
		fmt				= self.yam[general]['fmt']			; print("fmt            ", fmt)

				
		
		# specific
		
		n_axis_splines	= self.yam[f_name]['n_axis_splines']	; print("n_axis_splines ", n_axis_splines)

		
		### -------------------- basic checks
		
		# test whether coordfiles exist
		#
		# coordfiles are expected to be in tsv format
		
		mds_coordfile	= self.basename + "." + dis_algo + ".mds"
		coa_coordfile	= self.basename + "." + dis_algo + ".coa"
		
		if coord=="mds":
			check_file_exists(mds_coordfile)
		
		if coord=="tsne":
			print("\n-------------------------------------------")
			print("The number of axis cannot exceed 3 for tsne")
			print("parallel coordinates are relevant for mds only")
			print("-------------------------------------------\n")
			return
		
		if coord=="coa":
			check_file_exists(coa_coordfile)
		
		if varname:
			check_file_exists(self.charfile)
			
		# checks parameters
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		
		### ------------------------ running
		
		# color selection
		
		dicol			= {}
		#
		if varname:		
			char 			= yap.charmat(charfile=self.charfile)
			v_tax			= char.get_tax(varname)
			v_col, dicol	= yap.def_colors(v_tax)
		else:
			v_col = "blue"

		# instance of class "point_cloud"
		

		os.makedirs("plots", exist_ok=True)
		#
		if coord=="mds":
			base4plot	= "plots/" + mds_coordfile + ".parallel." + str(n_axis_splines)  
			pc			= yap.point_cloud(coordfile=mds_coordfile)
			plotfile	= base4plot + "." + fmt
			print("plotfile is:  ", plotfile)
			#
			if self.questions:
				go = testif(plotfile)
			else:
				go = True			
		#
		if coord=="coa":
			base4plot	= "plots/" + coa_coordfile + ".parallel." + str(n_axis_splines)  
			pc			= yap.point_cloud(coordfile=mds_coordfile)
			plotfile	= base4plot + "." + fmt
			print("plotfile is:  ", plotfile)
			#
			if self.questions:
				go = testif(plotfile)
			else:
				go = True			
		# go ...
		
		if go:
			print("plotting parallel coordinates with", n_axis_splines, "axes")
			if save:
				imfile 	= plotfile
			else:
				imfile	= None
			pc.plot_splines(v_col, n_axis=n_axis_splines, legend=legend, dicol=dicol, title=None, x11=True, imfile=imfile, fmt=fmt)	
		else:
			return
		
	# ------------------------------------------------------------------
	
	def pc_heatmap(self):
		"""Heatmap of the coordinate file
		
		@ Key block:	pc_
		
		Parameters of the method
		------------------------
		@ axis_i		; integer	; first axis (starting at 1)
		@ axis_j		; integer	; second axis
		@ bins			; integer	; number of bins for the 2D histogram		
		
		Parameters for displaying
		-------------------------
		@ cmap			; string	; color map for coloring pixels
		@ x11			; boolean	; whether to display the plot on the screen
		@ save 			; string	; whether to save the plot as a file
		@ fmt			; string	; format of file with saved plot		
		
		Parameters for I/O
		------------------
		@ coord			; string 	; origine of coordfile (mds or tsne)
		@ dis_algo		; string 	; method with which distances have been computed
		@ itype			; string	; format of coordfile
		
		I/O suffixes
		------------
		infile	: .coord
		outfile	: .hm.<fmt>, e.g. hm.pdf
		
		Notes
		-----
		This method should be preferred for plotting coordinate files with 
			many items (like beyond several thousands)
		-> The method starts from the extraction of coordinates i and j of the coordinate file.
		-> A 2D histogram with number of bins indicated by 'bins' is built
			i.e. the number of reads projected on a same pixel
			it is log transformed
		-> This number is colored according to the colormap
		 
		
		
		af, 23.02.09 ; 23.05.04, 23.08.21
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general 
		coord 			= self.yam[general]['coord']				; print("\ncoord           ", coord)
		dis_algo		= self.yam[general]['dis_algo']			; print("dis_algo        ", dis_algo)
		x11				= self.yam[general]['x11']				; print("x11             ", x11)
		save			= self.yam[general]['save']				; print("save            ", save)
		fmt				= self.yam[general]['fmt']				; print("fmt             ", fmt)

		
		# specific
		
		axis_i 			= self.yam[f_name]['axis_i']			; print("axis_1          ", axis_i)
		axis_j 			= self.yam[f_name]['axis_j']			; print("axis_2          ", axis_j)
		bins 			= self.yam[f_name]['bins']			; print("bins            ", bins)
		cmap			= self.yam[f_name]['cmap']			; print("cmap            ", cmap)
		log				= self.yam[f_name]['log']			; print("log             ", log)

		
		### ---------------- basic checks
		
		coordfile	= self.basename + "." + dis_algo + "." + coord
		
		check_file_exists(coordfile)
		if self.questions:
			if not check_parameters(self.yam): return
		
		### --------------- running
		
		pc	= yap.point_cloud(coordfile=coordfile)
		pc.heatmatrix(i=axis_i-1, j=axis_j-1, bins=bins, log=log)
		if save:
			imfile = "plots/" + coordfile + ".hm"
		else:
			imfile = None
		pc.heatmap(cmap=cmap, x11=x11, imfile=imfile, fmt=fmt)

	# ------------------------------------------------------------------
	
	def pc_spikes(self):
		"""3D spikes of the coordinate file
		
		@ Key block:	pc_
		
		Parameters of the method
		------------------------
		@ axis_i		; integer	; first axis (starting at 1)
		@ axis_j		; integer	; second axis
		@ bins			; integer	; number of bins for the 2D histogram
		
		Parameters for displaying
		-------------------------
		@ cmap			; string	; color map for coloring pixels
		@ x11			; boolean	; whether to display the plot on the screen
		@ save 			; string	; whether to save the plot as a file
		@ fmt			; string	; format of file with saved plot		
		
		Parameters for I/O
		------------------
		@ coord			; string 	; origine of coordfile (mds or tsne)
		@ dis_algo		; string 	; method with which distances have been computed
		@ itype			; string	; format of coordfile
		
		
		I/O suffixes
		------------
		infile	: .coord
		outfile	: .spikes.<fmt>, e.g. .spikes.png
		
		
		Notes
		-----
		
		-> This method is very close to pc_heatmap
			- the same information is plotted
			- i.e. a 2D histogram of read density per pixel
		-> Instead of a heatmap, this displays a 3D plot with the 
			value of the 2D histogeam as elevation
		-> parameters are common with pc_heatmap
		
		af, 23.02.09 ; 23.05.04, 23.08.21
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general 
		dis_algo		= self.yam[general]['dis_algo']		; print("dis_algo        ", dis_algo)
		coord 			= self.yam[general]['coord']			; print("\ncoord           ", coord)
		x11				= self.yam[general]['x11']			; print("x11             ", x11)
		save			= self.yam[general]['save']			; print("save            ", save)
		fmt				= self.yam[general]['fmt']			; print("fmt             ", fmt)

		
		# specific
		
		axis_i 			= self.yam[f_name]['axis_i']			; print("axis_1          ", axis_i)
		axis_j 			= self.yam[f_name]['axis_j']			; print("axis_2          ", axis_j)
		bins 			= self.yam[f_name]['bins']			; print("bins            ", bins)

		
		### ---------------- basic checks
		
		mds_coordfile	= self.basename + "." + dis_algo + ".mds"
		
		check_file_exists(mds_coordfile)
		if self.questions:
			if not check_parameters(self.yam): return
		
		### --------------- running
		
		pc	= yap.point_cloud(coordfile=mds_coordfile)
		#pc.heatmatrix(i=axis_i-1, j=axis_j-1, bins=bins)
		if save:
			imfile = "plots/" + mds_coordfile + ".spikes"
		else:
			imfile = None
		pc.spikeshow(i=axis_i, j=axis_j, bins=bins, title=None, x11=x11, imfile=imfile, fmt="png")

	# ==================================================================
	#
	#			 building.otus()
	#
	# ==================================================================
	#
	#	Three methods are avaiable
	#		cc
	#		pc_label
	#		swarm
	
	def otu_build_islands(self):
		"""Builds OTUs as isolated island of spikes from a 2D histogram
		
		@ Key block:	pc_
		
		Parameters of the method
		------------------------
		@ level		; float		; value in the heatmap to be part of an OTU
		@ axis_i	; integer	; first axis (starting at 1)
		@ axis_j	; integer	; second axis
		@ bins		; integer	; number of bins for the 2D histogram
		
		
		Parameters for displaying
		-------------------------
		No display
		
		Parameters for I/O
		------------------
		@ coord			; string 	; origine of coordfile (mds or tsne)
		@ dis_algo		; string 	; method with which distances have been computed
		@ itype			; string	; format of coordfile
		
		
		I/O Suffixes
		------------
		infile	: .coord
		outfile	: .clus
		 
		Notes
		-----
		
		-> The OTUs are built from the 2D histogram of a heatmap.
		
		[documentation to be finalized]
		
		af, 23.08.15
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
		coord 			= self.yam[general]['coord']		; print("coord           ", coord)
		dis_algo		= self.yam[general]['dis_algo']		; print("dis_algo        ", dis_algo)
		
		# specific
		
		bins 			= self.yam[f_name]['bins']			; print("\nbins            ", bins)
		level			= self.yam[f_name]['level']			; print("level            ", level)
		heatmap			= self.yam[f_name]['heatmap']		; print("heatmap          ", heatmap)
		#
		axis_i 			= self.yam[f_name]['axis_i']			; print("axis_1          ", axis_i)
		axis_j 			= self.yam[f_name]['axis_j']			; print("axis_2          ", axis_j)
		

		
		
		### ---------------- basic checks
		
		coordfile	= self.basename + "." + dis_algo + "." + coord
		clusfile	= coordfile + ".ild.clus"
		
		print('coordfile is', coordfile)
		print('clusfile is ', clusfile)
		
		check_file_exists(coordfile)
		if self.questions:
			if not check_parameters(self.yam): return
		
		### --------------- running
		
		pc	= yap.point_cloud(coordfile=coordfile)
		pc.heatmatrix(bins=bins)
		if heatmap:
			pc.heatmap(i=axis_i-1, j=axis_j-1, cmap='viridis', range=None)
		part = pc.islands(level=level, seq_id=self.seq_id)
		part.write_clus(clusfile=clusfile)
		print("OTU partition written in", clusfile)


	# ------------------------------------------------------------------

	def otu_build_cc(self):
		"""Computes OTUs as connected components of the distance graph 
		
		@ Key block: graph_
		
		Parameters of the method
		-------------------------
		@ gap			; integer	; gap for building gap
		
		Parameters for displaying
		-------------------------
		no display
		
		Parameters for I/O
		------------------
		@ distype		; string	; type of distance file
		@ dis_algo		; string 	; method with which distances have been computed
		
		I/O suffixes
		------------
		infile	: <dis_algo>.dis or <dis_algo>.h5
		outfile	: .clus
		
		Notes
		-----
		
		How it works:
		- loads a class distances
		- translates it into a class graph according to the gap
		- builds a partition with otu being cc
		
		
		distype is 'dis' or 'h5'
		
		af, 22.03.22; 22.05.16; 22.08.27; 23.04.23 ; 23.05.04, 23.07.07
		23.08.04, 23.08.19
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
		dis_type	= self.yam[general]["dis_type"]			; print("\ndis_type     ", dis_type) 
		dis_algo	= self.yam[general]["dis_algo"]			; print("dis_algo     ", dis_algo,"\n") 
		
		# specific
		gap			= self.yam[f_name]["gap"]		; print("gap          ", gap) 
		
		if self.questions:
			if not check_parameters(self.yam): return
				
		### ------------ naming files
		#
		#	cc_disfile		distance file on which to build the graph
		#	cc_clusfile		file where to save structure <clus>
		
		cc_basename			= self.basename + "." + dis_algo
		cc_disfile			= cc_basename + "." + dis_type
		ccgr_basename		= cc_basename + ".g" + str(gap)
		cc_clusfile			= ccgr_basename + ".clus"
		cc_clusfile_seqid	= ccgr_basename + ".seqid.clus"
		comment 			= ["sample is " + self.basename, "with distance " + dis_algo, "with gap " + str(gap)]
				
		# instances a class <distances> as dis or h5
		
		if dis_type=="h5":
			if self.h5file==cc_disfile:
				dis	= self.dis
			else:
				dis	= yap.distances(h5file=cc_disfile)
		#
		if dis_type=="dis":
			dis		= yap.distances(disfile=cc_disfile)

		#### ----------------- running
		
		print("building the graph")
		gr		= dis.to_graph(gap=gap)
		print("building connected components")
		clus 	= gr.build_cc()
		print("building the partition")
		part 	= yap.partition(clus=clus)
		#
		print("saving the partition")
		part.write_clus(clusfile=cc_clusfile)
		
	# ------------------------------------------------------------------

	def otu_build_hac(self):
		"""Computes OTUs as clusters with HAC 
		
		
		Notes
		-----
		
		HAC must have been done beforhand, in order to have a first not too false
		evaluation of the number of clusters ...
		
		linkage methods are
			average, centroid, complete, median, single, ward, weighted
		
		af, 23.11.28, 24.01.17
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
		dis_type	= self.yam[general]["dis_type"]			; print("dis_type   ", dis_type) 
		dis_algo	= self.yam[[general]"dis_algo"]			; print("dis_algo   ", dis_algo) 
		
		# specific 
		
		linkage		= self.yam[f_name]["linkage"]		; print("\nlinkage    ", linkage) 
		height 		= self.yam[f_name]["height"]		; print("height     ", height) 
		n_clus 		= self.yam[f_name]["n_clus"]		; print("n_clus     " , n_clus) 
		

		
	
		if self.questions:
			if not check_parameters(self.yam): return
		
				
		### ------------ naming files
		#
		
		hac_basename		= self.basename + "." + dis_algo
		hac_clusfile		= hac_basename + "." + linkage + ".h" + str(height) + ".clus"
		hac_linkfile		= hac_basename + "." + linkage + ".link"
		
		comment 			= ["sample is " + self.basename, "with distance " + dis_algo]
		
		### ------------ basic checks
		
		check_file_exists(hac_linkfile)
		
				#### ----------------- running
		
		print("loading file", hac_linkfile, "as linkage matrix Z")
		tr	= yap.tree(hac_linkfile, meth=linkage, linkfile=hac_linkfile)
		
		# building partition
		
		if n_clus*height > 0:
			print("\nplease select between clustering by height or by number")
		else:
			if n_clus > 0:
				print("building a partition with", n_clus, "classes")
				part	= tr.get_clusters_bynumber(n_clus)
			#
			if height > 0:
				print("building a partition at height", height)
				part 	= tr.get_clusters_byheight(height)
			#
			part.write_clus(clusfile=hac_clusfile)
		
	# ------------------------------------------------------------------

	def dis_hac(self):
		"""Computes HAC on a distnce array
		
		
		Notes
		-----
		
		linkage methods are
			average, centroid, complete, median, single, ward, weighted
		
		af, 23.11.28, 24.01.17
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
		dis_type	= self.yam[general]["type"]				; print("dis_type        ", dis_type) 
		dis_algo	= self.yam[general]["algo"]				; print("dis_algo        ", dis_algo) 
		
		# specific
		linkage		= self.yam[f_name]["linkage"]		; print("linkage         ", linkage) 
		plot_Z		= self.yam[f_name]["plot_Z"]		; print("plot_Z          ", plot_Z) 
		log_heights	= self.yam[f_name]["log_heights"]	; print("log_heigths     ", log_heights) 		
		

		
	
		if self.questions:
			if not check_parameters(self.yam): return
		
				
		### ------------ naming files
		#
		
		hac_basename		= self.basename + "." + dis_algo
		hac_disfile			= hac_basename + "." + dis_type
		hac_linkfile		= hac_basename + "." + linkage + ".link"
		
		comment 			= ["sample is " + self.basename, "with distance " + dis_algo]
		
		### ------------ basic checks
		
		check_file_exists(hac_disfile)

		### ------------- loading an instance of class distances
		
		if dis_type=="dis":
			dis		= yap.distances(disfile=hac_disfile)
		if dis_type=="h5":
			dis	= self.dis
		
		#### ----------------- clustering
		
		tr		= dis.to_tree(meth=linkage, zfile=hac_linkfile)
		
		# drawing linkage matrix
		
		if plot_Z:
			tr.Z_heights(Log=log_heights)
			
	# ------------------------------------------------------------------

	def hac_dendrogram(self):
		"""Plots the dendrogram of a HAC
		
		
		Notes
		-----
		
		linkage methods are
			average, centroid, complete, median, single, ward, weighted
		
		af, 23.11.28, 24.01.17
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
		varname 	= self.yam[general]["varname"]			; print("varname         ", varname) 
		dis_type	= self.yam[general]["dis_type"]				; print("dis_type        ", dis_type) 
		dis_algo	= self.yam[general]["dis_algo"]				; print("dis_algo        ", dis_algo) 
		
		# specific
		linkage		= self.yam[f_name]["linkage"]		; print("linkage         ", linkage) 
		dendrogram	= self.yam[f_name]["dendrogram"]	; print("dendrogram      ", dendrogram) 
		truncate 	= self.yam[f_name]["truncate"]		; print("truncate        ", truncate) 

		
	
		if self.questions:
			if not check_parameters(self.yam): return
		
				
		### ------------ naming files
		#
		
		hac_basename		= self.basename + "." + dis_algo
		hac_disfile			= hac_basename + "." + dis_type
		hac_linkfile		= hac_basename + "." + linkage + ".link"
		
		### ------------ basic checks
		
		check_file_exists(hac_disfile)
		#
		if varname:
			check_file_exists(self.charfile)
				
		### ------------- loading the linkage matrix
		

		
		if varname:	
			char 		= yap.charmat(charfile=self.charfile)
			v_tax		= char.get_tax(varname)
		else:
			v_tax	= None
		
		#### ----------------- running
		
		
		tr	= yap.tree(hac_linkfile, meth=linkage, linkfile=hac_linkfile)
		
		if dendrogram:
			tr.draw(labels=v_tax, truncate=-1)
		#
		if truncate>0:
			tr.draw(truncate=truncate)
		
	# ------------------------------------------------------------------
	
	def otu_build_swarm(self):
		"""Builds OTUs with swarm
		
		Keys
		----
		
		@ d 	integer		d parameter of Swarm
		
		af & jmf, 23.08.18, 23.11.16
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# specific
		
		d			= self.yam[f_name]['d']		; print("d            ", d)
		#
		
		### ------------- basic checks
		
		check_file_exists(self.fasfile)
			
		print("\nBuilding OTUs with SWARM\n")
		if self.questions:
			if not check_parameters(self.yam): return
		
		### ------------- naming files
		
		fasfile		= self.basename + '_noN.fas'
		if os.path.exists(fasfile):
			pass
		else:
			print(fasfile, "does not exists")
			print("replaced by", self.basename + ".fas")
			fasfile	= self.basename + ".fas"
		#
		clusfile 	= self.basename + ".swarm.clus"
		print("\nclusfile will be ", clusfile)
		
		### --------------- running
		
		fas			= yap.fasta(fasfile=fasfile)
		clus 		= fas.swarm(d=d)
		part 		= yap.partition(clus=clus)
		part.write_clus(clusfile=clusfile)
		#print("partition written in file", clusfile)
			
		

		
	# ------------------------------------------------------------------
	
	def old_otu_2charfile(self):
		"""Adds OTUs as new character in charfile - to be updated
		
		
		!!!!! new_varname has to be transferred to parameters !!!!
		
		Notes
		-----
		
		- builds vec_otu
			vec_otu		list of strings		vec_otu[i]: otu of sequence <i>
		af, 23.08.04
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
<<<<<<<< HEAD:yap_lib_beforediving.py
		dis_type	= self.yam["dis_type"] 	; print("\ndistype      ", dis_type)
		gap			= self.yam["otu_gap"]		; print("gap          ", gap) 
		dis_algo	= self.yam["dis_algo"]	; print("dis_algo     ", dis_algo,"\n") 
		#
		meth		= self.yam['otu_meth']			; print("method       ", meth)
		new_varname	= self.yam['otu_new_varname']	; print("new_varname  ", meth)
========
		# general
		
		distype		= self.yam[general]["distype"] 	; print("\ndistype      ", distype)
		dis_algo	= self.yam[general]["dis_algo"]	; print("dis_algo     ", dis_algo,"\n") 
		
		# specific
		gap			= self.yam[f_name]["gap"]		; print("gap          ", gap) 
		meth		= self.yam[f_name]['meth']		; print("method       ", meth, "\n")
>>>>>>>> modules:yap_lib/yap_lib_2keys.py
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		### --------- builds vec_otu (see notes)
		
		ccgr_basename		= self.basename + "." + dis_algo + ".g" + str(gap)
		clusfile_seqid		= ccgr_basename + ".seqid.clus"
		
		vec_otu				= ['']*self.n_seq
		with open(clusfile_seqid, "r") as f_clus:
			for cc, line in enumerate(f_clus):
				cc_str 	= line[:-1]
				cc_pl	= cc_str.split('\t')
				for read in cc_pl:
					i			= self.seq_id.index(read)
					vec_otu[i]	= "otu_" + str(cc) 
		
		### ----------- adds vect_otu as new last column in charfile
		
		flag		= os.path.isfile(self.charfile)
		if flag==True:
			char 	= yap.charmat(charfile=self.charfile)
			char.add_varname(varname=new_varname, new_col=vec_otu)
		else:
			return("charfile", self.charfile, "does not exist!!!")
			
	# ------------------------------------------------------------------
	
	def otu_2charfile(self):
		"""Adds OTUs as new character in charfile - to be updated
		
		
		!!!!! new_varname has to be transferred to parameters !!!!
		
		Notes
		-----
		
		- builds vec_otu
			vec_otu		list of strings		vec_otu[i]: otu of sequence <i>
		af, 23.08.04
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		dis_type	= self.yam["dis_type"] 	; print("\ndistype      ", dis_type)
		gap			= self.yam["otu_gap"]		; print("gap          ", gap) 
		dis_algo	= self.yam["dis_algo"]	; print("dis_algo     ", dis_algo,"\n") 
		#
		meth		= self.yam['otu_meth']			; print("method       ", meth)
		new_varname	= self.yam['otu_new_varname']	; print("new_varname  ", meth)
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		### --------- builds vec_otu (see notes)
		
		ccgr_basename	= self.basename + "." + dis_algo + ".g" + str(gap)
		clusfile		= ccgr_basename + ".clus"
		
		vec_otu				= ['']*self.n_seq
		with open(clusfile, "r") as f_clus:
			for cc, line in enumerate(f_clus):
				print(cc)
				cc_str 	= line[:-1]
				cc_pl	= cc_str.split('\t')
				cc_int	= [int(item) for item in cc_pl]
				print(cc_pl)
				print(cc_int)
				for i in cc_int:
					vec_otu[i]	= "otu_" + str(cc) 
		
		### ----------- adds vect_otu as new last column in charfile
		
		flag		= os.path.isfile(self.charfile)
		if flag==True:
			char 	= yap.charmat(charfile=self.charfile)
			char.add_varname(varname=new_varname, new_col=vec_otu)
		else:
			return("charfile", self.charfile, "does not exist!!!")
			
		
	# ------------------------------------------------------------------
	
	def otu_composition(self):
		"""Displays the composition of each OTU
		
		Keys
		----
		
		meth		; string	; method with which OTUs have been built
		gap			; integer	; gap for building OTUs if method is cc
		dis_algo	; string	; method with which distances have been computed
		varname		; string	; character selected for composition
		# write otufile
		min_size	; integer	; minimum size for computing the composition of an OTU
		comp2screen	; boolean	; whether to display composition on screen
		comp2file	; boolean	; whether to pront composition in a file
		
		af, 23.07.07
		"""
		print("\nparameters otu_composition")
		print("--------------------------")
		# 
		meth		= self.yam["otu_meth"]			; print("\nmeth         ", meth) 
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general  
		otu_meth		= self.yam[general]["otu_meth"]			; print("\notu_meth         ", otu_meth) 
		varname			= self.yam[general]["varname"]		; print("\nvarname      ", varname, "\n")
		x11				= self.yam[general]"x11"]			; print("x11          ", x11)
		save			= self.yam[general]["save"]			; print("save         ", save, "\n")
		#
		if otu_meth in ["cc","hac","islands"]:
			coord		= self.yam[general]["coord"]		; print("\ncoord        ", coord) 
			dis_algo	= self.yam[general]["dis_algo"]		; print("dis_algo     ", dis_algo) 
		
		# specific
		
		if otu_meth=="cc":
			gap			= self.yam[f_name]["gap"]			; print("\ngap          ", gap) 
		if otu_meth=="hac":
			linkage		= self.yam[f_name]["linkage"]			; print("linkage      ", linkage)
			height		= self.yam[f_name]["height"]			; print("height       ", height)
		
		min_size	= self.yam[f_name]["min_size"]		; print("min_size     ", min_size)

		
		# checking parameters
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		if meth=="dbscan":
			print("\nmethod is dbscan => cc_0 is the noide\n")
		# extends the basename to keep track of the algo for distances
		# and gap 
		
		if meth=="cc":
			cls_basename	= self.basename + "." + dis_algo + ".g" + str(gap)
		if meth=="hac":
			cls_basename	= self.basename + "." + dis_algo + "." + linkage + ".h" + str(height)
		if meth=="islands":
			cls_basename	= self.basename + "." + dis_algo + "." + coord + ".ild"
		if meth=="swarm":
			cls_basename	= self.basename + ".swarm"
		if meth=="dbscan":
			cls_basename	= self.basename + "." + dis_algo + ".db"
		#
		clusfile			= cls_basename  + ".clus"
		print("method for OTU is", meth, "and clusfile is", clusfile)
		#
		if meth=="cc":
			comment	= ["# basename is " + self.basename, "# method for otus is " + meth, "# method for distances is " + dis_algo, "# gap is " + str(gap)]
		if meth=="pc_label":
			comment	= ["# basename is " + self.basename, "# method for otus is " + meth, "# method for distances is " + dis_algo, "# dimension reduction is " + coord]
		else:
			comment=[]		
		
		# loads character file and selects v_tax: the column of header given by <varname>

		char 			= yap.charmat(charfile=self.charfile)
		v_tax 			= char.get_tax(varname)
		#v_col, dicol	= yap.def_colors(v_tax)
		
		# loads seq_ids
		
		seq_id			= char.get_seq_id()		
		
		# initialize an instance of class partition 
		
		part 		= yap.partition(clusfile=clusfile, basename=self.basename, seq_id=seq_id)
		
			
		# gives the composition of each cc according to v_tax (see above for v_tax)
		# 	- on screen if comp2screen is True, 
		#	- as a file <cls_basename>.ocf if comp2file is True
		
		if x11:
			part.composition(var=v_tax, min_size=min_size)
		if save:
			ocffile 	= cls_basename + ".ocf"
			#				 				
			print_col("writing composition in file", ocffile, color="cyan")
			part.composition(var=v_tax, min_size=min_size, x11=False, ocffile=ocffile, comment=comment)
		
		
	# -------------------------------------------------------
	
	def otu_rank_size(self):
		"""Rank-size plot of partition in OTUs
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general 
		otu_meth		= self.yam[general]["otu_meth"] 				; print("\nmeth         ", meth)
		dis_algo		= self.yam[general]["dis_algo"]				; print("dis_algo     ", dis_algo) 
		coord			= self.yam[general]["coord"]					; print("coord        ", coord) 				
		
		# specific
		
		log				= self.yam[f_name]["log"] 			; print("log          ", log)
		dots			= self.yam[f_name]["dots"] 			; print("dots         ", dots)
		upto			= self.yam[f_name]["upto"] 			; print("upto         ", upto)
		min_size		= self.yam[f_name]["min_size"] 		; print("min_size     ", min_size)
		gap				= self.yam[f_name]["gap"]			; print("gap          ", gap) 		
		#
		
		if self.questions:
			if not check_parameters(self.yam): return
			
		
		if otu_meth=="cc":
			cls_basename	= self.basename + "." + dis_algo + ".g" + str(gap)
		if otu_meth=="islands":
			cls_basename	= self.basename + "." + dis_algo + "." + coord + ".ild"
		if otu_meth=="swarm":
			cls_basename	= self.basename + ".swarm"
		if otu_meth=="dbscan":
			cls_basename	= self.basename + "." + dis_algo + ".db"
		if otu_meth=="hac":
			cls_basename	= self.basename + "." + dis_algo + "." + hac_meth + ".h" + str()
		#
		clusfile			= cls_basename  + ".clus"
		print("method for OTU is", meth, "and clusfile is", clusfile)
		
		part 	= yap.partition(clusfile=clusfile)
		part.plot_rank_size(log=log, dots=dots, upto=upto)
	
	# ------------------------------------------------------------------
	
	def otu_ami(self):
		"""Computes the Adjusted Mutual Information score - to be completed
		
		
		Notes
		-----
		The score is computed between the partition as OTUs and as
		taxonomic names.
		
		af, 23.11.27
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# which method for OTUs 
		
		otu_meth			= self.yam[general]["otu_meth"] 			; print("\notu_meth         ", otu_meth)
		
		if otu_meth=="hac":
			linkage			= self.yam[f_name]["linkage"]		; print("linkage     ", linkage) 
		
		if otu_meth=="cc":
			gap				= self.yam[f_name]["gap"]			; print("gap          ", gap) 		
				
		if otu_meth in ["cc","hac","islands"]:
			dis_algo		= self.yam[general]["dis_algo"]		; print("dis_algo     ", dis_algo) 
			coord			= self.yam[general]["coord"]			; print("coord        ", coord) 
		
		# taxonomic names
		varname			= self.yam[general]["varname"]		; print("varname      ", varname) 
		
		### -------------------- checking parameters
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		### -------------------- building clusfile to be read
		
		if otu_meth=="cc":
			cls_basename	= self.basename + "." + dis_algo + ".g" + str(gap)
		if otu_meth=="islands":
			cls_basename	= self.basename + "." + dis_algo + "." + coord + ".ild"
		if otu_meth=="swarm":
			cls_basename	= self.basename + ".swarm"
		if otu_meth=="dbscan":
			cls_basename	= self.basename + "." + dis_algo + ".db"
		if otu_meth=="hac":
			cls_basename	= self.basename + "." + dis_algo + "." + linkage
		#
		clusfile			= cls_basename  + ".clus"
		
		### ------------------- building taxonomic names
		
		char 		= yap.charmat(charfile=self.charfile)
		v_tax		= char.get_tax(varname)
		
		### ------------------- computation of the AMI
		
		part		= yap.partition(clusfile=clusfile)
		res			= part.ami(v_tax)
		print("\nmethod for OTU is", meth, "and clusfile is", clusfile)
		print("there are", len(v_tax), "items")
		print("AMI is", res)

	# ------------------------------------------------------------------
	
	def otu_splitting(self):
		"""Splits fasta, character and distance files according to OTUs
		
		af, 23.07.07
		"""
		
		# displays parameters
		
		print("\n-> parameters for otu splitting")
		print("------------------------")
		
			
		# for I/O (finding linkfile)
		
		otu_meth	= self.yam['otu_meth']				; print("\notu_meth     ", otu_meth, "\n")
		linkage		= self.yam["hac_linkage"] 			; print("linkage      ", linkage)
		height		= self.yam["hac_height"] 			; print("height       ", height)
		dis_type	= self.yam["dis_type"] 				; print("dis_tye      ", dis_type)
		gap			= self.yam["otu_split_gap"]			; print("gap          ", gap) 
		dis_algo	= self.yam["dis_algo"]				; print("dis_algo     ", dis_algo) 
		
		# 
		
		rank_size	= self.yam['otu_split_rank_size']	; print("rank_size    ", rank_size)
		min_size	= self.yam["otu_split_min_size"]	; print("min_size     ", min_size)
		
		#
		
		split_fas	= self.yam["otu_split_fas"]			; print("split_fas    ", split_fas)
		split_char	= self.yam["otu_split_char"]		; print("split_char   ", split_char)
		split_dis	= self.yam["otu_split_dis"]			; print("split_dis    ", split_dis, "\n")
				
		
		# checking parameters
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		# extends the basename to keep track of the algo for distances
		# and gap 
		
		if otu_meth=="cc": 
			cls_basename	= self.basename + "." + dis_algo + ".g" + str(gap)
		elif otu_meth=="swarm":
			cls_basename	= self.basename + ".swarm"
		elif otu_meth=="hac":
			cls_basename	= self.basename + '.' + dis_algo + '.' + linkage + ".h" + str(height) 
		#
		clusfile			= cls_basename  + ".clus"
		print("clusfile is", clusfile)
		#
		disfile = self.basename + "." + dis_algo + '.' + dis_type

	# ==================================================================
	#
	#			 building.subproject()
	#
	# ==================================================================
	#
	
	def xtract_subproject(self):
		"""Extracts some files to build a new project
		
		Notes
		-----
		
		- extraction can be by default according to some value of the character file
				(varname, value of varname)
				
		- rows to be extracted are in list <which>
		
		- for distances, the extracted submatrix is in format <dis_type = dis>
		"""
		
		# where it is ...
		
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		print_through_which(f_name)
		
		# parameters
		print_parameters(f_name)
		
		# general
		
		dis_algo			= self.yam[general]['dis_algo']			; print("\ndis_algo          ", dis_algo)
		dis_type			= self.yam[general]['dis_type']			; print("dis_type          ", dis_type)
		varname				= self.yam[general]['varname']			; print("varname           ", varname)
		value				= self.yam[general]['sub_value']			; print("value             ", value)
		#
		sub_basename		= self.yam[f_name]['basename']		; print("sub_basename      ", sub_basename)
		sub_char			= self.yam[f_name]['char']			; print("sub_char          ", sub_char)
		sub_dis				= self.yam[f_name]['dis']			; print("sub_dis           ", sub_char)
		
		if self.questions:
			if not check_parameters(self.yam): return
		
		
		os.makedirs(sub_basename, exist_ok=True)
		
		### -------------- copies yaml file and renames it
		
		cmd	= "cp " + self.configfile + " " + sub_basename
		os.system(cmd)
		cmd	= "mv " + sub_basename + "/" + self.configfile + " " + sub_basename + "/" + sub_basename + ".yaml"
		os.system(cmd)
		
		print_col("yaml file copied in subdirectory <" + sub_basename + "> and renamed", color="cyan")
		
		### ---------------  extracting character file
		
		if sub_char:
			char	= yap.charmat(charfile=self.charfile)
			which	= char.xtract_by_name(varname=sub_varname, value=value, outfile=sub_basename + "/" + sub_basename + '.char')
			print_col("character file of subproject <" + sub_basename + " is extracted in directory <" + sub_basename + ">", color="cyan")
			
		### ---------------- extracting fasta file (compulsory)
		
		fasfile			= self.fasfile
		sub_fastafile	= sub_basename + "/" + sub_basename + '.fas'
		fas				= yap.fasta(fasfile=fasfile)
		fas.write_subfile(which=which, subfile=sub_fastafile)
		print_col("fasta file of subproject <" + sub_basename + " is extracted in directory <" + sub_basename + ">", color="cyan")
		
		### -----------------  extracting distance file (recommended)
		
		if sub_dis:
			if dis_type=="dis":
				disfile		= self.basename + "." + dis_algo + ".dis" 
				dis 	 	= yap.distances(disfile = disfile)
			#
			if dis_type=="h5":
				h5file		= self.basename + "." + dis_algo + ".h5" 
				dis 	 	= yap.distances(h5file = h5file)
			#
			subdisfile	= sub_basename + "/" + sub_basename + '.' + dis_algo + '.dis' 
			dis.xtract_submatrix(which=which, subdisfile=subdisfile, sep="\t")
			print_col("distance file of subproject <" + sub_basename + "> is extracted as <" + subdisfile + ">", color="cyan")
		

