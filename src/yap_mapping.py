#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Maps fasta files on reference databases

@ which bases format	: either R-Syst like or blast
@ with which tool		: either distance based or with blast

Criteria for quality	: 	with blast		pident
							with distance	gap
				

af, 24.02.08, 24.02.26

"""

import os
import time
from print_color import print as print_col
#
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle	# for legend in plots
from operator import itemgetter
#
import yap_classes as yap
#
from yapotu_lib import *


"""
Mapping can be done
	- with distances or with blast
	- on an OTU or a sample

The four combinations are available, each with a dedicated class

=================== =============================================
			||		otu				|		sample 				|
================================================================|
distances	||  mapping_otu			|  mapping_sample			|
			||						|							|
blast		||  mapping_blast_otu	|  mapping_blast_sample		|
=================================================================


heritage of classes is as follows

                                  ----- mapping_dis_otu 
                                /
             ------ mapping_dis 
           /                    \
          /                      ----- mapping_dis_sample
         /
mapping - 
         \
          \                       ----- mapping_blast_otu
           \                     /
             ------ mapping_blast     
                                 \
                     -            ----- mapping_blast_sample 
                    
                     
af, 24.02.14, 24.02.26
"""


# ======================================================================
#
#			dic_annot()
#
# ======================================================================

class dic_annot():
	"""
	"""
	def __init__(self, dic):
		"""
		"""
		self.dic	= dic
		
	
	# ------------------------------------------------------------------
	
	def add2char(self, header, charfile):
		"""Adds an inventory as a character in character file
		
		arguments
		---------
		@ dic_annot		dictionary		see parse_sample_besthit
		@ header		string			header of the new character
		@ charfile		string			character file where to add the charname
		
		Notes
		-----
		The methods first tests whether the character already exists or not.
		If it does not exist, it adds ot
		If it exists, it asks whether it should duplicated, or not
		
		af, 24.02.29
		"""
		#
		print(self.dic)
		print_col('\n[yap_mapping]:[dic_annot]:[add2char()]', color='magenta')
		print("\nnew header is       :", header)
		print("charfile is         :", charfile)
		go			= False
		char 		= yap.charmat(charfile=charfile)
		headers		= char.cols.tolist()
		print_col("current headers are :", (" ; ").join(headers), color="cyan")
		#
		if header in headers:
			print_col(header, "already in headers - do you want to rewrite it?", color="red")
			ch = input()
			if ch in ["y", "yes"]:
				go	= True
				print_col("character will be rewritten", color="magenta")
			else:
				go	= False
				print_col(header, "already in headers - new character not added", color="magenta")
				return
		else:
			go	= True
		#
		if go:
			seq_ids		= char.index.tolist()	# first column of character file
			n_queries	= len(seq_ids) 
			new_col		= ['']*n_queries
			for i, seq_id in enumerate(seq_ids):
				if seq_id in self.dic:
					new_col[i]	= self.dic[seq_id]
				else:
					new_col[i]	= "no_hit"
			#
			char.add_varname(header, new_col)
			print_col("\nnew character", header, "added to character file", charfile, color="green")
			
	# ------------------------------------------------------------------
	
	def inventory(self, x11, invfile):
		"""
		af, 24.03.15
		"""
		v_tax	= list(self.dic.values())
		res		= yap.count_and_order(v_tax)
		n_item	= len(res)
		#
		if x11:
			print("\ntaxon " + "\t" + " counts")
			print("-------------")
			for i in range(n_item):
				tup		= res[i]
				taxon	= tup[0]
				nb		= tup[1]
				print(taxon, "->", nb)
		#
		if invfile:
			with open(invfile,"w") as f_inv:
				item 	= "taxon" + "\t" + "counts"
				f_inv.write(item + "\n")
				for i in range(n_item):
					tup		= res[i]
					taxon	= tup[0]
					nb		= str(tup[1])
					item	= taxon + "\t" + nb
					f_inv.write(item + "\n")
			print("inventory written in file", invfile)

# ======================================================================
#
#			mapping()
#
# ======================================================================


class mapping():
	"""
	"""
	def __init__(self, reference, charname, verbose=True):
		"""
		Arguments
		---------
		@ reference:	string		basename of the files of the reference db
		@ charname:		string		character in the reference db
		
		
		Attributes
		----------
		@ refid				list of strings		references id
		@ dic_reftax		a dictionary		keys	: references id
												value	: taxon of the reference id
												(according to charname)
		@ set_taxa			list of strings		list of taxa (charname) in reference
							
		af, 24.02.15, 24.04.04 
		"""
		
		print_col('[yap_mapping]:[mapping]:[init()]', color='magenta')
		
		# building the attributes
		ref_charfile	= reference + ".char"
		print_col("reference charfile is", ref_charfile, color="cyan")
		char 			= yap.charmat(charfile=ref_charfile, verbose=verbose)
		v_tax			= char.get_tax(charname)
		set_taxa		= list(set(v_tax))
		refid 			= char.index.tolist()
		#
		dic_reftax		= {}
		for i, item in enumerate(refid):
			dic_reftax[item]	= v_tax[i]
		#
		# setting the attributes
		self.refid		= refid			# reference seqids
		self.v_tax		= v_tax			# character values per refid
		self.set_taxa	= set_taxa		# set of character values
		self.dic_reftax	= dic_reftax	# dictionary refid -> taxon 
		
	
	# ------------------------------------------------------------------
	
	def annot(self, v_dis, v_refid, gap):
		"""annotates an (unspecified) query sequence from its profiles of distances with references
		
		to be upgraded towards annotation of a neighborhood
		a neighborhood is a list of reference with pident above a given value
												   distance below a given gap
			
		
		Arguments
		---------
		@ v_dis		list of floats		distances between the query and the references
		@ v_refid	list of strings		references on which mapping is done
		@ gap		integer				gap for selecting neighbors
		
		returns
		-------
		@ annot		string				the annotation of the query for the prescribed gap
							
		af, 24.02.15
		"""
		print_col('[yap_mapping]:[mapping]:[annot()]', color='magenta')
		#
		# select neighbors
		#
		which 	= [w for w, val in enumerate(v_dis) if val <= gap]
		m_refid	= [v_refid[w] for w in which]
		#
		# annotates neigbors
		#
		m_taxa	= [self.dic_reftax[item] for item in m_refid]
		#
		# annotates the query
		#
		if len(m_taxa)==0:
			annot 	= "no_hit"
		else:
			if len(m_taxa)==1:
				annot	= m_taxa[0]
			else:
				set_taxa 	= list(set(m_taxa))
				if len(set_taxa) == 1:
					annot 	= set_taxa[0]
				else:
					annot	= "ambiguous"
		#
		return annot
		
	# ------------------------------------------------------------------
	
	def annot_loop(self, v_dis, v_refid, gap_max):
		"""loop of annotation of a query along a list of gaps
		
		Arguments
		---------
		@ v_dis, v_refid	: as for get_annot()
		@ gap_max	integer		maximal gap considered
		
		@returns
		--------
		@ v_annot	list of strings		list of annotation for each gap from 0 to gap_max
		
		af, 24.02.16
		"""
		print_col('[yap_mapping]:[mapping]:[annot_loop()]', color='magenta')
		v_annot	= ['']*gap_max
		for gap in range(gap_max):
			annot			= self.get_annot(v_dis, v_refid, gap)
			v_annot[gap]	= annot
		#
		return v_annot
		
	

			

		
	
# ======================================================================
#
#			mapping_otu()
#
# ======================================================================

class mapping_withdis(mapping):
	"""
	=============================
	Mapping an OTU with distances
	=============================
	
	The sequence is as follows:
	
	1 -	map_dissec()		: computing the reference x query distance file (with disseq of diskm)
	2 -	annotate_queries()	: annotates all queries for all gaps below gap_max (-> Tab, set_taxa)
	3 -	plot_inventories()	: plots the inventory over all gap less than gap_max
	 
	mapping_disfile --> (Tab, set_taxa) --> plot
	
	af, 24.02.26
	"""
	def __init__(self, reference, charname):
		"""
		see __init__() of class mapping
		af, 24.02.15
		"""
		mapping.__init__(self, reference, charname)
		

	# ------------------------------------------------------------------

	def map_dissec(self, query_fasfile, ref_fasfile, mapping_disfile, algo="sw", dis_type="dis"):
		"""computes a full distance matrix references x queries
		
		Arguments
		---------
		@ query_fasfile		string		name of the query fasta file
		@ ref_fasfile		string		name of the reference fasta file
		@ mapping_disfile	string		name of the file with distances
		@ algo, dis_type				as in disseq
		
		returns
		-------
		no output
		writes the file
		
		Notes
		-----
		It is just a call to disseq with query and reference
		warning: for the matrix to have more rows than coumns, it is necessary to swap references and queries
		(-ref queryfile -query reffile)
		
		af, 24.02.16
		"""
		print_col("\ncomputing query x ref distances with dissec ...", color='green')
		cmd		= "disseq -ref " +  query_fasfile + " -query " + ref_fasfile  + " -out " + mapping_disfile + ' &'
		print("command line is", cmd)
		os.system(cmd)
		print_col("distances written in file", mapping_disfile, color='green')
		
	# ------------------------------------------------------------------
	
	def annotates_queries(self, queries, mapping_disfile, gap_max):
		"""Annotates all queries for all gaps from reference x queries distance file
		
		arguments
		---------
		@ queries			list of strings		queries tseq_id in a fasta file)
		@ mapping_disfile	string				references x queries distance file
		@ gap_max			integer				maximum gap to be tested
		
		@ returns
		---------
		set_taxa		list of strings		list of taxa 
		Tab				numpy array			annotation per query and per gap
		
		Notes
		-----
		Tab			row		a query
					column	a gap
					value	a taxon (including ambiguous or no_hit)
				
		set_taxa	list of taxa met at least once with one query sequence
		
		The reference x queries distance file must have been computed beforehand
		
		This method is useful for small sets of queries and not too large databases, like 
		mapping an OTU on a R-Syst reference database. Other wise, the calculation
		of the reference x queries distance file is too costly. 
		
		af, 24.02.16, 24.02.26
		"""
		# loads ref x queries distance file
		dis			= yap.distances(disfile=mapping_disfile, zero_diagonal=False, test_symmetry=False, set_diag_to_zero=False)		
		Dis			= dis.Dis
		# annotates all queries for a gap
		n_queries	= Dis.shape[1] 
		v_annot		= ['']*n_queries 
		#
		Tab			= np.array(['']*(n_queries*gap_max), dtype=object)
		Tab.shape	= (n_queries, gap_max)
		#
		for q, query in enumerate(queries):
			v_dis		= Dis[:,q]
			Tab[q,:]	= self.get_annot_loop(v_dis, self.refid, gap_max)
		#
		tab			= Tab.ravel()
		set_taxa	= list(set(tab)) 
		return set_taxa, Tab
	
	# ------------------------------------------------------------------
	
	def plot_inventories(self, set_taxa, Tab, x11, imfile, fmt):
		"""plots the inventory over all gap less than gap_max
		
		arguments
		---------
		@ set_taxa		list of strings		set of taxa met at least once for one gap
		@ Tab			numpy array			annotatiopn of a query for a given gap
		@ x11			boolean				whether to display inventoiry on screen
		@ imfile		string				name of file where to save the plot
		@ fmt			string				format of the saved plot
		
		Notes
		-----
		@set_taxa and @Tab come from "annotates_queries"
		
		It produces a plot with
		- gaps from 0 to gap_max on x axis
		- one curve per taxon in set_taxa
		- the number of queries annotated as the taxon for the gap in x on y axis
		
		A table with gap in rows and taxa in columns and number of hits
		can be displayed on the screen if x11 is True.
		
		"""
		n_taxa		= len(set_taxa)
		gap_max		= Tab.shape[1]
		Tab_count	= np.zeros((gap_max, n_taxa), dtype=int)
		#
		for g in range(gap_max):
			print("gap =", g)
			pl	= Tab[:,g].tolist()
			res	= count_and_order(pl)
			print(res)
			for tup in res:
				taxon	= tup[0]
				j		= set_taxa.index(taxon)
				Tab_count[g,j]	= int(tup[1])
		print(Tab_count)
		#
		v_col	= ['blue','green','red','cyan','magenta','orange']
		dicol	= {}
		for i, item in enumerate(set_taxa):
			dicol[item]	= v_col[i] 
		#
		p, keys = yap.get_legend(dicol)
		plt.legend(p, keys, loc='upper center', bbox_to_anchor=(0, 1.02, 1, 0.1),
		ncol=3, fontsize=8, fancybox=True, shadow=True)
		#
		for j in range(n_taxa):
			col		= dicol[set_taxa[j]]
			plt.plot(Tab_count[:,j], c=col)
		plt.xlabel("gap")
		plt.ylabel("counts of hits per taxon")
		#
		if imfile:
			plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
			print("plot saved in file", imfile)			
		if x11:
			plt.show()
		#
# ======================================================================
#
#			mapping_sample()
#
# ======================================================================


class mapping_dis_sample(mapping):
	"""
	"""
	def __init__(self, reference, charname):
		"""
		self.set_taxa		list of strings		taxa present in the sample
		self.refid			list of strings		seq_id of the references
		self.dic_reftax		dictionary			taxa of the references
													keys	: refid
													value	: which taxon
		
		af, 24.02.15
		"""
		mapping.__init__(self, reference, charname)
		
	# ------------------------------------------------------------------
	
	def load_sparse_disfile(self, sparse_disfile):
		"""loads three coluns distance file
		Distance file is in a sparse format
		Dis has three columns, extracted here
		   ______________________________________________
		   column      | variable  | what it is
		   _______________________________________________ 
		   - first     | queries   | query id
		   - second    | ref       | reference id
		   - third     | dis       | distance between both
		"""
		print_col("\n[yap_mapping][mapping_dis_sample][load_sparse_disfile()]", color="magenta")
		print("\nloading distance file; this may take a while ...")
		t_deb 	= time.time()
		Dis		= np.loadtxt(sparse_disfile, delimiter="\t", dtype=object).astype(str)
		t_fin	= time.time()
		print("loading took:", t_fin - t_deb, "sec.")
		queries	= Dis[:,0].tolist()                          # query id's
		ref		= Dis[:,1].tolist()                          # references which match per query
		dis		= np.array(Dis[:,2],dtype=float).tolist()    # distance of match
		print("distance file loaded ; ", len(dis), "matched pairs\n")
		#
		return	queries, ref, dis
	
	# ------------------------------------------------------------------
	
	
	def check_refids(self, ref, refid):
		"""
		"""
		print_col("[yap_mapping]:[mapping_dis_sample]:[check_refids()]", color="magenta")
		set_ref				= list(set(ref))
		which_3colnotindb	= [rid for rid in set_ref if rid not in refid]
		if len(which_3colnotindb) > 0:
			print_col("ref reads in sparse disfile and not in db", color="red")
			for item in which_3colnotindb:
				print_col(item, color="red")
		else:
			print_col("all references in sparse disfile are in db too", color="cyan")
		
	# ------------------------------------------------------------------
	
	def diagno_syst(self, gap_max, queries, ref, dis, v_refid, v_tax):
		"""
		
		af, 24.03.14							
		"""
		print_col("\n[yap_mapping]:[mapping_dis_sample]:[diagno_syst()]", color="magenta")
		dic_annot		= {}
		set_queries		= list(set(queries))
		n_queries		= len(set_queries)
		
		# slices the rows of sample distance file by queries
		n			= len(queries)
		i			= 0
		j			= 0
		q_num		= 0
		while j < n:
			query 	= queries[i]		# which query
			while queries[j]==query:
				if j == n-1:
					break
				else:
					j	= j+1
			i_deb	= i					# first row with this query
			i_fin	= j					# last  row with this query 
			i		= j+1
			j		= i
			#
			query_rows  = range(i_deb, i_fin+1)							# rows with a given query
			#
			neigh	= []		# list of taxa of neighbors, if any
			for gap in list(range(1+gap_max)):		# loop over all gaps from 0 to gap_max (included)
				#
				which       = [ii for ii in query_rows if dis[ii] <= gap] 	# those at distance <= gap
				# if at least one hit
				#	<annot> is the name which will be given to the query
				#				- "taxon"			if hits on this reference taxon only
				#				- "ambiguous"		if hits on several reference taxa
				#				- "unknown"			if no hit
				if len(which) > 0:
					q_taxa		= [""]*len(which)		# list of reference taxa with at least one hit
					for ii, row in enumerate(which):
						try:
							k			= v_refid.index(ref[row])
							taxon		= v_tax[k]
							q_taxa[ii]	= taxon
						except:
							print_col("reference", ref[row], "is not on the reference database", color="red")
							q_taxa[ii]	= "unknown"
					q_taxa_set	= list(set(q_taxa))		# set of reference taxa with at least one hit
					if len(q_taxa_set)==1:				# if only one reference taxon
						annot	= q_taxa_set[0]			# 		transfer of the taxon for annotation
					else:								# if not
						annot	= "ambiguous"			#		ambiguous
				else:
					annot	= "unknown"					# no hit
				#
				neigh.append(annot)
			#
			neigh_comp	= list(set(neigh))
			try:
				neigh_comp.remove('unknown')
			except:
				pass
			try:
				neigh_comp.remove('ambiguous')
			except:
				pass
			if len(neigh_comp)==0:
				dic_annot[query]	= 'no_hit'
			else:
				dic_annot[query]	= neigh_comp[0]
			#
			print("query", q_num, "/", n_queries, end="\r")
			q_num	= q_num+1
			#
		#
		return(dic_annot)
					
				
				
	
	# ------------------------------------------------------------------
	
	def diagno_syst_old(self, gap_max, queries, ref, dis, v_refid, v_tax):
		"""
		all_taxa_in_inv		list			list of reference taxa with at list one hit for at least one gap
		dic_inv				dictionary		one dictionary per gap
											keys: 	referece taxa with hits on at lest one query
											value:	number of queries with hit on the reference taxon
									
		"""

		#query_set	= list(set(queries))
		all_taxa_in_inv	= []
		pl_inv			= [] 
		dic_annot		= {}
		n				= len(queries)

		for gap in list(range(1+gap_max)):		# loop over all gaps from 0 to gap_max (included)
			#
			print("gap = " + str(gap) + " ...")
			dic_inv		= {}
			
			# slices the rows of sample distance file by queries
			i			= 0
			j			= 0
			while j < n:
				query 	= queries[i]		# which query
				while queries[j]==query:
					if j == n-1:
						break
					else:
						j	= j+1
				i_deb	= i					# first row with this query
				i_fin	= j					# last  row with this query 
				i		= j+1
				j		= i
				#
				query_rows  = range(i_deb, i_fin+1)							# rows with a given query
				which       = [ii for ii in query_rows if dis[ii] <= gap] 	# those at distance <= gap
				
				# if at least one hit
				#	<annot> is the name which will be given to the query
				#				- "taxon"			if hits on this reference taxon only
				#				- "ambiguous"		if hits on several reference taxa
				#				- "unknown"			if no hit
				if len(which) > 0:
					q_taxa		= [""]*len(which)		# list of reference taxa with at least one hit
					ii			= 0
					for row in which:
						try:
							k			= v_refid.index(ref[row])
							taxon		= v_tax[k]
							q_taxa[ii]	= taxon
						except:
							print("reference", ref[row], "is not on the reference database")
							q_taxa[ii]	= "unknown"
						ii	= ii+1
					q_taxa_set	= list(set(q_taxa))		# set of reference taxa with at least one hit
					if len(q_taxa_set)==1:				# if only one reference taxon
						annot	= q_taxa_set[0]			# 		transfer of the taxon for annotation
					else:								# if not
						annot	= "ambiguous"			#		ambiguous
				else:
					annot	= "unknown"					# no hit
					
				# adds one unit on the count (value) of key "annot" 
				try:
					dic_inv[annot]	= dic_inv[annot] + 1	# adds one unit for the taxon <annot> which already exists
				except:
					dic_inv[annot] = 1						# sets to 1 a new reference taxon
				print(query, gap, annot)
			#
			# once the inventory of the gap has been done, adds the inventory for this gap in the list <pl_inv>
			pl_inv.append(dic_inv)
			# completes the list of reference taxa with at least one hit whatever the gap
			all_taxa_in_inv		= all_taxa_in_inv + list(dic_inv.keys())

		# builds the inventories
		#	- Tab_inv		array		row			a reference taxon
		#								column		a gap
		#								cell[i,j]	number of queries with annotation taxon (row) for the gap (col)
		print("building Tab")
		taxa_in_inv	= sorted(list(set(all_taxa_in_inv)))		# all taxa found at least once in alphabetic order
		n_taxa		= len(taxa_in_inv)
		Tab_inv		= np.zeros(shape=(n_taxa, 1+gap_max), dtype=int)
		for gap in list(range(1+gap_max)):
			print("gap =", gap)
			dic_here	= pl_inv[gap]
			for i, taxon in enumerate(taxa_in_inv):
				try:
					Tab_inv[i,gap]	= dic_here[taxon]
				except:
					pass
		#
		return Tab_inv, taxa_in_inv
		
	# ------------------------------------------------------------------
	
	def gap_optim(self, Tab):
		"""
		"""
		print("finds optimal gap")
		n				= Tab.shape[0]
		n_ambi			= Tab[n-2,:]
		n_nohit			= Tab[n-1,:]
		n_out			= n_ambi + n_nohit
		pl_n_out		= n_out.tolist()
		gap_optim		= pl_n_out.index(min(n_out))
		print("optimal gap is", gap_optim)	
		#
		return gap_optim
	
	# ------------------------------------------------------------------
	
	def build_inventory(self, Tab, gap_optim, taxa_in_inv, invfile):
		"""
		"""	
		print("extracting taxa with non zero counts")
		counts		= Tab[:-2, gap_optim].tolist()
		#
		which		= [i for i,val in enumerate(counts) if val > 0]
		counts 		= [int(counts[i]) for i in which]
		taxa 		= [taxa_in_inv[w] for w in which]
		print(len(counts), "counts and", len(taxa), "taxa")
		#
		print("sorting from most abundent to less abundent")
		ind, _	= zip(*sorted(enumerate(counts), key=itemgetter(1)))
		ind		= ind[::-1]
		counts_sorted		= [counts[i] for i in ind]
		taxa_sorted			= [taxa[i] for i in ind]
		n_item				= len(ind)
		#
		print("writing inventory to file", invfile)
		with open(invfile,"w") as f_inv:
			item 	= "taxon" + "\t" + "numbers"
			f_inv.write(item + "\n")
			for i in range(n_item):
				taxon	= taxa_sorted[i]
				nb		= str(counts_sorted[i])
				item	= taxon + "\t" + nb
				f_inv.write(item + "\n")
		

# ======================================================================
#
#           mapping with blast
#
# ======================================================================


class mapping_blast(mapping):
	"""
	=========================
	Mapping an OTU with blast
	=========================
	
	Contains
		- init()				provides reference and charnamefr all methods
		- blast_queries()		provides the 14 columns file of blast outputs on a query file
		- read_blastfile()		reads the blastfile
		- get_hit()				gets the pident and taxon of blasting a query on a subject, for a given row of blastfile
		
	af, 24.02.26
	"""
	def __init__(self, reference, charname, verbose=True):
		"""
		Attributes
		----------
		
		@ reference			string				basename of reference db in blast formats
		@ refid				list of strings		references id
		@ set_taxa			list of strings		list of taxa (charname) in reference
		@ dic_reftax		a dictionary		keys	: references id
												value	: taxon of the reference id
		see __init__() of class mapping
		af, 24.02.15
		"""
		print_col('[yap_mapping]:[mapping_blast]:[init()]', color='magenta')
		mapping.__init__(self, reference, charname, verbose)
		self.reference	= reference

	# ------------------------------------------------------------------
	
	def blast_queries(self, num_threads, queries_fasfile, num_alignments, blastfile, verbose=True):
		"""blasts a set of queries on a blast database
		
		Notes
		-----
		- The blast db is given through self.reference
		"""
		print_col('[yap_mapping]:[mapping_blast]:[blast_queries()]', color='magenta')
		cmd	= "blastn -num_threads "  + str(num_threads) + " -db " + self.reference + " -query " + queries_fasfile
		cmd	= cmd + " -outfmt '6 qaccver saccver pident length mismatch gapopen qstart qend sstart send evalue' " 
		cmd	= cmd + "-num_alignments " + str(num_alignments) + " -out " + blastfile 
		#
		if verbose:
			print("blast command line is")
			print(cmd)
		os.system(cmd)
		if verbose:
			print_col("blast outfile is written in", blastfile, color="green")
		
	# ------------------------------------------------------------------
	
	def read_blastfile(self, blastfile):
		"""reads the blastfile
		
		@ argument
		----------
		blastfile		string		name of file with blast output (14 columns)
		
		@ returns
		---------
		queries			list of strings		seq_ids of queruies which have been blasted
		refid			list of strings		ids of subjects produced by blast
		Values			numpy array			table of values provided by blast
		 
		af, 24.02.22
		"""
		print_col('[yap_mapping]:[mapping]:[read_blastfile()]', color='magenta')
		Tab		= np.loadtxt(blastfile, delimiter="\t", dtype=object)
		queries	= Tab[1:,0].astype(str).tolist()
		refid	= Tab[1:,1].astype(str).tolist()
		Values	= Tab[:, 2:]
		Values	= Values[1:,:].astype(float)
		#
		return queries, refid, Values
		
	# ------------------------------------------------------------------
	
	def get_hit(self, i, queries, refid, pident):
		"""
		arguments
		---------
		@ i				integer				row for annotation
		@ queries		list of strings		all queries
		@ refid			list of strings		all subject ids 
		@ pident		list of floats		all pident
		
		returns
		-------
		@ query			string		query id at row i
		@ tup			tuple		annotation of subject at row i
		
		Notes
		-----
		tup is a tuple with
			tup[0]		string		taxon of subject
			tupe[1]		float		pident of mapping query on subject
			
		af, 24.02.23, 24.02.26
		"""
		print_col('[yap_mapping]:[mapping_blast]:[get_hit()]', color='magenta')
		query			= queries[i]
		ref 			= refid[i]
		taxon			= self.dic_reftax[ref]
		pid				= pident[i]
		tup				= (taxon, pid)
		#
		return query, tup
		
	# ------------------------------------------------------------------
	
	def blast_inventory(self, dic_annot, sort, x11, invfile):
		"""Builds an inventory after annotation with blast
		arguments
		---------
		@ dic_annot		dictionary		see parse_sample_besthit()
		@ sort			boolean			whether to sort by decresing counts
		@ x11			boolean			whether to display on screen
		@ invfile		string			file where to save the inventory
		@ add2char		boolean			whether to add as a character in character file
		
		returns
		-------
		inv				list of tuples		see notes
		
		Notes
		-----
		- dic_annot has been produced by parse_sample_besthit() or parse_sample_neighborhood()
		
		- inv is a list of tuples. Each tuple corresponds to a taxon in reference database with a hit 
			with quality (measured by pident) higher than pident threshold.
			
		- if tup is such a tuple
			tup[0]	is the taxon
			tup[1]	is the number of queries with this annotation
		
		- if sort is False, taxa in inventory are given in alphabetical order	
		
		af, 24.02.26
		"""
		#
		print_col('[yap_mapping]:[mapping_blast]:[blast_inventory()]', color='magenta')
		taxa		= list(dic_annot.values())
		set_taxa	= sorted(list(set(taxa)))
		n_taxa		= len(set_taxa)
		#
		if sort:
			inv		= yap.count_and_order(taxa)
		else:
			inv		= [("",0)]*n_taxa
			for i, taxon in enumerate(set_taxa):
				count	= taxa.counts(taxon)
				inv[i]	= (taxon, counts) 
		#
		if invfile:
			with open(invfile,"w") as f_inv:
				item 	= "Taxon" + "\t" + "counts"
				f_inv.write(item + "\n")
				for i in range(n_taxa):
					tup		= inv[i]
					item 	= tup[0] + "\t" + str(tup[1])
					f_inv.write(item + "\n")
			print_col("Inventory written in file", invfile, color="cyan")
		#
		if x11:
			print("Taxon" + "\t" + "counts")
			for i in range(n_taxa):
				tup		= inv[i]
				item 	= tup[0] + "\t" + str(tup[1])
				print(item)
		#
		return inv
		
		
# ======================================================================

class mapping_blast_otu(mapping_blast):
	"""
	"""
	def __init__(self, reference, charname, verbose=True):
		"""
		see __init__() of class mapping
		af, 24.02.26
		"""
		mapping_blast.__init__(self, reference, charname, verbose)
		
	# ------------------------------------------------------------------
	
	def parse_otu(self, blastfile, x11, hitsfile):
		"""
		arguments
		---------
		@ blastfile		string				name of output blast file
		
		returns
		-------
		@ pl_annot		list of tuples		see notes 
		
		Notes
		-----
		
		pl_query and pl_annot are each a list of tuples, with, if tup is a tuple in the list
			- tup[0]	a string,	is a taxon in ferenece database
			- tup[1]	a float,	is the pident of blast of the query on the subject
			
		pl_query	is a such a list for each query, with all subjects it has a hit with
		pl_annot	is a such a list for each query over the whiole OTU
		
		af, 24.02.27 
		"""
		queries, refid, Values	= self.read_blastfile(blastfile)
		pident					= Values[:,0]
		n			= len(queries)
		i			= 0
		query		= queries[0]
		pl_annot	= []
		pl_query	= []
		with open(hitsfile, "w") as f_hits:
			if x11:
				print(query, color="magenta")
			if hitsfile:
				f_hits.write(query + '\n')
			while i < n:
				if queries[i]	== query:
					ref 	= refid[i]
					taxon	= self.dic_reftax[ref]
					pid		= pident[i]
					tup		= (taxon, pid)
					pl_query.append(tup)
					pl_annot.append(tup)
					i		= i + 1
				else:
					if x11:
						print(pl_query)
					if hitsfile:
						for tup in pl_query:
							item	= '( ' + tup[0] + ' ; ' + str(tup[1]) + ' )'
							f_hits.write( item + '\n')
					query = queries[i]
					if x11:
						print(query, color="magenta")
					if hitsfile:
						f_hits.write(query + '\n')
					pl_query	= []
			#
			return pl_annot
		
	# ------------------------------------------------------------------
	
	def parse_otu_display(self, pl_annot, min_pident, x11=True):
		"""
		af, 24.02.23
		"""
		
		n_hit	= len(pl_annot)
		x		= [""]*n_hit
		y		= [0]*n_hit
		for i in range(n_hit):
			x[i]	= pl_annot[i][0]
			y[i]	= pl_annot[i][1]
		#
		if min_pident > 0:
			#
			#	x	string	taxa with pident > min_pident
			#	y	float	corresponding pident
			#
			which 		= [w for w, val in enumerate(y) if val >= min_pident]
			x			= [x[w] for w in which]
			y			= [y[w] for w in which]
			n_hit		= len(which)
			#
			if n_hit > 0:
				#
				if x11:
					set_hits	= list(set(x))
					n_taxa		= len(set_hits)
					m			= min(y)
					for i, taxon in enumerate(set_hits):
						which 	= [w for w, tax in enumerate(x) if tax==taxon]
						n_which	= len(which)
						y_which	= [y[w] for w in which]
						plt.scatter([i]*n_which, y_which, s=10)
						plt.text(i, m, taxon, rotation="vertical")
					plt.show()
				#
				inv	= yap.count_and_order(x)
				#
			else:
				inv	= []
			#
			print("\nInventory with pident >=", min_pident, color="cyan")
			if len(inv) > 0:
				for tup in inv:
					print(tup[0], "->", tup[1])
			else:
				print("no_hit")
			#
			return inv

# ======================================================================

class mapping_blast_sample(mapping_blast):
	"""
	============================
	Mapping an sample with blast
	============================
	
	Contains
	--------
	
		- init()						provides reference and charnamefr all methods
		
		- add2char()					adds an inventory as a new character in character file
		- parse_sample_besthit()		annotates all queries with the best hit
		- parse_sample_neighborhood()	annotates queries with reference neighborhood
		
		- besthit_inventory()			builds an inventiry by filtering on pident
		
	Notes
	-----
	
	- parse_sample_xxx	produces a dictionary dic_annot with
		keys
		values		a tuple with
						tup[0]
						tup[1]
						
	af, 24.02.26
	"""
	def __init__(self, reference, charname, verbose=True):
		"""
		see __init__() of class mapping
		af, 24.02.26
		"""
		mapping_blast.__init__(self, reference, charname, verbose)
	

		
	# ------------------------------------------------------------------
	
	def parse_besthit(self, blastfile, min_pident):
		"""
		argument
		--------
		@ blastfile		string		a 14-col blastfile to read
		@ min_qual		float		minimum pident for keeping besthit
		
		returns
		-------
		@ dic_besthit		dictionary		keys	all queries
											values	a tuple (see notes)
		@ dic_annot			dictionary		keys	all queries
											values	taxon of best hit of no_hit (see notes)
											
									
		Notes
		-----
		The tuple is
			- tup[0]	a taxon
			- tup[1]	the pident of the blast of the query on the subject
		
		dic_besthit is -> query:(taxon, pident)
		 
		if pident of the blast is	more then min_qual, then dic_annot is	query: taxon
									less									query: no_hit 
		
		af, 24.02.26
		"""
		queries, refid, Values	= self.read_blastfile(blastfile)
		pident					= Values[:,0]
		n			= len(queries)
		i			= 0
		dic_besthit	= {}
		dic_annot	= {}
		#
		query, tup				= self.get_hit(i, queries, refid, pident)
		dic_besthit[query]		= tup
		dic_annot[query]		= tup[0]
		if tup[1] < min_pident:
			dic_annot[query]	= 'no_hit'
		#
		while i < n:
			if queries[i]	== query:
				i		= i + 1
			else:
				query, tup				= self.get_hit(i, queries, refid, pident)
				dic_besthit[query]		= tup
				dic_annot[query]		= tup[0]
				if tup[1] < min_pident:
					dic_annot[query]	= 'no_hit'
		#
		return dic_besthit, dic_annot
		
	# ------------------------------------------------------------------
	
	def parse_neighborhood(self, blastfile, min_pident):
		"""
		arguments
		---------
		@ blastfile		string		name of the blastfile to parse
		@ min_pident	float		minimum pident for defining neighborhood
		
		returns
		-------
		@ dic_annot		dictionary		key		a query
										value	an annotation
		
		Notes
		-----
		
		annotation is
			- a taxonn 		if the neighborhoods exists, and is taxonomically homogeneous
			- ambiguous,	if the neighborhoods exists, and is taxonomically htereogeneous
			- no_hit,		if the neighborhood is empty
			 
		af, 24.02.23, 24.02.26
		""" 
		queries, refid, Values	= self.read_blastfile(blastfile)
		pident					= Values[:,0]
		dic_annot				= {}
		#
		n			= len(queries)
		i			= 0
		j			= 0
		while j < n:
			query 	= queries[i]		# which query
			while queries[j]==query:
				if j == n-1:
					break
				else:
					j	= j+1
			i_deb	= i					# first row with this query
			i_fin	= j					# last  row with this query 
			i		= j+1
			j		= i
			#
			query_rows	= range(i_deb, i_fin+1)									# rows with a given query
			which		= [ii for ii in query_rows if pident[ii] >= min_pident] 	# those with pident >= filtre
			m_refid		= [refid[w] for w in which]
			#
			# annotates neigbors
			#
			m_taxa	= [self.dic_reftax[item] for item in m_refid]
			#
			# annotates the query
			#
			if len(m_taxa)==0:
				annot 	= "no_hit"
			else:
				if len(m_taxa)==1:
					annot	= m_taxa[0]
				else:
					set_taxa 	= list(set(m_taxa))
					if len(set_taxa) == 1:
						annot 	= set_taxa[0]
					else:
						annot	= "ambiguous"
			dic_annot[query]	= annot 
		#
		return dic_annot
		
	# ------------------------------------------------------------------
	

# ======================================================================
#
#           That's all folks!
#
# ======================================================================
      
 









