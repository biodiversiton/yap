#!/usr/bin/env python3

"""
Licence header
--------------

pre_yapsh: python3 script, part of yap

Copyright (C) 2021, INRAE

This program is free software: you can redistribute it and/or modify it 
under the terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. 
If not, see <https://www.gnu.org/licenses/>.
"""

import sys
import os
import shutil
import re

def next_seq(handle):
 myID  = False
 mySeq = ''
 for line in handle:
  line = line.rstrip('\n')
  if line.startswith('>'):
   if myID:
    yield myID,mySeq
   mySeq = ''
   myID = line[1:].split()[0]
  else:
   mySeq += line
 yield myID,mySeq

def retN(matchobj): return 'N'

def revcomp(seq):
  comp = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'N': 'N',
          'a': 'T', 'c': 'G', 'g': 'C', 't': 'A', 'n': 'N'}
  return "".join(comp[n] for n in seq[::-1])

try: basename = sys.argv[1]
except IndexError: basename = os.getcwd().split('/')[-1]
print(f'fasta file: {basename}.fas')

#Check non(ACGT)
print('\nCheck non(ACGT)', end='')
isX = False
prevID = ''
with open(f'{basename}.fas' ,'r') as fas_in:
  for myID, mySeq in next_seq(fas_in):
    if len(mySeq) < 100:
      exit(f'\n{myID} {len(mySeq)} too short')
      
    if myID == prevID:
      exit(f'Error: Duplicated sequence identifier: {myID}')

    prevID = myID
    for n in mySeq:
      n = n.upper()
      if n not in ('A', 'C', 'G', 'T'):
        print(f". There are non(ACGT) nucleotides in reads ({n} in {myID})")
        isX = True
        #break
    #if isX: break

if isX:
  withN = {}
  print(f"Change X to N, 'N' are removed for dereplication")
  swarmFile = f'{basename}_noN.fas'
  with open(f'{basename}.fas' ,'r') as fas_in,\
       open(swarmFile, 'w') as s_out:
    for myID, mySeq in next_seq(fas_in):
      mySeq = mySeq.upper()
      withN[myID] =  re.sub(r'[^ACGT]',retN,mySeq)
      if 'N' in withN[myID]:
        print(f'>{myID}\n{withN[myID].replace("N","")}', file=s_out)
      else:
        print(f'>{myID}\n{withN[myID]}', file=s_out)
else:
  swarmFile = f'{basename}.fas'
  print(': None')

#dereplicate
print(f'Dereplicate {swarmFile}')
cmd = f'swarm -d 0 -a 1 -z -t 6 -w derep.fas -o removed_replicates\
        {swarmFile}'
os.system(cmd)

with open('removed_replicates', 'r') as r_in,\
     open(f'{basename}_removed_replicates', 'w') as r_out:
  for l in r_in:
    if len(l.split()) < 2: break
    print(l, end='', file=r_out)

"""
#orient reads
print("Put all reads in the same orientation", end='', flush=True)
cmd = f'diskm_detect_revcomp.py -b derep -k 6'
os.system(cmd)

cmd = f'swarm -d 0 -a 1 -z -t 6 -w derep2.fas -o removed_replicates_2\
        derep.fas 2> /dev/null'
os.system(cmd)

#reinject N
if isX:
  with open('derep2.fas', 'r') as derep,\
       open(f'{basename}_withN.fas', 'w') as fas_out:
    for myID, mySeq in next_seq(derep):
      wKey =  myID.split(';size')[0]
      if wKey in withN and 'N' in withN[wKey]:
        mySeq = withN[wKey]
      print(f'>{myID}\n{mySeq}', file=fas_out)

"""
os.rename(f'{basename}.fas', f'{basename}_raw.fas')
os.rename('derep2.fas', f'{basename}.fas')


print(f'\n{basename}.fas is dereplicated and does not contain non(ACGT) nucleotides')
if os.path.exists(f'{basename}_withN.fas'):
  print(f'{basename}_withN.fas is dereplicated and contains N')
if os.path.getsize(f'{basename}_removed_replicates') == 0:
  os.remove(f'{basename}_removed_replicates')
else:
  print(f'{basename}_removed_replicates contains removed reads (2nd and following columns)')


for f in ('removed_replicates', 'removed_replicates', 'derep_NotOriented.fas',
          'derep.fas','removed_replicates_2'):
  try:   os.remove(f)
  except FileNotFoundError: pass

