#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
yapotu library

Contains functions which are called by yapotu library

maintainer:		Alain Franc
contact			alain.franc@inrae.fr
Contributors:	Alain Franc
				Jean-Marc Frigerio
started August, 23rd, 2018
version 20.07.10
"""

"""
------------------------------------------------------------------------

				contents
				
------------------------------------------------------------------------

Useful functions	count_and_order()	counts the number of occurences of each item in a list, and reorders

Loading files		next_seq()			for reading fasta files, creates a generator which provides the next seq of a fasta file, at each call
					load_fasta()		loads a fasta file ; uses next_seq defined above (avoids use of BioSeq)
					load_ascii()		!!!!! voir si on le maintient ...
					load_disfile()		loads distance file
					load_charmat()		loads charater file
					load_chars_hdf5()	loads characters and charnames
					load_hdf5()			loads hdf5 file
					
supervised learning with blast		hits_with_distances()
									parse_blast_output()
									get_taxo_from_ncbi()
									
OTU builder			build_otu_igraph()
					build_otu_csgraph()
					
heatmaps			build_count_matrix()
					plot_count_matrix()
					density_heatmap()
					coord2pixel()
					build_spike()


from declic			sort_clusters()
					set_colors()
					plot_scatter()
					
clustering			clustering()
					clustering_draw_tree()
					clustering_draw_tree_coloredlabels()
					get_clusters()
					cluster_composition()
					
otu display			get_otu_membership()
					get_distances_within_otu()
					get_distances_several_otu()
					display_graph_otu_nx()
					display_graph_otu()

"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage as ndi
# for clustering
import scipy.spatial.distance as ssd	# for computing distances and condense with squareform()
import scipy.cluster.hierarchy as sch   # for handling linkage matrices

import time
#import datetime

# selection of library for graph NOTE : in fine, ne se reposer que sur networkx
"""
try:
	import igraph as ig
	print("library igraph found")
	libgraph = "igraph"
except ModuleNotFoundError:
	import scipy.sparse.csgraph as sg
	print("library igraph not found")
	print("library sparse.csgraph loaded instead")
	libgraph = "csgraph"
"""
import networkx as nx

# for sorting dictionaries along a value
import operator
import sqlite3
import h5py # temporaire

# ======================================================================
#
#			useful functions
#
# ======================================================================


def xLogx(x):
	"""
	x*Log(x) for Shannon index 
	"""
	
	if x==0:
		xLogx	= 0
	else:
		xLogx	= x*np.log(x)
	#
	return xLogx
	
# ----------------------------------------------------------------------

def shannon(vec):
	"""
	Shannon entropy of a distribution
	"""
	Sh	= 0
	for x in vec:
		Sh	= Sh - xLogx(x)
	#
	return Sh
# ----------------------------------------------------------------------


def count_and_order(pl):
	"""
	counts the number of occurences of each item in a list, and reorders
	
	
	**argument**
	
	`pl` ; a python list of characters

	**returns**
	
	`res` ; a dictionary
	
	- keys: value of the character
	
	- value : number of elements in the list
	
	
	**example**
	
	if `pl = [a,b,a,c,c,b,a]`, then
	
	`res` is a dictionary with `res[a]=3` ; `res[b]=2` ; `res[c]=2` 
	
	*af, Pieroton, 23/01/2018*
	"""
	
	lev	 = list(set(pl))
	count   = [pl.count(item) for item in lev] # nb of occurence per character level
	dicount = dict(list(zip(lev,count)))		   # dictionary occ -> level
	res	 = sorted(iter(dicount.items()), key=operator.itemgetter(1), reverse=True)
	#
	return res

# ======================================================================
#
#		Loading files
#
# ======================================================================

def next_seq(handle):
	"""
	
	for reading fasta files
	creates a generator which provides the next seq of a fasta file, at each call
	
	Usage:
		with open(fasfile ,'r') as fas_in:
			for myID, mySeq in next_seq(fas_in):
				print myID,mySeq

	"""
	myID  = False
	mySeq = ''
	for line in handle:
		line = line.rstrip('\n')
		if line.startswith('>'):
			if myID:
				yield myID,mySeq
			mySeq = ''
			myID = line[1:]
		else:
			mySeq += line
	yield myID,mySeq

# ----------------------------------------------------------------------



def load_fasta(fasfile):
	"""
	uses next_seq defined above
	"""
	with open(fasfile,'r') as fasta_sequences:
		seq_id	  = []
		words	    = []
		for myId, myFasta in next_seq(fasta_sequences):
			seq_id.append(myId)
			words.append(myFasta)
		seq_len = [len(w) for w in words]
	#
	return words, seq_id, seq_len

# ----------------------------------------------------------------------	

def load_charfile(charfile):
	"""
	
	arguments
	---------
	@ charfile		string		name of file to upload
	@ short			True.False	if taxonic names have to be simplified
	
	outputs
	-------
	@ Char			array		characters - all columns but without headers
	@ varnames		list		headers
	@ n_seq			integer		number of sequences (rows of Char)
	
	af, @ gazinet, october 26, 2019
	"""
	print("\nloading charfile", charfile)
	Char		= np.loadtxt(charfile, delimiter = '\t', dtype=object)	# full character file
	varnames	= Char[0,:].astype(str).tolist()						# headers as list "varnames"
	Char		= Char[1:,:].astype(str)								# character array without the headers
	seqid		= Char[:,0].astype(str).tolist()						# seqid in a list (one per row)
	n_seq		= len(seqid)											# number of reads
	n_headers	= len(varnames)											# number of columns
	print(n_seq, "sequences")
	#
	# in case of partial name only (e.g. non full botanical names)
	"""
	if short:
		for j in range(1, n_headers):				# over columns with taxonomic name only
			for i in range(n_seq):				# over all sequences
				item		= Char[i,j]
				xx			= item.split(" ")
				Char[i,j]	= xx[0]
	print("with simplified botanical names")
	print("size of the array", Char.shape)
	"""
	#
	return Char, varnames, seqid, n_seq
	
# -----------------------------------------------------------------------

def load_disfile_new(disfile=None):
	"""
	"""
	#
	f_dis	= open(disfile, "r")
	#
	lines	= f_dis.readlines()
	n		= len(lines)
	print("distance file has", n-1, "rows")
	rnames	= ['']*(n-1)
	cnames	= ['']*(n-1)
	Dis		= np.zeros((n-1,n-1), dtype=float)
	#
	for i, line in enumerate(lines):
		line	= line[:-1]
		x	= line.split('\t')
		if i==0:
			cnames	= x[1:]
		else:
			rnames[i-1]	= x[0]
			Dis[i-1,:]	= [float(val) for val in x[1:]]
	#
	return Dis, rnames, cnames

# -----------------------------------------------------------------------

def load_ascii(filename, delimiter="\t", headers=True, rownames=True):
    """
    """
        
    if ((not headers) and (not rownames)):
        A    = np.loadtxt(filename, delimiter=delimiter, dtype=float)
        return A
    #
    if (headers and (not rownames)):
        A        = np.loadtxt(filename, delimiter=delimiter, dtype=object)
        pl_headers    = A[0,:].astype(str).tolist()
        A        = A[1:,:]
        A        = np.array(A, dtype=float)
        return A, pl_headers
        
    if ((not headers) and rownames):
        A            = np.loadtxt(filename, delimiter=delimiter, dtype=object)
        pl_rownames     = A[:,0].astype(str).tolist()
        A            = A[:,1:]
        A            = np.array(A, dtype=float)
        return A, pl_rownames
        
    if (headers and rownames):
        A            = np.loadtxt(filename, delimiter=delimiter, dtype=object)
        pl_headers    = A[0,1:].astype(str).tolist()
        pl_rownames = A[1:,0].astype(str).tolist()
        A           = A[1:,1:]
        A           = np.array(A, dtype=float)
        return A, pl_headers, pl_rownames        

# ----------------------------------------------------------------------

def load_disfile(disfile):
	"""
	"""
	Dis, cnames, rnames = load_ascii(disfile, headers=True, rownames=True)
	#
	colnames	= [i.strip('"') for i in cnames]
	rownames	= [i.strip('"') for i in rnames]
	#
	return Dis, rownames, colnames

# ----------------------------------------------------------------------

def load_histfile(histfile):
	"""
	"""
	A, cnames, rnames = load_ascii(histfile, headers=True, rownames=True)
	#
	colnames	= [i.strip('"') for i in cnames]
	rownames	= [i.strip('"') for i in rnames]
	#
	return A, rownames, colnames
	
# ----------------------------------------------------------------------

def load_charmat(charfile):
	"""
	"""
	Char			= np.loadtxt(charfile, delimiter = '\t', dtype=object)
	varnames		= Char[0,:].astype(str).tolist()
	Char			= Char[1:,:]
	seq_id			= Char[:,0].astype(str).tolist()
	n_items			= len(seq_id)
	n_headers		= len(varnames)  
	#
	return Char
  
# ----------------------------------------------------------------------
def load_chars_hdf5(filename):
	"""
	loads characters and charnames
	"""
	hf			= h5py.File(filename,'r')
	charname	= []
	characters	= []
	for c in  hf['charname'][:].astype(str):
		charname.append(c)
	for c in hf['characters'][:].astype(str):
		characters.append(c)

	return charname, characters
  
# ----------------------------------------------------------------------

def load_reads_hdf5(filename):
	"""
	"""
	hf    = h5py.File(filename,'r')
	seqid = []
	word  = []
	for s in  hf['seqid'][:].astype(str):
		seqid.append(s.rstrip('\n'))
	for w in hf['word'][:].astype(str):
		word.append(w.rstrip('\n'))

	return seqid,word  
	
# ----------------------------------------------------------------------

def load_hdf5(filename):
	"""
	loads distances, seqid and sequences
	"""

	hf = h5py.File(filename,'r')
	try:
		A = hf['distances'][:]
	except KeyError:
		A = hf['distance'][:]
	#
	try: 
		row_seqid = hf['row_seqid']
	except KeyError:
		seqid = []
		word  = []
		for s in  hf['seqid'][:].astype(str):
			seqid.append(s.rstrip('\n'))
		for w in hf['word'][:].astype(str):
			word.append(w.rstrip('\n'))
		return A,seqid,word

	row_seqid = list(map(lambda x: x.astype(str).rstrip('\n') ,hf['row_seqid']))
	row_word  = list(map(lambda x: x.astype(str).rstrip('\n') ,hf['row_word']))
	col_seqid = list(map(lambda x: x.astype(str).rstrip('\n') ,hf['col_seqid']))
	col_word  = list(map(lambda x: x.astype(str).rstrip('\n') ,hf['col_word']))
	#
	return A, row_seqid, row_word, col_seqid, col_word
	



	
# ======================================================================
#
#		supervised learning with blast
#
# ======================================================================

def hits_with_distances(query2refdisfile, gap):
	"""
	input
	-----
	
	@ query2refdisfile		numpy array		pairwise distance file queries x referebces
	@ gap					integer			maximum distances for a hit
	
	outputs
	-------
	
	@ qseqid				list			see notes
	@ sacc					list			see notes
	@ n_hits				integer			nmber of hits
	
	notes
	-----
	
	qseqid and sacc are two lists of same length
	they must be read pairwise: they give all pairs query x ref such that d(query, ref) <= gap
	for element [i] for i in 0 ... n-hits-1
		qseqid[i]	a query
		sacc[i]		a reference
		such that d(query, acc) <= gap)
	
	the reference is known as sacc for "accession" in blast, because the same file is built with blast on ncbi as well
	
	af, @ Gazinet, revised April 14, 2019
	"""

	# Dis: pairwise distances query x ref
	A			= np.loadtxt(query2refdisfile, delimiter="\t", dtype=bytes)
	query_id	= A[0,1:].astype(str).tolist()
	ref_id		= A[1:,0].astype(str).tolist()
	Dis			= A[1:,:]
	Dis			= Dis[:,1:]
	Dis			= np.array(Dis, dtype=float)
	n_ref		= Dis.shape[0]
	n_query		= Dis.shape[1]

	#
	qseqid	= []
	sacc	= []
	for i, query in enumerate(query_id):
		#
		print(query)
		#
		dis		= Dis[:,i]
		which 	= [ii for ii,d in enumerate(dis) if d <= gap] 
		cand	= [ref_id[iii] for iii in which]
		if len(cand) > 0:
			for a, item in enumerate(cand):
				qseqid.append(query)
				sacc.append(item)
		else:
			print(query, "-> no hits")
	#
	n_hits		= len(qseqid)
	#
	return qseqid, sacc, n_hits

# ----------------------------------------------------------------------

def parse_blast_output(blastoutfile, align_min, pident_min):
	"""
	"""
	### parsing blast otput and selecting
	
	# produces two lists
	#	- qseqid		query seqid with at least on high quality hit
	#	- sacc			subject (ref) with at least one good hit on a query
	#	sacc[i] has a good hit on qseqid
	#
	#	and an array <inv> with matching qseqid in column 0 and sacc in column 1
	# loading
	
	headers 	= ['qseqid' , 'sacc' , 'slen' , 'qstart' , 'qend' , 'sstart' , 'send' , 'evalue' , 'score' , 'length' , 'pident']
	B			= np.loadtxt(blastoutfile, delimiter="\t", dtype=bytes)
	n			= B.shape[0]
	
	print("file to parse has", B.shape[0], "and", B.shape[1], "columns.")


	# parsing - the different variables required from blast are given as numpy 1D-arrays
	qseqid		= B[:,headers.index('qseqid')].astype(str)		# query seqid
	sacc		= B[:,headers.index('sacc')].astype(str)		# subject accession
	slen		= B[:,headers.index('slen')].astype(float)		# subject length
	qstart		= B[:,headers.index('qstart')].astype(float)	# qstart for all items
	qend		= B[:,headers.index('qend')].astype(float)		# qend for all items
	pident		= B[:,headers.index('pident')].astype(float)	# pident for all items
	
	# some small calculations
	l_align		= qend-qstart														# alignment length
	qual_len	= [i for i, item in enumerate(l_align) if item >= align_min]		# those items with alignement length larger than align_min
	qual_ident	= [i for i, item in enumerate(pident) if item >= pident_min]		# those items with pident larger than ident_min
	which 		= np.intersect1d(np.array(qual_len), np.array(qual_ident)).tolist()	# intersection of both
	sacc		= [sacc[i] for i in which]											# accessions with a high quality hit on at least one query
	qseqid		= [qseqid[i] for i in which]										# queries with at least one accession with a high quality hit
	n_hits 		= len(which)		# total number of quality accessions, whatever the query
	#
	return qseqid, sacc, n_hits

# ----------------------------------------------------------------------

def get_taxo_from_ncbi(subject, taxo):
	"""
	@ output: res	list ['order', 'family', 'genus', 'species']
	"""
	taxo.execute(
	  'SELECT taxonomy_taxid FROM Accession WHERE "accession"="%s"' % (subject)
	)
	try:
		taxid = taxo.fetchone()[0]
	except: 
		pass
	try:
		subject += '.1'
		taxo.execute(
		'SELECT taxonomy_taxid FROM Accession WHERE "accession"="%s"' % (subject)
		)
		taxid = taxo.fetchone()[0]
	except:
		return ['','','','']

	query	= 'Query'
	taxo.execute(
		'WITH RECURSIVE ascendant(x,y,z) AS (SELECT "%s",NULL,NULL \
		  UNION ALL\
		  SELECT Taxonomy.parent_id,Taxonomy.rank_id, Taxonomy.name\
		  FROM Taxonomy, ascendant\
		  WHERE Taxonomy.tax_id = ascendant.x\
		  AND Taxonomy.parent_id IS NOT NULL)\
		SELECT x, z, Rank.name FROM ascendant LEFT JOIN Rank ON y=Rank.id;' % taxid
	)
	#
	res = []
	lin = {}
	for t in (taxo.fetchall()):
		lin[t[2]] = t[1]
	for t in ['order', 'family', 'genus','species']:
		try:
			res.append(lin[t])
		except KeyError:
			res.append('')
	#print(res)
	return res


# ======================================================================
#
#			OTU builder
#
# ======================================================================

def build_otu_igraph(A):
	"""
	argument
	--------
	@ A		2D numpy array of boolean	afjacency matrix of the graph
	
	output
	------
	@ clus	list of lists
	
	notes
	-----
	<clus> is a list of connected components with igraph or csgraph
	an otu is a connected component
	it is given as a list of indices (read <i> for row <i> of distance file)
	clus is a list of lists
	
	af, Gazinet, 25/01/2020	
	"""
	tg		= time.time()
	g		= ig.Graph.Adjacency(A.tolist(), mode= 'undirected')
	tgf		= time.time()
	print("took", tgf-tg, "seconds with igraph")
	print("\nbuilding the cc ...")
	tc			= time.time()
	clus		= list(g.clusters())
	tcf			= time.time()
	print("...took", tcf-tc, "seconds with igraph")
	#
	return clus
	
# ----------------------------------------------------------------------

def build_otu_csgraph(A):
	"""
	argument
	--------
	@ A		2D numpy array of boolean	adjacency matrix of the graph
	
	output
	------
	@ clus	list of lists
	
	notes
	-----
	<clus> is a list of connected components with igraph or csgraph
	an otu is a connected component
	it is given as a list of indices (read <i> for row <i> of distance file)
	clus is a list of lists
	
	af, Gazinet, 25/01/2020
	"""
	tg		= time.time()
	g		= sg.csgraph_from_dense(A)
	tgf	= time.time()
	print("took", tgf-tg, "seconds with scipy")
	tc		= time.time()
	res		= sg.connected_components(g, directed=False)
	nn_cc	= res[0]
	ccs		= res[1].tolist()
	clus 	= []
	for i_ccc in range(nn_cc):
		cc = [ic for ic, item in enumerate(ccs) if item==i_ccc]
		clus.append(cc)
	tcf			= time.time()
	print("...took", tcf-tc, "seconds with scipy")
	#
	return clus

# ----------------------------------------------------------------------

def build_otu_nx(A):
	"""
	wid: builds otu as connected components of a graph with networkx
	
	argument
	--------
	@ A		2D numpy array of boolean	adjacency matrix of the graph
	
	output
	------
	@ clus	list of lists
	
	notes
	-----
	<clus> is a list of connected components with igraph or csgraph
	an otu is a connected component
	it is given as a list of indices (read <i> for row <i> of distance file)
	clus is a list of lists
	
	af, Gazinet, 11/06/2020
	"""
	tg		= time.time()
	# todo!!

# ======================================================================
#
#			heatmaps
#
# ======================================================================

def build_count_matrix(F1, F2, bins=256, range=None, log=True, scale=False):
	"""
	builds the count matrix of number of items projected on a same pixel 
	in a 2D image (for plotting a heatmap)
	
	arguments
	---------
	@ F1	vector		first axis of the plot
	@ F2	vector		second axis of the plot
	@ bins	integer		number of pixel per edge of the image
	@ range				see help of numpy.histogram2d
	@ log	boolean		if True, logarithm of nb of items per pixel
	@ scale	boolean		if True, 
	
	outputs
	-------
	H		numpy array		matrix 
	xedges					the bin edges along the first dimension.
	yedges					the bin edges along the second dimension.
	
	note
	----
	it is a wrapper of function numpy.histogram2d()
	H[i,j] is the (log of the) number of items projected on pixel (i,j)
	
	af, @ Gazinet, 25/01/2020
	"""
	H, xedges, yedges = np.histogram2d(F1, F2, bins=bins, range=range)
	if log:
		H	= np.log(1+H)
	if scale:
		M	= np.max(H)
		H	= np.round((H/float(M))*255)
		H 	= np.array(H, dtype=int)
	#
	return H, xedges, yedges 
	
# ----------------------------------------------------------------------

def plot_count_matrix(H, axis_1, axis_2, fmt='png', title=None, x11=True, imfile=None):
	"""  
	wid:	plots the count matrix
	
	argument
	--------
	@ H			numpy array		count matrix
	@ axis_1	integer			first axis
	@ axis_2	integer			second axis
	@ fmt		string			format for saving plot in a file
	@ title		string			title of the plot
	@ x11		boolean			if True, plot displayed on the screen
	@ imfile	string			name of the file where to write the plot
	
	output
	------
	none	(a plot displayed on the screen and/or written in a file)
	
	Notes
	-----
	H is a count matrix produced by a call to <build_count_matrix>
	if x11 is true, the plot is displayed on the screen
	if imfile is a string (and not 'none'), the plot is written in file 'imfile'
	with format 'fmt'
	
	af, @ Gazinet, 25/01/2020
	"""
	#plt.matshow(H)
	plt.imshow(H)
	plt.xlabel("axis " + str(1+axis_1))
	plt.ylabel("axis " + str(1+axis_2))
	if title:
		plt.title(title)
	plt.colorbar()
	if imfile:
		plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
	if x11:
		plt.show()
		
# ----------------------------------------------------------------------

def density_heatmap(Y, bins, axis_1, axis_2, fmt="png", title=None, x11=True, imfile=None):
	"""
	wid: builds the heatmap
	
	arguments
	---------
	@ Y			numpy array
	@ bins		integer		the size of the image
	@ axis_1	integer		first axis of Y
	@ axis_2	integer		second axis of Y
	@ fmt		string		format for writing the heatmap in a file
	
	output
	------
	none	(displays the count matrix on the screen and/or writes it in a file)
	
	Notes
	-----
	Y is a numpy array of coordinates, as produced by the MDS
	
	af, @ Gazinet, 25/01/2020
	
	"""
	# builds count matrix
	F1					= Y[:,axis_1]
	F2					= Y[:,axis_2]
	H, xedges, yedges = build_count_matrix(F1, F2, bins=bins)

	# plotting count matrix
	plot_count_matrix(H, axis_1, axis_2, fmt=fmt, title=title, x11=x11, imfile=imfile)
	
# ----------------------------------------------------------------------

def coord2pixel(x, bins):
	"""
	probably deprecated
	use np.histogram2d() instead
	25/01/2020
	"""
	m	= np.min(x)
	M	= np.max(x)
	x_i = bins*(x - m)/(M - m)
	x_i	= np.floor(x_i)
	x_i	= np.minimum(x_i, bins-1)
	x_i	= [int(item) for item in x_i]
	#
	return x_i
	
# ----------------------------------------------------------------------

def build_spike(H, level):
	"""
	wid: idenfies islands of pixel with value higher than a given level
	
	arguments
	---------
	@ H			numpy array		a count matrix
	@ level		float			level to isolate islands
	
	outputs
	-------
	@ spikes
	@ num
	
	Notes
	-----
	
	af, @ Gazinet, 25/01/2020
	"""
	# spikes identification
	x, y 		= np.where(H > level)		# yields indices x and y for which [x,y] fulfills the condition
	n_points 	= len(x)					# counting the points
	print(n_points, "pixels with density larger than", level)

	Clus 		= np.zeros(H.shape) # Clus[i,j] = 1 if in a spike, 0 otherwise
	Clus[x,y]	= 1

	spikes, num	= ndi.label(Clus)	# miraculous function in scipy.ndimage
									# a spike is a unique spike
	#
	print(str(num), "spikes found")	

	f, (ax_left, ax_right) = plt.subplots(1, 2)
	ax_left.matshow(spikes)
	ax_right.matshow(H)
	plt.show()	
	
	#
	return spikes, num	

# ======================================================================
#
#		From declic()
#
# ======================================================================

# ----------------------------------------------------------------------	

def sort_clusters(clus):
	"""
	sorts the list of otus in size decreasing order
	"""
	clus.sort(key=len, reverse = True)
	sizes	= [len(item) for item in clus]		# size of each cc
	n_cc	= len(clus)							# number of cc
	print(n_cc,"otus computed by CAH")
	#
	return clus, sizes, n_cc
	
# ----------------------------------------------------------------------

def set_colors(charname=None, colorfile=None, highlighted=None, color="blue"):
	"""
	**what it does**
	
	sets colors for plotting

	**arguments**
	
	- `charname`  ; string ; selected character
	
	- `colorfile` ; string ; name of file for color selection
	
	- `highlighted` ; list ; which charcater values to highlight
	
	- `color` ; string ; color of highlighted values
	
	**returns**
	
	- `v_col` ; list ; list of colors per item
	
	- `dicolor` ; dictionary ; dictionary of colors per character value
	 
	**notes**
	
	`colorfile` : a tab delimited file, with headers and two columns
	
	- column 1:   a character value
	
	- column 2:   a color

	*@ Pierroton, 25/01/2018* 
	"""
	
	color_tab = ['blue', 'green', 'red', 'purple', 'magenta', 'cyan', 'chartreuse', 'orchid', 'peru',
	'olive', 'yellow', 'tomato']

	#
	dicolor = {} 
	if highlighted:
		v_col					   = ['grey' for i in range(n_items)]
		char, lev, count, dicount   = self.get_char(charname)
		for i, item in enumerate(char):
			if item == highlighted:
				v_col[i]	= color
				dicolor	 = {highlighted:color, "others":"grey"}
	else:
		if charname:
			char, lev, count, dicount	= self.get_char(charname)
			colors  = ['grey' for ii in lev]
			if colorfile:
				Col			 = np.loadtxt(colorfile, delimiter = '\t', dtype=object)
				print("colorfile", Col)
				names   = Col[1:,0].tolist()
				col	 	= Col[1:,1].tolist()
				dicolor = dict(list(zip(names,col)))
				print("dicolor", dicolor)
				v_col   = ["grey"]*self.n_items
				for i, item in enumerate(char):
					if item in names:
						v_col[i] = dicolor.get(item)
			else:
				n_col   = min(len(color_tab), len(lev))
				for ii in range(n_col):
					colors[ii] = color_tab[ii]
				dicolor = dict(list(zip(lev, colors)))
				#
				v_col  = [dicolor.get(item) for item in char]
		else:
			v_col   = [col for item in range(self.n_items) ]
			dicolor = {"blue":"all items"}  # generates an error in plotting the legend
	#
	return v_col, dicolor
		
# ----------------------------------------------------------------------

def plot_scatter(Y, axis_1=1, axis_2=2, v_col=None, dicolor=None, dot_size=20, v_names=None, fontsize=10, legend=True, title=None,
			   x11=True, imfile=None, fmt = 'png'):
	""" 
	**what it does**
	
	plots axis i and j of a linear dimension reduction
	
	**arguments**
		@ Y		numpy array		coordonate matrix
	
	- `axis_1` ; integer ; rank of axis 1 (beginning at 1)
	
	- `axis_2` ; integer ; rank of axis 2 (beginning at 1)
	
	- `v_col` ; list ; a colors for each point 
	
	- `dicolor` ; dictionary with keys being characters, and the values the color for this character
	
	- `v_names` ; list ; a name to be written eventually per point 
	
	- `title` ; string ; title on top of the figure
	
	- `x11` ; boolean ; if True, the figure is displayed on screen
	
	- `imfile` ; string ; name of file where to save the plot
	
	- `fmt` ; string ; format for saved plot

	@ **kwargs  other arguments

	@   dot_size			dot size
	@   legend			  if True, legend displayed on figure		


	**notes**
	
	It is a generic function for plotting columns `i` and `j` of a coordinate array `Y`
	used for PCA, CA, MDS, etc ...
	
	The plot can be
	
	- dispalyed on the screen if :math:`x11 = True`
	
	- saved as a file with name in `imfile` 
	
	The array `Y` has `n` rows (`n` dots to be plotted)

	- axis are numbered from 1 upwards
	
	- `dicolor` must be given on top of v_col for plotting the legend

	*@Pierroton, 02/08/2017 ; revized 28/3/2018*

	"""

	# axis
	F1 = Y[:,axis_1-1]
	F2 = Y[:,axis_2-1]

	# labels of axis and title
	xlabel = 'axis %d' % axis_1
	ylabel = 'axis %d' % axis_2
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	if title:
		plt.title(title)
	#
	is_grey = [i for i, item in enumerate(v_col) if item == "grey"]
	no_grey = [i for i, item in enumerate(v_col) if item != "grey"]
	#
	F1_g = [F1[i] for i in is_grey]
	F2_g = [F2[i] for i in is_grey]
	plt.scatter(F1_g, F2_g, c="grey", s=5, zorder=1)
	#
	F1_c	= [F1[i] for i in no_grey]
	F2_c	= [F2[i] for i in no_grey]
	v_col_c = [v_col[i] for i in no_grey]
	plt.scatter(F1_c, F2_c, c=v_col_c, s=dot_size, zorder=2)
	#
	if v_names:
		for i, item in enumerate(v_names):
			plt.text(F1[i], F2[i], v_names[i], fontsize=fontsize)

	#
	if legend:
		p, keys = get_legend(dicolor)
		plt.legend(p, keys, loc='upper center', bbox_to_anchor=(0, 1.02, 1, 0.1),
		  ncol=3, fontsize=8, fancybox=True, shadow=True)

	if imfile:
		plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
	if x11:
		plt.show()
	plt.clf()
	#
	### ============================================================
	###
	###		 !!! warning !!!
	###
	### ------------------------------------------------------------
	###
	### plt.savefig MUST be called before plt.show
	### otherwise, it does not work
	### plt.show probably reinitializes something
	###
	### ============================================================
	
# ======================================================================
#
#		clustering
#
# ======================================================================


# ----------------------------------------------------------------------

def clustering(Dis, meth):
	"""
	"""
	dis 	= ssd.pdist(Dis)
	Z 		= sch.linkage(dis, meth)
	#
	return Z
	
# ----------------------------------------------------------------------

def clustering_draw_tree(Z, orientation, color_threshold, color_above_threshold, labels, meth, title=None):
	"""
	"""
	dend		= sch.dendrogram(Z=Z, orientation=orientation, color_threshold=color_threshold, above_threshold_color=color_above_threshold, labels=labels)
	if title:
		plt.title(title)
	plt.show()
	#
	return dend
	
# ----------------------------------------------------------------------

def clustering_draw_tree_coloredlabels(Z, orientation, color_threshold, color_above_threshold, labels, dic_lab_colors, tax_name, meth):
	"""
	"""
	# building the dendrogram
	dend			= sch.dendrogram(Z=Z, orientation=orientation, color_threshold=color_threshold, above_threshold_color=color_above_threshold, labels=labels)
	# Apply the right color to each label
	ax		= plt.gca()
	xlbls	= ax.get_ymajorticklabels()
	for lbl in xlbls:
		lbl.set_color(dic_lab_colors[lbl.get_text()])
	#
	plt.title("Taxon is " + tax_name + " ; method is " + meth)
	plt.show()
	#
	return dend
# ----------------------------------------------------------------------

def get_clusters(Z, height=-1, n_clusters=-1):
	"""
	getting clusters
	"""
	if height > 0:
		clus 		= sch.cut_tree(Z,height=height).tolist()
	if n_clusters > 0:
		clus 		= sch.cut_tree(Z,n_clusters=n_clusters).tolist()
	clus 		= [a[0] for a in clus]
	n_clus		= max(clus)
	clus_comp	= [] 
	for i in range(n_clus+1):
		which 		= [ii for ii, val in enumerate(clus) if val==i]
		clus_comp.append(which)
	#
	return clus_comp
	
# ----------------------------------------------------------------------

def cluster_composition(clus_comp, v_ord, v_fam, v_gen, dc_ord, dc_fam, dc_gen):
	"""
	"""
	for i, which in enumerate(clus_comp):
		clus_ord	= [v_ord[a] for a in which] 
		clus_fam	= [v_fam[a] for a in which] 
		clus_gen	= [v_gen[a] for a in which] 
		print("\ncluster", i)
		#
		if len(v_ord) > 0:
			clus_taxo	= count_and_order(clus_ord)
			what= ""
			for item in clus_taxo:
				what = what + item[0] + " -> " + str(item[1]) + " / " + str(dc_ord[item[0]]) + " ; "
			print(what)		
		#
		if len(v_fam) > 0:
			clus_taxf	= count_and_order(clus_fam)
			what= ""
			for item in clus_taxf:
				what = what + item[0] + " -> " + str(item[1]) + " / " + str(dc_fam[item[0]]) + " ; "
			print(what)
		#
		if len(v_gen) > 0:
			clus_taxg	= count_and_order(clus_gen)
			what= ""
			for item in clus_taxg:
				what = what + item[0] + " -> " + str(item[1]) + " / " + str(dc_gen[item[0]]) + " ; "
			print(what)

	


# ======================================================================
#
#				look at otus
#
# ======================================================================

def get_otu_membership(otufile):
	"""
	"""
	otu_dic	= {}
	with open(otufile,'r') as otuf:
		for line in otuf:
			line 		= line[:-1]
			x			= line.split("\t")
			otu			= x[0]
			otu_members	= []
			for item in x[1:]:
				otu_members.append(item)
			otu_dic[otu]	= otu_members
	#
	return otu_dic
	
# ----------------------------------------------------------------------

def get_distances_within_otu(otu_seqids, seqids, Dis):
	"""
	arguments
	---------
	wid: extracts from global pairwise distance file the subaray of the otu
	
	@ otu_seqids	list	list of seqids in the otu
	@ seqids		list	list of seqids of the whole dataset
	@ Dis			array	pairwise distances
	
	outputs
	-------
	otu_Dis			array	pairwise distances within the otu
	
	af, @ Gazinet, April 6th, 2020
	"""
	n		= len(otu_seqids)
	otu_Dis	= np.zeros((n,n))	# distances between sequenes within an otu
	#
	for i in range(n):
		iseq	= otu_seqids[i]
		try:
			k = seqids.index(iseq)
		except:
			print("!!! in get_distances_within_otu() !!!")
			print("iseq is", iseq)
		for j in range(i,n):
			jseq	= otu_seqids[j]
			try:
				m = seqids.index(jseq)
			except:
				print("!!! in get_distances_within_otu() !!!")
				print("jseq is", jseq)
			dis	= Dis[k,m]
			otu_Dis[i,j]	= dis
			otu_Dis[j,i]	= dis
	#
	return otu_Dis
	
# ----------------------------------------------------------------------

def get_distances_several_otu(otu_seqids_pl, seqids, Dis):
	"""
	arguments
	---------
	wid: extracts from global pairwise distance file the subaray of the otu
	
	@ otu_seqids_pl		list	list of list of seqids in the otus
	@ seqids			list	list of seqids of the whole dataset
	@ Dis				array	pairwise distances
	
	outputs
	-------
	otu_Dis			array	pairwise distances within the otu
	
	notes
	-----
	otu_seqids_pl is a list, say L
	element L[i] is the list of seqids of the reads/groups which are member of otu 'i'
	
	af, @ Gazinet, April 10th, 2020
	"""
	n_otus		= len(otu_seqids_pl)
	otu_seqids	= [] 
	otu_num		= []
	#
	for i in range(n_otus):
		n_inotu		= len(otu_seqids_pl[i])
		otu_seqids	= otu_seqids + otu_seqids_pl[i]
		otu_num		= otu_num + [i]*n_inotu
	#
	otu_Dis 	= get_distances_within_otu(otu_seqids, seqids, Dis)
	#
	return otu_Dis, otu_num	

# ----------------------------------------------------------------------

def display_graph_otu_nx(otu_Dis, gap, col_nodes ="red", col_edges ="gray", dot_size=20, title=None):
	"""
	"""
	# building the graph
	A	= 1*(otu_Dis <= gap)
	np.fill_diagonal(A,0)
	G	= nx.from_numpy_matrix(A)
	#
	pos=nx.spring_layout(G)
	#
	pi2 = np.pi*2
	node_size = dot_size*pi2
	nx.draw(G, pos=pos, node_color=col_nodes, edge_color=col_edges, node_size=node_size)
	

# ----------------------------------------------------------------------

def display_graph_otu(otu_Dis, gap, c_edges="gray", c_nodes="red", v_size=20, layout="fr", title=None):
	"""
	wid:	displays the graph of an otu 
	arguments
	---------
	@ otu_Dis	array		pairwise distances within the otu
	@ gap		float		an edge is set if dis <= gap
	@ c_edges	string		color of edges
	@ c_nodes	string		color of nodes
	@ v_size	float		size of nodes
	"""
	
	n_seq	= otu_Dis.shape[0]

	# building the graph
	A	= 1*(otu_Dis <= gap)
	np.fill_diagonal(A,0)
	g	= ig.Graph.Adjacency(A.tolist(), mode= 'undirected')

	# building the coordinates of the vertices from layout
	xy  = np.array(g.layout(layout))
	x   = xy[:,0]
	y   = xy[:,1]
	
	# plots edges first (zorder=1)
	for i in range(n_seq):
		for j in range(i, n_seq):
			if A[i,j] == 1:
				plt.plot([x[i],x[j]],[y[i],y[j]], c='grey', zorder=1)

	# and vertices (zorder=2)
	plt.scatter(x,y, c=c_nodes, s=v_size, zorder=2)
	#
	if title:
		plt.title(title)
	#
	plt.show()
	
# ----------------------------------------------------------------------

