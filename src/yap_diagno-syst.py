#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
------------------------------------------------------------------------

How it is called

------------------------------------------------------------------------

usage  diagno-syst.py <basename>

requires that a yaml configuration file <basename>.yaml is available

What it does:   builds an inventory by mapping an environmental sample
                on a reference database
                
This program is part of yapsh/tapotu suite, and inherits of all its practises.

------------------------------------------------------------------------

What is read from configuration file

------------------------------------------------------------------------

@ charfile  : the character file of the references
@ charname  : taxonomic level on which to label queries (a header of the character file)
@ gap_max   : upper limit for barcoding gap
@ tab       : if True, the array of inventories, one gap per column, is written
@ inventory : if True, the inventory derived from <Tab> is written (largest nb of reads per column)
@ sort      : if True, taxa in inventory sorted in decreasing order of nb of reads
@ stacks    : if True, the stacks per taxon will be written in a file per taxon in <stacks_taxon.fas>


------------------------------------------------------------------------

Notes

------------------------------------------------------------------------

-> the names of the output files are automatically generated
		and written or not according to the values of
		@ tab           the array of inventories, one gap per column,
		@ inventory     the inventory derived from <Tab>
		@ stacks        the stacks query reads per taxon, a file per taxon


-> The file names
@ fasfile   : fasta file (NGS) of the environmental sample
@ disfile   : the distance file between all queries and all references, sparse format
are built automatically from the basename


-> How it proceeds:

0 - loads the fasta file (if useful)
1 - loads the character file, and builds
2 - loads the distance file, built by mpi_disseq, sparse format
3 - annotates and builds the inventory, one per gap
4 - Organizes the inventories as a table
5 - Reduce phase: a single inventory

-> With more details ...

0 - loads the fasta file
    the fasta file must by in following format:
        - even line (0,2,4, ...): >seq_id
        - odd line  (1,3,5, ...): the sequence, without carriage return

1 - loads the character file, and builds
    ref_taxa    the taxa of column <charname> in character file
    ref_id      the corresponding reference seq_id

2 - loads the distance file, built by mpi_disseq, sparse format, 
    an array <Dis> with three columns
    - first     : queries   : query id
    - second    : ref       : reference id
    - third     : dis       : distance between both

3 - annotates and builds the inventory
    The objecive is to build a dictionary <taxon_seqids>
		key:	a taxon 
        value:	a list of strings ; all seqids annotated with the key
    This is done by a double loop:
		first 	on different gaps
			selects the rows in (queries, ref, dis) corresponding to a query

4 - Organizes the inventories as a table
    Res is a table with
        row         : taxa
        colums      : gaps
        Res[i,j]    : the number of reads of taxon <i> for gap <j>
    if Tab == True
        Res is written as <rep_fulldiagno.txt>

5 - Reduce phase: a single inventory
    the number of reads for the inventory is the maximum number over all gaps

last revision:  Dec 8, 2016, 23.08.31 (integration into yapshotu

# ----------------------------------------------------------------------

version 2, started Dec 2nd, 2021

Modifications :


"""


class diagno_syst():
	"""
	"""
	
	def __init__(self):
		"""
		"""
		print_header()

	# ----------------------------------------------------------------------

	def print_header():
		"""
		"""
		print("diagno-syst\n")
		print("Supervised clustering")
		print("builds an inventory by mapping an environmental sample")
		print("on a reference database")
		print("")
		print("Version: 0.0.2")
		print("Date: Dec 9, 2016 - revised March, 22, 2017 ; May 17, 2018 ; May 2nd, 2019, August 31st, 2023")
		print("Author: Alain Franc and Jean-Marc Frigerio")
		print("Maintainer: Alain Franc")
		print("Contact: alain.franc@inrae.fr")
		print("")
		print('Please cite: Frigerio & al. - 2016 - diagno-syst: a tool for')
		print('accurate inventories in metabarcoding - ArXiv:1611.09410')
		print("download @ http://arxiv.org/abs/1611.09410\n")

		# ----------------------------------------------------------------------



	#-----------------------------------------------------------------------
	#
	#        I - Loads parameters from configuration file
	#
	# ----------------------------------------------------------------------


	basename	= sys.argv[1]

	print("starting yap_diagno-syst ...")
	print("with basename:", basename)
	#
	print("loading yaml file ...")
	configfile		= basename + ".yaml"
	try:
		yam			= yaml.load(open(configfile), Loader=yaml.SafeLoader)
		print("[yap!]:yaml file loaded")
	except:
		print("yaml configuration file")
		print(configfile)
		print("does not exist")
		exit("program terminated")

	disfile				= basename + ".diagno.sw.dis"
	countfile			= basename + ".inv"
	taxa_seqids_file  	= basename + ".spt" 

	charfile	= yam['diagno_charfile']
	charname	= yam['diagno_charname']
	gap_max		= yam['diagno_gap_max']
	sort 		= yam['diagno_sort']


	fasfile				= None	# is however useful when allocating reads to taxa ...

	print("disfile      -> ", disfile)
	print("ref charfile -> ", charfile)


	# ----------------------------------------------------------------------
	#
	#       II - Loading fasta file of queries
	#
	# ----------------------------------------------------------------------

	if fasfile:
		# 0 - loading fasta file
		#
		#   note: dedat library is not used, as it is too slow
		#         the fasta file must by in following format:
		#           - even line (0,2,4, ...): >seq_id
		#           - odd line  (1,3,5, ...): the sequence, without carriage return

		print("\nloading fasta file ...")
		print("fasfile = ", fasfile)
		fasta       = np.loadtxt(fasfile, dtype=object)
		n           = len(fasta)
		query_seqid = [fasta[i][1:] for i in range(0, n, 2)]
		query_word  = [fasta[i] for i in range(1, n, 2)]
		query_len   = [len(word) for word in query_word]
		#plt.hist(query_len, 50, normed=1, facecolor='g', alpha=0.75)
		#plt.show()
		print("fasta file loaded")


	# ----------------------------------------------------------------------
	#
	#      III - loads character file
	#
	# ----------------------------------------------------------------------

	# 1 - loading character file
	#       ref_taxa    : list of taxa of <charname> in character file
	#       ref_id      : corresponding seq_id

	print("\nloading character file ...")
	Char            = np.loadtxt(charfile, delimiter='\t', comments='#', dtype=object).astype(str)
	print("Character file loaded")
	char_headers    = list(Char[0,:])
	k               = char_headers.index(charname)
	ref_taxa        = Char[1:,k]
	ref_id          = list(Char[1:,0])


	# ----------------------------------------------------------------------
	#
	#      IV - loads queries x references distance file, sparse format
	#
	# ----------------------------------------------------------------------

	"""
	Distance file is in a sparse format
	Dis has three columns, extracted here
	   ______________________________________________
	   column      | variable  | what it is
	   _______________________________________________ 
	   - first     | queries   | query id
	   - second    | ref       | reference id
	   - third     | dis       | distance between both
	"""

	print("loading distance file; this may take a while ...")
	Dis         = np.loadtxt(disfile, delimiter="\t", dtype=object).astype(str)
	n           = Dis.shape[0]                      # number of rows in DIS
	queries     = Dis[:,0]                          # query id's
	#query_set   = list(set(queries))                # list of query id's (uniq)
	ref         = Dis[:,1]                          # references which match per query
	dis         = np.array(Dis[:,2],dtype=float)    # distance of match
	print("distance file loaded ; ", n, "matched pairs\n")

	# ----------------------------------------------------------------------
	#
	#      V - Annotation and inventory
	#
	# ----------------------------------------------------------------------

	"""
	all_taxa_in_inv    list      list of reference taxa with at list one hit for at least one gap
	dic_inv        dictionary    one dictionary per gap
					  keys:   referece taxa with hits on at lest one query
					  value:  number of queries with hit on the reference taxon
				  
	"""

	# query_set  = list(set(queries))
	# all_taxa_in_inv  = []
	# pl_inv      = []
	# query_annot = {}

	# output of this section:
	taxon_seqids  = {}    	# key: a taxon 
							# value: a list of strings ; all seqids annotated with the key
				
	for gap in list(range(1+gap_max)):    # loop over all gaps from 0 to gap_max (included)
	  #
	  print("gap = " + str(gap) + " ...")
	  #dic_inv    = {}
	  
	  # slices the rows of sample distance file by queries
	  # query_rows	: all rows with same <query> as from ... to ...
	  # which		: list of all rows with same <query> with distance <= gap (with a hit, a row is a hit)
	  # q_taxa		: all subjects 
	  
	  i      = 0
	  j      = 0
	  while j < n:
		query   = queries[i]    # which query; queries[] comes from IV
		while queries[j]==query:
		  if j == n-1:
			break
		  else:
			j    = j+1
		i_deb    = i          # first row with this query
		i_fin    = j          # last  row with this query 
		i      = j+1
		j      = i
		#
		query_rows  = range(i_deb, i_fin+1)              			# rows with a given query
		which       = [ii for ii in query_rows if dis[ii] <= gap]   # those sequences at distance <= gap
		
		# if at least one hit
		#  <annot> is the name which will be given to the <query>
		#        - "taxon"      	if hits on this reference taxon only
		#        - "ambiguous"    	if hits on several reference taxa
		#        - "unknown"      	if no hit, ? (line 399)
		if len(which) > 0:
		  q_taxa  = [""]*len(which)    # list of reference taxa with at least one hit
		  ii        = 0
		  for row in which:
			try:
			  k				= ref_id.index(ref[row])
			  taxon        	= ref_taxa[k]
			  q_taxa[ii]    = taxon
			except:
			  q_taxa[ii]  = "unknown"
			ii  = ii+1
		  q_taxa_set  = list(set(q_taxa))	# set of reference taxa with at least one hit
		  if len(q_taxa_set)==1:            # if only one reference taxon
			annot  = q_taxa_set[0]          # transfer of the taxon for annotation
			#
			if annot not in ["ambiguous", "unknown"]:
			  if annot not in taxon_seqids.keys():
				taxon_seqids[annot]= [query]
			  else:
				taxon_seqids[annot].append(query)
		  else:                # if not
			annot  = "ambiguous"      #    ambiguous
		else:
		  annot  = "unknown"          # no hit
		  
		  
		# now, dictionary <taxon_seqids> has been built. It contains all the required information,
		# which is processed in next section
	 
	 
	# a sequence is repeated as many times as gaps it maps on a reference
	# this is corrected here  
	print("\nFin section V\n")  
	for key in list(taxon_seqids.keys()):
		pl_seqid			= taxon_seqids[key]
		uniq				= list(set(pl_seqid))
		taxon_seqids[key]	= uniq
		print(key, len(taxon_seqids[key])) 
	 
	# ----------------------------------------------------------------------
	#
	#      VI - writing output files (all gaps)
	#
	# ----------------------------------------------------------------------

	print("\nand now ... VI\n")

	# couting the seqids annotated wih a taxa

	dic_count  = {}  	# key: a taxon ; value: an integer; the number of seqids annotated with this taxon
	for key in list(taxon_seqids.keys()):
		dic_count[key]  = len(taxon_seqids[key]) 
		#print(key, dic_count[key])

	print("writing output files ...")

	#countfile      		= "test_diagno-syst/" + fullname + ".inv"
	#taxa_seqids_file  	= "test_diagno-syst/" + fullname + ".spt" 
	sort        		= True


	### ----------------- counts per taxon

	taxa    	= list(dic_count.keys())
	nb_reads  	= [dic_count[key] for key in taxa]
	n_taxa    	= len(taxa)


	# if sort == True, sorts the taxa in decreasing number of number of reads
	if sort:
		ind         = np.argsort(nb_reads).tolist()
		ind         = ind[::-1]
		nb_reads    = [nb_reads[i] for i in ind]
		#print("do ..") ; taxa_       = [taxa[i] for i in ind] ; print(".. done")
		taxa = sorted(dic_count, key=dic_count.get,reverse=True)
		print("\ntaxa triés?\n")
		#print(taxa)
	#print("\nafter sort\n")
	#for i, tax in enumerate(taxa):
		#print(tax, nb_reads[i])

	print("inventory in", countfile)
	with open(countfile,"w") as f_inv:
		headers   = "taxon" +"\t" + "nb"
		f_inv.write(headers + "\n")
		for i in range(n_taxa):
			item  = taxa[i] + "\t" + str(nb_reads[i])
			f_inv.write(item + "\n")
		
		
	### ------------ seqids per taxon

	print("seqids per taxon in", taxa_seqids_file)  
	with open(taxa_seqids_file,"w") as f_taxa_seqid:
		for taxon in taxa:
			val    		= taxon_seqids[taxon]
			#print(taxon, len(val))
			val_str  	= ("\t").join(val)
			item  		= taxon + "\t" + val_str
			f_taxa_seqid.write(item + "\n")


# ======================================================================
#
#           That's all folks!
#
# ======================================================================
      
 









