#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(à rédiger ...)

@ charfile  : the character file of the references
@ charname  : taxonomic level on which to label queries (a header of the character file)
@ gap_max   : upper limit for barcoding gap
@ tab       : if True, the array of inventories, one gap per column, is written
@ inventory : if True, the inventory derived from <Tab> is written (largest nb of reads per column)
@ sort      : if True, taxa in inventory sorted in decreasing order of nb of reads
@ stacks    : if True, the stacks per taxon will be written in a file per taxon in <stacks_taxon.fas>

af, 24.02.08

"""

import numpy as np
from operator import itemgetter
#
import yap_classes as yap


class diagno_syst():
	"""
	"""
	
	def __init__(self, basename, reference, charname, gap_max):
		"""
		"""
		self.fasfile		= basename + ".fas"
		self.disfile		= basename + ".diagno.sw.dis"
		self.invfile		= basename + "." + charname + ".inv"
		self.ref_charfile	= reference + ".char"
		self.ref_fasfile	= reference + ".fas"
		self.charname		= charname
		self.gap_max		= gap_max
	
	# ----------------------------------------------------------------------

	def print_diagno_syst_header(self):
		"""
		"""
		print("diagno-syst\n")
		print("Supervised clustering")
		print("builds an inventory by mapping an environmental sample")
		print("on a reference database")
		print("")
		print("Version: 0.0.3")
		print("Date: Dec 9, 2016 - 17.22.03, 18.05.17, 19.05.02, 23.08.31, 24.02.08")
		print("Author: Alain Franc and Jean-Marc Frigerio")
		print("Maintainer: Alain Franc")
		print("Contact: alain.franc@inrae.fr")
		print("")
		print('Please cite: Frigerio & al. - 2016 - diagno-syst: a tool for')
		print('accurate inventories in metabarcoding - ArXiv:1611.09410')
		print("download @ http://arxiv.org/abs/1611.09410\n")

	# ------------------------------------------------------------------
	
	def load_fasta_query(self):
		"""
		"""
		fas			= yap.fasta(fasfile=self.fasfile)
		#
		self.query_seqid = fas.seq_id
		self.query_word  = fas.seq_words
		self.query_len   = fas.seq_len
		print("fasta file loaded")
		#

	# ------------------------------------------------------------------
	
	def load_ref_charfile(self):
		"""loading reference character file
		
		return
		@ ref_taxa    : list of taxa of <charname> in character file
		@ ref_id      : corresponding seq_id
		
		af, 24.02.07
		"""

		print("\nloading character file ...")
		Char			= np.loadtxt(fname=self.ref_charfile, delimiter='\t', comments='#', dtype=object).astype(str)
		print("Character file loaded")
		char_headers		= list(Char[0,:])
		k					= char_headers.index(self.charname)
		self.ref_taxa		= Char[1:,k]
		self.ref_id			= list(Char[1:,0])
		self.set_ref_taxa	= list(set(self.ref_taxa)) 
		self.n_ref_taxa		= len(self.set_ref_taxa)
		#
	
	# ------------------------------------------------------------------
	
	def load_disfile(self):
		"""loads three coluns distance file
		Distance file is in a sparse format
		Dis has three columns, extracted here
		   ______________________________________________
		   column      | variable  | what it is
		   _______________________________________________ 
		   - first     | queries   | query id
		   - second    | ref       | reference id
		   - third     | dis       | distance between both
		"""

		print("\nloading distance file; this may take a while ...")
		Dis				= np.loadtxt(self.disfile, delimiter="\t", dtype=object).astype(str)
		self.n			= Dis.shape[0]                      # number of rows in DIS
		self.queries	= Dis[:,0]                          # query id's
		self.ref		= Dis[:,1]                          # references which match per query
		self.dis		= np.array(Dis[:,2],dtype=float)    # distance of match
		print("distance file loaded ; ", self.n, "matched pairs\n")
		#

	# ----------------------------------------------------------------------
	
	def get_range(self, i, n, queries):
		"""gets the range  row i_deb -> row i_fin (included) with a same query in Dis
		
		@ i			strtaing from row i
		@ n 		number of rows in Dis
		@ queries	ref_id as first column of Dis
		"""
		
		j		= i
		query   = queries[i]    # which query at row i; 
		while queries[j]==query:
			if j == n-1:
				break
			else:
				j	= j+1
		#
		query_rows	= range(i,j)		# rows with a given query
		i_new		= j + 1
		#
		return query, query_rows, i_new
		
	# ------------------------------------------------------------------
	
	def get_ref_taxa(self, which, ref, ref_id, ref_taxa):
		"""provides the list of reference taxa mapping on  the query
		"""
		q_taxa  = [""]*len(which)    # list of reference taxa with a hit
		for i, row in enumerate(which):
			ref_i		= ref[row] 
			try:
				k			= ref_id.index(ref_i)
				q_taxa[i]	= ref_taxa[k]
			except:
				q_taxa[i]	= "no_hit"
		#
		return q_taxa
		
	
	# ------------------------------------------------------------------
	
	def build_Tab(self):
		"""
		"""
		names 		= self.set_ref_taxa + ['ambiguous','no_hit']
		n_ref		= len(self.set_ref_taxa) + 2
		Tab			= np.zeros((n_ref, 1+self.gap_max)) 
		#
		for gap in list(range(1 + self.gap_max)):    # loop over all gaps from 0 to gap_max (included)
			#
			print("gap = " + str(gap) + " ...")
			#
			# selects the box in Dis with one query
			#
			i	= 0
			j	= 0
			while j < self.n:
				query, query_rows, i_new	= self.get_range(i, self.n, self.queries)		# rows of Dis with "query" in column 0
																							# query to be annotated
				i							= i_new
				j							= i_new
				which						= [ii for ii in query_rows if self.dis[ii] <= gap]	# the rows at distance <= gap
				#
				if len(which) > 0:
					q_taxa					= self.get_ref_taxa(which, self.ref, self.ref_id, self.ref_taxa)	# list of reference taxa where query has one hit
					set_q_taxa				= list(set(q_taxa))
					if len(set_q_taxa)==1:
						annot	= set_q_taxa[0]
					else:
						annot	= "ambiguous"
					#print(query, set_q_taxa, annot)
				else:
					annot		= "no_hit"
				k				= names.index(annot)
				Tab[k, gap]		= Tab[k,gap] + 1
			#
			vec			= Tab[:,gap]
			vec_taxon	= int(np.sum(vec[:-2])) 
			vec_ambi	= int(vec[n_ref-2])
			vec_nohit	= int(vec[n_ref-1])
			print([vec_taxon, vec_ambi, vec_nohit])
		#
		self.Tab	= Tab
		
	# ------------------------------------------------------------------
	
	def get_gap_optim(self):
		"""
		"""
		n				= self.Tab.shape[0]
		n_ambi			= self.Tab[n-2,:]
		n_nohit			= self.Tab[n-1,:]
		n_out			= n_ambi + n_nohit
		pl_n_out		= n_out.tolist()
		self.gap_optim	= pl_n_out.index(min(n_out))
		print("optimal gap is", self.gap_optim)	
	
	# ------------------------------------------------------------------
	
	def build_inventory(self):
		"""
		"""	
		print("extracting taxa with non zero counts")
		counts		= self.Tab[:-2,self.gap_optim].tolist()
		taxa		= self.set_ref_taxa
		which		= [i for i,val in enumerate(counts) if val > 0]
		counts 		= [int(counts[i]) for i in which]
		taxa 		= [taxa[i] for i in which]
		print(len(counts), "counts and", len(taxa), "taxa")
		#
		print("sorting from most abundent to less abundent")
		ind, _	= zip(*sorted(enumerate(counts), key=itemgetter(1)))
		ind		= ind[::-1]
		counts_sorted		= [counts[i] for i in ind]
		taxa_sorted			= [taxa[i] for i in ind]
		n_item				= len(ind)
		#
		print("writing inventory to file", self.invfile)
		with open(self.invfile,"w") as f_inv:
			item 	= "taxon" + "\t" + "numbers"
			f_inv.write(item + "\n")
			for i in range(n_item):
				taxon	= taxa_sorted[i]
				nb		= str(counts_sorted[i])
				item	= taxon + "\t" + nb
				f_inv.write(item + "\n")

	# ==================================================================
	#
	#				main()
	#
	# (même s'il n'y a pas de main() dans une classe ...)
	#
	# ==================================================================
	
	"""
	variables which are attributes of the class 
	
	from loading fasta file
		@ self.query_seq_id				list of strings			seq_id of query fasta file
		@ self.query_words				list of strings			sequences in query fasta file
		@ self.query_len				list of integers		lengths of sequences in query fasta file
	
	from loading reference charfile
		@ self.ref_taxa    				list of strings			taxa of <charname> in character file (the column)
		@ self.ref_id      				list of strings			corresponding seq_id		
		@ self.set_ref_taxa				list of strings			the set of ref_taxa
		@ self.n_ref_taxa				integer					number of diferent reference taxa

		
	from loading disfile
		@ self.n						integer					number of rows in Dis
		@ self.queries					list of st rings		queries (column 1)
		@ self.ref						list of strings			ref id (column 2)
		@ self.dis						list of floats			distances (column 3)
		
	from build_Tab
		@ self.Tab						numpy array				table of counts per taxon and gap
		
	from get_gap_optim
		@ self.gap_optim				integer					optimal gap (maximum of annotated reads)
	

	"""
	
	def diagno_syst(self):
		"""
		"""
		self.print_diagno_syst_header()
		
		### ------------------ loading files
		
		self.load_fasta_query()		# query fasta file
		self.load_ref_charfile()	# characters in reference
		self.load_disfile()			# queries x references distance file, sparse format
		
		### ------------------ annotation and inventory 
		
		print("\nstarting annotation ...")
		self.build_Tab()			# table with annotation for all gaps
		self.get_gap_optim()		# finding the gap with maximum of annotated reads
		self.build_inventory()		# building and writing the inventory
		
	# ------------------------------------------------------------------

# ======================================================================
#
#           That's all folks!
#
# ======================================================================
      
 









