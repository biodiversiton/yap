# ==================================================================
#
#			mapping
# 
# ==================================================================


import sys
import os
import yap_classes as yap
from datetime import datetime
import time
from print_color import print as print_col
import yap_lib_utils as yut

#from yap_lib import *
general	= "general"
mapping = "mapping"

# display version upon opening
__version__ = str(datetime.fromtimestamp(os.stat(__file__).st_mtime))
print_col(f'loading yap_lib/mapping, version {__version__[:16]}'.rjust(55), color="cyan")

# ==================================================================
#
#			mapping
# 
# ==================================================================

"""


"""

def blast_queries(self):
	"""Produces a file with quality of the local alignment of queries versus references

	requires
	--------
	- a fasta file <basename>.fas of queries

	returns
	-------
	- writes a blastn file <basename>.blastn: one row per query, and 11 columns

	Notes
	-----

	af, 24.02.26, 24.04.22, 24.09.12,
	"""

	# where it is ...

	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)

	# parameters
	yut.print_parameters(f_name)


	# mapping (general)
	reference		= self.yam[mapping]['reference']			; print("reference			", reference)
	meth			= self.yam[mapping]['meth']				; print("meth				", meth)
	 
	# specific
	num_threads		= self.yam[f_name]['num_threads']		; print("num_threads		", num_threads)
	num_alignments	= self.yam[f_name]['num_alignments']	; print("num_alignments		", num_alignments)

	if self.questions:
		if not yut.check_parameters(): return

	### ----------------- building files

	queries_fasfile	= self.fasfile
	print("query fasfile is", self.fasfile)
	#
	basename		= self.basename
	blastfile		= f'{self.basename}.blastn'
	print("blast outfile is", blastfile)
			 
	### ----------------- running

	mapp	= yap.mapping(basename, reference, meth)
	print_col("blasting ...", color="cyan")
	t_init	= time.time()
	mapp.blast_queries(blastfile, num_threads, num_alignments)
	t_end	= time.time()
	print_col("...took", t_end - t_init, "seconds", color="cyan")
		

# ----------------------------------------------------------------------

def best_hit(self):
	"""

	Notes
	-----

	af, 24.02.26, 24.04.22, 24.09.12,
	"""

	# where it is ...

	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)

	# parameters
	yut.print_parameters(f_name)


	# mapping (general)
	reference		= self.yam['mapping']['reference']			; print("reference			", reference)
	meth			= self.yam['mapping']['meth']				; print("meth				", meth)
	 
	# specific

	if self.questions:
		if not yut.check_parameters(): return

	### ----------------- building files

	basename		= self.basename
	mapfile			= f'{self.basename}.{meth}'
	besthit_file	= f'{mapfile}.besthit'
			 
	### ----------------- running
	
	mapp	= yap.mapping(basename, reference, meth)
	mapp.get_besthit(mapfile=mapfile, besthit_file=besthit_file)

# ----------------------------------------------------------------------

def annotating(self):
	"""

	Notes
	-----

	af, 24.09.12,
	"""

	# where it is ...

	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)

	# parameters
	yut.print_parameters(f_name)


	# mapping (general)
	reference		= self.yam['mapping']['reference']			; print("reference			", reference)
	meth			= self.yam['mapping']['meth']				; print("meth				", meth)
	 
	# specific
	varname		= self.yam[f_name]['varname']					; print("varname			", varname)

	if self.questions:
		if not yut.check_parameters(): return

	### ----------------- building files

	basename		= self.basename
	#
	mapfile			= f'{self.basename}.{meth}'
	besthit_file	= f'{mapfile}.besthit'
	annot_file		= f'{besthit_file}.{varname}'
			 
	### ----------------- running
	
	mapp			= yap.mapping(basename, reference, meth)
	ref_charfile	= mapp.ref_charfile
	mapp.annotates_with_besthit(varname=varname, besthit_file=besthit_file, annot_file=annot_file)
	
# ----------------------------------------------------------------------

def inventory(self):
	"""

	Notes
	-----

	af, 24.09.12,
	"""

	# where it is ...

	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)

	# parameters
	yut.print_parameters(f_name)


	# mapping (general)
	reference		= self.yam['mapping']['reference']			; print("reference			", reference)
	meth			= self.yam['mapping']['meth']				; print("meth				", meth)
	 
	# specific
	varname		= self.yam[f_name]['varname']					; print("varname			", varname)

	if self.questions:
		if not yut.check_parameters(): return

	### ----------------- building files

	mapfile			= f'{self.basename}.{meth}'
	besthit_file	= f'{mapfile}.besthit'
	annot_file		= f'{besthit_file}.{varname}'
	invfile			= f'{annot_file}.inv'

			 
	### ----------------- running
	
	mapp			= yap.mapping(self.basename, reference, meth)
	mapp.build_inventory(annot_file, invfile)
