# ==================================================================
#
#			methods for point clouds
# 
# ==================================================================

import os
import sys
from datetime import datetime

import yap_classes as yap
import yap_lib_utils as yut


from print_color import print as print_col



general	= "general"


#from yap_lib import *

# display version upon opening
__version__ = str(datetime.fromtimestamp(os.stat(__file__).st_mtime))
print_col(f'loading yap_lib/point_cloud, version {__version__[:16]}'.rjust(55), color="cyan")

# ======================================================================
#
#				pc_scatter()
#
# ======================================================================


def pc_scatter(self):
	"""Scatter plot after MDS or t-SNE

	Parameters of the method
	------------------------
	- axis_i		; integer	; first axis (starting at 1)
	- axis_j		; integer	; second axis
	- varname		; string	; taxon for coloring dots

	Parameters for display
	----------------------
	- color_range	; string	; discrete or continuous
	- legend		; boolean	; whether to display the legend for colors
	- d_size		; integer	; dot size
	- d_size_small	; integer	; small dot size
	- d_size_large	; integer	; large dot size
	- x11			; boolean	; whether to display the plot on the screen
	- save 			; string	; whether to save the plot as a file
	- fmt			; string	; format of file with saved plot

	Parameters for I/O
	------------------
	- coord			; string 	; origine of coordfile (mds or tsne)
	- dis_algo		; string 	; method with which distances have been computed
	- itype			; string	; format of coordfile		


	I/O suffixes
	------------
	infile	: .coord
	outfile	: 

	Notes
	-----

	There are several parameters which can be ordered into families:
	-> coord, dis_algo, itype: formats of I/O files
		- mds or tsne for 'coord' (which method has issued the coordinate file)
		- usually tsv for itype (type of the coordinate file, usually ascii)
		- sw, nw, kx for dis_algo
	-> axis_i, axis_j: which axis to draw: is to draw: axis_i (x axis) and axis_j (y axis)
		axis are counted naturally from 1 to r; yap translates into python (from 0 onwards)
	-> varname, legend : how to color dots if a charfile is available
	-> d_size, d_size_small, d_size_large: dot size
		large dots are those which are colored; small ones and those which are not colored
	-> x11, save, fmt: for displaying/saving the plot

	af, 22.05.16, 22.08.18, 23.05.03, 23.08.21
	""" 

	# where it is ...

	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)

	# parameters
	yut.print_parameters(f_name)

	# general 
	dis_algo		= self.yam[general]["dis_algo"]			; print("\ndis_algo     ", dis_algo)
	coord			= self.yam[general]['coord']			; print("coord        ", coord)
	varname			= self.yam[general]['varname']			; print("varname      ", varname)
	legend			= self.yam[general]['legend']			; print("legend       ", legend)
	x11				= self.yam[general]['x11']				; print("x11          ", x11)
	save 			= self.yam[general]['save']				; print("sav          ", save)
	fmt				= self.yam[general]['fmt']				; print("fmt          ", fmt)

	# specific 
	color_range		= self.yam[f_name]['color_range']		; print("color_range  ", color_range)
	colorfile		= self.yam[f_name]['colorfile']			; print("colorfile    ", colorfile)
	axis_i			= self.yam[f_name]['axis_i']			; print("axis_i       ", axis_i)
	axis_j			= self.yam[f_name]['axis_j']			; print("axis_j       ", axis_j)

	dot_size 	= self.yam[f_name]['dot_size']				; print("dot_size       ", dot_size)
	d_size_small	= self.yam[f_name]['d_size_small']		; print("d_size_small ", d_size_small)
	d_size_large	= self.yam[f_name]['d_size_large']		; print("d_size_large ", d_size_large)



	### ----------------- basic checks  

	# test whether coordfiles exist
	#
	# coordfiles are expected to be in tsv format

	mds_coordfile	= self.basename + "." + dis_algo + ".mds"
	tsne_coordfile	= self.basename + "." + dis_algo + ".tsne"
	coa_coordfile	= self.basename + "." + dis_algo + ".coa"

	if coord=="mds":
		yut.check_file_exists(mds_coordfile)

	if coord=="tsne":
		yut.check_file_exists(tsne_coordfile)
		
	if coord=="coa":
		yut.check_file_exists(coa_coordfile)
	#
	if varname:
		yut.check_file_exists(self.charfile)
		
	# checks parameters

	if self.questions:
		if not yut.check_parameters(): return

### ------------------ running 

# color selection

	if colorfile:
		colorfile			= self.basename + "_" + varname + ".col"
		if varname:		
			char 			= yap.charmat(charfile=self.charfile)
			v_tax			= char.get_tax(varname)
			v_col, dicol	= yap.set_colors_from_file(v_tax, colorfile)	
			#
			dot_size	= [-1]*len(v_col)
			for i, item in enumerate(v_col):
				if v_col[i] == "grey":
					dot_size[i]	= d_size_small
				else:
					dot_size[i]	= d_size_large
			
		else:
			print_col("you should select a varname", color="red")
	else:
		dicol			= {}
		#
		if varname:	
			char 			= yap.charmat(charfile=self.charfile)
			v_tax			= char.get_tax(varname)
			if color_range=="discrete":
				v_col, dicol	= yap.def_colors(v_tax)
			if color_range=="continuous":
				v_col			= v_tax
		else:
			v_col	= "blue"		

# instance of class "point_cloud"


	os.makedirs("plots", exist_ok=True)
	#
	if coord=="tsne":
		pc			= yap.point_cloud(coordfile=tsne_coordfile)
		base4plot	= "plots/" + tsne_coordfile + "." + str(axis_i) + "." + str(axis_j)
		plotfile	=  base4plot + "." + fmt
		print("plotfile is:  ", plotfile)
	#
	if coord=="mds":
		pc			= yap.point_cloud(coordfile=mds_coordfile)
		base4plot	= "plots/" + mds_coordfile + "." + str(axis_i) + "." + str(axis_j) 
		plotfile	= base4plot + "." + fmt
		print("plotfile is:  ", plotfile)
	#
	if coord=="coa":
		pc			= yap.point_cloud(coordfile=coa_coordfile)
		base4plot	= "plots/" + coa_coordfile + "." + str(axis_i) + "." + str(axis_j) 
		plotfile	= base4plot + "." + fmt
		print("plotfile is:  ", plotfile)
	#
	if self.questions:
		go = yut.testif(plotfile)
	else:
		go = True						

	if go:
		print("plotting axis", axis_i, "and axis", axis_j)
		if save:
			imfile 	= base4plot
		else:
			imfile	= None
		pc.scatter(axis_i-1,axis_j-1, v_col=v_col, dot_size=dot_size, legend=legend, dicol=dicol, color_range=color_range, x11=x11, imfile=imfile,fmt=fmt)
	else:
		return

# ------------------------------------------------------------------


def pc_splines(self):
	"""Displays parallel coordinates of a point cloud with splines

	Parameters of the method
	------------------------
	- n_axis_splines	; integer	; number of coordinates to take into account

	Parameter for display
	---------------------
	- varname			; string	; taxon for coloring splines
	- legend			; boolean	; whether to display the color legend
	- x11				; boolean	; whether to display the splines on the screen
	- save				; boolean	; whether to save the plot
	- fmt				; string	; format for the saved plot

	Parameters fo I/O
	-----------------
	- coord				; string	; which method has issued the coordinare file
	- dis_algo			; string	; method with which distances have been computed

	Suffixes for I/O
	----------------
	infile	: .coord, .char

	Notes
	-----
	-> coord			: mds or tsne (which method has issued the coordinate file)
	-> itype			: usually tsv (type of the coordinate file, usually ascii)
	-> dis_algo			: sw, nw, kx
	-> varname, legend : how to color dots if a charfile is available

	af, 22.05.16; 23.05.04, 23.08.21
	""" 

	# where it is ...

	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)

	# parameters
	yut.print_parameters(f_name)

	# general 
	dis_algo		= self.yam[general]["dis_algo"]		; print("dis_algo         ", dis_algo)
	coord			= self.yam[general]['coord']		; print("\ncoord        ", coord)
	varname			= self.yam[general]['varname']		; print("varname        ", varname)
	legend			= self.yam[general]['legend']		; print("legend         ", legend)
	save			= self.yam[general]['save']			; print("save           ", save)
	fmt				= self.yam[general]['fmt']			; print("fmt            ", fmt)
			
	# specific

	n_axis_splines	= self.yam[f_name]['n_axis_splines']	; print("n_axis_splines ", n_axis_splines)


	### -------------------- basic checks

	# test whether coordfiles exist
	#
	# coordfiles are expected to be in tsv format

	mds_coordfile	= self.basename + "." + dis_algo + ".mds"
	coa_coordfile	= self.basename + "." + dis_algo + ".coa"

	if coord=="mds":
		yut.check_file_exists(mds_coordfile)

	if coord=="tsne":
		print_col("\n-------------------------------------------", color="red")
		print_col("The number of axis cannot exceed 3 for tsne", color="red")
		print_col("parallel coordinates are relevant for mds only", color="red")
		print_col("-------------------------------------------\n", color="red")
		return

	if coord=="coa":
		yut.check_file_exists(coa_coordfile)

	if varname:
		yut.check_file_exists(self.charfile)
		
	# checks parameters

	if self.questions:
		if not yut.check_parameters(): return


	### ------------------------ running

	# color selection

	dicol			= {}
	#
	if varname:		
		char 			= yap.charmat(charfile=self.charfile)
		v_tax			= char.get_tax(varname)
		v_col, dicol	= yap.def_colors(v_tax)
	else:
		v_col = "blue"

	# instance of class "point_cloud"


	os.makedirs("plots", exist_ok=True)
	#
	if coord=="mds":
		base4plot	= "plots/" + mds_coordfile + ".parallel." + str(n_axis_splines)  
		pc			= yap.point_cloud(coordfile=mds_coordfile)
		plotfile	= base4plot + "." + fmt
		print("plotfile is:  ", plotfile)
		#
		if self.questions:
			go = yut.testif(plotfile)
		else:
			go = True			
	#
	if coord=="coa":
		base4plot	= "plots/" + coa_coordfile + ".parallel." + str(n_axis_splines)  
		pc			= yap.point_cloud(coordfile=mds_coordfile)
		plotfile	= base4plot + "." + fmt
		print("plotfile is:  ", plotfile)
		#
		if self.questions:
			go = yut.testif(plotfile)
		else:
			go = True			
	# go ...

	if go:
		print_col("plotting parallel coordinates with", n_axis_splines, "axes", color="green")
		if save:
			imfile 	= plotfile
		else:
			imfile	= None
		pc.splines(v_col, n_axis=n_axis_splines, legend=legend, dicol=dicol, title=None, x11=True, imfile=imfile, fmt=fmt)	
	else:
		return

	# ------------------------------------------------------------------

def pc_heatmap(self):
	"""Heatmap of the coordinate file

	Parameters of the method
	------------------------
	- axis_i		; integer	; first axis (starting at 1)
	- axis_j		; integer	; second axis
	- bins			; integer	; number of bins for the 2D histogram		

	Parameters for displaying
	-------------------------
	- cmap			; string	; color map for coloring pixels
	- x11			; boolean	; whether to display the plot on the screen
	- save 			; string	; whether to save the plot as a file
	- fmt			; string	; format of file with saved plot		

	Parameters for I/O
	------------------
	- coord			; string 	; origine of coordfile (mds or tsne)
	- dis_algo		; string 	; method with which distances have been computed
	- itype			; string	; format of coordfile

	I/O suffixes
	------------
	infile	: .coord
	outfile	: .hm.<fmt>, e.g. hm.pdf

	Notes
	-----
	This method should be preferred for plotting coordinate files with 
		many items (like beyond several thousands)
	-> The method starts from the extraction of coordinates i and j of the coordinate file.
	-> A 2D histogram with number of bins indicated by 'bins' is built
		i.e. the number of reads projected on a same pixel
		it is log transformed
	-> This number is colored according to the colormap
	 


	af, 23.02.09 ; 23.05.04, 23.08.21
	"""

	# where it is ...

	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)

	# parameters
	yut.print_parameters(f_name)

	# general 
	coord 			= self.yam[general]['coord']		; print("\ncoord           ", coord)
	dis_algo		= self.yam[general]['dis_algo']		; print("dis_algo        ", dis_algo)
	x11				= self.yam[general]['x11']			; print("x11             ", x11)
	save			= self.yam[general]['save']			; print("save            ", save)
	fmt				= self.yam[general]['fmt']			; print("fmt             ", fmt)

	# specific

	axis_i 			= self.yam[f_name]['axis_i']		; print("axis_1          ", axis_i)
	axis_j 			= self.yam[f_name]['axis_j']		; print("axis_2          ", axis_j)
	bins 			= self.yam[f_name]['bins']			; print("bins            ", bins)
	cmap			= self.yam[f_name]['cmap']			; print("cmap            ", cmap)
	log				= self.yam[f_name]['log']			; print("log             ", log)


	### ---------------- basic checks

	coordfile	= self.basename + "." + dis_algo + "." + coord

	yut.check_file_exists(coordfile)
	if self.questions:
		if not yut.check_parameters(): return

	### --------------- running

	pc	= yap.point_cloud(coordfile=coordfile)
	pc.heatmatrix(i=axis_i-1, j=axis_j-1, bins=bins, log=log)
	if save:
		imfile = "plots/" + coordfile + ".hm"
	else:
		imfile = None
	pc.heatmap(cmap=cmap, x11=x11, imfile=imfile, fmt=fmt)

# ------------------------------------------------------------------

def pc_spikes(self):
	"""3D spikes of the coordinate file

	Parameters of the method
	------------------------
	- axis_i		; integer	; first axis (starting at 1)
	- axis_j		; integer	; second axis
	- bins			; integer	; number of bins for the 2D histogram

	Parameters for displaying
	-------------------------
	- cmap			; string	; color map for coloring pixels
	- x11			; boolean	; whether to display the plot on the screen
	- save 			; string	; whether to save the plot as a file
	- fmt			; string	; format of file with saved plot		

	Parameters for I/O
	------------------
	- coord			; string 	; origine of coordfile (mds or tsne)
	- dis_algo		; string 	; method with which distances have been computed
	- itype			; string	; format of coordfile


	I/O suffixes
	------------
	infile	: .coord
	outfile	: .spikes.<fmt>, e.g. .spikes.png


	Notes
	-----

	-> This method is very close to pc_heatmap
		- the same information is plotted
		- i.e. a 2D histogram of read density per pixel
	-> Instead of a heatmap, this displays a 3D plot with the 
		value of the 2D histogeam as elevation
	-> parameters are common with pc_heatmap

	af, 23.02.09 ; 23.05.04, 23.08.21
	"""

	# where it is ...

	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)

	# parameters
	yut.print_parameters(f_name)

	# general 
	dis_algo		= self.yam[general]['dis_algo']		; print("\ndis_algo        ", dis_algo)
	coord 			= self.yam[general]['coord']		; print("coord           ", coord)
	x11				= self.yam[general]['x11']			; print("x11             ", x11)
	save			= self.yam[general]['save']			; print("save            ", save)
	fmt				= self.yam[general]['fmt']			; print("fmt             ", fmt)

	# specific

	axis_i 			= self.yam[f_name]['axis_i']		; print("axis_1          ", axis_i)
	axis_j 			= self.yam[f_name]['axis_j']		; print("axis_2          ", axis_j)
	bins 			= self.yam[f_name]['bins']			; print("bins            ", bins)


	### ---------------- basic checks

	mds_coordfile	= self.basename + "." + dis_algo + ".mds"

	yut.check_file_exists(mds_coordfile)
	if self.questions:
		if not yut.check_parameters(): return

	### --------------- running

	pc	= yap.point_cloud(coordfile=mds_coordfile)
	#pc.heatmatrix(i=axis_i-1, j=axis_j-1, bins=bins)
	if save:
		imfile = "plots/" + mds_coordfile + ".spikes"
	else:
		imfile = None
	pc.spikeshow(i=axis_i, j=axis_j, bins=bins, title=None, x11=x11, imfile=imfile, fmt="png")
