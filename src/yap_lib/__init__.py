#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""
af, 23.06.01, 24.01.12, 24.01.16, 24.02.07, 24.11.19
"""
import sys
import os
import traceback
import yaml
import glob
import shutil
from datetime import datetime
from print_color import print as print_col

import numpy as np
import scipy.cluster.hierarchy as sch   # for handling linkage matrices
import matplotlib.pyplot as plt

import yap_classes as yap

from yapotu_lib import *

import yap_lib_utils as yut

# display version upon opening
__version__ = str(datetime.fromtimestamp(os.stat(__file__).st_mtime))
print_col(f'loading yap_lib/__init__, version {__version__[:16]}'.rjust(55), color="cyan")

general = 'general'


# ======================================================================
#
#			class dsl  
#
# ======================================================================

class yapotu_dsl():
	"""Main class of yap"""
  
	import yap_lib.yap_lib_character
	import yap_lib.yap_lib_distances
	import yap_lib.yap_lib_fasta
	import yap_lib.yap_lib_mapping
	import yap_lib.yap_lib_otu_build
	import yap_lib.yap_lib_point_cloud
	import yap_lib.yap_lib_misc
	imported = [yap_lib_character, yap_lib_distances, yap_lib_fasta, yap_lib_mapping, yap_lib_otu_build, yap_lib_point_cloud, yap_lib_misc] # type: ignore # to make mypy happy

	# ------------------------------------------------------------------
	
	def __init__(self, basename):
		"""initialises a yapotu_dsl() class
		
		Parameters
		----------
		
		basename	string
					the basename of the project
		
		Notes
		-----
		- begins by loading the yaml file from the argument <basename>
		
		- initialize tells whether to initialise with
			a fastq file
			a fasta file
			a h5 file
		
		- three possibilities are handled in the init()
		
		- if initialisation =="fastq",
		 
		- if initialisation=="fastas", 
			-> starts with a fastafile and a character file
			-> checks that fasta and character file exist
			-> if not, returns an error
		
		- if initilisation=="h5", there are no fasta file nor character file
			-> loads an h5 distance file (the distance is given in key init_dis_algo)
				the name of the file is 
				<basename>.<init_dis_algo>.<h5>
			-> loads the h5file as a distance class
			-> puts it as an attribute is deferred
			-> writes a fasta file and a void character file from h5file
			
		af, 22.07.16, 22.11.12, 23.08.04, 23.08.21, 24.01.16, 24.04.03
		"""
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_which(f_name)
		
		
		### -------------- basename and files as attributes
		
		self.basename		= basename
		self.configfile 	= basename + '.yaml'
		self.fasfile		= basename + '.fas'
		self.fastqfile		= basename + '.fastq'
		self.charfile		= basename + '.char'
		
		### ------------ loading yaml file
		
		print("loading yaml file ...")
		try:
			self.yam = yaml.load(open(self.configfile), Loader=yaml.SafeLoader)
			print("yaml file loaded")
		except Exception as e:
			traceback.print_exc()
			return
			
		### -------------- Initialisation
		
		# ------ parameters for initialisation 
		yut.print_parameters(f_name)
		
		initialisation	= self.yam[f_name]['initialisation']		; print("\ninitialisation     ", initialisation)
		dereplicate		=  self.yam[f_name]['dereplicate']			; print("dereplicate        ", dereplicate)
		if_questions	= self.yam[f_name]["if_questions"]			; print("if_questions       ", if_questions)
		self.questions	= if_questions
		

		#  general parameters
		
		#yut.print_through_which(general)
		yut.print_parameters(general)
		#
		coord		= self.yam[general]['coord']	; print("coord       ", coord)
		dis_algo	= self.yam[general]['dis_algo']	; print("dis_algo    ", dis_algo)
		dis_type	= self.yam[general]['dis_type']	; print("dis_type    ", dis_type)
		fmt			= self.yam[general]['fmt']		; print("fmt         ", fmt)
		otype		= self.yam[general]['otype']	; print("otype       ", otype)
		otu_meth	= self.yam[general]['otu_meth']	; print("otu_meth    ", otu_meth)
		save		= self.yam[general]['save']		; print("save        ", save)
		varname		= self.yam[general]['varname']	; print("varname     ", varname)
		x11			= self.yam[general]['x11']		; print("x11         ", x11)
		legend  	= self.yam[general]['legend']	; print("legend      ", legend)

		if self.questions:
			if not yut.check_parameters(): 
				return
		else:
			print_col("mode is non interactive", color="magenta")
		"""
		If this key is set to "yes", the program will ask some questions
		like displaying the parameters and asking whether they are correct.
		If this key is set to "no", no question is asked (and the implicit answer 
		to corretness is "yes").
		Questions are asked within an input() command, which cannot be 
		implemented on a Galaxy server. 
		"""

		### -------------- initialisation with a fastq file
		
		if initialisation=="fastq":
			if  os.path.isfile(self.fasfile):
				print_col(f'{self.fasfile} exists, still  want to work with {self.fastqfile} ?', color='red')

			print_col("\nInitialisation with a fastqfile", color="green")
			print_col("--------------------------------", color="green")
			
			#to_fasta	= self.yam[f_name]['to_fasta']			; print("\nto_fasta        ", to_fasta)
			qual_min 	= self.yam[f_name]['qual_min']		; print("\nqual_min      ", qual_min)
			orientate	= self.yam[f_name]['orientate']			; print("orientate       ", orientate)
		
			if self.questions:
				if not yut.check_parameters(): return
			
			if os.path.isfile(self.fastqfile):
				print("fastq file", self.fastqfile, "exists, loading fastq file ...")
				if not os.path.isfile(f'{self.fastqfile}_origin'):
						shutil.copy2(f'{self.fastqfile}', f'{self.fastqfile}_origin')	
				#dereplicate
				cmd = f'vsearch --fastx_uniques {self.fastqfile}_origin --sizeout --fastqout {self.fastqfile}'
				os.system(cmd)

				fq	= yap.fastq(fastqfile=self.fastqfile)
				self.seq_id		= fq.seq_id
				self.n_seq		= fq.n_seq
				print_col("\nThere are", self.n_seq, "reads\n", color="cyan")
				fq.to_fasta(qual_min=qual_min)
			else:
				print_col("fastq file", self.fastqfile, "does not exist", color="red")
				print_col("check if we can use a h5 or a fasta file", color="cyan")
				return
			
		### -------------- initialisation with a fasta file
		
		if initialisation=="fasta":
			if os.path.isfile(self.fasfile):
				print("fasta file", self.fasfile, "exists, loading fasta file ...")
				fas = yap.fasta(fasfile=self.fasfile) # to get the seq_ids

				#dereplicate
				if dereplicate: fas.dereplicate()

				self.seq_id		= fas.seq_id
				self.seq_words    = fas.seq_words
				self.n_seq		= fas.n_seq
				print_col("\nThere are", self.n_seq, "reads\n", color="cyan")
			else:
				print_col("fasta file", self.fasfile, "does not exist", color="red")
				print_col("check if we can use a h5 or a fastq file", color="cyan")
				return
		
		### -------------- initialisation with a hdf5 file
		
		# loads the h5file as an attribute
		
		if initialisation=="h5":
			#
			print_col("\nparameter if loading hdf5 file", color="green")
			print_col("------------------------------", color="green")
			
			h5_2_fas		= self.yam[f_name]['h5_2_fas']			; print("h5_2_fas       ", h5_2_fas)
			#
			if self.questions:
				if not yut.check_parameters(): return
			#
			print("-> loads h5file ...")
			self.h5file		= self.basename + "." + dis_algo + ".h5"
			if os.path.isfile(self.h5file):
				print("h5 file", self.h5file, "exists, ok!")
				dis				= yap.distances(h5file=self.h5file)
				self.seq_id = dis.seq_id
				self.dis		= dis
			else:
				print_col("h5 file", self.h5file, "does not exist!:", color="red")
				sys.exit("program terminated")
				
				
			### ------------------ writes a fasta file from the h5 file
			
			if h5_2_fas==True:
				#
				print("-> writes fasta file from h5 file")
				with open(self.fasfile,"w") as f_fas:
					for i in range(dis.n):
						seqid	= ">" + dis.seq_id[i]
						f_fas.write(seqid + "\n")
						f_fas.write(dis.words[i] + "\n")
				#
				self.seq_id	= dis.seq_id
				self.n_seq	= dis.n
				#

		### --------------- creates a void charfile if charfile absent
		
		if os.path.isfile(self.charfile):
			print_col("\ncharacter file", self.charfile, "exists, ok!", color="green")
		else:
			print_col("charfile absent; writing a void charfile", color="magenta")
			with open(self.charfile, "w") as f_char:
				item = "SEQ_ID" + "\t" + "void"
				f_char.write(item + "\n")
				for read in self.seq_id:
					item 	= read + "\t" + "not_assigned"
					f_char.write(item + '\n')
		
	# ==================================================================
