# ==================================================================
#
#			 building.otus()
#
# ==================================================================
#
#	Three methods are avaiable
#		dbscan (under development)
#		cc
#		pc_label
#		swarm

import sys
import os
from datetime import datetime
from print_color import print as print_col

import yap_classes as yap
import yap_lib_utils as yut

general = "general"

# display version upon opening
__version__ = str(datetime.fromtimestamp(os.stat(__file__).st_mtime))
print_col(f'loading yap_lib/otu_build, version {__version__[:16]}'.rjust(55), color="cyan")

# ----------------------------------------------------------------------

def otu_build_dbscan(self):
	"""(OTUs with DBScan - under construction)
	"""
	
	print("[yap!]:[dbscan()]")
	
	print("\nparameters for dbscan")
	print("---------------------------")
	
	
	# parameters for the method
	# general
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)

	
	dis_algo		= self.yam[general]['dis_algo']	; print("dis_algo...", dis_algo)
	dis_type		= self.yam[general]['dis_type']	; print("dis_type..", dis_type)
	save 			= self.yam[general]['save']		; print("save.......", save)
	
	
	# specific
	
	gap				= self.yam[f_name]['gap']		; print("\ngap........", gap)
	min_sample		= self.yam[f_name]['min_sample']	; print("min_sample.", min_sample)
	
	# parameters for I/O
	

	#
	
	# checks parameters
	
	if self.questions:
		if not yut.check_parameters(): return
	
	
	dbscan_basename	= self.basename + "." + dis_algo	# extended basename
	dbscan_disfile	= dbscan_basename + "." + dis_type
	dbscan_clusfile = dbscan_basename + ".db.clus"
	
	if dis_type=="dis":
		dis		= yap.distances(disfile=dbscan_disfile)
	if dis_type=="h5":
		dis		= self.dis
	
	print("distance file loaded ...")
	print("running dbscan ...")
	part 	= dis.dbscan(gap=gap, min_sample=min_sample)
	if save:
		print("partition saved in file", dbscan_clusfile)
		part.write_clus(clusfile=dbscan_clusfile)
	
	 


def otu_build_islands(self):
	"""Builds OTUs as isolated island of spikes from a 2D histogram
	
	@ Key block:	pc_
	
	Parameters of the method
	------------------------
	@ level		; float		; value in the heatmap to be part of an OTU
	@ axis_i	; integer	; first axis (starting at 1)
	@ axis_j	; integer	; second axis
	@ bins		; integer	; number of bins for the 2D histogram
	
	
	Parameters for displaying
	-------------------------
	No display
	
	Parameters for I/O
	------------------
	@ coord			; string 	; origine of coordfile (mds or tsne)
	@ dis_algo		; string 	; method with which distances have been computed
	@ itype			; string	; format of coordfile
	
	
	I/O Suffixes
	------------
	infile	: .coord
	outfile	: .clus
	 
	Notes
	-----
	
	-> The OTUs are built from the 2D histogram of a heatmap.
	
	[documentation to be finalized]
	
	af, 23.08.15
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general
	coord 			= self.yam[general]['coord']		; print("coord					 ", coord)
	dis_algo		= self.yam[general]['dis_algo']		; print("dis_algo				", dis_algo)
	
	# specific	
	bins 			= self.yam[f_name]['bins']			; print("\nbins						", bins)
	level			= self.yam[f_name]['level']			; print("level						", level)
	heatmap			= self.yam[f_name]['heatmap']		; print("heatmap					", heatmap)
	#
	axis_i 			= self.yam[f_name]['axis_i']			; print("axis_1					", axis_i)
	axis_j 			= self.yam[f_name]['axis_j']			; print("axis_2					", axis_j)
	

	
	
	### ---------------- basic checks
	
	coordfile	= self.basename + "." + dis_algo + "." + coord
	clusfile	= coordfile + ".ild.clus"
	
	print('coordfile is', coordfile)
	print('clusfile is ', clusfile)
	
	yut.check_file_exists(coordfile)
	if self.questions:
		if not yut.check_parameters(): return
	
	### --------------- running
	
	pc	= yap.point_cloud(coordfile=coordfile)
	pc.heatmatrix(bins=bins)
	if heatmap:
		pc.heatmap(i=axis_i-1, j=axis_j-1, cmap='viridis', range=None)
	part = pc.islands(level=level, seq_id=self.seq_id)
	part.write_clus(clusfile=clusfile)
	print("OTU partition written in", clusfile)


# ------------------------------------------------------------------

def otu_build_cc(self):
	"""Computes OTUs as connected components of the distance graph 
	
	@ Key block: graph_
	
	Parameters of the method
	-------------------------
	@ gap			; integer	; gap for building gap
	
	Parameters for displaying
	-------------------------
	no display
	
	Parameters for I/O
	------------------
	@ dis_type		; string	; type of distance file
	@ dis_algo		; string 	; method with which distances have been computed
	
	I/O suffixes
	------------
	infile	: <dis_algo>.dis or <dis_algo>.h5
	outfile	: .clus
	
	Notes
	-----
	
	How it works:
	- loads a class distances
	- translates it into a class graph according to the gap
	- builds a partition with otu being cc
	
	
	dis_type is 'dis' or 'h5'
	
	af, 22.03.22; 22.05.16; 22.08.27; 23.04.23 ; 23.05.04, 23.07.07
	23.08.04, 23.08.19
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general
	dis_type	= self.yam[general]["dis_type"]			; print("\ndis_type		 ", dis_type) 
	dis_algo	= self.yam[general]["dis_algo"]			; print("dis_algo		 ", dis_algo,"\n") 
	
	# specific
	gap			= self.yam[f_name]["gap"]				; print("gap			 ", gap) 
	
	if self.questions:
		if not yut.check_parameters(): return
			
	### ------------ naming files
	#
	#	cc_disfile			distance file on which to build the graph
	#	cc_clusfile			file where to save structure <clus>
	#	cc_clusfile_seqid	file where to save structure <clus> by seqid
	#	
	
	cc_basename			= self.basename + "." + dis_algo
	ccgr_basename		= cc_basename   + ".g" + str(gap)
	#
	cc_disfile			= cc_basename   + "." + dis_type
	cc_clusfile			= ccgr_basename + ".clus"
	cc_clusfile_seqid	= ccgr_basename + ".seqid.clus"
	#
	comment 			= ["sample is " + self.basename, "with distance " + dis_algo, "with gap " + str(gap)]
			
	# instances a class <distances> as dis or h5
	
	print_col(f'loading distance file {cc_disfile}', color="cyan")
	if dis_type=="h5":
		dis	= yap.distances(h5file=cc_disfile)
	#
	if dis_type=="dis":
		dis		= yap.distances(disfile=cc_disfile)

	#### ----------------- running
	
	print_col(f'\nrunning ...', color="green")
	print("1 - building the graph")
	print("2 - building connected components")
	print("3 - building the partition")
	print("4 - saving the partition")
	#
	gr		= dis.to_graph(gap=gap)
	print("building connected components")
	clus 	= gr.build_cc()
	print("building the partition")
	part 	= yap.partition(clus=clus)
	#
	print("saving the partition")
	part.write_clus(clusfile=cc_clusfile)
	
# ------------------------------------------------------------------

def otu_build_hac(self):
	"""Computes OTUs as clusters with HAC 
	
	
	Notes
	-----
	
	HAC must have been done beforhand, in order to have a first not too false
	evaluation of the number of clusters ...
	
	linkage methods are
		average, centroid, complete, median, single, ward, weighted
	
	af, 23.11.28, 24.01.17
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general
	dis_type	= self.yam[general]["dis_type"]			; print("dis_type	 ", dis_type) 
	dis_algo	= self.yam[general]["dis_algo"]			; print("dis_algo	 ", dis_algo) 
	
	# specific 	
	linkage		= self.yam[f_name]["linkage"]		; print("\nlinkage		", linkage) 
	height 		= self.yam[f_name]["height"]		; print("height		 ", height) 
	n_clus 		= self.yam[f_name]["n_clus"]		; print("n_clus		 " , n_clus) 
	

	

	if self.questions:
		if not yut.check_parameters(): return
	
			
	### ------------ naming files
	#
	
	hac_basename		= self.basename + "." + dis_algo
	hac_clusfile		= hac_basename + "." + linkage + ".h" + str(height) + ".clus"
	hac_linkfile		= hac_basename + "." + linkage + ".link"
	
	comment 			= ["sample is " + self.basename, "with distance " + dis_algo]
	
	### ------------ basic checks
	
	yut.check_file_exists(hac_linkfile)
	
			#### ----------------- running
	
	print("loading file", hac_linkfile, "as linkage matrix Z")
	tr	= yap.tree(hac_linkfile, meth=linkage, linkfile=hac_linkfile)
	
	# building partition
	
	if n_clus*height > 0:
		print("\nplease select between clustering by height or by number")
	else:
		if n_clus > 0:
			print("building a partition with", n_clus, "classes")
			part	= tr.get_clusters_bynumber(n_clus)
		#
		if height > 0:
			print("building a partition at height", height)
			part 	= tr.get_clusters_byheight(height)
		#
		part.write_clus(clusfile=hac_clusfile)
	
# ------------------------------------------------------------------

def otu_build_swarm(self):
	"""Builds OTUs with swarm
	
	Keys
	----
	
	@ d 	integer		d parameter of Swarm
	
	af & jmf, 23.08.18, 23.11.16
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# specific	
	d			= self.yam[f_name]['d']		; print("d						", d)
	#
	
	### ------------- basic checks
	
	yut.check_file_exists(self.fasfile)
		
	print("\nBuilding OTUs with SWARM\n")
	if self.questions:
		if not yut.check_parameters(): return
	
	clusfile 	= f'{self.basename}.swarm.d{d}.clus'
	print("\nclusfile will be ", clusfile)
	
	### --------------- running
	fas			= yap.fasta(fasfile=f'{self.basename}.fas')
	clus 		= fas.swarm(d=d)
	part 		= yap.partition(clus=clus)
	part.write_clus(clusfile=clusfile)

