# ==================================================================
#
#			methods for fasta file
# 
# ==================================================================

import sys
import yap_classes as yap

from yap_lib import *
import yap_lib_utils as yut

# display version upon opening
__version__ = str(datetime.fromtimestamp(os.stat(__file__).st_mtime))
print_col(f'loading yap_lib/character, version { __version__[:16]}'.rjust(55), color="cyan")

# ==================================================================
#
#			methods for character file
# 
# ==================================================================	

def char_inventory(self):
  """Number of item for each value of a variable in charmat
      
  @ Key block:	char_
  
  Parameters of the function
  --------------------------
  @ varname	; string	; name of the variable 
  @ mode		; string	; mode for displaying results 
  @ min_size	; integer	; minimum size for the taxon to be displayed
  
  Parameters for I/O
  ------------------
  
  None
  
  I/O suffixes
  ------------
  infile	: char
  outfile	: none
  
  Notes
  -----
  -> the values of varname are ordered in decreasing order of counts
  -> mode is	2cols	if two columns
        df		if with a data frame
        
  af, 22.04.12 ; 22.05.16 ; 22.07.16 ; 23.04.28
  """
  
  # where it is ...
  
  f_name	= sys._getframe().f_code.co_name 		# name of the method
  yut.print_through_which(f_name)
  
  # parameters
  yut.print_parameters(f_name)
  
  # general 
  varname			= self.yam[general]['varname']	; print("\nvarname  : ", varname)
  
  # specific
  min_size		= self.yam[f_name]['min_size']	; print("min_size : ", min_size)
  mode			= self.yam[f_name]['mode']		; print("mode     : ", mode)
  
  #### --------------- basic checks
  
  yut.check_file_exists(self.charfile)
  if self.questions:
    if not yut.check_parameters(): return
  
  ### ----------------- running		
  
  char			= yap.charmat(charfile=self.charfile)
  res, count_df	= char.get_inventory(varname)
  #
  if mode=="2cols":
    n	= len(res)
    for i in range(n):
      item 	= res[i]
      if item[1] >= min_size:
        print(item[0], " -> ", item[1])
  #
  if mode=="df":
    print(count_df)		
  

# ------------------------------------------------------------------

def char_stats(self):
  """Provides the number of different values for each varname
  
  Parameters
  ----------
  no parameters
  
  af, 22.04.12; 22.05.16 ; 23.04.28
  """
  
  # where it is ...
  
  f_name	= sys._getframe().f_code.co_name 		# name of the method
  yut.print_through_which(f_name)
  
  # parameters
  yut.print_parameters(f_name)
  
  #### --------------- basic checks
  
  yut.check_file_exists(self.charfile)
  if self.questions:
    if not yut.check_parameters(): return
  
  ### ------------- running 
  
  char		= yap.charmat(charfile=self.charfile)
  char.get_stats()
  
# ------------------------------------------------------------------

def char_show(self):
  """Displays the character file on the screen
  
  @ Key block:	char_
      
  Parameters
  ----------
  no parameters
  
  I/O suffixes
  ------------
  
  infile	: char
  
  Notes
  -----
  To be used for small character files only
  
  af, 22.07.16, 23.04.28
  """
  
  # where it is ...
  
  f_name	= sys._getframe().f_code.co_name 		# name of the method
  yut.print_through_which(f_name)
  
  # parameters
  yut.print_parameters(f_name)
  
  #### --------------- basic checks
  
  yut.check_file_exists(self.fasfile)
  if self.questions:
    if not yut.check_parameters(): return
  
  ### ------------- running 		
  
  char		= yap.charmat(charfile=self.charfile)
  char.show()
  
# ------------------------------------------------------------------

def set_colors(self):
  """
  af, 24.04.25
  """ 
  
  # where it is ...
  f_name	= sys._getframe().f_code.co_name 		# name of the method
  yut.print_through_which(f_name)
  
  # parameters
  yut.print_parameters(f_name)
  
  # general
  varname			= self.yam[general]['varname']	; print("\nvarname  : ", varname)
  
  ### ---------------- file names
  
  colorfile	= self.basename + "_" + varname + ".col"
      
  #### --------------- basic checks
  
  yut.check_file_exists(self.charfile)
  if self.questions:
    if not yut.check_parameters(): return
    
  ### --------------- running
  
  char 	= yap.charmat(charfile=self.charfile)
  
  # writes colorfile
  char.build_colorfile(varname, default_col="grey", colorfile=colorfile)
  
  # interacts with colorfile
  
# ------------------------------------------------------------------

def sequences_per_taxon(self):
  """Provides a file with the list of sequences labelled with taxa
  af, 24.03.12
  """
  
  print_col("\nParameters for sequences per taxon", color="green")
  print_col("----------------------------------", color="green")
  varname			= self.yam['varname']	; print("\nvarname  : ", varname)
  
  
  sptfile		= self.basename + '.' + varname + '.spt'
  
  char 	= yap.charmat(charfile = self.charfile)
  char.seq_by_taxa(varname, sptfile)


def otu_2charfile(self, new_varname="otu"):
  """Adds OTUs as new character in charfile - to be updated
  
  
  !!!!! new_varname has to be transferred to parameters !!!!
  
  Notes
  -----
  
  - builds vec_otu
    vec_otu		list of strings		vec_otu[i]: otu of sequence <i>
  af, 23.08.04
  """
  
  # where it is ...
  
  f_name	= sys._getframe().f_code.co_name 		# name of the method
  yut.print_through_which(f_name)
  
  # parameters
  yut.print_parameters(f_name)
  
  # general
  
  distype		= self.yam[general]["dis_type"] 	; print("\ndistype      ", distype)
  dis_algo	= self.yam[general]["dis_algo"]	; print("dis_algo     ", dis_algo,"\n") 
  
  # specific
  gap			= self.yam[f_name]["gap"]		; print("gap          ", gap) 
  meth		= self.yam[f_name]['meth']		; print("method       ", meth, "\n")
  
  if self.questions:
    if not yut.check_parameters(): return
  
  ### --------- builds vec_otu (see notes)
  
  ccgr_basename		= self.basename + "." + dis_algo + ".g" + str(gap)
  clusfile_seqid		= ccgr_basename + ".seqid.clus"
  
  vec_otu				= ['']*self.n_seq
  with open(clusfile_seqid, "r") as f_clus:
    for cc, line in enumerate(f_clus):
      cc_str 	= line[:-1]
      cc_pl	= cc_str.split('\t')
      for read in cc_pl:
        i			= self.seq_id.index(read)
        vec_otu[i]	= "otu_" + str(cc) 
  
  ### ----------- adds vect_otu as new last column in charfile
  
  flag		= os.path.isfile(self.charfile)
  if flag==True:
    char 	= yap.charmat(charfile=self.charfile)
    char.add_varname(varname=new_varname, new_col=vec_otu)
  else:
    return("charfile", self.charfile, "does not exist!!!")
  
# ------------------------------------------------------------------
