# ==================================================================
#
#			yap_lib methods for fasta file
# 
# ==================================================================

"""
API for implementation with yapsh of methods of class fasta 
"""

import sys
import yap_classes as yap

from yap_lib import *
import yap_lib_utils as yut



# display version upon opening
__version__ = str(datetime.fromtimestamp(os.stat(__file__).st_mtime))
print_col(f'loading yap_lib/fasta, version {__version__[:16]}'.rjust(55), color="cyan")

# ======================================================================

def fas_len_hist(self):
	"""Displays the histogram of sequences lengths
	
	requires
	--------
	- a fasta file 
	
	returns
	-------
	- a file with the plot of sequence length histogram
		
	Notes
	-----
	
	- If save == True, the name of the file is built automatically
		as plots/<basename>.hist.<fmt>
	
	
	af, 22.04.12 ; 22.05.16 ; 23.04.28, 24.03.06
	"""
	
	# where it is ...
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general 
	x11	= self.yam[general]['x11']		; print("x11		 ", x11)
	save	= self.yam[general]['save']		; print("save		", save)
	fmt	= self.yam[general]['fmt']		; print("fmt		 ", fmt)
	
	# specific
	bins	= self.yam[f_name]['bins']		; print("bins		", bins)
	kde	= self.yam[f_name]['kde']		; print("kde		 ", kde)
	color	= self.yam[f_name]['color']		; print("color	 ", color)
	title	= self.yam[f_name]['title']		; print("title	 ", title)
	

	
	#### --------------- basic checks
	
	yut.check_file_exists(self.fasfile)
	if self.questions:
		if not yut.check_parameters(): return
	
	### ----------------- builds I/O filenames and lodas input files.
	
	fas	= yap.fasta(fasfile=self.fasfile)
	if save:
		imfile	= "plots/" + self.basename + '.hist'
	else:
		imfile = None
	
	### ----------------- running
					
	fas.plot_length_histogram(bins=bins, col=color, kde=kde, title=title, x11=x11, imfile=imfile, fmt=fmt)
	
# ------------------------------------------------------------------

def fas_len_plot(self):
	"""Displays the rank-size plot of the sequences lengths
	
	requires
	--------
	- a fasta file
	
	returns
	-------
	- a rank_size plot
	
	
	Notes
	-----
	
	- If save == True, the name of the file is built automatically
		as plots/<basename>.lenplot.<fmt>
	
	
	22.04.12 ; 22.05.16 ; 23.04.28 ; 23.05.06, 24.03.06
	"""
	
	# where it is ...
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general
	x11			= self.yam[general]['x11']		; print("x11			 ", x11)
	save		= self.yam[general]['save']		; print("save			", save)
	fmt			= self.yam[general]['fmt']		; print("fmt			 ", fmt)
	
	# specific
	color		= self.yam[f_name]['color']		; print("color		 ", color)
	dots		= self.yam[f_name]['dots']		; print("dots			", dots)

	
	#### --------------- basic checks
	
	yut.check_file_exists(self.fasfile)
	if self.questions:
		if not yut.check_parameters(): return
	
	
	### ------------------ running
	
	fas	= yap.fasta(fasfile=self.fasfile)
	if save:
		imfile	= "plots/" + self.basename + '.lenplot'
	else:
		imfile = None
	fas.plot_length(dots=dots, col=color, x11=x11, imfile=imfile, fmt=fmt)	

# ==================================================================

def phylo_phyML(self):
	"""Builds a phylogenetic tree with PhyML
	
	
	!!!! to be translated to block phyloTree
	
	@ Key block:	fas
	
	Parameters of the method
	------------------------
	varname	; string	; which taxon for labelling the leaves
	
	Parameters for I/O
	------------------
	
	I/O suffixes
	--------------
	infile	: fas
	
	Notes
	-----
	-> if varname is a taxonomic name, its values will be displayed as
		labels of the leaves
	-> if varname==None, the seq_ids will be used as labels
	
	Documentation to be continued
	
	af & jmf, ..., 23.04.29
	
	"""
	
	# where it is ...
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general
	varname		= self.yam[general]['varname']		; print("varname		 ", varname)
	
	# specific
	labels		= self.yam[f_name]['labels']	; print("\nlabels			", labels)
	
	
	#### --------------- basic checks
	
	yut.check_file_exists(self.fasfile)
	
	if self.questions:
		if not yut.check_parameters(): return
	
	### ----------------------
	
	
	if labels:
		#yut.check_file_exists(self.charfile)
		charfile	= self.charfile
	else:
		charfile	= None
		varname		= None
	
	### ------------------ running		
	
	print(charfile)
	print(varname)
	phylo	= yap.phyloTree(fasfile=self.fasfile)
	phylo.phyML(charfile=charfile, varname=varname)
	
	
# ------------------------------------------------------------------

def fas_aligned(self):
	"""Analyses an aligned fatsa file
	
	af, 23.12.23
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	######################################################
	# specific - revoir si besoin de otu ; faire sans ....
	######################################################
	
	
	otu			= self.yam[f_name]['otu']		; print("\notu	 ", otu)
	nb_all		= self.yam[f_name]['nb_all']	; print("nb_all", nb_all)
	entropy		= self.yam[f_name]['entropy']	; print("entropy", entropy)
	het_pos		= self.yam[f_name]['het_pos']	; print("het_pos", het_pos)
	
	if self.questions:
		if not yut.check_parameters(): return
	
	mafft_dir 	= "mafft/"
	if otu>0:
		mafftfile	= mafft_dir + "otu_" + str(otu) + "_mafft.fas" 
	else:
		mafftfile	= self.basename + "_mafft.fas"
	yut.check_file_exists(mafftfile)
	
	title = self.basename + "; otu_" + str(otu)
	
	
	if otu>0:
		het_mafftfile	= "mafft/analyses/otu_" + str(otu) + "_het_mafft.fas"
		nb_alleles_plot	= "mafft/analyses/otu_" + str(otu) + "_nb_alleles"
		entropy_plot	= "mafft/analyses/otu_" + str(otu) + "_entropy"
	else:
		het_mafftfile	= self.basename + "_het_mafft.fas"
		nb_alleles_plot	= self.basename + "_nb_alleles"
		entropy_plot	= self.basename + "_entropy"
	
	mft			= yap.aligned_fas(mafftfile=mafftfile)
	mft.alleles_by_position()
	#
	if nb_all:
		mft.plot_nb_alleles(title=title, imfile=nb_alleles_plot)
	#
	if entropy:
		mft.plot_alleles_entropy(title=title, imfile=entropy_plot)
	#
	if het_pos:
		mft.build_hetrogeneous_positions(sub_mafftfile = sub_mafftfile)
		
# ------------------------------------------------------------------ù

def histograms(self):
	"""Builds the histogram table of	kmers of a fasta file
	
	@ k		; integer;		the length of the kmer
	
	
	Notes
	-----
	
	Each row 'i' is a sequence or a read, each column 'j' is a kmer of given length,
	the value 'x' at cell (i,j) is the number of occurences of kmer 'j'
	in sequence 'i'.
	
	af, 24.01.08
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# specific 
	k	= self.yam[f_name]['k']		; print("\nk	 ", k)
	
	if self.questions:
		if not yut.check_parameters(): return
	
	
	# reading fasta file
	fas	= yap.fasta(fasfile=self.fasfile)
	
	# building kmer table with jellyfish
	fas.get_histograms(k=k)
	
# ==================================================================
#
#			methods for computing pairwise distances
# 
# ==================================================================	

def disedi(self):
	"""Computes pairwise distances on a fasta file through pairwise alignment
	
	@ Key block:	dis_
	
	Parameters of the function
	--------------------------
	@ algo	; string	; which algorithm to use

	
	Parameters for I/O
	------------------
	@ otype	; string	; type of output file		
	
	I/O suffixes
	------------
	Infile			: fas
	Outfile			: <algo>.dis, <algo>.h5
	
	Notes
	-----
	
	-> otype: types of output file;	can be 'ascii' with tab as separator, or hdf5; 
		- otype = dis	for ascii file
		- otype = h5	for hdf5file
	
	-> algo:	can be
		- sw		for Smith-Waterman dissimilarity
		- nw		for Needleman-Wunsch dissimilarity
		
	-> kmer based distances can be computed with method diskm
	
	af & jmf, 22.08.12, 22.08.27, 23.05.02, 23.08.21, 23.11.19, 25.01.13
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	#################################################
	###### Attention, à revoir (otype, dis_type)
	#################################################
	
	dis_type	= self.yam[general]['dis_type']		; print("\ndis_type		 :", dis_type)
	dis_algo	= self.yam[general]['dis_algo']		; print("dis_algo	:", dis_algo)
	
	
	#### --------------- basic checks
	
	
	yut.check_file_exists(self.fasfile)
			
	if self.questions:
		if not yut.check_parameters(): return
	
	### ------------- running 				
	
	
	
	# extend the basename for specifying the distance
	
	dis_basename		= self.basename + "." + dis_algo
	print("\ndis_basename is", dis_basename)

	# checks whether the file <disbasename>.dis already exists
	# before overwriting it ...
	#
	### !!! what about h5 ??? (af, 23.11.16)
	### 23.11.19 => not necessary because it is not computed with yapotu ....
	#
	if self.questions:
		go = yut.testif(dis_basename + ".dis")
	else:
		go = True
	
	# Computing distances
	
	if go:
		fas	= yap.fasta(fasfile=self.fasfile)
		fas.disedi(dis_type=dis_type, algo=dis_algo)
	else:
		return
# ------------------------------------------------------------------	

def diskm(self):
	"""Computes pairwise diddimilarities with kmers
	
	@ Key block:	dis_
	
	Parameters of the method
	------------------------
	@ kmer	; integer	; length of kmers 
	
	Parameters for I/O
	------------------
	@ otype	; string 	; type of distance file
	
	I/O suffixes
	------------
	infile	: .fas
	outfile	: k<x>.dis, k<x>.h5 
				(<x> is the length of the kmer)
				
	Notes
	-----
	
	-> the length of kmers is given as a plain integer, like 12, and not 'k12'
		this is due to a transparent call to jelly_diskm
		
	-> about jellyfish
		...
		
	af & jmf, 22.08.16, 22.08.27, 23.05.02, 23.11.24
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general 
	
	dis_type	= self.yam[general]['dis_type']		; print("dis_type", dis_type)
	
	###########################################################################
	# specific (!!!!! attention .... faut-il remonter kmer en <general> ? !!!!!
	###########################################################################
	
	kmer		= self.yam[f_name]['kmer']		; print("kmer", kmer)
	
	#### --------------- basic checks
	
	yut.check_file_exists(self.fasfile)
	if self.questions:
		if not yut.check_parameters(): return
	
	### ------------- running 						

	# extend the basename for specifying the distance

	dis_algo			= 'k' + str(kmer) 
	dis_basename		= self.basename + "." + dis_algo
	print("\ndis_basename is", dis_basename)

	# checks whteher the file <sample>.kx.dis already exists
	# before overwriting it ...
	if self.questions:
		go = yut.testif(dis_basename + ".dis")
	else:
		go = True
			
	# runs the computation of kmers with jellyfish
	
	if go:
		cmd = f'diskm -q {self.basename}.fas -o {self.basename} -k {kmer} -f {dis_type}'
		print("\n" + "-> command line:	" + cmd)
		os.system(cmd)
	else:
		pass

# ------------------------------------------------------------------

def phylo_distances(self):
	"""Computes pairwise distances between leaves on a phylogram
	
	
	to be trabslated to a block phyloTree
	
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# no parameters?
	
	nwfile		= self.basename + '.newick'
	disfile		= self.basename + '.evol.dis'
	#
	phylo		= yap.phyloTree(fasfile=self.fasfile)
	phylo.evol_distances(nwfile=nwfile, disfile=disfile)#

def dereplicate(self):
	"""dereplicate fasfile with swarm"""
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)

  #dereplicate

	cmd = f'vsearch --fastx_uniques {self.basename}.fas --sizeout --fastaout {self.basename}_derep.fas'
	print(cmd)
	os.system(cmd)



# ======================================================================
#
#					That's all, folks!
#
# ======================================================================

