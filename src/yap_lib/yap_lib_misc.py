# ==================================================================
#
#			unclassified methods
# 
# ==================================================================

import sys
import os
import shutil
from datetime import datetime
from print_color import print as print_col


import yap_classes as yap
import yap_lib_utils as yut
#from yap_lib import *

general	= "general"

# display version upon opening
__version__ = str(datetime.fromtimestamp(os.stat(__file__).st_mtime))
print_col(f'loading yap_lib/misc, version {__version__[:16]}'.rjust(55), color="cyan")


def hac_dendrogram(self):
	"""Plots the dendrogram of a HAC
	
	
	Notes
	-----
	
	linkage methods are
		average, centroid, complete, median, single, ward, weighted
	
	af, 23.11.28, 24.01.17
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general
	varname 	= self.yam[general]["varname"]			; print("varname				 ", varname) 
	dis_type	= self.yam[general]["dis_type"]				; print("dis_type				", dis_type) 
	dis_algo	= self.yam[general]["dis_algo"]				; print("dis_algo				", dis_algo) 
	
	# specific
	linkage		= self.yam[f_name]["linkage"]		; print("linkage				 ", linkage) 
	dendrogram	= self.yam[f_name]["dendrogram"]	; print("dendrogram			", dendrogram) 
	truncate 	= self.yam[f_name]["truncate"]		; print("truncate				", truncate) 

	

	if self.questions:
		if not yut.check_parameters(): return
	
			
	### ------------ naming files
	#
	
	hac_basename		= self.basename + "." + dis_algo
	hac_disfile			= hac_basename + "." + dis_type
	hac_linkfile		= hac_basename + "." + linkage + ".link"
	
	### ------------ basic checks
	
	yut.check_file_exists(hac_disfile)
	#
	if varname:
		yut.check_file_exists(self.charfile)
			
	### ------------- loading the linkage matrix
	

	
	if varname:	
		char 		= yap.charmat(charfile=self.charfile)
		v_tax		= char.get_tax(varname)
	else:
		v_tax	= None
	
	#### ----------------- running
	
	
	tr	= yap.tree(hac_linkfile, meth=linkage, linkfile=hac_linkfile)
	
	if dendrogram:
		tr.draw(labels=v_tax, truncate=-1)
	#
	if truncate>0:
		tr.draw(truncate=truncate)
	
# ------------------------------------------------------------------

def otu_composition(self):
	""" Displays the composition of each OTU
	
	Keys
	----
	
	meth		; string	; method with which OTUs have been built
	gap			; integer	; gap for building OTUs if method is cc
	dis_algo	; string	; method with which distances have been computed
	varname		; string	; character selected for composition
	# write otufile
	min_size	; integer	; minimum size for computing the composition of an OTU
	comp2screen	; boolean	; whether to display composition on screen
	comp2file	; boolean	; whether to pront composition in a file
	
	af, 23.07.07
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general	
	otu_meth		= self.yam[general]["otu_meth"]			; print("\notu_meth				 ", otu_meth) 
	varname			= self.yam[general]["varname"]		; print("\nvarname			", varname, "\n")
	x11				= self.yam[general]["x11"]			; print("x11					", x11)
	save			= self.yam[general]["save"]			; print("save				 ", save, "\n")
	#
	if otu_meth in ["cc","hac","islands"]:
		coord		= self.yam[general]["coord"]		; print("\ncoord				", coord) 
		dis_algo	= self.yam[general]["dis_algo"]		; print("dis_algo		 ", dis_algo) 
	
	# specific
	if otu_meth=="cc":
		gap			= self.yam[f_name]["gap"]			; print("\ngap					", gap) 
	if otu_meth=="hac":
		linkage		= self.yam[f_name]["linkage"]			; print("linkage			", linkage)
		height		= self.yam[f_name]["height"]			; print("height			 ", height)
	
	min_size	= self.yam[f_name]["min_size"]		; print("min_size		 ", min_size)
	min_size = int(min_size)

	
	# checking parameters
	
	if self.questions:
		if not yut.check_parameters(): return
	
	if otu_meth=="dbscan":
		print("\nmethod is dbscan => cc_0 is the noide\n")
	# extends the basename to keep track of the algo for distances
	# and gap 
	
	if otu_meth=="cc":
		cls_basename	= self.basename + "." + dis_algo + ".g" + str(gap)
	if otu_meth=="hac":
		cls_basename	= self.basename + "." + dis_algo + "." + linkage + ".h" + str(height)
	if otu_meth=="islands":
		cls_basename	= self.basename + "." + dis_algo + "." + coord + ".ild"
	if otu_meth=="swarm":
		cls_basename	= self.basename + ".swarm"
	if otu_meth=="dbscan":
		cls_basename	= self.basename + "." + dis_algo + ".db"
	#
	clusfile			= cls_basename	+ ".clus"
	print("method for OTU is", otu_meth, "and clusfile is", clusfile)
	#
	if otu_meth=="cc":
		comment	= ["# basename is " + self.basename, "# method for otus is " + otu_meth, "# method for distances is " + dis_algo, "# gap is " + str(gap)]
	if otu_meth=="pc_label":
		comment	= ["# basename is " + self.basename, "# method for otus is " +otu_meth, "# method for distances is " + dis_algo, "# dimension reduction is " + coord]
	else:
		comment=[]		
	
	# loads character file and selects v_tax: the column of header given by <varname>

	char 			= yap.charmat(charfile=self.charfile)
	v_tax 			= char.get_tax(varname)
	#v_col, dicol	= yap.def_colors(v_tax)
	
	# loads seq_ids
	
	seq_id			= char.get_seq_id()		
	
	# initialize an instance of class partition 
	
	part 		= yap.partition(clusfile=clusfile, basename=self.basename, seq_id=seq_id)
	
		
	# gives the composition of each cc according to v_tax (see above for v_tax)
	# 	- on screen if comp2screen is True, 
	#	- as a file <cls_basename>.ocf if comp2file is True
	
	if x11:
		part.composition(var=v_tax, min_size=min_size)
	if save:
		ocffile 	= cls_basename + ".ocf"
		#				 				
		print_col("writing composition in file", ocffile, color="cyan")
		part.composition(var=v_tax, min_size=min_size, x11=False, ocffile=ocffile, comment=comment)
	
# -------------------------------------------------------

def clusfile_by_seqid(self):
	"""rewrites a clusfile by seqids
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	otu_meth	= self.yam[general]["otu_meth"]; 	print("\notu_meth   ", otu_meth)
	dis_algo	= self.yam[general]["dis_algo"]; 	print("dis_algo     ", dis_algo)
	gap			= self.yam[general]["gap"]; 		print("gap          ", gap)
	hac_meth	= self.yam[general]["hac_meth"]; 	print("hac_meth     ", hac_meth)
	
	
	
	if otu_meth=="cc":
		cls_basename	= self.basename + "." + dis_algo + ".g" + str(gap)
	if otu_meth=="islands":
		cls_basename	= self.basename + "." + dis_algo + "." + coord + ".ild"
	if otu_meth=="swarm":
		cls_basename	= self.basename + ".swarm"
	if otu_meth=="dbscan":
		cls_basename	= self.basename + "." + dis_algo + ".db"
	if otu_meth=="hac":
		cls_basename	= self.basename + "." + dis_algo + "." + hac_meth + ".h" + str()
	#
	clusfile			= cls_basename	+ ".clus"
	
	part 	= yap.partition(clusfile=clusfile, seq_id=self.seq_id)
	part.write_clus_byseqid()
# -------------------------------------------------------

def otu_rank_size(self):
	"""Rank-size plot of partition in OTUs
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general 
	otu_meth	= self.yam[general]["otu_meth"]; 	print("\notu_meth   ", otu_meth)
	dis_algo	= self.yam[general]["dis_algo"]; 	print("dis_algo     ", dis_algo) 
	coord		= self.yam[general]["coord"];		print("coord        ", coord) 				
	x11			= self.yam[general]["x11"];			print("x11          ", x11) 				
	save		= self.yam[general]["save"];		print("save         ", save) 				
	fmt			= self.yam[general]["fmt"];			print("fmt          ", fmt) 				
	
	# specific
	log				= self.yam[f_name]["log"] 		; print("log        ", log)
	dots			= self.yam[f_name]["dots"] 		; print("dots       ", dots)
	upto			= self.yam[f_name]["upto"] 		; print("upto       ", upto)
	min_size		= self.yam[f_name]["min_size"]	; print("min_size   ", min_size)
	gap				= self.yam[f_name]["gap"]		; print("gap        ", gap) 		
	#
	upto = int(upto)

	if self.questions:
		if not yut.check_parameters(): return
		
	
	if otu_meth=="cc":
		cls_basename	= self.basename + "." + dis_algo + ".g" + str(gap)
	if otu_meth=="islands":
		cls_basename	= self.basename + "." + dis_algo + "." + coord + ".ild"
	if otu_meth=="swarm":
		cls_basename	= self.basename + ".swarm"
	if otu_meth=="dbscan":
		cls_basename	= self.basename + "." + dis_algo + ".db"
	if otu_meth=="hac":
		cls_basename	= self.basename + "." + dis_algo + "." + hac_meth + ".h" + str()
	#
	clusfile			= cls_basename	+ ".clus"
	print("method for OTU is", otu_meth, "and clusfile is", clusfile)
	
	if save:
		imfile	= cls_basename + "_rs"
	else:
		imfile	= None
	
	part 	= yap.partition(clusfile=clusfile)
	part.plot_rank_size(log=log, dots=dots, min_size=min_size, upto=upto, x11=x11, imfile=imfile, fmt=fmt)

# ------------------------------------------------------------------

def otu_ami(self):
	"""Computes the Adjusted Mutual Information score - to be completed
	
	
	Notes
	-----
	The score is computed between the partition as OTUs and as
	taxonomic names.
	
	af, 23.11.27
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# which method for OTUs
	
	otu_meth			= self.yam[general]["otu_meth"] 			; print("\notu_meth				 ", otu_meth)
	
	if otu_meth=="hac":
		linkage			= self.yam[f_name]["linkage"]		; print("linkage		 ", linkage) 
	
	if otu_meth=="cc":
		gap				= self.yam[f_name]["gap"]			; print("gap					", gap) 		
			
	if otu_meth in ["cc","hac","islands"]:
		dis_algo		= self.yam[general]["dis_algo"]		; print("dis_algo		 ", dis_algo) 
		coord			= self.yam[general]["coord"]			; print("coord				", coord) 
	
	# taxonomic names
	varname			= self.yam[general]["varname"]		; print("varname			", varname) 
	
	### -------------------- checking parameters
	
	if self.questions:
		if not yut.check_parameters(): return
	
	### -------------------- building clusfile to be read
	
	if otu_meth=="cc":
		cls_basename	= self.basename + "." + dis_algo + ".g" + str(gap)
	if otu_meth=="islands":
		cls_basename	= self.basename + "." + dis_algo + "." + coord + ".ild"
	if otu_meth=="swarm":
		cls_basename	= self.basename + ".swarm"
	if otu_meth=="dbscan":
		cls_basename	= self.basename + "." + dis_algo + ".db"
	if otu_meth=="hac":
		cls_basename	= self.basename + "." + dis_algo + "." + linkage
	#
	clusfile			= cls_basename	+ ".clus"
	
	### ------------------- building taxonomic names
	
	char 		= yap.charmat(charfile=self.charfile)
	v_tax		= char.get_tax(varname)
	
	### ------------------- computation of the AMI
	
	part		= yap.partition(clusfile=clusfile)
	res			= part.ami(v_tax)
	print("\nmethod for OTU is", otu_meth, "and clusfile is", clusfile)
	print("there are", len(v_tax), "items")
	print("AMI is", res)

def dada2_params(self):
	"""Run dada2_params::plotQualityProfile

	let choosing:
	  truncLen_R1
	  truncLen_R2
	print
	  trimLeft_R1
	  trimLeft_R2
	"""

	cmd = 'dada2_params.r'
	os.system(cmd)

# ----------------------------------------------------------------------

def dada2(self):
	"""Run dada2

	"""

	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)

	# parameters
	yut.print_parameters(f_name)

	# specific
	truncLen_R1    = self.yam[f_name]["truncLen_R1"]   ; print("truncLen_R1  ",truncLen_R1)
	truncLen_R2    = self.yam[f_name]["truncLen_R2"]   ; print("truncLen_R2  ",truncLen_R2)
	trimLeft_R1    = self.yam[f_name]["trimLeft_R1"]   ; print("trimLeft_R1  ",trimLeft_R1)
	trimLeft_R2    = self.yam[f_name]["trimLeft_R2"]   ; print("trimLeft_R2  ",trimLeft_R2)

	### -------- launches dada2 on <basename.fastq> --- writes a ASV.fas
	if 	truncLen_R2 and trimLeft_R2:
		cmd = f'dada2_pairend.r --trimLeft_forward {trimLeft_R1} -r {trimLeft_R2} --truncLen_r1 {truncLen_R1} --truncLen_r2 {truncLen_R2}'
		os.system(cmd)
	else:
		cmd = f'dada2_singleend.r --trimLeft_forward {trimLeft_R1} --truncLen_r1 {truncLen_R1}'
		os.system(cmd)

	### --------- prepares the writing of the clusfile
	with open('ASVs.fas', 'r')  as asv:
		cluster = {}
		tab = "\t"
		for l in asv:
			s_id = l[1:-1]
			seq = asv.readline()[:-1]
			cluster[s_id] = []
			for i,w in enumerate(self.seq_words):
				if seq in w:
					cluster[s_id].append(str(i))
 ### ------------ writes ASVs.clus (yapotu format of clusfile)

	with open(f'{self.basename}.asv.clus', 'w') as clus:
		for a in cluster:
			print(f'{a}\t{tab.join(cluster[a])}', file=clus)

	asv_dir = self.basename + '_asv'
	os.makedirs(asv_dir, exist_ok=True)
	for f in ['filtered', 'resume.csv', 'ASVs_counts.tsv']:
		os.rename(f, f'{asv_dir}/{f}')

	os.rename('ASVs.fas',f'{asv_dir}/{asv_dir}.fas')
	shutil.copy2(self.configfile, f'{asv_dir}/{asv_dir}.yaml')

# ==================================================================
#
#			 building.subproject()
#
# ==================================================================
#

def xtract_subproject(self):
	"""Extracts some files to build a new project
	
	Notes
	-----
	
	- extraction can be by default according to some value of the character file
			(varname, value of varname)
			
	- rows to be extracted are in list <which>
	
	- for distances, the extracted submatrix is in format <dis_type = dis>
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general
	dis_algo			= self.yam[general]['dis_algo']			; print("\ndis_algo					", dis_algo)
	dis_type			= self.yam[general]['dis_type']			; print("dis_type					", dis_type)
	varname				= self.yam[general]['varname']			; print("varname					 ", varname)
	
	#
	sub_basename		= self.yam[f_name]['basename']		; print("sub_basename			", sub_basename)
	sub_char			= self.yam[f_name]['char']			; print("sub_char					", sub_char)
	sub_dis				= self.yam[f_name]['dis']			; print("sub_dis					 ", sub_char)
	value				= self.yam[f_name]['sub_value']			; print("value						 ", value)
	
	if self.questions:
		if not yut.check_parameters(): return
	
	
	os.makedirs(sub_basename, exist_ok=True)
	
	### -------------- copies yaml file and renames it
	
	cmd	= "cp " + self.configfile + " " + sub_basename
	os.system(cmd)
	cmd	= "mv " + sub_basename + "/" + self.configfile + " " + sub_basename + "/" + sub_basename + ".yaml"
	os.system(cmd)
	
	print_col("yaml file copied in subdirectory <" + sub_basename + "> and renamed", color="cyan")
	
	### ---------------	extracting character file
	
	if sub_char:
		char	= yap.charmat(charfile=self.charfile)
		#which	= char.xtract_by_name(varname=sub_varname, value=value, outfile=sub_basename + "/" + sub_basename + '.char')
		which	= char.xtract_by_name(varname=varname, value=value, outfile=sub_basename + "/" + sub_basename + '.char')
		print_col("character file of subproject <" + sub_basename + " is extracted in directory <" + sub_basename + ">", color="cyan")
		
	### ---------------- extracting fasta file (compulsory)
	
	fasfile			= self.fasfile
	sub_fastafile	= sub_basename + "/" + sub_basename + '.fas'
	fas				= yap.fasta(fasfile=fasfile)
	fas.write_subfile(which=which, subfile=sub_fastafile)
	print_col("fasta file of subproject <" + sub_basename + " is extracted in directory <" + sub_basename + ">", color="cyan")
	
	### -----------------	extracting distance file (recommended)
	
	if sub_dis:
		if dis_type=="dis":
			disfile		= self.basename + "." + dis_algo + ".dis" 
			dis 	 	= yap.distances(disfile = disfile)
		#
		if dis_type=="h5":
			h5file		= self.basename + "." + dis_algo + ".h5" 
			dis 	 	= yap.distances(h5file = h5file)
		#
		subdisfile	= sub_basename + "/" + sub_basename + '.' + dis_algo + '.dis' 
		dis.xtract_submatrix(which=which, subdisfile=subdisfile, sep="\t")
		print_col("distance file of subproject <" + sub_basename + "> is extracted as <" + subdisfile + ">", color="cyan")



