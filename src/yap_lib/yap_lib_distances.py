# ==================================================================
#
#			methods for distance
# 
# ==================================================================

"""

coa_kmers		builds a contingency table of kmers (if distance kx)
dis_graph		builds a graph from a barcoding gap
dis_hac			Runs an aggregative hierarchical clustering
dis_heatmap		Displays a heatmap of the distance matrix with seaborn
dis_hist		Histogram of distances
dis_mst			builds and plots a minimum spanning tree
mds				runs a MDS
tsne			runs a t-SNE

af, 24.07.01
"""

import sys
import os
from datetime import datetime
from print_color import print as print_col

from yapotu_lib import *

import yap_classes as yap
import yap_lib_utils as yut



general	= "general"


# display version upon opening


__version__ = str(datetime.fromtimestamp(os.stat(__file__).st_mtime))
print_col(f'loading yap_lib_distances, version {__version__[:16]}'.rjust(55), color="cyan")



# ==================================================================
#
#			dis_hist()
# 
# ==================================================================

def dis_hist(self):
	"""Histogram of distances
	
	- Key block:	dhist_
			
	Parameters of the method
	------------------------

	- bins		; integer	; number of bars in the histogram
	- col		; string	; color of the histogram
	- kde		; boolean	; whether to add kernel density estimation
	- title		; string	; title of the plot
	
	Parameters for I/O
	------------------
	- itype		; string	; type if distance file
	- dis_algo	; string 	; method with which distances have been computed
	- x11		; boolean	; whether to display the histogram on the screen
	- imfile	; boolean 	; whether to save the histogram as a file
	- fmt		; string	; format of the file		
	
	I/O suffixes
	------------
	infile	: .<dis_algo>.dis, <dis_algo>.h5	(e.g. sw.dis)
	outfile	: .<dis_algo>.dhist.<fmt> (e.g. .k8.dhist.eps) 
	
	Notes
	-----
	
	af, 22.08.19, 23.05.02, 23.08.21, 24.06.27
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general
	
	dis_type	= self.yam[general]['dis_type']	; print("\ndis_type       ", dis_type)
	dis_algo	= self.yam[general]['dis_algo']	; print("dis_algo       ", dis_algo)		
	x11			= self.yam[general]['x11']		; print("x11            ", x11)
	save 		= self.yam[general]['save']		; print("save           ", save)
	fmt			= self.yam[general]['fmt']		; print("fmt            ", fmt)
	
	# specific
	
	title		= self.yam[f_name]['title']		; print("title          ", title)
	bins		= self.yam[f_name]['bins']		; print("bins           ", bins)
	col			= self.yam[f_name]['col']		; print("col            ", col)
	kde			= self.yam[f_name]['kde']		; print("kde            ", kde)
	

	
	#### --------------- basic checks
	
	dhistbasename	= self.basename + "." + dis_algo
	dhistdisfile	= dhistbasename + "." + dis_type 
	yut.check_file_exists(dhistdisfile)
	
	if self.questions:
		if not yut.check_parameters(): return
	
	### ----------------------- running
	
	# building file names
	
	dhisth5file		= dhistbasename + ".h5" 	# to be implemented
	
	
	print("loading distance file as an instance of class distances")
	if dis_type=="dis":
		dis	= yap.distances(disfile=dhistdisfile)
		self.seq_id	= dis.seq_id
	if dis_type=="h5":
		dis		= yap.distances(h5file=dhisth5file)
	# 
	if save:
		imfile 	= "plots/" + dhistbasename + ".dhist"
	else:
		imfile = None
	#
	dis.histogram(bins=bins, col=col, kde=kde, title=title, x11=x11, imfile=imfile, fmt=fmt)


# ==================================================================
#
#			dis_heatmap()
# 
# ==================================================================


def dis_heatmap(self):
	"""Displays a heatmap of the distance matrix 
	
	- Key block:	dheat_
	
	Parameters of the function
	--------------------------
	
	- varname	; string	; caracter for labeling rows and colulmns of the heatmap
	- seaborn	; boolean	; whether to use seaborn
	
	Parameters for I/O
	------------------
	- itype		; string 	; format of the distance file
	- dis_algo	; string	; methof with which distances have been computed
	- x11		; boolean	; whether to display the histogram on the screen
	- save		; boolean 	; whether to save the histogram as a file
	- fmt		; string	; format of the file				
	
	I/O suffixes
	------------
	infile	: <dis_algo>.dis or <dis_algo>.h5
	outfile	: [à faire]
	
	Notes
	-----
	
	-> if varname==no, the seqi_ids are used to label rows and columns
	
	af, ..., 23.05.02, 24.06.27
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general
	
	dis_type		= self.yam[general]['dis_type']			; print("\ndis_type			 ", dis_type)
	dis_algo		= self.yam[general]['dis_algo']			; print("dis_algo			 ", dis_algo)
	varname			= self.yam[general]['varname']			; print("varname				", varname)
	save			= self.yam[general]['save']		; print("save					 ", save)
	fmt				= self.yam[general]['fmt']			; print("fmt						", fmt)		
	
	# specific
	seaborn			= self.yam[f_name]['seaborn']		; print("seaborn				", seaborn)

	
	#### --------------- basic checks
	
	heatbasename		= self.basename + "." + dis_algo
	heatdisfile			= heatbasename + "." + dis_type
	#
	yut.check_file_exists(heatdisfile)
	if varname:
		yut.check_file_exists(self.charfile)
	#
	if self.questions:
		if not yut.check_parameters(): return
	
	### ----------------------- running
	
	# loading distance file 
	
	print("loading distance file as an instance of class distances")
	if dis_type=="dis":
		dis	= yap.distances(disfile=heatdisfile)
		self.seq_id	= dis.seq_id
	elif dis_type=="h5":
		dis		= self.dis
	else:
		print("\ndis_type", dis_type, "not recognized\n")
		return
		

	# if labels with taxa
	
	if varname:
		char 		= yap.charmat(charfile=self.charfile)
		labels		= char.get_tax(varname)
	else:
		labels	= self.seq_id
						
	
	# drawing heatmap
	
	if seaborn:
		if save:
			imfile	= "plots/" + heatbasename + ".hm"
		else:
			imfile=None
		#			
		dis.clus_heatmap(labels=labels, imfile=imfile, fmt=fmt)
	else:
		tr				= dis.to_tree()
		dend			= sch.dendrogram(Z=tr.Z, no_plot=True)
		leaves_order	= dend["ivl"]
		items_order		= [int(s) for s in leaves_order]
		#
		dis.heatmap(items_order=items_order)


# ==================================================================
#
#			dis_mst()
# 
# ==================================================================


def dis_mst(self):
	"""Minimum spanning tree
	
	- Key block:	mst_
	
	Parameters of the function
	--------------------------
	- varname	; string 	; taxon selected for coloring nodes
	- d_size	; integer	; dot size

	
	Parameters for I/O
	------------------
	- itype		; string	; type of input distance file
	- dis_algo	; string	; method for building distance matrix
	
	I/O suffixes
	------------
	infiles	: .dis or .h5
				.char
	outfile	: 
			
	>>>>> [à compléter : sauvegarde du plot]
	
	<basename>.<dis_algo>.dis or <basename>.<dis_algo>.h5
	self.charfile	if varname given 
	
	
	af, 22.11.14, 23.05.02, 24.06.27
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general 
	
	dis_type	= self.yam[general]['dis_type']		; print("\ndis_type       =", dis_type)
	dis_algo	= self.yam[general]['dis_algo']		; print("dis_algo           =", dis_algo)
	varname		= self.yam[general]["varname"]		; print("varname            =", varname)
	legend		= self.yam[general]["legend"]		; print("legend             =", legend)
	
	# specific
	
	dot_size	= self.yam[f_name]["dot_size"]		; print("dot_size           =", dot_size)
	
	### --------------------	basic checks
	
	# building names 
	mst_basename	= self.basename + "." + dis_algo	# extended basename
	mst_disfile		= mst_basename	+ "." + dis_type 	# disfile to be read
		
	# checking necessary files exist
	
	yut.check_file_exists(mst_disfile)
	if varname:
		yut.check_file_exists(self.charfile)
	
	# check parameters
	
	if self.questions:
		if not yut.check_parameters(): return
	
	### ----------------- running
	
	# loading files
	
	if dis_type=="dis":
		dis				= yap.distances(disfile=mst_disfile)
	elif dis_type=="h5":
		dis				= self.dis
	else:
		print("type of distance file anadequate!")
	
	# loading charfile
	
	#v_tax, v_col, dicol = load_charfile(charfile=self.charfile, varname=varname)
	char 			= yap.charmat(charfile=self.charfile)
	v_tax			= char.get_tax(varname)
	v_col, dicol	= yut.def_colors(v_tax)
	
	# building and plots mst
	
	A_tree, Gx	= dis.to_mst()
	#gr	= 
	#gr.plot_mst(v_tax=v_tax, legend=legend, dicol=dicol, v_col=v_col, dot_size=dot_size)
	yut.plot_mst(A_tree, Gx, v_tax=v_tax, legend=legend, dicol=dicol, v_col=v_col, dot_size=dot_size)
	#

	
# ==================================================================
#
#			dis_graph()
# 
# ==================================================================

def dis_graph(self):
	"""Builds the graph from distance array
	
	Parameter of the method
	-----------------------
	- gap			; integer	; gap for building gap
	
	Parameters for displaying
	-------------------------
	- tool			; string	; tool for displaying the graph
	- varname		; string	; taxon according to which to color the nodes
	- d_size		; integer	; dot size
	- legend		; boolean	; whether to display the legen for colors		
	
	Parameter for I/O
	-----------------
	- dis_type		; string	; type of distance file		
	- dis_algo		; string 	; method with which distances have been computed
	- x11			; boolean	; whether to display the graph on screen
	- save			; boolean	; whether to save the plot of the graph
	- fmt			; string	; format of the file 				
	
	I/O suffixes
	------------
	infile	: .<dis_algo>.<dis_type>, e.g. sw.dis
	outfile	: .<dis_algo>.<dis_type>.g<gap>.graph.<fmt>, e.g. sw.dis.g7.graph.eps
	
	
	Notes
	-----
	-> tool is the tool in networkxs used to display the graph
		default value is "quick", which works pretty well.
	
	-> dis_type is 'dis' or 'h5', and specifies which distance matrix to read.
	
	af, 22.03.22; 22.05.16; 22.08.27; 23.04.23 ; 23.05.04, 23.07.07
	23.08.04, 23.08.19, 24.06.27
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general
	dis_type	= self.yam[general]["dis_type"] 	; print("\ndis_type      ", dis_type)
	dis_algo	= self.yam[general]["dis_algo"]		; print("dis_algo        ", dis_algo) 
	varname		= self.yam[general]["varname"]		; print("varname         ", varname)
	x11			= self.yam[general]["x11"] 			; print("x11             ", x11)
	legend		= self.yam[general]["legend"]		; print("legend          ", legend)
	fmt			= self.yam[general]["fmt"]			; print("fmt             ", fmt)
	save		= self.yam[general]["save"]			; print("save            ", save, "\n")
	
	# specific
	gap			= self.yam[f_name]["gap"]			; print("gap             ", gap) 
	tool		= self.yam[f_name]["tool"] 			; print("tool            ", tool)
	d_size		= self.yam[f_name]["d_size"]		; print("d_size          ", d_size)

	
	### ------------- basic checks
	
	cc_basename		= self.basename + "." + dis_algo
	cc_disfile		= cc_basename + "." + dis_type 
	
	yut.check_file_exists(cc_disfile)
	
	if self.questions:	
		if not yut.check_parameters(): return
	
	### ------------ running
	
	# loads character file and selects v_tax: the column of header given by <varname>
	if varname:
		char 			= yap.charmat(charfile=self.charfile)
		v_tax 			= char.get_tax(varname)
		v_col, dicol	= yut.def_colors(v_tax)
	
	# extends the basename to keep track of the algo for distances
	
	ccgr_basename		= self.basename + "." + dis_algo + ".g" + str(gap)
			
	# instances a class <distances> as dis

	if dis_type=="h5":
		if self.h5file==cc_disfile:
			dis	= self.dis
		else:
			dis	= yap.distances(h5file=cc_disfile)
	#
	if dis_type=="dis":
		dis		= yap.distances(disfile=cc_disfile)

	### ------------ running
	
	gr		= dis.to_graph(gap=gap)
	
	# plotting the graph
	
	if dis_type=="h5":
		x11	= False
		print("x11 has been set to False, because of the number of reads")
		print("the graph has not been visualized because of its size.")
	else:
		if save:
			imfile 	= "plots/" + ccgr_basename + ".graph"
		else:
			imfile = None
		#
		gr.plot(tool=tool, col_node=v_col, col_edge="gray", dot_size=d_size, legend=legend, dicol=dicol, title=ccgr_basename, x11=x11, imfile=imfile, fmt=fmt)
		if imfile:
			print("graph has been saved in file named", imfile + '.' + fmt)
		else:
			print("\ngraph not plotted!\n")
		

# ==================================================================
#
#			mds()
# 
# ==================================================================

def mds(self):
	"""Multidimensional Scaling
	
	Parameters of the method
	------------------------
	- meth		; string	; method for MDS
	- n_axis	; integer	; numner of axis to compute		
	
	Parameters for display
	----------------------
	None
	
	Parameters for I/O
	------------------
	- itype		; string	; type of distance file
	- otype		; string	; type of output coordonate file
	- dis_algo	; string	; method with which distances have been computed
	
	I/O suffixes
	-------------
	infile	:.<dis_algo>.dis or <dis_algo>.h5
	outfile	: <dis_algo>.mds, e.g. sw.mds
	
	
	Notes
	-----
	
	-> itype must be 'tsv' or 'h5'
		- if itype=='dis', the distance array is in ascii
		- if iytpe=='h5', the distance dile is in hdf5
	-> otype usually is 'tsv', because coordinate files have a limited number of colulmns
	-> meth selects between 'evd', 'svd', 'grp'; 
		- svd is default value for medium sized arrays (like up to 10,000)
		- grp is recommended method beyond
	-> n_axis is the number of axis to be computed
		- if meth==svd, it is recommended to have something like 30 axis
		- if meth==grp, this figure should be much higher, like 1,000
	-> dis_algo must be 'sw' or 'nw' (see disseq)
	
	
	af, 22.05.16, 22.08.27, 23.05.03, 24.06.27
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# paremeters
	yut.print_parameters(f_name)
	
	# general
	dis_type	= self.yam[general]['dis_type']		; print("\ndis_type        =", dis_type)
	dis_algo	= self.yam[general]['dis_algo']		; print("dis_algo        =", dis_algo)
	
	# specific
	n_axis		= self.yam[f_name]['n_axis']		; print("n_axis          =", n_axis)
	meth		= self.yam[f_name]['meth']			; print("method          =", meth)
	nb_eig		= self.yam[f_name]['nb_eig']		; print("nb_eig          =", nb_eig)
	

	### -------------------------------- basic checks
	
	# building file names
	
	mds_basename	= self.basename + "." + dis_algo	# extended basename
	mds_disfile		= mds_basename	+ "." + dis_type

	# checks	
	
	yut.check_file_exists(mds_disfile)
	if self.questions:
		if not yut.check_parameters(): return
	
	### --------------- running 
	
	# names of files to be written
	
	mds_coordfile	= mds_basename + ".mds" 			# coordfile to be written
	
	# instance of class "distances"

	print("loading distance file as an instance of class distances")
	#
	if dis_type=="dis":
		dis		= yap.distances(disfile=mds_disfile, zero_diagonal=False, test_symmetry=False, set_diag_to_zero=False)
	elif dis_type=="h5":
		dis		= self.dis
	else:
		print_col("dis_type apparently inadequate ...", color="red")
		
	#	tests whether the file <sample>.<dis_algo>.mds already exists
	if self.questions:
		go = yut.testif(mds_coordfile)
	else:
		go = True		
	
	# --------------- running mds 
	
	if go:		
		dis.mds(k=n_axis,meth=meth, nb_eig=nb_eig, pc=False, coordfile=mds_coordfile)
	else:
		return
	
#
# ==================================================================
#
#			coa_kmers()
# 
# ==================================================================

def coa_kmers(self):
	"""Correspondence Analysis on kmers contingency table
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	
	# specific
	k 			= self.yam[f_name]['k']				; print('\nk				 ', k)

	#
	r			= self.yam[f_name]['r']				; print('r				 ', r)
	transpose	= self.yam[f_name]['transpose']		; print('transpose ', transpose)
	meth		= self.yam[f_name]['meth']			; print('meth			', meth)

	#
	plot_coa	= self.yam[f_name]['plot_coa']		; print('plot_coa	', plot_coa)
	
	### -------------------------------- basic checks
	
	# building file names
	
	histfile	= self.basename + ".k" + str(k) + ".histo"
	
	# checks	
	
	yut.check_file_exists(histfile)
	if self.questions:
		if not yut.check_parameters(): return
	
	### --------------- running 
	
	hist 	= yap.histo_kmers(histfile=histfile)
	hist.coa(r=r, meth=meth, transpose=transpose)
	hist.write_components()
	
	if plot_coa:
		hist.plot_coa()
	

# ==================================================================
#
#			tsne()
# 
# ==================================================================


def tsne(self):
	"""runs t-SNE
	
	Parameters of the method
	------------------------
	- dim		; integer	; dimension ot point cloud
	- p			; integer	; see notes (default: p = -1)
	
	Parameters for display
	----------------------
	None
	
	Parameters for I/O
	------------------
	- itype		; string	; type of input distance file
	- otype		; string	; type of output coordinates file
	- dis_algo	; string	; method with which distances have been computed
	
	I/O suffixes
	------------
	infile	: <dis_algo>.dis or <dis_algo>.h5
	outfile	: <dis_algo>.tsne, like k12.tsne
	
	
	Notes
	-----
	
	-> itype must be 'tsv' or 'h5' (as for mds)
		- if itype=='dis', the distance array is in ascii
		- if iytpe=='h5', the distance dile is in hdf5
	-> otype usually is 'tsv', because coordinate files have a limited number of colulmns
	-> dim: can be 2 or 3; recommended to keep it at dim=2
	-> p: number of coordinates to keep in the mds as first step;
		- recommended to keep it at p=-1 (all coordinates computed)
	-> dis_algo: as for mds; can be sw or nw	
	
	af, 22.08.06, 22.08.27, 23.05.03, 24.06.27
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	dis_algo	= self.yam[general]['dis_algo']			; print("\ndis_algo        ", dis_algo)
	dis_type	= self.yam[general]['dis_type']			; print("dis_type        ", dis_type)
	dim			= self.yam[f_name]['dim']				; print("dim             ", dim)
	p			= self.yam[f_name]['p']					; print("p               ", p)
	pc			= self.yam[f_name]['pc']				; print("pc              ", pc)

	
	### -------------------------------- basic checks
	
	# building file names
	
	tsne_basename	= self.basename + "." + dis_algo	# extended basename
	tsne_disfile	= tsne_basename + "." + dis_type 
	
	# checks	
	
	yut.check_file_exists(tsne_disfile)
	if self.questions:
		if not yut.check_parameters(): return
	
	### --------------- running 
	
	# extending basename, and names of distance files to be used
	
	tsne_coordfile	= tsne_basename + ".tsne" 			# coordfile to be written
	print(f'coordfile to be written is {tsne_coordfile}')
	
	# instance of class "distances"

	print("loading distance file as an instance of class distances")
	if dis_type=="dis":
		dis		= yap.distances(disfile=tsne_disfile)
	elif dis_type=="h5":
		dis		= self.dis
	else:
		print("dis_type apparently inadequate ...")
		
	#	tests whether the file <sample>.<dis_algo>.mds already exists
	if self.questions:
		go = yut.testif(tsne_coordfile)
	else:
		go = True
				
	# --------------- tsne
	
	if go:		
		#dis.tsne(n_tsne=dim, mode="comp", p=p, pc=pc, coordfile=tsne_coordfile)
		print_col(f'tsne coordfile = {tsne_coordfile}', color="green" )
		dis.tsne(n_tsne=dim, mode="comp", p=p, coordfile=tsne_coordfile)
	else:
		return

# ==================================================================
#
#			dis_hac()
# 
# ==================================================================

def dis_hac(self):
	"""Computes HAC on a distnce array
	
	
	Notes
	-----
	
	linkage methods are
		average, centroid, complete, median, single, ward, weighted
	
	af, 23.11.28, 24.01.17, 24.06.27
	"""
	
	# where it is ...
	
	f_name	= sys._getframe().f_code.co_name 		# name of the method
	yut.print_through_which(f_name)
	
	# parameters
	yut.print_parameters(f_name)
	
	# general
	dis_type	= self.yam[general]["dis_type"]		; print("\ndis_type          ", dis_type) 
	dis_algo	= self.yam[general]["dis_algo"]		; print("dis_algo          ", dis_algo) 
	
	# specific
	linkage		= self.yam[f_name]["linkage"]		; print("linkage            ", linkage) 
	plot_Z		= self.yam[f_name]["plot_Z"]		; print("plot_Z             ", plot_Z) 
	log_heights	= self.yam[f_name]["log_heights"]	; print("log_heigths        ", log_heights) 		
	

	

	if self.questions:
		if not yut.check_parameters(): return
	
			
	### ------------ naming files
	#
	
	hac_basename		= self.basename + "." + dis_algo
	hac_disfile			= hac_basename + "." + dis_type
	hac_linkfile		= hac_basename + "." + linkage + ".link"
	
	comment 			= ["sample is " + self.basename, "with distance " + dis_algo]
	
	### ------------ basic checks
	
	yut.check_file_exists(hac_disfile)

	### ------------- loading an instance of class distances
	
	if dis_type=="dis":
		dis		= yap.distances(disfile=hac_disfile)
	if dis_type=="h5":
		dis	= self.dis
	
	#### ----------------- clustering
	
	tr		= dis.to_tree(meth=linkage, zfile=hac_linkfile)
	
	# drawing linkage matrix
	
	if plot_Z:
		tr.Z_heights(Log=log_heights)
		
# ==================================================================
#
#			That's all, folks!
# 
# ==================================================================
