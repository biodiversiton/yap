#!/usr/bin/env python3

import functools
import glob
import os
import re
import shutil
import subprocess
import sys
import traceback
import warnings
from datetime import datetime

import numpy as np
import yaml
from print_color import print as printc
from prompt_toolkit import PromptSession
from prompt_toolkit.completion import NestedCompleter, WordCompleter
from prompt_toolkit.history import FileHistory
from prompt_toolkit.styles import Style
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import (
    QApplication,
    QCheckBox,
    QComboBox,
    QDialog,
    QDoubleSpinBox,
    QFormLayout,
    QFrame,
    QGroupBox,
    QLabel,
    QLineEdit,
    QPushButton,
    QSpinBox,
    QVBoxLayout,
)

import yap_lib

def _init(y):
    compl = {}
    compl["do"] = {}
    compl["check"] = {}
    compl["?"] = {}
    compl["help"] = {}
    compl["set"] = {}
    compl["show"] = {}
    compl["run"] = {}
    compl["prompt color"] = None
    compl["prompt on"] = None
    compl["prompt off"] = None
    compl["switch"] = None
    compl["exit"] = None
    compl["quit"] = None
    compl["load yaml"] = None
    compl["create yaml"] = None

    func = set()
    func.add("general")
    func.add("__init__")
    for i in y.imported:
        with open(i.__file__, "r") as code:
            for l in code:
                if l.startswith("def"):
                    l = l.split()[1].split("(")[0]
                    func.add(l)

    for com in ("do", "check", "?", "help", "show", "set"):
      for f in sorted(func, key= lambda x: x.__repr__()):
            compl[com][f] = {}
            if f not in y.yam or y.yam[f] is None:
                y.yam[f] = {"_key": "_value"}

            if com == "set":
                for k in y.yam[f]:
                    compl[com][f][k] = None
                    
    compl['help']['yap_classes'] = None
    compl['help']['refcard'] = None
    compl['help']['tutorial'] = None
    compl['?']['yap_classes'] = None
    compl['?']['refcard'] = None
    compl['?']['tutorial'] = None


    del compl["do"]["general"]
    del compl["do"]["__init__"]
    del compl["help"]["general"]
    del compl["help"]["__init__"]
    del compl["?"]["general"]
    del compl["?"]["__init__"]

    func.discard('general')


    do_compl = compl["do"]
    w_compl = WordCompleter(do_compl)
    for c in compl["do"]:
        compl["do"][c] = w_compl

    for s in glob.glob("*.yap"):
        compl["run"][s] = None

    yap_completer = NestedCompleter.from_nested_dict(compl)

    histfile = os.environ["HOME"] + "/.yap_history"
    session = PromptSession(completer=yap_completer, history=FileHistory(histfile))

    return func, session, yap_completer

def _save(y, input_field):
    new_param = {}
    for section in input_field:
        new_param[section] = {}
        for i, it in enumerate(input_field[section].item):
            if isinstance(it, QLineEdit):
                v = it.text()
                if "." in v:
                    try:
                        v = float(v)
                    except ValueError:
                        pass
                else:
                    try:
                        v = int(v)
                    except ValueError:
                        pass
                if v == "False":
                    v = False
                    
                if v == "True":
                    v = True

            elif isinstance(it, QCheckBox):
                v = True if it.checkState() == Qt.Checked else False

            elif isinstance(it, QComboBox):
                v = it.currentText()
            new_param[section][input_field[section].name[i]] = v

    date = datetime.now().strftime("%d_%B_%Y_%H:%M:%S")
    oldYaml = f"{y.configfile}.old"
    os.rename(y.configfile, oldYaml)

    for s in new_param:
      for v in new_param[s]:
        y.yam[s][v] = new_param[s][v]
        
    with open(y.configfile, "w") as y_out:
        print("#", "-" * 40, file=y_out)
        print("#      Configuration file\n#\n#", "-" * 40, "\n#", file=y_out)
        print(f"# {date}\n#\n#", "-" * 40, "\n", file=y_out)
        
            
        #general first
        print("general:", file=y_out)
        for v in y.yam["general"]:
            if v == 'None': v=''
            print(f'  {v}: {y.yam["general"][v]}', file=y_out)

        for k in sorted(y.yam):
            if k == "__Run_OK__" or k == "general" or "_key" in y.yam[k]:
                continue
            print("\n#", "-" * 40, file=y_out)
            print(f"{k}:", file=y_out)
            for v in y.yam[k]:
                if v == 'None': v=''
                print(f"  {v}: {y.yam[k][v]}", file=y_out)

    printc(f"{y.configfile} updated", color="magenta")

def genLayout(qvBoxLayout, y, params, meth):
    if len(params["general"]) == 0:
      return

    genLayout = QFormLayout()
    genLayout.section = "general"

    genQframe = QFrame()
    genQframe.setLineWidth(3)
    genQframe.setMidLineWidth(0)
    genQframe.setLayout(genLayout)

    genQL = QLabel("general parameters:")
    genQL.setAlignment(Qt.AlignCenter)
    qvBoxLayout.addWidget(genQL)
    qvBoxLayout.addWidget(genQframe)

    genLayout.item = []
    genLayout.name = params["general"]
    for p in params["general"]:
      pText = ""
      if p in y.yam["general"]:
          pText = str(y.yam["general"][p])
      else:
          continue
      if p in _rc["bool"]:
          genLayout.item.append(QCheckBox())
          if pText in ("True", "true", "Yes", "yes"):
              genLayout.item[-1].setCheckState(Qt.Checked)
      elif p in _rc["list"]:
          genLayout.item.append(QComboBox())
          genLayout.item[-1].addItems([pText] + _rc["list"][p])
          if p == "dis_algo" and meth not in ("disedi", "phylo_distances"):
              klist = set()
              for file in glob.glob(f'{y.basename}.k[0-9]*.{y.yam["general"]["dis_type"]}'):
                  m = re.search(y.basename + r"\.(k\d+)\.(dis|h5)", file)
                  if not m: continue
                  klist.add(m.groups()[0])
              genLayout.item[-1].addItems(klist)

          if p == "varname":
              klist = [pText]
              try:
                  klist += np.loadtxt(
                      y.charfile, delimiter="\t", dtype=str, max_rows=1
                  )[1:].tolist()
              except FileNotFoundError:
                  print("No or empty charfile !")

              genLayout.item[-1].addItems(klist)

      else:
          if pText == 'None': pText = ''
          genLayout.item.append(QLineEdit(pText))

      genLayout.addRow(f"{p}:", genLayout.item[-1])
    input_field = {}
    input_field["general"] = genLayout

def mapLayout(qvBoxLayout, y, params):
    if 'mapping' not in params: # and len(params["mapping"]) == 0:
      return
    
    mapLayout = QFormLayout()
    mapLayout.section = "mapping"

    mapQframe = QFrame()
    mapQframe.setLineWidth(3)
    mapQframe.setMidLineWidth(0)
    mapQframe.setLayout(mapLayout)

    mapQL = QLabel("mapping parameters:")
    mapQL.setAlignment(Qt.AlignCenter)
    qvBoxLayout.addWidget(mapQL)
    qvBoxLayout.addWidget(mapQframe)

    mapLayout.item = []
    mapLayout.name = params["mapping"]
    for p in params["mapping"]:
      pText = ""
      if p in y.yam["mapping"]:
          pText = str(y.yam["mapping"][p])
      if pText == 'None': pText = ''
      mapLayout.item.append(QLineEdit(pText))
      mapLayout.addRow(f"{p}:", mapLayout.item[-1])
    input_field = {}
    input_field["mapping"] = mapLayout

def _check(y, module, meth, run_bt=True):
    f = getattr(module, meth)
    filename = f.__code__.co_filename
    start = f.__code__.co_firstlineno
    end = [l for l in f.__code__.co_lines()][-1][-1]
    params = {meth: [], "general": []}
    param_list = []
    with open(filename, "r") as code:
        for i, l in enumerate(code):
            if i < start:
                continue
            if i > end:
                break
            m = re.search(rf"self.yam\[(\w+)]\[['\"](\w+)['\"]\]", l)

            if m:
                section = meth if m.groups()[0] == "f_name" else m.groups()[0]
                if section not in params:
                  params[section] = []
                param_list.append(m.groups()[1])
                params[section].append(m.groups()[1])
                y.yam["__Run_OK__"] = "OK"


    # No params
    if len(param_list) == 0:
        y.yam["__Run_OK__"] = "OK"
        print(f"No parameters for {meth}")
        return
    
    input_field = {}
    metLayout = QFormLayout()
    metLayout.section = meth
    metQframe = QFrame()
    metQframe.setLineWidth(3)
    metQframe.setMidLineWidth(0)
    metQL = QLabel(f"{meth} parameters:")
    metQL.setAlignment(Qt.AlignCenter)
    metQframe.setLayout(metLayout)
    qvBoxLayout = QVBoxLayout()

    #Add "General parameters"
    genLayout(qvBoxLayout, y, params, meth)

    #Add "Mapping" parameters
    mapLayout(qvBoxLayout, y, params)
    
    qvBoxLayout.addWidget(metQL)
    qvBoxLayout.addWidget(metQframe)

    metLayout.item = []
    metLayout.name = params[meth]

    for p in params[meth]:
        pText = ''
        if p in y.yam[meth]:
            pText = str(y.yam[meth][p])
        if p in _rc["bool"]:
            metLayout.item.append(QCheckBox())
            if pText in ("True", "true", "Yes", "yes"):
                metLayout.item[-1].setCheckState(Qt.Checked)
        elif p in _rc["list"]:
            metLayout.item.append(QComboBox())
            metLayout.item[-1].addItems([pText] + _rc["list"][p])
            if p == "varname":
                klist = [pText]
                klist += np.loadtxt(y.charfile, delimiter="\t", dtype=str, max_rows=1)[
                    1:
                ].tolist()
                metLayout.item[-1].addItems(klist)
        else:
            if pText == 'None': pText = ''
            metLayout.item.append(QLineEdit(pText))

        metLayout.addRow(f"{p}:", metLayout.item[-1])
    input_field[meth] = metLayout

    window = QDialog()
    window.setWindowTitle(meth)

    exit_bt = QPushButton("Exit")
    exit_bt.setShortcut(QKeySequence("Ctrl+E"))
    exit_bt.clicked.connect(window.reject)
    qvBoxLayout.addWidget(exit_bt)

    save_bt = QPushButton("Save")
    save_bt.setShortcut(QKeySequence("Ctrl+S"))
    save_bt.clicked.connect(functools.partial(_save, y, input_field))
    qvBoxLayout.addWidget(save_bt)

    if run_bt:
        run_bt = QPushButton("Run")
        run_bt.clicked.connect(window.accept)
        run_bt.setShortcut(QKeySequence("Ctrl+R"))
        run_bt.setShortcut(QKeySequence("Return"))
        qvBoxLayout.addWidget(run_bt)

    window.setLayout(qvBoxLayout)

    res = window.exec()

    if res == QDialog.Rejected:
        window.close()
        y.yam["__Run_OK__"] = "NO"
        #return

    if res == QDialog.Accepted:
        y.yam["__Run_OK__"] = "OK"
        window.close()

    for section in input_field:
        for i, it in enumerate(input_field[section].item):
            if isinstance(it, QLineEdit):
                v = it.text()
                if "." in v:
                    try:
                        v = float(v)
                    except ValueError:
                        pass
                else:
                    try:
                        v = int(v)
                    except ValueError:
                        pass
                if v in ("False", "None", "No", "N"):
                    v = False
                if v in ("True", "Yes", "Y"):
                    v = True

            elif isinstance(it, QCheckBox):
                v = True if it.checkState() == Qt.Checked else False

            elif isinstance(it, QComboBox):
                v = it.currentText()

            if v == "":
                printc(
                    f"{input_field[section].name[i]}: {param_list[i]} missing",
                    tag="Warning",
                    tag_color="red",
                    format="bold",
                    color="cyan",
                )
                y.yam["__Run_OK__"] = "Missing arguments"
            else:
                y.yam[section][input_field[section].name[i]] = v

def _create_yaml(y, basename):
    params = {}
    params["general"] = set()
    meth = None
    for mod in y.imported:
        module = getattr(yap_lib, mod.__name__.split("yap_lib.")[1])
        filename = module.__file__
        with open(filename, "r") as code:
            for i, l in enumerate(code):
                if l.startswith("def"):
                    f = re.search(r"def (\w+)", l)
                    meth = f.groups()[0]
                    params[meth] = set()
                m = re.search(rf"self.yam\[(\w+)\]\[['\"](\w+)['\"]\]", l)
                if m:
                    sec, arg = m.groups()
                    if sec == "f_name":
                        sec = meth
                    params[sec].add(arg)

    f = y.__init__
    filename = f.__code__.co_filename
    in_class = False
    with open(filename, "r") as code:
        for i, l in enumerate(code):
            if l.startswith("class"):
                in_class = True
            if not in_class:
                continue
            if l.startswith("\tdef"):
                f = re.search(r"\tdef (\w+)", l)
                meth = f.groups()[0]
                params[meth] = set()

            m = re.search(rf"self.yam\[(\w+)\]\[['\"](\w+)['\"]\]", l)
            if m:
                sec, arg = m.groups()
                if sec == "f_name":
                    sec = meth
                params[sec].add(arg)

    date = datetime.now().strftime("%d %b %Y %H:%M:%S")
    if os.path.exists(f"{basename}.yaml"):
        oldYaml = f"{y.configfile}-{date}"
        os.rename(y.configfile, oldYaml)

    with open(f"{basename}.yaml", "w") as yf:
        print("#", "-" * 40, file=yf)
        print("#      Configuration file\n#\n#", "-" * 40, "\n#", file=yf)
        print(f"# {date}\n#\n#", "-" * 40, "\n", file=yf)

        for m in ("__init__", "general"):
            print(f"{m}:", file=yf)
            for l in params[m]:
                if l == "initialisation":
                    if not os.path.exists(f"{basename}.fas"):
                        if os.path.exists(f"{basename}.fastq"):
                            print(f"  {l}:  fastq", file=yf)
                        elif os.path.exists(f"{basename}.sw.h5"):
                            print(f"  {l}:  h5", file=yf)
                        else:
                            print(f"  {l}:", file=yf)
                    else:
                        print(f"  {l}:  fasta", file=yf)
                elif l == "if_questions":
                    print(f"  {l}:  True", file=yf)
                else:
                    print(f"  {l}:", file=yf)
            print("\n#", "-" * 40, file=yf)
        for m in sorted(params):
            if m in ("general", "__init__"):
                continue
            print(f"{m}:", file=yf)
            for l in sorted(params[m]):
                print(f"  {l}:", file=yf)
            print("\n#", "-" * 40, file=yf)


                    ################################
                    #                              #
                    #             Main             #
                    #                              #
                    ################################
def main():
    try:
        _basename = sys.argv[1]
    except IndexError:
        sys.exit(
            f"Usage:\tyapsh [-h]\n\tyapsh basename [script.yap]\n\tyapsh demo guyane\n\tyapsh demo malabar"
        )

    _YAPSH_HELP = """
    after the prompt: yap:[projectname]->
        Hit <TAB> for a list of available commands (autocompletion)
        type ! and a shell command
        type a python instruction
    """
    if _basename == "-h":
        sys.exit(_YAPSH_HELP)
    if _basename == "demo":
        demo = sys.argv.pop()
        if demo == "guyane":
            datadir = os.path.dirname(__file__)
            #Installation with pip install -e .
            if datadir.endswith('src'):
              datadir = f'{datadir[:-4]}/laurales'
            #installation with pip install . or pip3 install git+https://gitlab.inria.fr/biodiversiton/yap.git
            else:
              datadir = f'{os.environ["HOME"]}/.local/share/yap'
            shutil.rmtree('DEMO_DIR', ignore_errors=True)
            shutil.copytree(f'{datadir}', 'DEMO_DIR')
            os.chdir('DEMO_DIR')
            _basename = 'atlas_guyane_laurales'
        elif demo == "malabar":
            os.makedirs(f'{os.environ["HOME"]}/.local/share/yap', exist_ok=True)
            os.chdir(f'{os.environ["HOME"]}/.local/share/yap')
            _basename = "190204_BM_Ben_Tey_B_rbcL"
            if not os.path.exists("190204_BM_Ben_Tey_B_rbcL.sw.h5"):
                os.system(
                    "wget https://entrepot.recherche.data.gouv.fr/api/access/datafile/148489 --output-document=190204_BM_Ben_Tey_B_rbcL.h5"
                )
                os.rename("190204_BM_Ben_Tey_B_rbcL.h5", "190204_BM_Ben_Tey_B_rbcL.sw.h5")
        else:
            sys.exit(
                f"Usage:\tyapsh [-h]\n\tyapsh basename [script.yap]\n\tyapsh demo guyane\n\tyapsh demo malabar"
            )
    else:
        _yamls = glob.glob("*.yaml")
        if len(_yamls) > 1:
            printc(
                "Many Yaml files in this directory !",
                tag="!! WARNING !!",
                tag_color="red",
                format="bold",
                color="cyan",
            )

        if f"{_basename}.yaml" not in _yamls:
            printc(
                f"{_basename}.yaml does not exist. Should I create it ? (y/n) ",
                color="green",
            )
            if input().lower() in ("y", "yes"):
                _create_yaml(yap_lib.yapotu_dsl, _basename)
                print(f"{_basename}.yaml created")
            else:
                sys.exit()


    _y = yap_lib.yapotu_dsl(_basename)
    _app = QApplication([])

    if not _y.yam:
        _y.yam = {}
    func, session, yap_completer = _init(_y)
    for d in ("char", "dis", "fas", "mafft", "phylo", "plots", "blast"):
        os.makedirs(d, exist_ok=True)

    _script = False
    cmds = []
    _scriptfile = "command"

    style = Style.from_dict(
        {
            "prompt": "red",
        }
    )
    message = [("class:prompt", f"yap:{_basename}-> ")]
    _pr = "on"

    # yapsh basename script.yap
    if len(sys.argv) == 3:
        _script = True
        cmds = [""]
        _script_line = 0

    while True:
        if _script:
            if len(cmds) == 0:
                _script = False
                continue
            rep = cmds.pop(0)
            rep = rep.lstrip(" ")
            _script_line += 1
        else:
            try:
                rep = session.prompt(message, style=style, completer=yap_completer)
            except (EOFError, KeyboardInterrupt):
                break  # Ctrl-d, Ctrl-c
        if rep in ("exit", "quit", "q", "quit()", "q()"):
            break

        if len(sys.argv) == 3:
            rep = f"run {sys.argv.pop()}"

        if rep == "":
            continue

        if rep == "help" or rep == '?':
            print(_YAPSH_HELP)
            continue

        if rep == "run":
            continue
        if rep.startswith("run "):
            _scriptfile = rep.split("run ")[1]
            if not _scriptfile:
                continue
            _script = True
            _script_line = 0
            try:
                with open(_scriptfile, "r") as scr:
                    cmds = scr.readlines()
            except FileNotFoundError:
                printc(f"{_scriptfile} not found", color="cyan")
            continue

        if ";" in rep:
            _script = True
            _script_line = 0
            cmds = rep.split(";")
            continue

        if rep == "do":
            for m in sorted(func):
                print(m)
            print()
            continue

        if rep.startswith("do "):
            methods = rep.split()[1:]

            exit_loop = False
            for m in methods:
                if m not in func:
                    printc(f'\n !!! Method {m} unknown !!\n', color='red')
                    exit_loop = True
                    break
            if exit_loop: continue

            for m in methods:
                for mod in _y.imported:
                    module = getattr(yap_lib, mod.__name__.split("yap_lib.")[1])
                    try:
                        getattr(module, m)
                    except AttributeError:
                        continue
                    if _y.yam["__init__"]["if_questions"]:
                        _check(_y, module, m)
                    else: _y.yam["__Run_OK__"] = "OK"
                    if "__Run_OK__" in _y.yam and _y.yam["__Run_OK__"] == "OK":
                        try:
                            exec(f"module.{m}(_y)")
                            break
                        except Exception as e:
                            if "DEBUG" in os.environ:
                                traceback.print_exc()
                            else:
                                print(e, end=" ")
                            if _script:
                                print(f"in {_scriptfile} line {_script_line}", end=" ")
                                _script = False
                            print()
            continue

        if rep == "check":
            continue
        if rep.startswith("check "):
            m = rep.split()[1]
            if m not in func:
                printc(f'\n !!! Method {m} unknown !!\n', color='red')
                continue
            if m == "general" or m == "__init__":
                module = getattr(yap_lib, "yapotu_dsl")
                _check(_y, module, "__init__", run_bt=False)
                _y.__init__(_y.basename)
                continue
            for mod in _y.imported:
                module = getattr(yap_lib, mod.__name__.split("yap_lib.")[1])
                try:
                    getattr(module, m)
                    _check(_y, module, m, run_bt=False)
                except AttributeError:
                    continue
            continue

        if rep == "show":
            for k in _y.yam:
                if isinstance(_y.yam[k], dict):
                    print(f"{k}:")
                    for l in _y.yam[k]:
                        print(f"\t{l}:\t{_y.yam[k][l]}")
                else:
                    print(f"{k}:\t\t{_y.yam[k]}")
            print()
            continue

        if rep.startswith("show "):
            r = rep.split()
            if r == ["show"]:
                continue
            try:
                _y.yam[r[1]]
            except KeyError:
                continue

            if isinstance(_y.yam[r[1]], dict):
                print(f"{r[1]}:")
                for k in _y.yam[r[1]]:
                    try:
                        if r[2] in k:
                            print(f"\t{k}: {_y.yam[r[1]][k]}")
                    except IndexError:
                        print(f"\t{k}: {_y.yam[r[1]][k]}")
            continue

        if rep.startswith("set "):
            r = rep.split()
            if r == ["set"]:
                continue
            if not r[1] in _y.yam:
                continue
            if isinstance(_y.yam[r[1]], dict):

                if len(r) == 2:
                    for k in _y.yam[r[1]]:
                        print(f"{k}:\t{_y.yam[r[1]][k]}")
                elif len(r) == 3:
                    print(f"{r[1]}:\t{r[2]}:  ", end="")
                    if r[2] in _y.yam[r[1]]:
                        print(f"{_y.yam[r[1]][r[2]]}")
                    else:
                        print("None")

                elif len(r) == 4:
                    if r[3] in ("True", "true" "yes", "Yes"):
                        r[3] = True
                    elif r[3] in ("False", "false", "no", "No"):
                        r[3] = False
                    _y.yam[r[1]][r[2]] = r[3]
                    print(f"{r[1]}:\t{r[2]}: {r[3]}")
                else:
                    print(f"Too much parameters ({len(r)}) for setting")
                continue

            else:
                if len(r) == 2:
                    print(f"{r[1]}:\t{_y.yam[r[1]]}")
                elif len(r) == 3:
                    if r[2] in ("True", "true" "yes", "Yes"):
                        r[2] = True
                    elif r[2] in ("False", "false", "no", "No"):
                        r[2] = False
                    _y.yam[r[1]] = r[2]
                    print(f"{r[1]}:\t{r[2]}")
            continue

        if rep.startswith("prompt "):
            _pr = rep.split("prompt ")[1]
            if not _pr in ("on", "off"):
                continue
            if _pr == "on":
                message = [("class:prompt", f"yap:{_basename}-> ")]
            else:
                message = [("class:prompt", f"yap-> ")]
            continue

        if rep.startswith("? ") or rep.startswith("help "):
            r = rep.split()[1]
            datadir = os.path.dirname(__file__)
            #Installation with pip install -e .
            if datadir.endswith('src'):
              cmd = f'xdg-open {datadir[:-4]}/documentation'
            else:
              cmd = f'xdg-open {os.environ["HOME"]}/.local/share/yap/doc'
            if r == 'refcard':
              os.system(f'{cmd}/yapref.pdf')
              continue
            if r == 'tutorial':
              os.system(f'{cmd}/yapdoc.pdf')
              continue


            if r == 'yap_classes':
              os.system('xdg-open ../doc/_build/html/index.html')
              continue

            for mod in _y.imported:
                module = getattr(yap_lib, mod.__name__.split("yap_lib.")[1])
                try:
                    f = getattr(module, r)
                    print(f.__doc__)
                except (AttributeError, IndexError):
                    continue
            continue

        if rep.startswith("!"):
            try:
                subprocess.run([rep[1:]], shell=True, check=True, executable="/bin/bash")
            except:
                pass
            continue

        if rep == "load yaml":
            try:
                _y.yam.update(yaml.load(open(_y.configfile), Loader=yaml.SafeLoader))
                print(f"[yap!]: {_y.configfile} loaded")
            except Exception as e:
                traceback.print_exc()

            _y = yap_lib.yapotu_dsl(_basename)
            func, session, yap_completer = _init(_y)
            continue

        if rep == "create yaml":
            _create_yaml(_y, _basename)
            printc(f"Empty {_y.configfile} created", color="green")
            continue

        if rep == "switch":
            continue
        if rep.startswith("switch "):
            dirname = rep.split("switch ")[1]
            curr_dir = os.getcwd()
            try:
                os.chdir(dirname)
            except FileNotFoundError:
                print(f"directory {dirname} not found")
                continue
            try:
                _basename = glob.glob("*.yaml")[0][:-5]
            except IndexError:
                print(f'No yaml file in {dirname}')
                os.chdir(curr_dir)
                continue
            _pr = "on"
            message = [("class:prompt", f"yap:{_basename}-> ")]
            _y = yap_lib.yapotu_dsl(_basename)
            if not _y.yam:
                _y.yam = {}
            func, session, yap_completer = _init(_y)
            for d in ("char", "dis", "fas", "mafft", "phylo", "plots", "blast"):
                os.makedirs(d, exist_ok=True)
            continue
 
        try:
            exec(rep)
        except Exception as e:
            if "DEBUG" in os.environ:
                traceback.print_exc()
            else:
                print(f"{e}", end=" ")
            if _script:
                print(f"in {_scriptfile} line {_script_line}", end=" ")
                _script = False
            print()
            continue

_rc = {
    "list": {
        "coord": ["mds", "tsne"],
        "dis_algo": ["sw", "nw"],
        "dis_type": ["dis", "h5"],
        "otype": ["tsv"],
        "fmt": ["png", "eps", "pdf"],
        "otu_meth": ["cc", "hac", "islands", "swarm"],
        "varname": [],
        "meth": ["svd", "evd", "grp"],
        "color_range": ["discrete", "continuous"],
        "initialisation": ["fasta", "fastq", "h5"],
        "mode": ["2cols", "df"]
    },
    "bool": [
        "if_questions",
        "to_fasta",
        "save",
        "x11",
        "load_h5",
        "h5_2_fas",
        "kde",
        "colorfile",
    ],
}


if __name__ == '__main__':
    main()
