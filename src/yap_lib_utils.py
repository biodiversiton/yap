#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from print_color import print as print_col

import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
#import graphviz as gr						# for plot_mst

from matplotlib.patches import Rectangle	# for legend in plots

import operator	# for sorting dictionaries along a value

#from yapotu_lib import *

import yap_lib_utils as ylib



def plot_mst(A_tree, Gx, v_tax=[], legend=True, dicol={}, v_col=[], dot_size=20, title=None, x11=True, imfile=None, fmt="png"):
	"""
	
	********************************************************************
	***************** à reprendre **************************************
	********************************************************************
	
	af, 22.11.17, 24.04.26, 23.05.05
	"""
	pos = nx.nx_pydot.graphviz_layout(Gx, prog="twopi")

	n	= len(v_tax)
	x	= np.zeros(n)
	y	= np.zeros(n)
	for i in range(n):
		x[i]	= pos[i][0]
		y[i]	= pos[i][1]
		
	for i in range(n):
		for j in range(n):
			if A_tree[i,j] > 0:
				plt.plot([x[i],x[j]],[y[i],y[j]], c="black", linewidth=0.5, zorder=1)
	# legend
	#p, keys = get_legend(dicol)
	#plt.legend(p, keys, loc='upper center', bbox_to_anchor=(0, 1.02, 1, 0.1),
	#ncol=3, fontsize=8, fancybox=True, shadow=True)
	
	# plot
	plt.scatter(x,y,c=v_col,s=dot_size, zorder=2)
	
	#plt.show()	
	
	if legend:
		p, keys = ylib.get_legend(dicol)
		plt.legend(p, keys, loc='upper center', bbox_to_anchor=(0, 1.02, 1, 0.1),
		  ncol=3, fontsize=8, fancybox=True, shadow=True)				
	if title:
		if legend:
			plt.xlabel(title)
		else:
			plt.title(title)
	#
	if imfile:
		plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
		print("graph saved in file", imfile + "." + fmt)
	if x11:
		plt.show()
	
# ----------------------------------------------------------------------
	
def plotsave(self, x11=True, imfile=None, fmt="png"):
	""" plot or saves a graphics
	
	Parameters
	----------
	
	x11 : boolean
		plots on screen if True
		
	imfile : string
		saves the graphics as file `imfile`
		
	fmt : string
		format for grapgic file
		
	Returns
	-------
	
	None
	
	
	Notes
	-----
	
	* If `imfile==None`, no file is saved 
	* accepted formats are: `png`, `pdf`, `eps`
	
	af, 21.08.28 
	"""
	if imfile:
		self.savefig("%s.%s" % (imfile, fmt), format=fmt)
	if x11:
		self.gray()
		self.show()
#
#plt.plotsave = plotsave


# ----------------------------------------------------------------------

def count_and_order(pl):
	"""counts the number of occurences of each item in a list, and reorders
	
	
	Parameters
	----------
	
	pl : list of strings
		any character

	Returns
	-------
	
	res : a dictionary
		keys -> value of the character
		value -> number of elements in the list
	
	
	Notes
	-----
	
	if `pl = [a,b,a,c,c,b,a]`, then `res` is a dictionary with `res[a]=3` ; `res[b]=2` ; `res[c]=2` 
	
	af, 18.01.23 ; 21.07.18
	"""
	lev		= list(set(pl))
	count	= [pl.count(item) for item in lev] # nb of occurence per character level
	dicount	= dict(list(zip(lev,count)))		   # dictionary occ -> level
	res		= sorted(iter(dicount.items()), key=operator.itemgetter(1), reverse=True)
	#
	return res
	



# ------------------------------------------------------------------

def def_colors(var, dicol=None, defcol = "grey", no_col="unknown", continuous=False):
	"""Sets the colors of the points from a variable
	
	Parameters
	----------
	
	var : list of strings
			the variable to select colors
			
	dicol : dictionary
			a dictionary between values of `var` and colors
			
	defcol : string
			default color for a point if not driven by `var`
			
	Returns
	-------
	
	v_cols : list of strings
				a color per item
				
	dicol : dictionary
			the dictionary as in input or a one built by the method
			
	Notes
	-----
	For `dicol` 
	
	- the keys are the possible values of `var`
	- the values are the corresponding colors
	
	If a dictionary is given in `dicol`, it builds a list `v_col` of one color per item with the value given by the dictionary
	
	If not, a dictionary is built with following rule:
	
	- the possible values of `var` are listed
	- the number of times ech value appears is counted
	- the values are sorted by occurence in decreasing number
	- colors are attributed with a given rule from the largest to the smallest
	- if there are too many possible values (it is hard to distinguish clearly more than  10 colors), the last ones are set with default color
	
	
	af, 21.07.17, 22.04.24
	"""
	if not dicol:
		cols	= ["blue","green","red","cyan","chartreuse","magenta","orange","purple","brown","yellow", "lightgreen", "navy"]
		co		= count_and_order(var)
		names	= [item[0] for item in co]
		n_types	= len(co)
		dicol	= {}
		#for item in list(set(var)):
		for item in names:
			dicol[item] = defcol		
		for i in range(min(len(cols),len(co))):
			col			= cols[i]			
			item 		= co[i][0]			
			if item != no_col:
				dicol[item] = col
	v_col	= [dicol[item] for item in var]
	#
	return v_col, dicol	
	
# ----------------------------------------------------------------------

def set_colors_from_file(v_tax, colorfile):
	"""
	af, 24.04.25
	"""
	n_seq	= len(v_tax)
	Col		= np.loadtxt(colorfile, delimiter="\t", dtype=object)
	taxa	= Col[1:,0].astype(str).tolist()
	cols	= Col[1:,2].astype(str).tolist()
	n_tax	= Col.shape[0] -1
	#
	dicol	= {}
	for i in range(n_tax):
		taxon 			= taxa[i]
		dicol[taxon]	= cols[i]
	#
	v_col	= ['grey']*n_seq
	for i, taxon in enumerate(v_tax):
		k				= taxa.index(taxon)
		if cols[k] 		!= 'grey':
			v_col[i]	= dicol[taxon] 
	#
	return v_col, dicol



# ----------------------------------------------------------------------	
	
def dot_size(v_col, d_small=5, d_large=20, def_col="grey"):
	"""selects dot size
	
	Parameters
	----------
	
	v_col : a list of strings
		list of colors per item
		
	d_small : integrer
		size of dots if color is `def_col`
		
	d_large : integer
		size of dots if color is not `def_col`
		
	def_col : string
		default color
		
	Returns
	-------
	
	v_dsize	: list of integers
		sizes of dots
		
	Notes
	-----
	
	Dot sizes ae such that
	
	* items which are highlighted by a color (non default) have a large size
	* those who are not highlighted (default color, usually grey) have a small size
	
	af, 21.08.29
	
	"""
	n		= len(v_col)
	v_dsize	= [d_large]*n
	which 	= [i for i, val in enumerate(v_col) if val==def_col]
	for i in which:
		v_dsize[i]	= d_small
	#
	return v_dsize
	

# ----------------------------------------------------------------------

def get_legend(dicolor):
	"""gets the legend in a plot
	
	Parameters
	----------
	
	dicolor : 	 dictionary
	
	Returns
	-------
	
	None
	
	
	Notes
	-----
	
	It adds a panel with the legend in plots of point clouds or graphs.
	
	`dicolor` is the dictionary with variable names as keys and color as value.
	
	af, revised 21.08.01
	"""
	keys = list(dicolor.keys())
	cols = [dicolor[keys[i]] for i in range(len(keys))]
	ind = []
	# _ng for non grey
	cols_ng = []
	keys_ng = []
	for i,c in enumerate(cols):
		if c != 'grey':
			cols_ng.append(c)
			keys_ng.append(keys[i])
	p = [Rectangle((0, 0), 1, 1, fc=col) for col in cols_ng]
	#
	return p, keys_ng
# ----------------------------------------------------------------------

def heatmap(disfile, cmap="Reds", title=None, x11=True, imfile=None, fmt="png" ):
	"""
	"""
	dis	= distances(disfile=disfile)
	tr	= dis.to_tree()
	tr.draw()
	#hmfile		= "../plots/heatmaps/heatmap_cc_" + str(cc)
	#title		= "Heatmap of distances ; cluster " + str(cc)
	dis.heatmap(cmap=cmap, items_order=tr.nodes_order)	
	
	
# ----------------------------------------------------------------------

def print_through_init(c_name, col="green"):
	"""
	"""
	print_col(f'\n-------------------> [yap_classes]:[{c_name}]:[init()]', color = col)

# ----------------------------------------------------------------------

def print_through_meth(c_name, f_name, col="green"):
	"""
	"""
	print_col("\n------------------ > [yap_classes]:[" + c_name + "]:[" + f_name + "()]", color = col)
	
# ----------------------------------------------------------------------

def print_through_which(method, col="cyan"):
	"""
	"""
	item =  "\n----------------------> [yap_lib]:[" + method + "()]\n"
	print_col(item, color=col)
	
# ----------------------------------------------------------------------

def print_parameters(method, col="green"):
	"""
	"""
	item 	= "parameters for " + method
	n		= len(item)
	under	= ''.join(['-']*n) 
	print_col("\n" + item, color=col)
	print_col(under, color=col)
	
# ----------------------------------------------------------------------

def check_file_exists(filename):
	"""checks whether a file exists in the working directory
	
	@ filename	; string 	; filename to check
	
	af, ..., 23.04.29
	"""
	print("\nfile to read is", filename)
	#
	flag	= os.path.isfile(filename)
	if flag:
		print_col("file exists, ok!!\n", color="green")
	else:
		print_col("\n!!! warning !!!", color="red")
		print_col("file", filename, "does not exist", color="red")
	#
	return
	
# ----------------------------------------------------------------------

def check_overwrite(file_to_write):
	"""checks whether a file already exists before overwriting it
	
	@ file_to_write	; string ; name of the file to overwrite
	
	af, ..., 23.04.28
	"""
	flag	= os.path.isfile(file_to_write)
	if flag:
		print_col("\n!!! file ", file_to_write + " already exists !!!", color="red")
		print_col("do you want to overwrite it? (y/n)", color="red")
		ch = input()
		if ch=="y":
			go = True
		else:
			go = False
	else:
		go = True
	#
	return go
	
# ----------------------------------------------------------------------

def check_parameters():
	"""displays on the screen the selected parameters
	
	Notes
	-----
	- permits to accept / decline the parameter choice
		if declined, the program is terminated
		
	af, ..., 23.04.29
	"""
	print_col("\nare you ok with these choices ? (y/n) ", color="green")
	ch	= input()
	#
	if ch in ["n", "no"]:
		print_col("\nok; program terminated, try again!", color="red")
		return False
	if ch in ["y", "yes"]:
		print_col("\nok; let us move on!!\n", color="green")
	return True
	
# ----------------------------------------------------------------------

def testif(file2test):
	""" tests whether file file2test exists
	
	go = True  if it exists
	go = false if it does not exist
	
	af, 22.08.18
	"""
	flag	= os.path.isfile(file2test)
	if flag:
		#
		print_col("\n!!! file ", file2test, "already exists !!!", color="magenta")
		print_col("do you want to overwrite it? (y/n)", color="magenta")
		ch 	= input()
		#
		if ch=="y":
			go = True
		else:
			go = False
	else:
		go = True
	#
	return go
	
	
