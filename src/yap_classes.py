#!/usr/bin/env python3

"""
Licence header
--------------

yap_classes: python3 library for handling distance datasets between sequences or reads

Copyright (C) 2021, INRAE

This program is free software: you can redistribute it and/or modify it 
under the terms of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. 
If not, see <https://www.gnu.org/licenses/>.

date: 21.09.18
"""
#print("\nloading yap_classes.py, version 24.01.08")
"""
print("\nFor a short tutorial:")
print("- start ipython3 by typing $ipython3")
print("- in ipython, type:")
print("In [1]: import yap_classes as yap")
print("In [2]: ? yap.tuto.intro")
print("In [3]: ? yap.tuto.overview")
"""

import os
import sys
import re
import subprocess
import tempfile
import shutil
import glob
from print_color import print as print_col
from datetime import datetime 
import yaml
import traceback

# for testing time needed for a procedure
import time

# for sorting dictionaries along a value
import operator

import numpy as np
import pandas as pd
import pydiodon as dio
from sklearn.manifold import TSNE
from sklearn.cluster import DBSCAN
from sklearn.metrics.cluster import adjusted_mutual_info_score

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D # for 3D plotting
from matplotlib.patches import Rectangle	# for legend in coord.plot_coord
from matplotlib import cm	# colormaps ... - in pc.spikeshow()

import seaborn as sns

# for graphs
import networkx as nx
import scipy.sparse.csgraph as ssc	# for mst()
import scipy.sparse as ssp			# for mst()
from networkx.drawing.nx_pydot import graphviz_layout
#from networkx.algorithms import community as nac	# from networkx.algorithms import community

# for clustering
import scipy.spatial.distance as ssd	# for computing distances and condense with squareform()
import scipy.cluster.hierarchy as sch   # for handling linkage matrices

# for statistics
import scipy.stats as stat
import scipy.interpolate as sin	 # for splines

# image processing
import skimage as skim

# phylogenies
import ete3 as ete3

# jelly fish
import dna_jellyfish as jf	# for jellyfish (histograms)

from Bio import SeqIO

# and the best!

from yapotu_lib import *
import yap_lib_utils as yut

""" deprecated
for d in ('char','dis','fas','mafft','phylo','plots', 'blast'):
  os.makedirs(d, exist_ok=True)
"""
  
# display version upon opening
__version__ = str(datetime.fromtimestamp(os.stat(__file__).st_mtime))
print_col(f'loading yap_classes, version {__version__[:16]}'.rjust(55), color="cyan")



# ======================================================================
#
#			local library
#
# ======================================================================

def plot_mst(A_tree, Gx, v_tax=[], legend=True, dicol={}, v_col=[], dot_size=20, title=None, x11=True, imfile=None, fmt="png"):
	"""
	
	********************************************************************
	***************** à reprendre **************************************
	********************************************************************
	
	af, 22.11.17, 24.04.26, 23.05.05
	"""
	pos = graphviz_layout(Gx, prog="twopi")

	n	= len(v_tax)
	x	= np.zeros(n)
	y	= np.zeros(n)
	for i in range(n):
		x[i]	= pos[i][0]
		y[i]	= pos[i][1]
		
	for i in range(n):
		for j in range(n):
			if A_tree[i,j] > 0:
				plt.plot([x[i],x[j]],[y[i],y[j]], c="black", linewidth=0.5, zorder=1)
	# legend
	#p, keys = get_legend(dicol)
	#plt.legend(p, keys, loc='upper center', bbox_to_anchor=(0, 1.02, 1, 0.1),
	#ncol=3, fontsize=8, fancybox=True, shadow=True)
	
	# plot
	plt.scatter(x,y,c=v_col,s=dot_size, zorder=2)
	
	#plt.show()	
	
	if legend:
		p, keys = get_legend(dicol)
		plt.legend(p, keys, loc='upper center', bbox_to_anchor=(0, 1.02, 1, 0.1),
		  ncol=3, fontsize=8, fancybox=True, shadow=True)				
	if title:
		if legend:
			plt.xlabel(title)
		else:
			plt.title(title)
	#
	if imfile:
		plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
		print("graph saved in file", imfile + "." + fmt)
	if x11:
		plt.show()
	
# ----------------------------------------------------------------------
	
def plotsave(self, x11=True, imfile=None, fmt="png"):
	""" plot or saves a graphics
	
	Parameters
	----------
	
	x11 : boolean
		plots on screen if True
		
	imfile : string
		saves the graphics as file `imfile`
		
	fmt : string
		format for grapgic file
		
	Returns
	-------
	
	None
	
	
	Notes
	-----
	
	* If `imfile==None`, no file is saved 
	* accepted formats are: `png`, `pdf`, `eps`
	
	af, 21.08.28 
	"""
	if imfile:
		self.savefig("%s.%s" % (imfile, fmt), format=fmt)
	if x11:
		self.gray()
		self.show()
#
plt.plotsave = plotsave


# ----------------------------------------------------------------------

def count_and_order(pl):
	"""counts the number of occurences of each item in a list, and reorders
	
	
	Parameters
	----------
	
	pl : list of strings
		any character

	Returns
	-------
	
	res : a dictionary
		keys -> value of the character
		value -> number of elements in the list
	
	
	Notes
	-----
	
	if `pl = [a,b,a,c,c,b,a]`, then `res` is a dictionary with `res[a]=3` ; `res[b]=2` ; `res[c]=2` 
	
	af, 18.01.23 ; 21.07.18
	"""
	lev		= list(set(pl))
	count	= [pl.count(item) for item in lev] # nb of occurence per character level
	dicount	= dict(list(zip(lev,count)))		   # dictionary occ -> level
	res		= sorted(iter(dicount.items()), key=operator.itemgetter(1), reverse=True)
	#
	return res
	



# ------------------------------------------------------------------

def def_colors(var, dicol=None, defcol = "grey", no_col="unknown", continuous=False):
	"""Sets the colors of the points from a variable
	
	Parameters
	----------
	
	var : list of strings
			the variable to select colors
			
	dicol : dictionary
			a dictionary between values of `var` and colors
			
	defcol : string
			default color for a point if not driven by `var`
			
	Returns
	-------
	
	v_cols : list of strings
				a color per item
				
	dicol : dictionary
			the dictionary as in input or a one built by the method
			
	Notes
	-----
	For `dicol` 
	
	- the keys are the possible values of `var`
	- the values are the corresponding colors
	
	If a dictionary is given in `dicol`, it builds a list `v_col` of one color per item with the value given by the dictionary
	
	If not, a dictionary is built with following rule:
	
	- the possible values of `var` are listed
	- the number of times ech value appears is counted
	- the values are sorted by occurence in decreasing number
	- colors are attributed with a given rule from the largest to the smallest
	- if there are too many possible values (it is hard to distinguish clearly more than  10 colors), the last ones are set with default color
	
	
	af, 21.07.17, 22.04.24
	"""
	if not dicol:
		cols	= ["blue","green","red","cyan","chartreuse","magenta","orange","purple","brown","yellow", "lightgreen", "navy"]
		co		= count_and_order(var)
		names	= [item[0] for item in co]
		n_types	= len(co)
		dicol	= {}
		#for item in list(set(var)):
		for item in names:
			dicol[item] = defcol		
		for i in range(min(len(cols),len(co))):
			col			= cols[i]			
			item 		= co[i][0]			
			if item != no_col:
				dicol[item] = col
	v_col	= [dicol[item] for item in var]
	#
	return v_col, dicol	
	
# ----------------------------------------------------------------------

def set_colors_from_file(v_tax, colorfile):
	"""
	af, 24.04.25
	"""
	n_seq	= len(v_tax)
	Col		= np.loadtxt(colorfile, delimiter="\t", dtype=object)
	taxa	= Col[1:,0].astype(str).tolist()
	cols	= Col[1:,2].astype(str).tolist()
	n_tax	= Col.shape[0] -1
	#
	dicol	= {}
	for i in range(n_tax):
		taxon 			= taxa[i]
		dicol[taxon]	= cols[i]
	#
	v_col	= ['grey']*n_seq
	for i, taxon in enumerate(v_tax):
		k				= taxa.index(taxon)
		if cols[k] 		!= 'grey':
			v_col[i]	= dicol[taxon] 
	#
	return v_col, dicol



# ----------------------------------------------------------------------	
	
def dot_size(v_col, d_small=5, d_large=20, def_col="grey"):
	"""selects dot size
	
	Parameters
	----------
	
	v_col : a list of strings
		list of colors per item
		
	d_small : integrer
		size of dots if color is `def_col`
		
	d_large : integer
		size of dots if color is not `def_col`
		
	def_col : string
		default color
		
	Returns
	-------
	
	v_dsize	: list of integers
		sizes of dots
		
	Notes
	-----
	
	Dot sizes ae such that
	
	* items which are highlighted by a color (non default) have a large size
	* those who are not highlighted (default color, usually grey) have a small size
	
	af, 21.08.29
	
	"""
	n		= len(v_col)
	v_dsize	= [d_large]*n
	which 	= [i for i, val in enumerate(v_col) if val==def_col]
	for i in which:
		v_dsize[i]	= d_small
	#
	return v_dsize
	

# ----------------------------------------------------------------------

def get_legend(dicolor):
	"""gets the legend in a plot
	
	Parameters
	----------
	
	dicolor : 	 dictionary
	
	Returns
	-------
	
	None
	
	
	Notes
	-----
	
	It adds a panel with the legend in plots of point clouds or graphs.
	
	`dicolor` is the dictionary with variable names as keys and color as value.
	
	af, revised 21.08.01
	"""
	keys = list(dicolor.keys())
	cols = [dicolor[keys[i]] for i in range(len(keys))]
	ind = []
	# _ng for non grey
	cols_ng = []
	keys_ng = []
	for i,c in enumerate(cols):
		if c != 'grey':
			cols_ng.append(c)
			keys_ng.append(keys[i])
	p = [Rectangle((0, 0), 1, 1, fc=col) for col in cols_ng]
	#
	return p, keys_ng


# ----------------------------------------------------------------------

def heatmap(disfile, cmap="Reds", title=None, x11=True, imfile=None, fmt="png" ):
	"""
	"""
	dis	= distances(disfile=disfile)
	tr	= dis.to_tree()
	tr.draw()
	#hmfile		= "../plots/heatmaps/heatmap_cc_" + str(cc)
	#title		= "Heatmap of distances ; cluster " + str(cc)
	dis.heatmap(cmap=cmap, items_order=tr.nodes_order)	
	
	
# ----------------------------------------------------------------------

#def print_through_init(c_name, col="purple"):
#	"""
#	"""
#	print_col("\n-[yap_classes]:[" + c_name + "]:[init()]", color = col)

# ----------------------------------------------------------------------

#def print_through_meth(c_name, f_name, col="purple"):
#	"""
#	"""
#	print_col("\n--[yap_classes]:[" + c_name + "]:[" + f_name + "()]", color = col)


# ======================================================================
#
#	   				Tutorials
#
# ======================================================================


class tuto():
	"""
	Provides short tutorials for elementary operations
	"""
	def intro():
		
		"""Tutorial for using `yap_classes` 
		
		
		
		* Calling yapotu
		
		
		The library is called ỳap_classes`and is called by 
		
		>>> import yap_classes as yap
		
		
		* Dependances
		
		Main libraries called by yapotu are: matplotlib, panda, scipy, networkx, scikit-learn
		
		
		* Graphical outputs
		
		Several methods yield graphical outputs. All calls have the same arguments:
		
		>>> myPlot(..., x11=True, imfile=None, fmt="png")
		
		where:
		
		* if x11 = `True`, the plot is displayed on the screen (and not if `False`)
		* if `imfile` is a string (not None), the plot is saved in file imfile
		* with format `fmt`, e.g. in trucmuche.eps for myPlot(..., x11=True, imfile="trucmuche", fmt="eps")
		
		* Tutorial
		
		Try:
		
		* ? yap.tuto.overview   for an overview
		* ? yap.tuto.distances	for class distances
		
		af, 21.09.20	
		"""
		
		pass
		
	# ------------------------------------------------------------------
		
	def overview():
		"""Overview
	
		* Classes
		
		Main classes are:
		
		- distances   : the basic class; methods for analysing a distance array
		- point_cloud : methods for displaying a point cloud
		- graph       : methods for analyzing and displaying a graph
		- tree        : methods for analyzing and displaying a tree
		
		Typically, an elementary study aggregates following steps, (arguments are omitted, see the tuto of each function for further details)
		
		* First, load a distance file as a `distances` object
		
		>>> disfile = "datasets/lauraceae.dis"
		>>> dis = yap.distances(disfile=disfile)    # loading distance file
		
		
		Then, folliwing analysis can be done (elementary examples)
		
		* MDS
		
		>>> pc  = dis.mds()                         # mds is run, the output `pc` is a point_cloud object
		>>> pc.scatter()                            # the point cloud is displayed and/or saved 
		
		* t-SNE
		
		>>> pc  = dis.tsne()             # tsne is run, the output `pc`is a point_cloud object
		>>> pc.scatter()                 # the point cloud is displayed and/or saved 
		
		
		* Associated graph
		
		It is possible to associate a graph to any distande array by threshholding:
		- a gap is given
		- the nodes are the items in rows and columns of the distance array
		- there is an edge (i,j) iff D[i,j] <= gap
		
		Building and displaying such a graph can be done with:
		
		>>> gap = 12                        # chosing a value for the gap
		>>> gr = dis.to_graph(gap)          # building the graph (`gr`is an object of class `graph`)
		>>> gr.plot()                       # displaying and/or saving the graph
		
		
		* Clustering
		
		Here is a simple way to implement Hierarchical Aggregative Clustering and drawing a tree
		
		>>> tr = dis.to_tree()               # clustering and building the tree (linkage matrix)
		>>> tr.draw(...)                     # drawing the tree
		
		af, 21.09.20		
		"""
		
		pass
		
	# ------------------------------------------------------------------
	
	def distances():
		""" tuto for class distances
		
		
		* The basic class is class `distances` which provides several methods for analysing a distance array.
		
		Let `Dis` be a distance array, written in a file `mydisfile`. An instance of class `distances` can be initialized  by
		
		>>> dis = yap.distances(disfile=mydisfile)
		
		
		There exists 5 basic methods for analyzing array `Dis`, each implemented as a method in `distances`:
		
		* mds() for running MDS, as
		
		>>> pc = dis.mds(...)
		
		where `pc` is an instance of class `point_cloud`. See ? yap.distances.mds for  further details.
		
		* tsne() for running t-SNE, as
		
		>>> pc = dis.tsne(...)
		
		where `pc` is an instance of class `point_cloud`. See ? yap.distances.tsne for  further details.
		
		* to_graph() for building a graph by thresholding, as
		
		>>> gr = dis.to_graph(gap=...)
		
		where `gr` is an instance of class `graph`. See ? yap.distances.to_graph for  further details.
		
		* to_tree for building a tree (e.g. after clustering), as
		
		>>> tr = dis.to_tree(...)
		
		where `tr` is an instance of a class `tree`. See ? yap.distances.to_tree for  further details.
		
		
		* There is one method for visualizing a distance array as a heatmap:
		
		>>> dis.heatmap(...)
		
		see ? distances.heatmap for further details
		
		
		af, 21.09.18, ongoing ...
		"""
		
		pass


# ======================================================================
#
#	   				Fasta-qualité
#
# ======================================================================

class fastq():
	"""Initializes a project with a fastq file
	af -> jmf : peux tu stp inclure ici les trois méthodes :
	- production d'un fichier fasta connaissant un fastq [to_fasta()]
	- enlèvement des N [remove_N()]
	- déréplication [dereplicate()] ==> sur fasta pas fastq
	
	attention : to_fasta() ne marche que si le suffice du fastaq est "fastq"
	on le conserve ainsi ?
	
	on peut remplacer le nom de la classe comme pre-treatment()
	(si le "-" est accepté dans les noms de classe)
	
	on peut ajouter trois attributs boléen à la classe 
	- to_fasta
	- remove_N
	- dereplicate
	avec False comme valeur par défaut, et mis à jour True lorsque la méthode correspondante est réalisée
	ou toute autre solution avec un .status .... 
	"""
	def __init__(self, fastqfile=None):
		"""
		"""
		self.c_name	= "fastq"
		yut.print_through_init(self.c_name)
		#
		self.basename   = fastqfile[:-6]
		self.seq_id     = []
		self.seq_words  = []
		self.seq_len    = []
		self.seq_qual   = []
		self.n_seq      = 0
		for rec in SeqIO.parse(fastqfile,'fastq'):
			self.seq_id.append(rec.id)
			self.seq_words.append(rec.seq)
			self.seq_len.append(len(rec.seq))
			self.seq_qual.append(rec.letter_annotations["phred_quality"])
			self.n_seq += 1

	def to_fasta(self, qual_min=20):
		with open(f'{self.basename}.fas', 'w') as fas_h:
			if qual_min == 0:
				for i, sid in enumerate(self.seq_id):
					print(f'>{sid}\n{self.seq_words[i]}', file=fas_h)
			else:
				for i,q_list in enumerate(self.seq_qual):
					mySeq = self.seq_words[i]
					for q in q_list:
						if q < qual_min:
							mySeq = mySeq[:i] + 'N' + mySeq[i+1:]
					print(f'>{self.seq_id[i]}\n{mySeq}', file=fas_h)

			

	# ------------------------------------------------------------------
	def to_fasta_obsolete(self, qual_min=20):
		"""write into a fasta file, N if qual < qual_min
		""" 
		fasta_out = self.basename + '.fas'
		with open(fasta_out, 'w') as fas_h:
			for s in range(self.n_seq):
				print(f'>{self.seq_id[s]}', file=fas_h)
				for q in range(len(self.seq_qual[s])):
					nucl = self.seq_words[s][q]
					if self.seq_qual[s][q] < qual_min:
						nucl = 'N'
					print(nucl,end='', file=fas_h)
				print( file=fas_h)
		print(fasta_out, 'saved')

# ======================================================================
#
#	   				Fasta
#
# ======================================================================

class fasta():
	"""Object attached to a fasta file

	Attributes
	----------

	seq_id : list of strings
			   sequence identifiers
			   
	
	seq_len : list of integers
				sequence length
				
	words : list of strings
			 sequences
			 
	n_seq : integer
			number of sequences
	
	fasfile : string
			   the name of the file on the disk
	
	Notes
	-----
	
	Calls mafft (alignment) and seaview (visualization) 
	
	af, from `declic`; revised : 21.08.01
	"""

	# ------------------------------------------------------------------

	def __init__(self, fasfile=None, h5file=None, verbose = False):
		"""loading from a fasta file
		
		Parameters
		----------
		
		fasfile	: string
			name of fasta file to read
			
		h5file : string
			name of hdf5file where to read the sequences
		
		Returns
		-------
		seq_id	: list of strings
				sequence identifiers
					
		seq_len : list of integers
				sequence lengths
				
		seq_words : list of strings
				sequences
				
		n_seq : integer
				number of sequences

		Notes
		-----
			Does not use SeqIO.parse of biopython any longer: jmf 01 feb 2019
			
		af & jmf, from `declic`; 21.08.28
		"""
		
		self.c_name		= "fasta"
		yut.print_through_init(self.c_name)
		
		#print("running yap_classes:fasta:init()")
		flag	= False
		if fasfile and h5file:
			exit("I need either fasfile or h5file, not both")
		
		# if from hdf5file	
		if h5file:
			hf 			= h5py.File(h5file, 'r')
			seq_id 		= []
			seq_words  	= []
			for s in  hf['seqid'][:].astype(str):
				seq_id.append(s.rstrip('\n'))
			for w in hf['word'][:].astype(str):
				seq_words.append(w.rstrip('\n'))
			x				= h5file.split(".")
			self.basename	= x[0]
			flag			= True


		# if from a fasta file
		if fasfile:
			x	= fasfile.split(".")
			self.basename	= x[0]
			#
			print_col("loading fasta file ...", color="green")
			t_deb 		= time.time()
			k	= 0
			with open(fasfile,'r') as fasta_sequences:
				seq_id		= []
				seq_words	= []
				seq_len		= []
				for myId, myFasta in self.next_seq(fasta_sequences):
					seq_id.append(myId)
					seq_words.append(str(myFasta))
					seq_len.append(len(str(myFasta)))
					k	= k+1
					if k % 1000 == 0:
						print(k, end='\r')
			t_fin		= time.time()
			elapsed		= t_fin-t_deb
			print_col(f'... took {str(elapsed)} second.', color="green")
			flag			= True
		#
		#seq_len = [len(w) for w in seq_words]
		#
		self.seq_id		= seq_id
		self.seq_len	= seq_len
		self.seq_words	= seq_words
		self.n_seq		= len(seq_id)
		self.fasfile	= fasfile			   # will be useful for some methods, like <view> or <align>
		#
		# test for doubled identifiers
		#
		"""
		seq_names   = list(set(self.seq_id))	# list of dereplicated seq_id's
		for item in seq_names:				  # for each name ...
			nn  = self.seq_id.count(item)	   # nbre of sequences with the name as seq_id
			if nn > 1:						  # not very good ...
				print("!!! ------------ warning -----------------------------------   !!!")
				print("sequence " + item + " is present " + str(nn) + " times")
				print("It is highly likely that further calculation will be negatively impacted")

				print("------------------------------------------------------------------")
			#
			if verbose:
				print("Fasta file " + fasfile + " loaded ...")
		"""
		#
		self.verbose	= verbose
		#

	# ------------ Set functions ---------------------------------------

	# for reading fasta files
	def next_seq(self, handle):
		""" creates a generator which provides the next seq of a fasta file, at each call

		Usage:
			>>> with open(fasfile ,'r') as fas_in:
			>>> 	for myID, mySeq in next_seq(fas_in):
			>>> 		print myID,mySeq
		"""
		myID  = False
		mySeq = ''
		for line in handle:
			line = line.rstrip('\n')
			if line.startswith('>'):
				if myID:
					yield myID,mySeq
				mySeq = ''
				myID = line[1:].split()[0]
			else:
				mySeq += line
		yield myID,mySeq

	# ------------------------------------------------------------------
	
	def set_seq_id(self, vec_id):
		"""sets sequence identifiers

		Parameters
		----------
		vec_id : list of strings
			list of sequence identifiers

		Returns
		-------
		
		None
		
		Notes
		-----
		
		* updates self.n_seq at the same time if void
		* checks whether self.n_seq is consistent with this new instance

		af, from `declic`, revised 23.07.07
		"""
		print("run yap_classes:fasta:set_seq_id()")
		self.seq_id = [item for item in vec_id]
		if self.n_seq == -1:
			self.n_seq	  = len(vec_id)
		elif self.n_seq != len(vec_id):
				print("!!! warning !!!")
				print("in <dec_fasta.set_seq_id>, you try to set a list ")
				print("of sequence id's of size different from other attributes")

	# ------------------------------------------------------------------

	def set_seq_len(self, vec_len):
		"""sets sequence lengths
		
		Parameters
		----------
		
		vec_len : list of integers
			lengths of sequences

		Notes
		-----
		
		* `self.n_seq` is updated as well
		* Must be used after that self.seq_id has been updated.
		* As the reference for traceability is `self.seq_id`, a warning message is given if the length of <vec_len> is different from the number of items in self.seq_id

		af, from `declic`, revised 23.07.07
		"""
		print("[yap_classes]:[fasta]:[set_seq_len()]")
		self.seq_len	= [item for item in vec_len]
		if self.n_seq == -1:
			self.n_seq	  = len(self.seq_len)
		elif self.n_seq != len(vec_len):
				print("!!! you try to set a list of sequence lengths of size different from other attributes")

	# ------------------------------------------------------------------

	def set_words(self, vec_words):
		"""sets sequence as words
		
		Parameters
		----------
		
		vec_words : list of strings
			sequences

		Notes
		-----
		
		* `self.n_seq` and `self.seq_len` are updated as well.
		* `self.n_seq` is updated with `self.seq_id` only.
		* Must be used after that `self.seq_id` has been updated.
		* As the reference for traceability is `self.seq_id`, a warning message is given if the length of `vec_words` is different from the number of items in self.seq_id

		af, from `declic`, revised 23.07.07
		"""
		print("run yap_classes:fasta:set_words()")
		self.words	  = [item for item in vec_words]
		self.seq_len	= [len(item) for item in vec_words]
		if self.n_seq == -1:
			self.n_seq   = len(self.words)
		elif self.n_seq != len(vec_words):
				print("!!! you try to set a list of sequences of size different from other attributes")
				
				
	 
				
	# ------------------------------------------------------------------

	def xtract_by_seqid(self, seq_id, xtracted_fasfile=None):
		"""selects the fasta subset by retaining items in seq_id only

		Parameters
		----------
		
		seq_id : list of strings
					list of seq_id to be selected to build a fasta subfile
					
		xtracted_fasfile : string
			names of file for subfile
			
		Returns
		-------
		fas_subfile   dec_fasta type

		Notes
		-----
		
		* seq_id need not to be a subset of self.seq_id, neither in same order

		* sequences in both seq_id and self.seq_id will be selected (intersection)
		in the same order then in self.seq_id.

		* Reads in seq_id and not in self.seq_id will be printed as
		"sequence xxxx is not in fasta file".

		usage:
		prj.fasta.xtract_by_seqid(seq_id=..., xtracted_fasfile=None)

		last revision: af, Cayenne, 18/01/2017 !!!ongoing!!!

		"""

		print("run yap_classes:fasta:xtract_by_seqid()")
		if len(self.seq_id)  == 0:
			print("!!! warning: you extract a fasta subclass from an empty class")
		#
		
		# tests whether items in input seq_id are actually seq_id's in fasta object
		pl = []								 # will contain all self.seq_id in seq_id
		for item in seq_id:
			if self.seq_id.count(item) == 1:	# seq_id[i] is in self.seq_id
				i = self.seq_id.index(item)	 # index
				pl.append(i)					# added in <pl>
			else:
				print("sequence <" + item + "> is not in fasta file")
		#
		fas_subfile	 = fasta()		   # will be returned

		# prepares the initialization from <pl>
		vec_id		  = [self.seq_id[i] for i in pl]
		vec_len		 = [self.seq_len[i] for i in pl]
		vec_words	   = [self.words[i] for i in pl]

		# implements initialization after preparation
		fas_subfile.set_seq_id(vec_id)
		fas_subfile.set_seq_len(vec_len)
		fas_subfile.set_words(vec_words)

		# if xtracted_fasfile: writes fas_subfile on disk in fasta format
		if not xtracted_fasfile:
			xtracted_fasfile = "xtracted_fasta.fas"
		fas_subfile.fasfile = xtracted_fasfile
		fas_subfile.write(fas_outfile=xtracted_fasfile)
		#
		return fas_subfile		
	
	# ------------------------------------------------------------------
	
	def write_subfile(self, which=[], seq_ids=[], subfile=None):
		"""
		"""
		with open(subfile, "w") as f_sub:
			if len(which)>0:
				for w in which:
					item 	= ">" + self.seq_id[w]
					f_sub.write(item + "\n")
					item	= self.seq_words[w]
					f_sub.write(item + "\n")
	# ------------------------------------------------------------------
	
	def write(self):
		"""
		"""
		print("run yap_classes:fasta:write()")
		fastafile	= self.basename + ".fas"
		print("fasta file to be written is", fastafile)
		#
		# warning: to be used if initialization with h5file only
		#
		with open(fastafile, "w") as f_fas:
			for i in range(self.n_seq):
				item = ">" + self.seq_id[i]
				f_fas.write(item + "\n")
				item = self.seq_words[i]
				f_fas.write(item + "\n")

	# ------------------------------------------------------------------
	
	def plot_length(self, col="red", dots=True, d_size=20, title=None, x11=True, imfile=None, fmt="png"):
		"""Plots the lengths of the sequences in decreasing order
		
		Parameters
		----------
		
		col : string
			color of the plot
			
		dots : boolean
			whether to plot a dot for each sequence
			
		d_size : integer
			size of the dots if plotted
			
		title : string
			title of the plot
			
		x11 : boolean
			whether to display the plot on the screen
			
		imfile : string
			name of the file where to save the plot
			
		fmt : string
			format of the saved file
			
		Notes
		-----
		
		
		af, 23.07.07
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		y	= sorted(self.seq_len, reverse=True)
		x	= list(range(len(y)))
		plt.plot(y, c=col)
		if dots:
			plt.scatter(x,y,c="red",s=d_size)
		plt.xlabel("rank")
		plt.ylabel("sequence length")
		if title:
			plt.title(title)
		#
		if imfile:
			plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
			print("plot saved in file ", imfile + "." + fmt)
		if x11:
			plt.show()
	
	# ------------------------------------------------------------------

	def plot_length_histogram(self, bins=50, col="orange", kde=True, title=None, x11=True, imfile=None, fmt="png"):
		"""plots the histogram of lengths of sequences
		
		Parameters
		----------
		
		bins : integer
			number of bars in the histogram
			
		col : string
			color of the bars
			
		kde : boolean
			whether to add the kernel density estimation
			
		title : string
			title of the plot
			
		x11 : boolean
			whether to display the plot on the screen
			
		imfile : string
			name of the file for saving plot
			
		fmt : string
			format for saved plot
			

		Notes
		-----
		
		kde is computed and plotted with  seaborn.   
		
		af, 22/09/2016, 23.04.22, 23.07.07
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		name = "seq_len"
		s = pd.Series(self.seq_len)
		s.to_frame(name = name) 
		sns.displot(s, bins=bins, color=col, kde=kde)
		#
		plt.title(title)
		#
		if imfile:
			plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
		#
		if x11:
			plt.show()				

	# ------------------------------------------------------------------

	def align(self, aligned_file=None, view=True):
		"""Multiple alignment with mafft
		
		Parameters
		----------
		
		aligned_file : string
			name of the file with aligned sequences (in fasta format)
			
		view : boolean
			if True, displays the aligned file on screen with seaview

		Notes
		-----
		
		* aligns the fasta data set
		* if no aligned_file name is given, it will be called <basename>_mafft.fas

		af, from `declic`, revised 21.01.10, 23.07.07, 24.04.25
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		if not aligned_file:
			aligned_file	= self.basename + "_mafft.fas"
		cmd = "mafft  --reorder " + self.fasfile  + " > " + aligned_file + " 2> /dev/null"
		os.system(cmd)
		
		if view:
			os.system("seaview " + aligned_file)
			
	# ------------------------------------------------------------------
	
	def disedi(self, dis_type="dis", algo="sw"):
		"""
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		cmd = f'disedi -q {self.fasfile} -o {self.basename} -a {algo} -f {dis_type}'
		print(cmd)
		os.system(cmd)
		
	# ------------------------------------------------------------------
	
	def get_histograms(self, k=6):
		print("run yap_classes:fasta:get_histograms()")
		def rev_complement(seq):
		  comp = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'N': 'N',
				  'a': 'T', 'c': 'G', 'g': 'C', 't': 'A', 'n': 'N'}
		  return "".join(comp[n] for n in seq[::-1])
		
		# jellyfish kmers
		def jfkm(seq_ids):
			for f in seq_ids:
				h_k = {}
				f += '.jf'
				km =  jf.ReadMerFile(f)
				for mer, count in km:
				  h_k[str(mer)] = count
				h_count = np.array([], dtype='int32')
				for ik,km in enumerate(all_k):
					try:             h_k[km]
					except KeyError: h_k[km] = 0
					h_count = np.append(h_count,h_k[km])
				print(f[:-3] + '\t' + '\t'.join(h_count.astype(str)),file=histo_h)
		
		seq_ids = []
		word    = []
		for f in glob.glob('*.jfas'): os.remove(f)
		for f in glob.glob('*.jf'):   os.remove(f)
		
		with open(self.fasfile,'r') as fas_in:
			for myID, mySeq in next_seq(fas_in):
				mySeq = mySeq.replace('\n','')
				myID = myID.split()[0]
				with open(myID + '.jfas', 'w') as fas_out:
					print(f'>{myID}\n{mySeq}', file=fas_out)
				seq_ids.append(myID)
				word.append(mySeq)

		cmd = "parallel jellyfish count -m " + str(k) + " -s 100000 -c 1 {.}.jfas -o {.}.jf ::: *.jfas"
		os.system(cmd)

		all_k = set()
		for f in seq_ids:
			f += '.jf'
			km =  jf.ReadMerFile(f)
			for mer, count in km:
				all_k.add(str(mer))

		all_k = sorted(all_k)
		with open(f'{self.basename}.k{k}.histo', 'w') as histo_h:
			print('SEQ_ID\t' + '\t'.join(all_k),file=histo_h)
			jfkm(seq_ids)

		print(len(seq_ids), 'reads')
		print(4**k,'kmers possibles')
		print(len(all_k), 'réalisés')

		for f in glob.glob('*.jf'):
		  os.remove(f)
		for f in glob.glob('*.jfas'):
		  os.remove(f)

	# --------------- CoA of histograms
	
	def coa_kmers(self, histfile, transpose=True):
		"""
		"""
		print("ok for coa")


	# -------------- Visualization -------------------------------------

	def view(self):
		"""	displays with dots and colors a fasta file on the screen

		Notes
		-----
		uses a call to seaview

		af & jmf, from `declic` 
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		# quite easy ...
		# lire le fichier fasta  
		os.system("seaview " + self.fasfile)
		
	# ----------------------------------------------------------------------
	
	def dereplicate(self):
		"""
		Dereplicate 'in-place' with: vsearch --fastx_uniques
		os.rename(f'{self.basename}.fas', f'{self.basename}.dup.fas')
		cmd = f'vsearch  --fastx_uniques {self.basename}.dup.fas --fasta_width 0 --sizeout --fastaout {self.basename}.fas'
		os.system(cmd)
    """
		
	# -------------------------------------------------------------------
	
	def swarm(self, basename=None, d=1):
		"""
		Launch swarm
		  swarm -d <d> -a 1 -z -t 6

		 Save results in an .clus file

		jmf, 22.08.25 -- jeudi 17 août 2023 16:12, 24.02.01
		mercredi 06 novembre 2024 11:31
		"""
    
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		#fasfile witout N's for swarm
		if os.path.isfile(f'{self.basename}.fastq'):
			shutil.copy2(f'{self.basename}.fas', f'{self.basename}.fas_with_N')
			fastq(fastqfile=f'{self.basename}.fastq').to_fasta(qual_min=0)
		else:
			with open(f'{self.basename}.fas', 'r') as fas_in, open('noN.fas', 'w') as fas_out:
				for myID, mySeq in next_seq(fas_in):
					mySeq = mySeq.replace('N', '')
					print(f'>{myID}\n{mySeq}', file=fas_out)
			cmd = f'vsearch  --fastx_uniques noN.fas --fasta_width 0 --sizeout --fastaout {self.basename}_derep.fas 2> /dev/null'
			print(cmd)
			os.system(cmd)
    
		
		#swarm
		cmd = f'swarm -d {str(d)} -a 1 -z -t 6 -o SWARM {self.basename}_derep.fas'
		if 'DEBUG' not in os.environ:
			cmd += ' 2> /dev/null'
		print(cmd)
		os.system(cmd)


		#parse swarm
		otu  = []
		clus = []
		with open('SWARM','r') as swarm:
			for l in swarm:
				l = l.rstrip('\n')
				otu.append(l + ' ')
		for o in sorted(otu, key=lambda item: len(item), reverse=True):
			s = sorted([self.seq_id.index(i.split(';')[0]) for i in o.split()])
			clus.append(s)
			
		os.remove('SWARM')
		os.remove('noN.fas')
		os.remove(f'{self.basename}_derep.fas')
		
		clus 	= [sorted(xx) for xx in clus]
		clus.sort(key=len, reverse = True)

		'''
		n_item = sum([len(i) for i in clus])
		if self.n_seq != n_item:
			print(f'{self.n_seq=} {n_item=}')
			print("\nNot all reads in OTUs !!\nProbably because SWARM can't create OTUs\nCheck the presence of non ACGTN nucleotide in reads\n")
		'''

		return clus


		
	# ------------------------------------------------------------------
	
	def afc(self, k=6):
		"""
		to do ...
		
		- gets one histogram of kmers by sequence, in counts 
		- builds the array sequance x histogram 
		- it is a contingency table
		- do the CoA of the array
		
		af, 23.12.26
		"""
		pass
# ======================================================================
#
#	   				mafft
#
# ======================================================================


class aligned_fas():
	"""
	aligned fatsa file 
	"""
	
	def __init__(self, mafftfile=None):
		"""loading from an aligned fasta file
		
	
		af, 23.12.23
		"""
		self.c_name		= "aligned_fas"
		yut.print_through_init(self.c_name)
		
		# print_col("run yap_classes:aligned_fas:init()", color="purple")
		
		if mafftfile:
			with open(mafftfile,'r') as fasta_sequences:
				seq_id		= []
				seq_words	= []
				for myId, myFasta in self.next_seq(fasta_sequences):
					seq_id.append(myId)
					seq_words.append(str(myFasta))
		#
		seq_len = [len(w) for w in seq_words]
		#
		self.seq_id		= seq_id
		self.seq_len	= seq_len
		self.seq_words	= seq_words
		self.n_seq		= len(seq_id)
		self.n_pos		= self.seq_len[0]
		self.mafftfile	= mafftfile
		#
		print("There are", self.n_seq, "sequences and", self.n_pos, "positions")
		self.mafftfile	= mafftfile			   # will be useful for some methods, like <view> or <align>
		#
		# test for doubled identifiers
		#
		seq_names   = list(set(self.seq_id))	# list of dereplicated seq_id's
		for item in seq_names:				  # for each name ...
			nn  = self.seq_id.count(item)	   # nbre of sequences with the name as seq_id
			if nn > 1:						  # not very good ...
				print("!!! ------------ warning -----------------------------------   !!!")
				print("sequence " + item + " is present " + str(nn) + " times")
				print("It is highly likely that further calculation will be negatively impacted")

				print("------------------------------------------------------------------")
			#
		#
		os.makedirs("mafft/analyses", exist_ok=True)
		
	# ------------------------------------------------------------------
	
	# for reading fasta files
	def next_seq(self, handle):
		""" creates a generator which provides the next seq of a fasta file, at each call

		Usage:
			>>> with open(fasfile ,'r') as fas_in:
			>>> 	for myID, mySeq in next_seq(fas_in):
			>>> 		print myID,mySeq
		"""
		myID  = False
		mySeq = ''
		for line in handle:
			line = line.rstrip('\n')
			if line.startswith('>'):
				if myID:
					yield myID,mySeq
				mySeq = ''
				myID = line[1:]
			else:
				mySeq += line
		yield myID,mySeq
	
	# ------------------------------------------------------------------
	
	def seaview(self):
		"""
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		cmd	= "seaview " + self.mafftfile + " &"
		os.system(cmd)
		
	# ------------------------------------------------------------------
	
	def alleles_by_position(self, x11=True):
		"""
		
		@ Tab	nupy array	row:		a sequence
							columns:	counts of a, c, g, t, '-' in the sequence
		
		@ sh	Shannon entropy of the allele diversity per position
		
		order of alleles is a, c, g, t, -
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		dic		= {}
		nb_all	= [-1]*self.n_pos
		Tab		= np.zeros((self.n_pos, 5))
		#
		for i in range(self.n_pos):
			alleles 	= []	# will contain alleles of all sequences at same position <i>
			for words in self.seq_words:
				alleles.append(words[i])
			Tab[i,0]	= alleles.count('a')
			Tab[i,1]	= alleles.count('c')
			Tab[i,2]	= alleles.count('g')
			Tab[i,3]	= alleles.count('t')
			Tab[i,4]	= alleles.count('-')
			all_set		= list(set(alleles))
			nb_all[i]	= len(all_set)
			dic[i]		= all_set
		#
		sh	= [0]*self.n_pos
		for i in range(self.n_pos):
			distrib	= Tab[i,:]/self.n_seq
			sh[i]	= shannon(distrib)
		#
		self.nb_all	= nb_all
		self.dic	= dic
		self.Tab	= Tab
		self.sh		= sh
		#
		Tabfile	= "tabfile.csv"
		n		= Tab.shape[0]
		with open(Tabfile, "w") as f_tab:
			for i in range(n):
				vec		= Tab[i,:]
				pl		= [str(int(val)) for val in vec]
				for j, s in enumerate(pl):
					if s == '0':
						pl[j]	= '.'
				item 	= ('\t').join(pl)
				f_tab.write(item + "\n")
			
	# ------------------------------------------------------------------
	
	def show_tab(self):
		"""
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		cmd	= "libreoffice tabfile.csv &"
		os.system(cmd)
		 
	# ------------------------------------------------------------------
	
	def plot_position_entropy(self, col="blue", title=None, x11=True, imfile=None, fmt="png"):
		"""
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		x	= list(range(len(self.sh)))
		plt.scatter(x, self.sh, c=col, s=3)
		plt.xlabel("position in sequence")
		plt.ylabel("Entropy of alleles diversity")
		if title:
			plt.title(title)
		#
		if imfile:
			plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
			print_col("Entropy per position plotted in file", imfile, color="cyan")		
		if x11:			
			plt.show()
				
	# ------------------------------------------------------------------
	
	def plot_nb_alleles(self, col="red", title=None, x11=True, imfile=None, fmt="png"):
		"""
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		x	= list(range(len(self.nb_all)))
		plt.scatter(x, self.nb_all, c=col, s=5)
		plt.xlabel("position in sequence")
		plt.ylabel("Number of alleles")
		if title:
			plt.title(title)
		#
		if imfile:
			plt.savefig("%s.%s" % (imfile, fmt), format=fmt)		
		if x11:
			plt.show()
	
	# ------------------------------------------------------------------
	
	def build_heterogeneous_positions(self, sub_mafftfile):
		"""
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		which 	= [i for i, val in enumerate(self.nb_all) if val>1]
		n_sub	= len(which)
		print("There are", n_sub, "heterogeneous positions")
		#
		seq_sub_words	= [""]*self.n_seq
		for i in range(self.n_seq):
			s			= self.seq_words[i]
			pl_s		= list(s)
			sub_pls		= [val for w, val in enumerate(pl_s) if w in which]
			sub_word	= ''.join(sub_pls)
			seq_sub_words[i]	= sub_word
		#
		self.seq_sub_words	= seq_sub_words
		#
		with open(sub_mafftfile, "w") as f_fas:
			for i in range(self.n_seq):
				item 	= ">" + self.seq_id[i]
				f_fas.write(item + '\n') 
				f_fas.write(seq_sub_words[i] + '\n') 
		#
		os.system("seaview " + sub_mafftfile + " &")
	
	# ------------------------------------------------------------------
	
	def complete_analyse(self):
		"""
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		self.alleles_by_position()
		self.plot_alleles_entropy()
		self.plot_nb_alleles()
		
		

# ======================================================================
#
#	   				Histograms
#
# ======================================================================

class histo_kmers():
	""" Tabel of kmers histograms
	"""
	
	def __init__(self, histfile):
		"""
		"""
		self.c_name	= "histo_kmers"
		yut.print_through_init(self.c_name)
		#
		A, rownames, colnames = load_histfile(histfile)
		(n,p)	= A.shape
		#
		self.A			= A
		self.seq_id 	= rownames
		self.kmers 		= colnames
		#
		print("matrix A has", n, "rows and", p, "columns")
		#
		x	= histfile.split('.')
		self.basename	= x[0]
		xx	= x[1]
		print(xx)
		k	= xx[1:]
		self.coordfile	= self.basename + ".k" + k + ".coa"
		
	# ------------------------------------------------------------------
	
	def coa(self, r, meth, transpose):
		"""CoA of histogram table
		
		Notes
		-----
		
		The rows are the sequences, and the colulmns are the kmers
		There may be more kmers than sequences, especially in barcoding
		In such a case, the table must be tranposed (transpose=True)
		
		For the outcomes:
		- Y is the point cloud of sequences
		- Y_k is the point cloud of kmers
		whatever the value of 'transpose'.
		Both can be plotted together as classically in CoA with method plot_coa()
		
		The parameter 'k' of the CoA in diodon is called here 'r'
		(here, 'k' is the length of kmers)
		It should be tuned to a given value when method 'grp' is used
		Otherwise, it can be left at its default value 'r = -1'
		
		af, 24.01.08
		"""
		L, Y, Y_k = dio.coa(self.A, k=r, meth=meth, transpose=transpose)
		#
		if transpose==True:
			Y, Y_k	= Y_k, Y
		#
		(n,p)	= Y.shape
		print("matrix of reads point cloud has", n, "rows and", p, "columns")
		#
		self.L		= L
		self.Y		= Y
		self.Y_k	= Y_k
		#
		return
		
	# ------------------------------------------------------------------
	
	def plot_coa(self):
		"""plots CoA of histogram table
		
		
		af, 24.01.08
		"""
		dio.plot_coa(Y_r=self.Y, Y_c=self.Y_k, col_dsize=5, col_col="gray")
		
	# ------------------------------------------------------------------

	def write_components(self):
		"""Writes the component array `self.Y_c` in a file
		
		Parameters
		----------
		
		coordfile : string 
					the name of the file
					
		Returns
		-------
		
		Nothing
		
		
		Notes
		-----
		
		It writes a file in ascii format with `\t` as delimiters.
		
		af, 21.07.18
		"""
		print("writing components ...")
		#
		(n,p)	= self.Y.shape
		print(n,p)
		headers	= ['F'+str(i+1) for i in range(p)]
		#
		with open(self.coordfile, "w") as out_handle:
			headers		= ["SEQ_ID"] + headers
			item	= ('\t').join(headers)
			out_handle.write(item + "\n")
			for i in range(n):
				xi		= self.Y[i,:]
				xi_str	= [str(val) for val in xi]
				xi_str 	= [self.seq_id[i]] + xi_str
				string 	= ("\t").join(xi_str)
				out_handle.write(string + "\n")
			out_handle.close()		


# ======================================================================
#
#						charmat
#
# ======================================================================

class charmat():
	"""Some elementary methods on a character file
	
	Attributes
	----------
	
	char_df : panda data frame
			  the characters as a dataframe with
			  indices = sequence identifiers
			  columns = taxonmic levels
					
	index : panda Series
			  the index of the data frame char_df
					
	cols : panda Series
			  the column names (headers) of the data frame
					
	n_items : integer
			   the number of items (= sequences) in the data frame
					
	n_var : integer
			 the number of variables (= taxonmic levels)
		
	basename : string
				the basename 
	
	Notes
	-----
	
	* The caracter file is uploaded as a panda data frame.
	* the index is composed of sequence identifiers
	* the columns are the taxonomic levels (like order - family - genus - species)
	* the value for index `seqid` and taxonomic level `tax` is the name at level `tax` of the organism having provided sequence `seqid`.
	
	
	There are two families of methods: 
	
	* the ones providing some elementary statistics on the diversity of taxonomic names
	* the ones providing some data structures for coloring points in point clouds or graphs according to the names at a given taxonomic level.
	
	af, 21.07.18, revised 21.07.30
	"""
	def __init__(self, charfile, verbose=True):
		"""Initialisation of the class
		
		Parameters
		----------
		
		charfile : string
					the name of the character file to upload
					
		Returns
		-------
		
		self.char_df : panda data frame
					the characters as a dataframe with
					indices = sequence identifiers
					columns = taxonmic levels
					
		self.index : panda Series
					the index of the data frame char_df
					
		self.cols : panda Series
					the column names (headers) of the data frame
					
		self.n_items : integer
					the number of items (= sequences) in the data frame
					
		self.n_var : integer
					the number of variables (= taxonmic levels)
		
		self.basename : string
					the basename 
		Notes
		-----
		
		The character file is uploaded as a panda dataframe, which is named `char_df` throughout the methods.
		
		af, revised with pandas 21.07.30
		"""
		
		self.c_name		= "charmat"
				
		# loading charfile as a panda data frame
		if verbose:
			yut.print_through_init(self.c_name)
		#
		self.char_df	= pd.read_csv(charfile, delimiter="\t", index_col="SEQ_ID") 
		if verbose:
			print_col("character file loaded with pandas", color="green")
		self.index		= self.char_df.index
		self.cols		= self.char_df.columns
		self.n_items	= self.char_df.shape[0] 
		self.n_var		= self.char_df.shape[1] 
		self.charfile	= charfile
		#
		if verbose:
			print_col(self.n_items, "items - ", self.n_var, "varnames", color="cyan")
			print("\nvarnames are:")
			print_col((" ; ").join(self.cols), color="cyan")
		
		# retrieving basename
		x				= str.split(charfile,".")
		xx				= x[:-1]
		self.basename	= (".").join(xx)
		
		# verbosity
		
		self.verbose	= verbose
		
	# ------------------------------------------------------------------
	
	def get_stats(self):
		"""gives the number of different values per variable
		revised with pandas 21.07.30
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		cols 	= self.char_df.columns.values.tolist()
		data 	= [0]*self.n_var
		val_df	= pd.Series(data, index=cols)
		#
		for taxon in cols:
			v_tax	= self.char_df[taxon].values
			taxa	= list(set(v_tax))
			val_df.loc[taxon]	= len(taxa)
		#
		print(val_df)
		#
		return val_df
	
	# ------------------------------------------------------------------
	
	def get_tax(self, varname):
		"""Returns a column of the array
		
		Parameters
		----------
		
		varname : string
			the name of the column (header)
				  
		Returns
		-------
		
		v_tax : list of strings
			the column of `A` with name `varname`
				
		af, 21.07.18
		revised with pandas 21.07.30
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		np_tax	= self.char_df[varname].values	# is a numpy array
		v_tax	= np_tax.tolist()
		#
		return v_tax
		
	# ------------------------------------------------------------------
	
	def get_seq_id(self):
		""" returns the list of seq_ids
		
		af, 22.08.23
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		seq_id	= self.index.tolist()
		#
		return(seq_id)
	
	# ------------------------------------------------------------------
	
	def get_inventory(self, varname):
		"""gives the number of occurences of each value for a selected variable
		
		Parameters
		----------
		
		varname : string
			name of the selected variable
					
		
		Returns
		-------
		count_df : Series of panda
					index = the different values of the varname selectedthe different values of the varname selected
					value = the number of times the value occurs in the column
		
		Notes
		-----
		
		Displays on the screen the number of occurences for each value of the variable
		
		af, 21.07.18
		revised with panda 21.07.30, renamed 24.02.26
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		v_tax		= self.get_tax(varname)
		res			= count_and_order(v_tax)
		ind			= [item[0] for item in res]
		val			= [item[1] for item in res]
		count_df	= pd.Series(val, index=ind)
		#
		return res, count_df
		
	# ------------------------------------------------------------------
	
	def seq_by_taxa(self, varname, sptfile):
		"""provides the list of seq_ids labelled with a given taxon
		
		Parameters
		----------
		varname		string		the character to select
		sptfile		string		the name of the file to write
		
		Notes
		-----
		
		The output file will be named <basename>.<varname>.spt
		
		The taxa are the one in column <varname> of character file 
		
		af, 24.03.11
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		# gets the column of character file, and erases taxon 'no_hit'
		v_tax	= self.get_tax(varname)
		taxa	= list(set(v_tax))
		taxa.remove('no_hit')
		n_taxa	= len(taxa)		
		print_col("\nthere are", n_taxa, "taxa", color="cyan")
		#
		seqids	= self.index.values.tolist()
		dic_spt	= {}
		for taxon in taxa:
			dic_spt[taxon]	= []
		for i, seq_id in enumerate(seqids):
			taxon	= v_tax[i]
			if taxon != 'no_hit':
				dic_spt[taxon].append(seq_id)
		#
		res	= count_and_order(v_tax)
		print(res)
		with open(sptfile, "w") as f_spt:
			for tup in res:
				taxon	= tup[0]
				if taxon != 'no_hit':
					pl_seqs	= ('\t').join(dic_spt[taxon])
					item 	= taxon + '\t' + pl_seqs
					f_spt.write(item + '\n')
		print_col("\nsequences per taxa written in file", sptfile, color="green")
		#
		#return dic_spt
		
		
	# ------------------------------------------------------------------
	
	def build_colorfile(self, varname, default_col="grey", colorfile=None):
		"""Builds a color file with grey for any value of the character
		
		Parameters
		----------
		
		varname : string
				    one of the taxonomic level in charfile
				    
		default_col : string
						default color when no specific color has been selected for a taxon
						
		colorfile : string
					name of the colorfile
					
		
		Returns
		-------
		
		Col_df : panda data frame
					data frame for color selection
		
		
		Notes
		-----
		
		Col_df has 
		
		* the taxa names for the selected level as index
		* two columns as values: the count of the name as integer, and the selected color as stringtwo columns as values: the count of the name as integer, and the selected color as string
		
		
		The default color is selected for any taxon. This can be updated with the method `change_color()`
		
		af, 21.07.26
		revised with pandas: 21.07.30
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		v_tax	= self.get_tax(varname)
		taxa	= sorted(list(set(v_tax)))
		n_taxa	= len(taxa)
		data	= np.empty((n_taxa,2),dtype=object)
		for i in range(n_taxa):
			data[i,0]	= v_tax.count(taxa[i])
			data[i,1]	= default_col
		Col_df	= pd.DataFrame(data, index=taxa, columns = ['counts','color'])
		#
		#if not colorfile:
		# defines automatically the name of colorfile as <basename>.col
		#	colorfile	= self.basename + ".col" 
		#
		#if colorfile:
		Col_df.to_csv(colorfile, sep="\t")
		#
		print_col("Color table written in file", colorfile, color="cyan")
		#
		#return Col_df
		
	# ------------------------------------------------------------------
	
	def set_colors(self, varname, colorfile):
		"""
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		v_tax	= self.get_tax(varname)
		taxa	= sorted(list(set(v_tax)))
		n_taxa	= len(taxa)
	
	# ------------------------------------------------------------------
	
	def get_colors(self, colorfile, v_tax):
		"""colors for a character from a colorfile
		
		Parameters
		----------
		
		colorfile : string
			a color file
			
		
		v_tax : list of strings
			a list of values per item of a character
			
		
		Returns
		-------
		
		v_col : list of strings
			a list of colors, one for each item
			
		dicol : dictionary
			key: a possible value of the character
			value: the color attibuted to it in colorfile
			
		
		Notes
		-----
		
		A color file is an ascii file with suffix `.col` with one row per item and three columns: 
		
		* the value of the character 
		* the number of occurences of the value
		* the selected color
		
		
		Most of values have color as `grey`, but a few ones which are highlighted by a specific non grey color
		
		
		af, 21.08.29
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		Col_df	= pd.read_csv(colorfile, delimiter="\t", index_col=0) 
		print(Col_df)
		ind	= Col_df.index.values.tolist()
		dicol	= {}
		
		#Col	= np.loadtxt(colorfile, delimiter="\t", dtype=object)
		#print(Col)
		for item in ind:
			dicol[item] = Col_df.loc[item, 'color']
		v_col	= [dicol[item] for item in v_tax]
		#
		return v_col, dicol
		
	# ------------------------------------------------------------------
	
	def change_colorfile(self, colorfile=None):
		"""Changes interactively the colors in colorfile
		
		Parameters
		----------
		
		colorfile : string
						name of the colorfile
						
		Returns
		-------
		
		Col_df : panda data frame
					the new dataframe for colors
		
		Notes
		-----
		
		The default value for `colorfile` is `None`. In such a case, the program selects <basename>.col as name for the color file. If a colorfile name is given, like in `change_colorfile(colorfile="toto.txt")`, then the colorfile name will be "toto.txt".
		
		The changes of colors for one taxon are done one by one interactively with the keyboard, with python command `input()`, as follows:
		
		* the first invite is to select a valid taxon
		* if the name typed is in the taxa list, a second invite asks for a new color
		* once it has been typed, the row for the taxon is updated accordingly
		* if the name typed is not in the taxa list, it means that the user wants to exit (no new color to update)
		* before exiting, a confirmation is asked; if the answer is yes, the programs exists the update of new colors, and the new colorfile is saved.
		
		
		af, 21.07.30
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		if not colorfile:
			# defines automatically the name of colorfile as <basename>.col
			colorfile	= self.basename + ".col" 
		#
		if colorfile:
			Col_df	= pd.read_csv(colorfile, delimiter="\t", index_col=0)  
		print(Col_df)
		#
		taxa	= Col_df.index.values.tolist()
		flag	= True
		while flag:
			rep 	= input ("enter a taxon name: ")
			if rep in taxa:
				print("ok, now select a color")
				col	= input("enter a color name: ")
				Col_df.loc[rep,'color']	= col
				print(Col_df)
			else:
				print("it is not a taxon; do you want to exit?")
				rep_exit	= input("type <yes> if you want to exist: ")
				if rep_exit=="yes":
					Col_df.to_csv(colorfile, sep="\t")
					flag = False
		#
		return Col_df
		
		
	# ------------------------------------------------------------------
	
	def show(self, outfile=None):
		"""
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		if outfile:
			outfile = outfile + ".txt"
			f 		= open(outfile,'w')
			toprint	= self.char_df.to_csv()
			print(toprint, file=f)
		else:
			print(self.char_df)
			
	# ------------------------------------------------------------------
	
	def add_varname(self, varname, new_col):
		"""adds a new character in character file
		
		Parameters
		----------
		
		varname		string
					name of the new variable
					
		new_col		list of strings (new column)
					values of the new variable
					
		af, 23.08.07
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		self.char_df[varname]	= new_col
		self.char_df.to_csv(self.charfile, sep='\t')
		
	# ------------------------------------------------------------------
	
	def xtract_by_name(self, varname, value, outfile=None):
		"""
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		v_tax	= self.get_tax(varname)
		which	= [i for i, item in enumerate(v_tax) if item==value]
		#
		if outfile:
			indices		= self.char_df.index.tolist()
			which_rows	= [indices[w] for w in which]
			sub_df		= self.char_df.loc[which_rows,:]
			sub_df.to_csv(outfile, sep='\t')
			print_col("character subfile written as", outfile, color="cyan")
		#
		return which
	
	# ------------------------------------------------------------------
	
	def write2tsv(self):
		"""
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		self.char_df.to_csv(self.charfile, sep='\t')
		
	# ------------------------------------------------------------------
	
	def write_subfile(self, which, varname, value, charsubfile):
		"""
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		self.char_df.loc[char_df[varname] == value]
		self.char_df.to_csv(charsubfile, sep='\t')
		
# ======================================================================
	
		
# ----------------------------------------------------------------------
#
#						yapgraph
#
# ----------------------------------------------------------------------

class graph():
	""" Class for building, analysing and displaying a graph built from a distance matrix
	
	Attributes
	----------
	A : numpy array
			adjacency matrix of the graph (boolean)
	clus :	list of lists
			connected components
	deg : list of integers
			degrees of nodes (in same order than rows in Dis)
	G : graph in networkx graph format
			graph built by thresholding the distance matrix
	n : integer
			number of sequences
	n_cc : integer
			number of cc			
	sizes : list of integers
			sizes of each cc
	
	"""
	#
	def __init__(self, A, G, basename=None, dis_algo=None, gap=None, seq_id=[]):
		""" Initialisation of an instance of class yapgraph
		
		Parameters
		----------
		
		A : numpy array
			adjacency matrix of the graph
			
		G : networkx objet
			a graph
			
			
		Returns
		-------
		
		n : integer
			the number of nodes of `G`
				
		Notes
		-----
		- the graph is given as a networkx object
		
		
		Examples
		--------
		
		Here is how to build a `graph` object from an adjacency matrix
		
		>>> A = ...                        # building the adjacency matrix
		>>> G = nx.from_numpy_matrix(A)    # building a `networkx` object
		>>> gr = yap.graph(A,G)            # building a `graph` object
		
		Here is how to build a graph by thresholding a distance matrix
		
		>>> disfile = ...                          # selecting a file with a distance matrix
		>>> gap = ...                              # selecting a gap
		>>> dis = yap.distances(disfile=disfile)   # reading the matrix
		>>> gr = dis.to_graph(gap)                 # building the graph with a given gap 
		>>> gr.plot()                              # plotting the graph
		
		af, 21.09.20
		"""
		#
		self.c_name	= "graph"
		yut.print_through_init(self.c_name)
		
		#
		self.A			= A.copy()
		self.G			= G
		self.n			= A.shape[0]
		self.basename	= basename
		self.dis_algo	= dis_algo
		self.gap 		= gap
		self.seq_id		= seq_id
		print("graph_disalgo", self.dis_algo)
		
	# ------------------------------------------------------------------
	
	def build_cc(self):
		""" Bulding connected components
		
		Parameters
		----------
		
		save_clus	:	True/False,		save clus (or not)
		set_part	:	True/False,		return class part (or not)
		
		Returns
		-------
		
		part:	instance of class partition if set_part is True
		
		Notes
		-----
		
		- builds as attribute:	clus	:	list of lists
											connected components
								sizes	: 	list
											sizes of each cc
								n_cc	:	integer
											number of cc
		
		- connected components are computed in graph in networkx format in attribute `G` with function `nx.connected_components()` of networks library.
		- each output (`clus`, `sizes`, `n_cc`) is stored as an attribute of the object with same name.
		
		- each cc is given as a list of nodes, and clus is the list of cc sorted (i) in decreasing size for cc (largest first) (ii) in increasing node index within a cc
		
		
		Examples
		--------
		
		>>> gr = ...                                  # building a graph as a yap.graph object
		>>> gr.build_cc()                             # building connected components
		>>> print(gr.n_cc, "connected components")    # prints the numver of connected components
		>>> for c in gr.clus:                         # prints all connected components (by nodes indices)
		...     print(c)
		...
		>>> print(gr.sizes)                           # prints sizes of connected components  
		
		
		af, 21.09.20, 22.08.27
		"""
		
		print("[yap_classes]:[graph]:[build_cc()]")
		comp 	= nx.connected_components(self.G)
		pl		= list(comp)
		clus	= [list(item) for item in pl]
		clus 	= [sorted(xx) for xx in clus]
			
		#
		clus.sort(key=len, reverse = True)
		#self.clus	= [cc for cc in clus]
		#self.sizes	= [len(item) for item in clus]		# size of each cc
		#self.n_cc	= len(clus)							# number of cc
		print("\nthere are", len(clus),"otus")
		#
		# part = partition(clus=clus)
		return clus
		#
		"""
		if clusfile:
			self.write_clus(clusfile=clusfile)
		#
		if to_part:
			part 		= sel.to_partition()
			#
			return part
		"""
		
		
	# ------------------------------------------------------------------
	
	def plot(self, tool = "quick", col_node="red", col_edge="gray", dot_size=10, legend=False, dicol=None, title=None, x11 = True, imfile=None, fmt="png"):
		""" Plots a graph in networks format
		
		Parameters
		----------
		
		tool : string
			   a method for displaying the graph
		
		col_nodes : string
				color of nodes
				
		col_edges : string
				color of edges
				
		dot_size : integer
				dot sizes
				
		legend : boolean
				wheter a legend for colors is displayed or not
				
		dicol : dictionary
				a dictionary of colors associated to features
				
		title : string
				the title of the plot
				
		x11 : boolean
				whether to display the graph on screen or not
				
		imfile : string
				file name for saving the plot
				
		fmt : string
				format for saving the plot
				
				
		
		Returns
		-------
		
		A plot
		
		Notes
		-----
		
		- the graph drawers of networkx are used
		- if `tool==quick`, simple method `nx.draw()` is used
		- if `tool==networkx`, the `spring` layout is computed, and `matplotlib` functions are used to display nodes and edges
		- we may have features associated to nodes, and wish to display each node with a color associated to its feature. To do this, select for `col_nodes` a list with as many items as nodes, with the color attributed to each node  
		- the legend can be displayed with `legend==True`. For this, a dictonary with features as keys and colors as values must be given in `dicol`.
		- if `x11==True`, the graph is displayed on the screen, and not if `x11==False`; default value is `True`
		- if `imfile` is given, the plot is saved with format `fmt` in file `imfile`
		
		af, 21.07.06
		"""
		#
		print("[yap_classes]:[graph]:[plot()]")
		print("plotting with tool", tool, "...")
		if tool=="quick":
			s = np.pi*dot_size*dot_size/4
			s = 2*dot_size
			f = plt.figure()
			nx.draw(self.G, node_color=col_node, edge_color=col_edge, node_size=s, ax=f.add_subplot(111))
			#
			if legend:
				p, keys = get_legend(dicol)
				plt.legend(p, keys, loc='upper center', bbox_to_anchor=(0, 1.02, 1, 0.1),
				  ncol=3, fontsize=8, fancybox=True, shadow=True)		
			if title:
				plt.xlabel(title)		# title is as xlabel to alllow room for legend and xlabels not necessary
			if imfile:
				f.savefig(imfile + "." + fmt)
				print("graph saved in file", imfile + "." + fmt)
			if x11:
				plt.show()
			else:
				plt.close()
			
		#
		if tool=="networkx": 
			#s = np.pi*dot_size*dot_size/4
			nx_pos	= nx.spring_layout(self.G)
			n		= len(list(nx_pos))
			xy		= np.zeros((n,2))
			x		= xy[:,0]
			y		= xy[:,1]
			for key in nx_pos.keys():
				i		= int(key)
				xy[i,:]	= nx_pos[key] 
			# plots edges first (zorder=1)
			for i in range(n):
				for j in range(i, n):
					if self.A[i,j] == 1:
						plt.plot([x[i],x[j]],[y[i],y[j]], c=col_edge, zorder=1)
			# and vertices (zorder=2)
			plt.scatter(x,y, c=col_node, s=dot_size, zorder=2)
			#nx.draw(self.G, node_color=v_col, edge_color=col_edge, node_size=s)
			if legend:
				p, keys = get_legend(dicol)
				plt.legend(p, keys, loc='upper center', bbox_to_anchor=(0, 1.02, 1, 0.1),
				  ncol=3, fontsize=8, fancybox=True, shadow=True)				
			if title:
				if legend:
					plt.xlabel(title)
				else:
					plt.title(title)
			#
			if imfile:
				plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
				print("graph saved in file", imfile + "." + fmt)
			if x11:
				plt.show()
		#
		if tool=="graphviz":
			print("plotting with graphviz")
			print("attributes for drawing")
			gvz = nx.nx_agraph.to_agraph(self.G)
			gvz.node_attr['style']		= 'filled'
			gvz.node_attr['fixedsize']	= 'shape'
			gvz.node_attr['shape']		= 'circle'
			gvz.node_attr['labelcolor'] = 'red'
			gvz.node_attr['color']		= col_node
			gvz.node_attr['label']		=  " "
			gvz.node_attr['overlap']	= 'scale'
			gvz.edge_attr['color']		= col_edge
			print("building layout")
			gvz.layout(prog='sfdp')
			print("writing the file")
			gvz.draw(graphfile="temp_graph", prog='sfdp')
			if x11:
				cmd	= "eog " + graphfile
				os.system(cmd) 
	

	
	
	# ------------------------------------------------------------------
	
	def get_degrees(self, plot=True, dots=False, scaled=False, title=None, x11=True, imfile=None, fmt="png"):
		""" Computes and dislays the degree of each node of the graph
		
		Parameters
		----------
		
		dots : boolean
				ig True, dots are added (one per node) in the plot
				
		scaled : boolean
				  if True, degrees are normalized (see notes)
				
		plot : boolean
			   if `True`, a rank-size plot of degrees is displayed
			   
		Returns
		-------
		
		deg : list of integers
				the degrees of each node
				
		Notes
		-----
		
		- the degrees are simply computed from adjacency matrix `A` as attribute of the object
		- they are stored as attribute `deg` of the object
		- they are sorted in decreasing order for display
		- and displayed if `plot==True`
		- if dots==True, one dot per node is added in the plot
		
		if scaled==True, degrees are normalized. each degree is divided by the size of the 
		graph, and the y axis in the plot is forced to be between 0 and 1.
		
		af, 21.07.06
		"""
		print("yapgraph: get_degrees(): ")
		deg			= np.sum(self.A, axis=1)
		if scaled:
			deg	= deg/self.n
		deg	= deg.tolist()
		self.deg	= deg
		#
		if plot:
			deg.sort(reverse=True)
			if scaled:
				plt.ylim([0, 1])		
			plt.plot(deg, c="red", zorder=1)
			if dots:
				x	= list(range(len(deg)))
				plt.scatter(x, deg, c="blue",s=20, zorder=2)
			plt.xlabel("rank of the node")
			plt.ylabel("degree of the node")
			if title:
				plt.title(title)
			plt.plotsave(plt, x11=x11, imfile=imfile, fmt=fmt)
		#
		return deg
			

	# ------------------------------------------------------------------
	
	def color_by_degree(self, pivot=[0.5, 0.8, 0.95], p_col = ['green', 'red', 'purple']):
		"""vector of nodes colors according to their degree
		
		
		Notes
		-----
		This is a draft version 
		
		Examples
		--------
		
		Here here an example with default colors and 
		
		>>> gr = ...                        # giving a graph as a yap.graph object
		>>> v_col = gr.color_by_degree()    # selecting a color according to the degree
		>>> gr.plot(v_col=v_col)            # drawing the graph with theses colors
		
		Here is an example with choice of colors
		
		>>> gr = ...                                   # giving a graph as a yap.graph object
		>>> pivot = [0.3, 0.6, 0.8, 0.9]               # pivot values
		>>> p_col = ['green','yellow','red','purple']  # colors for degree ranges
		>>> v_col = gr.color_by_degree(pivot, p_col)   # selecting a color according to the degree
		>>> gr.plot(v_col=v_col)                       # drawing the graph with theses colors
		
		
		
		af, 21.09.20
		
		"""
		
		n_piv	= len(pivot)		# number of values in pivot
		deg		= np.sum(self.A, axis=1).tolist()
		v_col	= ["blue"]*self.n
		for i, pv in enumerate(pivot):
			which 	= [j for j, val in enumerate(deg) if val >= pv*(self.n-1)]
			for w in which:
				v_col[w] = p_col[i]
		#
		which 	= [j for j, val in enumerate(deg) if val==(self.n-1)]
		for w in which:
			v_col[w] 	= "cyan"
		return v_col

	# ------------------------------------------------------------------
		
	
	def communities_old(self, depth=1):
		""" Deprecated, builds communities and plots the graph with one color per community
		"""
		print("searching communities")
		# colors for communities
		v_set	= ['blue','green','orange','cyan','chartreuse','red','magenta']
		print("Girvan-Newman algorithm ...")
		#cgn						= nx.community.girvan_newman(self.G)
		cgn						= nac.girvan_newman(self.G)
		print("... done")
		print(tuple(sorted(c) for c in next(cgn)))
		print("first step done")
		k=2
		for com in itertools.islice(cgn, k):
			print(tuple(sorted(c) for c in communities))
		print("step done")	
		top_level_communities	= next(cgn)
		print("top level community found")
		for k in range(depth):
			print("k =",k)
			#next_level_communities	= next(cgn)
		print("loop terminated")
		self.partition				= sorted(map(sorted, next_level_communities))
		cove						= nx.algorithms.community.coverage(self.G, self.partition)
		print(len(self.partition), "communities found")
		print("coverage is", cove)
		
	
	# ------------------------------------------------------------------
	
	def communities(self):
		"""
		"""
		#G = nx.barbell_graph(5, 1)
		communities_generator = nx.algorithms.community.girvan_newman(self.G)
		top_level_communities = next(communities_generator)
		next_level_communities = next(communities_generator)
		#
		clus	= sorted(map(sorted, next_level_communities))
		clus 	= [sorted(xx) for xx in clus]
		#
		clus.sort(key=len, reverse = True)
		self.clus	= [cc for cc in clus]
		self.sizes	= [len(item) for item in clus]		# size of each cc
		self.n_cc	= len(clus)							# number of cc		

	# ------------------------------------------------------------------
	
	def to_partition(self):
		"""
		"""
		print("[yap_classes]:[graph]:[to_partition()]")
		part 	= partition(clus=self.clus)
		#
		return part
		
	# ------------------------------------------------------------------
	
	def write_clus_deprecated(self, clusfile, comment=[]):
		"""writes the partition in a file
		
		Parameters
		----------
		clusfile : string
			name of the file to be written
			
		comment : list of strings
			comments to be written at head of the file
		
		Returns
		-------
		Nothing (writes a file)
		
		
		Notes
		-----
		
		- the file contains the indices of the nodes per cluster
			with one row per cluster
		
		af, 21.01.28 ; 21.07.10, 22.08.27
		"""

		print("[yap_classes]:[graph]:[write_clus()]")
		with open(clusfile, "w") as out_handle:
			if len(comment) > 0:
				for element in comment:
					item = "# " + element
					out_handle.write(item + "\n")
			#
			for cc, pl in enumerate(self.clus):
				pl_str	= [str(i) for i in pl]
				string 	= ("\t").join(pl_str)
				out_handle.write(string + "\n")
		print("partition written in file", clusfile)			
		
# ----------------------------------------------------------------------
#
#						otu builder
#
# ----------------------------------------------------------------------

class otu_builder():
	"""
	"""

	def __init__(self):
		"""
		"""
		self.clus	= []
		
	# ------------------------------------------------------------------
	

	# ------------------------------------------------------------------
	
	def to_partition(self):
		"""
		"""
		print("[yap_classes]:[graph]:[to_partition()]")
		#print(self.seq_id)
		#part 	= partition(clus=self.clus, basename=self.basename, gap=self.gap, seq_id=self.seq_id)
		part 	= partition(clus=self.clus, basename=self.basename,seq_id=self.seq_id)
		#
		return part
		
	# ------------------------------------------------------------------
	
	def write_clus_deprecated(self, clusfile):
		# --------------------------------------------------------------
		#
		#		pourquoi un write_clus ici ?
		#
		# --------------------------------------------------------------
		"""writes the partition in a file
		
		Parameters
		----------
		None
		
		Returns
		-------
		Nothing (writes a file)
		
		
		Notes
		-----
		
		- the name of the file is <basename>.<dis_algo>.<gap>.clus
		- the file contains the indices of the nodes per cluster
		
		af, 21.01.28 ; 21.07.10, 22.08.22
		"""
		#
		print("[yap_classes]:[otu_builder]:[write_clus()]")
		with open(clusfile, "w") as out_handle:
			for cc, pl in enumerate(self.clus):
				pl_str	= [str(i) for i in pl]
				string 	= ("\t").join(pl_str)
				out_handle.write(string + "\n")
		print("partition written in file", clusfile)				

# ----------------------------------------------------------------------
#
#						partition
#
# ----------------------------------------------------------------------

class partition():
	"""Provides some useful methods for characterising and analysing clusters
	
	Attributes
	----------
		
	basename : string
		name of the sample on which work is done
		
	clus : list of lists
		a partition of a set {1,n}
		
	n_clus : integer
		the number of clusters in partition       
		
	sizes : list of integers
		the sizes of clusters		       
		
	
	Notes
	-----
	
	- a clustering of a set of item is given as a partition of them
	- items are numbered from `0` to `n-1`
	- clusters are numbered from `0` to `n_clus-1`
	- cluster `c` is given as the list of item indices which are in `c`
	
	
	af, 21.01.10 - 21.07.07 - 21.07.18 - 21.12.02
	"""
	
	def __init__(self, clus=[], clusfile=None, basename=None, seq_id=[]):
		""" Initialisation
		
		Parameters
		----------
		
		clus : list of lists
			a partition of a set {1,n}
		       
		basename : string
			the name of the sample
		
		Returns
		-------
		
		sizes : list of integers
			the sizes of clusters
		        
		n_clus : integer
			the number of clusters in partition
		
		Notes
		----
		- the partition is not started by reading a file, but often called by other classes. Therefore, basename must be given to it explicitely. 
		  It is a class of post-tretment for otus and clustering, which both produce clusters
		- The format is given by format of clus: 
		       it is a  list of list, with clus[i] being the list of indices in cluster $i$ 
		       Then, sizes and number of clusters are computed, and stored.
		
		
		af, 21.01.28, 21.07.07
		"""
		self.c_name	= "partition"
		yut.print_through_init(self.c_name)
		#
		if len(clus) > 0:
			self.clus	= clus
		elif clusfile:
			self.clusfile	= clusfile
			clus 			= []
			with open(clusfile, "r") as f_clus:
				for line in f_clus:
					if line[0] in ['0','1','2','3','4','5','6','7','8','9']:
						line 	= line[:-1]
						pl		= line.split("\t")
						pl_int	= [int(x) for x in pl]
						clus.append(pl_int)
			self.clus		= clus
		# 
		
		self.sizes		= [len(cl) for cl in clus]
		self.n_clus		= len(clus)
		self.basename	= basename
		self.seq_id		= seq_id
		self.n_seq		= len(seq_id)
		self.clusfile	= clusfile
		
		### --------------- dictionary ine to seq_id
		
		dic= {}
		for i in range(self.n_seq):
			dic[i] = self.seq_id[i]
		#
		self.dic_index2seqid	= dic
		
	# ------------------------------------------------------------------
	
	def ami(self, v_tax):
		"""
		
		Parameters
		----------
		
		none
		
		Returns
		-------
		
		lst : list of integers
		
		Notes
		-----
		
			
		af, 23.11.26
		"""
		### --------------------- transform a clus structure into a list
		"""
		Translates a partition as a list. For example, if
			part = [[1,3,4],[0,5],[2]]
		then
			lst	 = [1,0,2,0,0,1]
		which measn that 
			0 is in cluster 1
			1 is in cluster 0
			2 is in cluster 2
		"""
		
		# l_clus is the total number of items 
		
		l_clus	= 0
		for cc in self.clus:
			l_clus	= l_clus + len(cc)
		# 
		print("# of items in partition:", l_clus)
		lst		= [0]*l_clus
		#
		for c, cc in enumerate(self.clus):
			for i in cc:
				lst[i]	= c
		
		### --------------------- transform the list of taxa as a list of classes
		
		set_tax		= list(set(v_tax))
		dic_tax	= {}
		for it, taxon in enumerate(set_tax):
			dic_tax[taxon] = it
		#
		lst_tax	= [-1]*len(v_tax)
		for it, taxon in enumerate(v_tax):
			lst_tax[it]	= dic_tax[taxon]
		
		### ----------------------- compares both lists
		
		if len(lst_tax) != len(lst):
			print("other list has not the same size as the partition")
			print("ami cannot be computed")
		else:
			res	= adjusted_mutual_info_score(lst, lst_tax)
		#
		return res
		
	# ------------------------------------------------------------------
	
	def write_clus(self, clusfile, comment=[]):
		"""writes the partition in a file
		
		Parameters
		----------
		clusfile : string
			name of the file to be written
			
		comment : list of strings
			comments to be written at head of the file
		
		Returns
		-------
		Nothing (writes a file)
		
		
		Notes
		-----
		
		- the file contains the indices of the nodes per cluster
			with one row per cluster
		
		af, 21.01.28 ; 21.07.10, 22.08.27
		"""

		print("[yap_classes]:[partition]:[write_clus()]")
		with open(clusfile, "w") as out_handle:
			if len(comment) > 0:
				for element in comment:
					item = "# " + element
					out_handle.write(item + "\n")
			#
			for cc, pl in enumerate(self.clus):
				pl_str	= [str(i) for i in pl]
				string 	= ("\t").join(pl_str)
				out_handle.write(string + "\n")
		print("partition written in file", clusfile)	
		
	# ------------------------------------------------------------------
	
	def write_clus_byseqid_old(self, comment = []):
		"""
		A REFAIRE
		"""
		print("fonction en chantier ...")
		#return
		
		clusfile = self.clusfile
		print(clusfile)
		x	= clusfile.split(".")
		print(x)
		m	= len(x)
		xx	= x[0:(m-1)]
		print(xx)
		clusfile_byseq	= (".").join(xx) + ".byseq.clus"
		print(clusfile_byseq)
		#
		with open(clusfile_byseq, "w") as f_clus:
			if len(comment) > 0:
				for element in comment:
					item = "# " + element
					f_clus.write(item + "\n")
			#
			for cc in self.clus:
				cc_seqid	= [self.dic_index2seqid[i] for i in cc]
				item		= ('\t').join(cc_seqid)
				f_clus.write(item + '\n')
		print("partition according to seqid written in file", clusfile_byseq)			
		
	# ------------------------------------------------------------------
	
	def write_clus_byseqid(self, clus_by_seqid_file=None, comment = []):
		"""
		"""
		clus		= self.clus
		n_clus		= self.n_clus 
		basename	= self.basename
		seq_id		= self.seq_id
		#
		if clus_by_seqid_file == None:
			clus_by_seqid_file	= f'{basename}.byseqid.clus'
		#
		with open(clus_by_seqid_file,"w") as f_clus:
			for c in range(n_clus):
				pl_ind		= clus[c]
				pl_seqid	= [self.seq_id[i] for i in pl_ind]
				item		= ("\t").join(pl_seqid)
				f_clus.write(item + "\n") 
	# ------------------------------------------------------------------
	
	def vec_of_classes(self, classfile=None):
		"""
		
		Parameters
		----------
		
		classfile : string
			name of file to save the classes
			
			
		Result
		------
		
		vec : list of integer
			the class to which each item belongs to (see notes)
			
		
		Notes
		-----
		
		A partition is given as a list `clus` of lists `cc[0], cc[1], ...`.
		
		Each `cc[i]` is the list of items (by indces) in class `i`. 
		
		This function provides a list of integer `vec` of length `n` if there are `n` items in total,
		with `vec[k]` being the class of item `k`.
		
		For example, if  `clus = [[0,1,6],[2,5],[3,4]]`  
		
		then `vec = [0,0,1,2,2,1,0]`.
		
		af, 21.09.21
			
		"""
		n	= sum(self.sizes)
		vec = [-1]*n
		for c, cl in enumerate(self.clus):
			for i in cl:
				vec[i] = c
		#
		if classfile:
			with open(classfile, "w") as f:
				for k, v in enumerate(vec):
					f.write(str(k) + '\t' + str(v) + "\n")
			f.close()
		#
		return vec
	
	# ------------------------------------------------------------------
	
	def write_otufile(self, otufile, comment):
		"""
		
		Notes
		-----
		in self.clus:
		- c is the index of the otu
		- cc is the list of indices of sequences in the otu
		
		af, 22.08.24
		"""
		print("[yap_classes]:[partition]:[write_otufile()]")
		#
		with open(otufile, "w") as f_otu:
			if len(comment) > 0:
				for com in comment:
					f_otu.write(com + "\n")
			for c, cc in enumerate(self.clus):
				otu_name	= "otu_" + str(c)
				seqs		= [self.seq_id[i] for i in cc] 
				pl_otu		= [otu_name] + seqs
				otu			= ('\t').join(pl_otu)
				f_otu.write(otu + '\n')
		print("otu file written in", otufile)

	# ------------------------------------------------------------------
	
	def composition(self, var, min_size=1, x11=True, ocffile=None, comment=[]):
		""" builds the composition of each cluster according to an external variable
		
		Parameters
		----------
		
		var : list
			list of features for each item
		 
		Returns
		-------
		
		dicount : a dictionary
			key is a 
		
		
		Notes
		-----
		- var must has been computed beforehand (e.g. with charmat)
		- if var=None, composition is given by indices
		
		af, 21.01.10, 21.07.24
		"""
		print("[yap_classes]:[partition]:[composition()]")
		co_tax		= count_and_order(var)
		dic_count	= {}
		for tup in co_tax:
			dic_count[tup[0]]	= int(tup[1])
		self.dicount	= dic_count
		#
		if x11:
			print("to screen")
			for c in range(self.n_clus):
				#out_handle.write("cc " + str(c) + "\n")
				cc		= self.clus[c]
				if len(cc) >= min_size:
					print("cc", c)
					cc_tax	= [var[i] for i in cc]
					taxa	= list(set(cc_tax))
					for tax in taxa:
						which 	= [i for i, truc in enumerate(cc_tax) if truc==tax]
						n_count	= len(which)
						res		= (tax, n_count, dic_count[tax])
						item 	= "\t" + res[0] + "\t" + str(res[1]) + "\t/" + str(res[2])
						print(item)
		#
		if ocffile:
			print("write to file", ocffile)
			#
			with open(ocffile, "w") as out_handle:
				if len(comment) > 0:
					for com in comment:
						out_handle.write(com + "\n")
				#
				for c in range(self.n_clus):
					cc		= self.clus[c]
					if len(cc) >= min_size:
						out_handle.write("\ncc " + str(c) + "\n")
						cc_tax	= [var[i] for i in cc]
						taxa	= list(set(cc_tax))
						for tax in taxa:
							which 	= [i for i, truc in enumerate(cc_tax) if truc==tax]
							n_count	= len(which)
							res		= (tax, n_count, dic_count[tax])
							item 	= "\t" + res[0] + "\t" + str(res[1]) + "\t/" + str(res[2])
							out_handle.write(item + "\n")
				#
				out_handle.close()
		
	# ------------------------------------------------------------------
	
	def plot_rank_size(self, col="red", log=True, title=None, dots=10, min_size=-1, upto=-1, x11=True, imfile=None, fmt="png"):
		""" Plots rank size curve of a cluster
		
		Parameters
		----------
		col : string
		      color of the curve
		      
		log : boolean
		      if True, plots log of sizes
		
		title : string
		        tiltle of the plot
		
		dots : integer
				plots a dot of size <dots> for each otu
				
		min_size : integer
					plots the rank-size up to size min_size
					
		upto : integer
				to be written
		x11 : boolean
		      if true, plots on the screen
		      
		imfile : string
		         file where to save the plot
		         
		fmt : string
		      format for the file with the plot
		      
		Returns
		-------
		
		None, displays a plot or save it as a file
		
		af, 21.01.28, 21.07.10
		"""
		
		print("clusters: plot_rank_size: plotting rank-size curve")
		#
		y	= self.sizes
		#
		if min_size > 0:
			y	= [val for val in y if val >= min_size] 
		#
		print_col(len(y), "otus will be displayed", color="cyan")
		if log:
			y	= np.log(y)
		#
		if upto>0:
			n	= len(y)
			m	= n - upto
			y	= y[:-m]
		#
		plt.plot(y, c=col)
		if dots > 0:
			x	= list(range(len(y)))
			plt.scatter(x, y, c='red', s=dots)
		plt.xlabel("rank")
		if log:
			plt.ylabel("Log of size")
		else:
			plt.ylabel("size")
		#
		if title:
			plt.title(title)
		#
		#plt.plotsave(plt, x11=x11, imfile=imfile, fmt=fmt)
		if title:
			plt.title(title)
		if imfile:
			plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
			print_col("plot saved in file", imfile + "." + fmt, color="cyan")			
		if x11:
			plt.show()
					
				
	# ------------------------------------------------------------------
	
	def write_sizes(self, sizefile, basename=None):
		"""writes the sizes of the clusters in a file
		
		Parameters
		----------
		meth : string
		       method used for computing distances
		       
		Returns
		-------
		Nothing  (writes a file)
		
		
		af, 21.01.28 ; 21.07.10
		"""
		print("clusters: write_sizes: writing cluster sizes in file", sizefile)
		with open(sizefile, "w") as out_handle:
			if basename:
				out_handle.write("# sample: " + basename + "\n")
			out_handle.write("# sizes of the clusters in the partition\n")
			for val in self.sizes:
				out_handle.write(str(val) + "\n")
				
	# ------------------------------------------------------------------
		
	def write_cc_fastas(self, fasdir="fas", fasfile=None, mafftdir="mafft", view=False, min_size=5, multiple=False):
		""" writes a fasta file of reads per otu, and possibly a multifasta
		
		Parameters
		----------
		
		fas : string
		      directory where the fasta files will be written
		      
		min_size : integer
		           minimum size of otus for a fasta file to be written
		           
		multiple : boolean
		           if True, a multifasta file will be written
		
		Returns
		-------
		
		Nothing (writes file(s))
		
		
		Notes
		-----
		
		- a multiple fastafile is a fasta file with one seed per otu
		- its name is <basename>_mult.fas
		
		af, 21.01.28 ; 21.07.10 ; 21.07.23 ; 21.08.04 ; 21.10.31
		""" 
		print("clusters.write_cc_fastas(): writing fasta files for all clusters")
		#
		if fasfile:
			fas				= fasta(fasfile)
			x				= fasfile.split(".")
			if len(x)==2:
				base4fas	= x[0]
			else:
				base4fas		= (".").join(x[:-1])
			multiplefas		= base4fas + "_mult_fas"
		else:
			exit("please give a fasta file!")
		#
		with open(multiplefas, "w") as mult_handle:
			#
			for cc in range(self.n_clus):
				# 
				otu		= "otu_" + str(cc)
				size	= self.sizes[cc]
				#
				if size >= min_size:
					which 			= self.clus[cc]
					cc_fastafile 	= fasdir + "/" + otu + ".fas"
					with open(cc_fastafile, "w") as out_handle:
						for i_seq in which:
							item = '>' + fas.seq_id[i_seq] + '\n'
							out_handle.write(item)
							item = fas.seq_words[i_seq] + '\n'
							out_handle.write(item)
					out_handle.close()
					if mafftdir:
						print("aligning fasta file " + cc_fastafile, "; size =", size)
						cc_mafftfile 	= mafftdir + "/" + otu + "_mafft.fas"
						cmd = "mafft " + cc_fastafile + " > " + cc_mafftfile + " 2> /dev/null"
						os.system(cmd)
						if view:
							os.system("seaview " + cc_mafftfile)
					if multiple:
						i_seq	= which[0]
						item 	= '>' + self.basename + "_otu_" + str(cc) + '\n'
						mult_handle.write(item)
						item 	= fas.words[i_seq] + '\n'
						mult_handle.write(item)						
						
			mult_handle.close()
				
	# ------------------------------------------------------------------

	def write_cc_distances(self, Dis, seq_id, disdir="dis", min_size=5):
		"""writes a text file with pairwise distances between reads within an otu
		
		Parameters
		----------
		
		disdir : string
		         the directory where the distance files will be written
		         
		min_size : integer
		         minimum size of otus for a fasta file to be written
		
		Returns
		-------
		nothing (writes files)
		
		af, 21.01.28, 21.07.10, 21.08.03
		""" 

		print("partition: writing otus distance files ...")
		#self.Dis 	= Dis
		self.seq_id	= seq_id
		#
		for cc in range(self.n_clus):
			# 
			size	= self.sizes[cc]
			otu		= "otu_" + str(cc)
			#
			#if disdir:
				#os.makedirs(disdir, exist_ok=True)
			if size >= min_size:
				print("otu " + str(cc) + " size = " + str(size) + chr(13))
				which 		= self.clus[cc]
				D_cc		= Dis[np.ix_(which,which)]
				seq_id_cc	= [self.seq_id[i] for i in which]
				if disdir:
					cc_disfile 	= disdir + "/" + otu + ".dis"
				else:
					cc_disfile 	= otu + ".dis"
				with open(cc_disfile, "w") as out_handle:
					item = ("\t").join(["seq_id"] + seq_id_cc )
					out_handle.write(item + "\n")
					for ii in range(D_cc.shape[0]):
						row 	= D_cc[ii,:]
						pl_row	= [seq_id_cc[ii]] + [str(x) for x in row]
						item 	= ("\t").join(pl_row)
						out_handle.write(item + "\n")						
				out_handle.close()
				

	# ------------------------------------------------------------------

	def write_cc_char(self, charfile=None, chardir="char", min_size=5):
		"""writes a text file with characters of the otu
		
		Parameters
		----------
		
		disdir : string
		         the directory where the distance files will be written
		         
		min_size : integer
		         minimum size of otus for a fasta file to be written
		
		Returns
		-------
		nothing (writes files)
		
		af, 21.01.28, 21.07.10
		""" 
		print("\nclusters: write_cc_char: writing characters for all cc")
		print("loaading charfile")
		if not charfile:
			charfile	= self.basename + ".char"
		char		= charmat(charfile)
		Char_df 	= char.char_df
		cols		= char.cols
		#
		for cc in range(self.n_clus):
			# 
			otu		= "otu_" + str(cc)
			size	= self.sizes[cc]
			#print(otu, " -- ", size, "items")
			#
			#os.makedirs(chardir, exist_ok=True)
			if size >= min_size:
				cc_charfile 	= chardir + "/" + otu + ".char"
				which 			= self.clus[cc]
				which_seq		= [Char_df.index[w] for w in which]
				Char_cc_df		= Char_df.loc[which_seq]
				Char_cc_df.to_csv(cc_charfile, sep="\t", index_label="SEQ_ID")


# ======================================================================
#
#						Distances
#
# ======================================================================


class distances():
	"""
	Parameters
	----------
	Dis : numpy array
	      distances between items
	 
	n : integer
			number of items in distance array
			
	seq_id : string
			list of sequence identifiers
			
	Notes
	-----
	Is the entry point for various calculations possible on a distance array:
	
	- MDS, with method `mds()`, which calls it from `pydiodon`, and yields an object `point_cloud`
	- t-SNE, with method `tsne()`, which calls it from `scikit-learn`, and yields an object `point_cloud`
	- building a threshold graph, with method `to_graph()`, and yields an object `graph`
	- clustering with HAC, with method `to_tree()`, which calls linkage() in `scipy.hierarchical.clustering`, and yields an object `tree`
	- SBM inference (to be implemented)
	
	af, 21.09.20
	"""
	
	# ------------------------------------------------------------------
	
	def __init__(self, h5file=None, disfile=None, headers=True, rownames=True, zero_diagonal=True, test_symmetry=False, set_diag_to_zero=False):
		"""Initialisation of a distances class with distance files
		
		
		Parameters
		----------
		
		h5file	: string
				file name of distance file to load in hdf5 format
		
		disfile	: string
				file name of distance file to load in ascii format
				
		Returns
		-------
		
		self.Dis : numpy array
					a distance array
					
				
		Notes
		-----
		- loads a distance matrix from a file, either in hdf5 or ascii format.
		- sets the attribute `Dis` with the distance matrix
		- computes the number of sequences and sets the attribute `n` with it.
		
		
		Examples
		--------
		
		Here is a way to read an ascii distance file 
		
		>>> mydisfile = "datasets/lauraceae.dis"
		>>> dis = yap.distances(disfile=mydisfile)
		
		It is compulsory to specify that the distance file `mydisfile` corresponds to argument `disfile`. If not, it will be considered as a hdf5 file and an error will be raised.
		
		It is expected as well that there are rownames and colulmn names in `mydisfile`. If not, it must be specified as
		
		>>> mydisfile = ...
		>>> dis = yap.distances(disfile=mydisfile, headers=False, rownames=False)		
		
		af, 21.09.21	
		"""
		#
		#################################"
		# check if
		# 		seqid or seq_id
		##################################
		
		self.c_name		= "distances"
		yut.print_through_init(self.c_name)
		#
		if h5file and disfile:
			exit("Huston, you have a problem: you should select between h5file or ascii file!")
		#
		if h5file:
			#
			x			= h5file.split(".")
			basename	= x[0]
			#
			print("loading hdf5 file", h5file)
			self.Dis, self.seq_id, self.words	= load_hdf5(h5file)
			self.n		= self.Dis.shape[0] 
			print("done,", self.n, "reads")

			
		#
		if disfile:
			#
			print("\nloading text file " + disfile)
			if (headers and rownames):
				Dis, rn, cn	= load_ascii(disfile, delimiter="\t", headers=headers, rownames=rownames)
				self.seq_id	= rn
			#
			if ((headers==False) and (rownames==False)):
				Dis			= load_ascii(disfile, delimiter="\t", headers=headers, rownames=rownames)
				self.seq_id	= []
			#
			if zero_diagonal==True:
				np.fill_diagonal(Dis,0)
			self.Dis	= Dis
			self.n		= self.Dis.shape[0] 
			print(self.n, "sequences")
		#
		if set_diag_to_zero:
			np.fill_diagonal(self.Dis,0)
		#
		if test_symmetry:
			print("\n[yap_classes][distances][init()][tests()]\n")
			self.tests()
			print("\ntests done!\n")
			
	# ------------------------------------------------------------------
	
	def set_Dis(self, Dis):
		"""Sets attribute `Dis` from external object
		
		Parameters
		----------
		
		Dis : numpy array
				array to be set as `self.Dis`
				
		Returns
		-------
		
		self.Dis : numpy array
					attribute set with value `Dis`
					
		Notes
		-----
		- fills the diagonal of `Dis` with zeros.
					
		af, 21.07.18, 24.12.06
		"""
		np.fill_diagonal(Dis,0)
		self.Dis	= Dis.copy()
		self.n	= self.Dis.shape[0] 
		print(self.n, "sequences")	
		
	# ------------------------------------------------------------------
	
	def set_seq_ids(self, seq_ids):
		"""
		
		Parameters
		----------
		
		seq_ids : list of strings
				list to be set as `self.seq_id`
				
		Returns
		-------
		
		self.seq_id : list of strings
					attribute set with value `seq_ids`
					
		
		Notes
		-----
		Sets as well attribute `self.n` with the length of list `seq_ids`.
					
		af, 21.07.18, 24.12.06
		"""
		self.seq_id	= seq_ids
		self.n		= len(seq_ids)
	# ------------------------------------------------------------------
	
	def tests(self, sym=True, n_val=10):
		"""Two tests on matrix `Dis`
		
		Notes
		-----
		
		tests whether:
		
		* diagonal elements are zero
		* the matrix is symmetric
		
		af, 21.09.18
		"""
		print("test for symmetry running ...")
		# setting the diagonal to zero
		dia	= np.diag(self.Dis)
		M	= np.max(np.abs(dia))
		print("maximum diagonal term:", M)	
		
		# testing if symmetric
		Mat	= self.Dis - self.Dis.T
		M	= np.max(np.abs(Mat))
		print("maximum discrepancy with symmetry:", M)
		if sym:	
			which 	= np.where(Mat>0)
			print(which)
			m	= min(len(which[0]), n_val)
			for x in range(m):
				i = which[0][x]
				j = which[1][x]
				print("(",i,j,")", self.Dis[i,j], Mat[i,j] , "  --  (",j,i, ")", self.Dis[j,i], Mat[j,i])
			
	# ------------------------------------------------------------------
	
	def histogram(self, bins=50, col="orange", alpha=0.75, kde=True, title=None, x11=True, imfile=None, fmt="png"):
		"""
		
		af, modifié 23.02.09
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		dis			= ssd.squareform(self.Dis)
		"""
		hist, _, _ 	= plt.hist(dis, bins=bins,facecolor=col, alpha=alpha)
		plt.xlabel("distances")
		plt.ylabel("counts")
		if title:
			plt.title(title)
		if imfile:
			plt.savefig("%s.%s" % (imfile, fmt), format=fmt)	
		plt.show()
		"""
		name = "dis"
		s = pd.Series(dis)
		s.to_frame(name = name) 
		sns.displot(s, bins=bins, color=col, kde=kde)
		#
		plt.title(title)
		#
		if imfile:
			plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
		#
		if x11:
			plt.show()
		
	
	# ------------------------------------------------------------------
	
	def to_tree(self, meth="ward", zfile=None):
		"""builds the linkage matrix for clustering
		
		Parameters
		----------
		
		meth : string
			method for hierarchical aggregative clustering
			
		zfile : string
			name of the file where to write linkage matrices (optional)
			
		Returns
		-------
		
		tree	: an instance of class `tree`
			the tree
			
		
		Notes
		-----
		This method 
		* computes a linkage matrix `Z` with `sch.linkage()`.
		* The array `Dis` is converted into a so called `squareform` to ensure that the algorithm starts with a distan ce matrix
		* builds a tree as an instance of class `tree` from `Z`
		* the linkage matrix can be saved as a `txt` file if the name is given in `zfile`
		
		
		See Also
		--------
		
		tree : a class for analysing / displaying trees		
		
		
		Examples
		--------
		
		>>> disfile = "datasets/lauraceae.dis"
		>>> method = 'average'
		>>> dis = yap.distances(disfile=disfile)
		>>> tr = dis.to_tree(meth=method)
		>>> tr.draw()
		
		
		af, 21.09.20
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		dis 	= ssd.squareform(self.Dis)	# Dis must be in squareform
		Z		= sch.linkage(dis, meth)
		if zfile:
			#
			with open(zfile, "w") as out_handle:
				n_nodes	= Z.shape[0]
				for i in range(n_nodes):
					z_row		= Z[i,:]
					z_row_pl	= [str(val) for val in z_row] 
					item 		= "\t".join(z_row_pl)
					out_handle.write(item + '\n')
				out_handle.close()
			print("linkage matrix written in file", zfile)
		#
		tr	= tree(Z)
		#
		return tr
	
	# ------------------------------------------------------------------
	
	def to_graph(self, gap):
		""" Building a graph from distance array
		
		Parameters
		----------
		
		gap	: integer or float
			  the gap for thresholding
				
		Returns
		-------
		
		gr	: an instance of class `graph`
			  the graph after thresholding (see notes)
		
		See Also
		--------
		
		graph : a class for analysing / displaying graphs
			  
				
		Notes
		-----
		
		- the graph is built by thresholding distance matrix in `Dis` with gap in `gap`.
		- each item in `Dis` is a node
		- there is an edge between nodes `i` ad `j` iff :math:`Dis[i,j] \leq gap`
		- The graph is issued in networkx format 
		
		
		Examples
		--------
		
		>>> disfile = "datasets/lauraceae.dis"
		>>> gap = 12
		>>> dis = yap.distances(disfile=disfile)
		>>> gr = dis.to_graph(gap)
		>>> gr.plot()
		
		
		af, 21.07.06
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		A = 1*(self.Dis <= gap)
		np.fill_diagonal(A,0)
		#
		print("building the associated graph")
		G	= nx.from_numpy_array(A)
		#
		gr = graph(A,G)
		#
		return gr		
	
	# ------------------------------------------------------------------
	
	def plot_graph(self, gap, red="mds", meth="grp", k=100, col_node="blue", col_edge="black", dot_size=5):
		"""
		to be suppressed??????
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		n	= self.Dis.shape[0] 
		A 	= 1*(self.Dis <= gap)
		np.fill_diagonal(A,0)
		#		
		if red=="mds":
			print("running MDS ...")
			Y, S 	= dio.mds(self.Dis, k=k, meth=meth, no_loop=True)
			x		= Y[:,0]
			y		= Y[:,1]
		#
		plt.scatter(x,y, c=col_node, s=dot_size)
		"""
		print("\nplotting graph ...")
		print("if n is large, please be patient ...")
		print("to let you know if you have time to take a cup of coffee ...")
		for i in range(n-1):
			print("i =",i, "/", n, end="\r")
			for j in range(i+1,n):
				if A[i,j]==1:
					plt.plot([x[i],x[j]],[y[i],y[j]], c=col_edge, zorder=1)
		plt.scatter(x,y, c=col_node, s=dot_size, zorder=2)
		#
		"""
		plt.show()
	# ------------------------------------------------------------------
	
	def to_mst(self):
		"""
		Minimum Spaning Tree
		af, 22.11.14
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		Dis_csr = ssp.csr_matrix(self.Dis)
		mst		= ssc.minimum_spanning_tree(Dis_csr)
		A_tree	= mst.toarray().astype(float)
		Gx		= nx.from_numpy_array(A_tree)
		#
		return A_tree, Gx
		
	# -------------------------------------------------------------------------------------------
	
	def mds(self,k=-1, meth="svd", loop=True, nb_eig=0, pc=True, coordfile=None):
		"""Classical Multidimensional Scaling of a distance array
		
		Parameters
		----------
		k : integer
			the number of coordinates in output file
			
		plot_eig : boolean
			if `True`, a cumulative plot of the eigenvalues is dispplayed
		
		Returns
		-------
		
		pc : an instance of class `point_cloud`
				the coordinates of the point cloud
				
		See Also
        --------
        
        tsne : another way to build a point cloud from a distance array
        point_cloud : methods for point clouds
        
		
		Notes
		-----
		
		- The calculation is deported to `pydiodon` library. The parameters are those of `pydiodon.mds()`.
		- if `k==-1`, all the coordinates associated with a positive eigenvalue are returned
		- `meth` can take the values `evd`, `svd` or `grp`. See notes in `pydiodon.mds()` for details
		- `no_loop` is just a way to compute the Gram matrix. See notes in `pydiodon.dis2gram()` for details
		
		
		References
		----------
		
		[1] I. Borg and P. J. F. Groenen; 2005; Modern Multidimensional Scaling; Springer Series in Statistics. Springer, second edition.
		[2] T.F. Cox and M. A. A. Cox; 2001; Multidimensional Scaling - volume 88 of Monographs on Statistics and Applied Probability. Chapman & al., Second edition
		[3] A. J. Izenman; 2008; Modern Multivariate Statistical Techniques, Springer, NY

		
		
		Examples
		--------
		
		Here is a simple example
		
		>>> disfile = "datasets/lauraceae.dis"    # selects a distance file
		>>> dis = yap.distances(disfile=disfile)  # reads the distance file
		>>> pc = dis.mds()                        # runs the MDS (an object of class 'point_cloud' is produced)
		>>> pc.scatter()                          # plots the point cloud, axis 1 & 2


		see `point_cloud` for further possibilities in scatter plot.
		
		
		af, 21.09.20
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		#print_col("\n[yap_classes]:[distances]:[mds()]\n", color="magenta")
		
		### typing
		
		Y : np.ndarray	# components of MDS
		S : np.ndarray 	# eigenvalues of MDS
		
		print("Call to diodon MDS")
		Y, S 	= dio.mds(self.Dis, k=k, meth=meth, loop=loop)
		self.Y	= Y		# for use in t-SNE ...
		#
		if coordfile:
			self.write_components(coordfile)
			print_col("File of components saved as", coordfile, color="cyan")
			
		if nb_eig > 0:
			print_col("plots the", nb_eig, "largest eigenvalues", color="cyan")
			dio.plot_eig(S, k=nb_eig, frac=False, cum=True, dot_size=20, col="red", title=None, x11=True, plotfile=None, fmt="png")
		#
		if pc:
			pc	= point_cloud(Y=Y)
			#
			return pc
		else:
			pass
		
	# -------------------------------------------------------------------------------------------
	
	def mds_diodon(self, outfile=None, eigenvalues=True, n_eig=10):
		"""
		runs the MDS with Cpp diodon code
		
		
		Notes
		-----
		
		As it is relevent for large data sets only, two options are fixed:
		- the input distance file is in hdf5 format
		- the method for SVD is Gaussian Random Projection
		
		The basename is deduced from the name of hdf5 file expected to be <basename>.h5
		The components are written in an ascii file named <basename>.coord
		the eigenvalues of the Gram matrix are written in an ascii file names <basename>.eigen
		
		af & jmf, 22.04.24
		"""
		print("\n MDS with C++ didon driver")
		base	= self.h5file[:-3]
		print("base is", base)
		coordfile	= base + ".coord"  
		eigenfile	= base + ".eigen"  
		print("coordfile is", coordfile)
		print("eigenfile is", eigenfile, "\n")
		cmd 		= "mds -if " + self.h5file + " -iff h5 -of -ocf " + coordfile + " -oef " + eigenfile + " -off txt  -ids distances"
		print("command line is", cmd, "\n")
		os.system(cmd)
		#
		# eigenvalues
		if eigenvalues:
			eig		= np.loadtxt(eigenfile, delimiter="\t", dtype=float)
			eig		= [eig[i] for i in range(n_eig)]
			plt.plot(eig, c="purple")
			plt.show()
			#
		# loading coordinate file
		Y	= np.loadtxt(coordfile, delimiter=' ', dtype = float)
		pc	= point_cloud(Y=Y)
		#
		return pc
	
	# ------------------------------------------------------------------
	
	def tsne(self, n_tsne=2, mode="comp", p=-1, coordfile=None, pc=False):
		"""Runs t-SNE (from scikit-learn) on a distance array
		
		Parameters
		----------
		
		n_tsne : integer
				 number of components to be computed (2 or 3)
				 
		mode : string
				on which data set to run t-SNE (see notes)
				
		p : integer
				number of component to keep in the MDS (see notes)
		
		Returns
		-------
		
		pc : an instance of class `point_cloud` 
				 components of the output of t-SNE
				 
				 
		See Also
		--------
		
		mds : another way to build a point cloud
		point_cloud : how to analyse and visualize a point cloud
		
		
				
		Notes
		-----
		
		- t-SNE can be run on the distance array `self.Dis` (with `mode="dis"`) or a component file (with `mode="comp"`). 
		- if with "comp", a MDS is run on `self.Dis` with `pydiodon`, keeping `p` components.
		
		
		Examples
		--------
		
		>>> disfile = "datasets/lauraceae.dis"
		>>> dis = yap.distances(disfile=disfile)
		>>> pc = dis.tsne()
		>>> pc_scatter()		
		
		
		af, 21.09.20
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)

		if mode=="dis":
			Y		= TSNE(n_components=n_tsne).fit_transform(self.Dis)
		if mode=="comp":
			X, S	= dio.mds(self.Dis, k=p) 
			Y		= TSNE(n_components=n_tsne).fit_transform(X)
		#
		self.Y	= Y		# for saving Y
		#
		if coordfile:
			self.write_components(coordfile)
			print_col(f'File of components saved as {coordfile}', color="cyan")
		#
		#self.Y	= Y
		#pc		= point_cloud(Y=Y)
		#
		#return pc		
		
	# ------------------------------------------------------------------
	
	def dbscan(self, gap, min_sample):
		"""
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		db = DBSCAN(eps=gap, min_samples=min_sample).fit(self.Dis)
		print("dbscan called with parameters", db)
		#
		labels = db.labels_
		#
		# Number of clusters in labels, ignoring noise if present.
		#
		n_clusters	= len(set(labels)) - (1 if -1 in labels else 0)
		n_noise		= list(labels).count(-1)
		#
		print("Estimated number of clusters: %d" % n_clusters)
		print("Estimated number of noise points: %d" % n_noise)
		#
		print(labels)
		#
		clus 	= []
		for c in range(n_clusters):
			w		= [i for i, val in enumerate(labels) if val==c]
			clus.append(w)
		#
		# sorting clus
		#
		clus 	= [sorted(xx) for xx in clus]
		clus.sort(key=len, reverse = True)
		noise	= [i for i, val in enumerate(labels) if val==-1]
		clus 	= [noise] + clus
		#
		part 	= partition(clus=clus)
		#
		return(part)
		
		 
	# ------------------------------------------------------------------
	
	def write_components_old(self, coordfile):
		"""Writes the component array `self.Y` in a file
		
		Parameters
		----------
		
		coordfile : string 
					the name of the file
					
		Returns
		-------
		
		Nothing
		
		
		Notes
		-----
		
		It writes a file in ascii format with `\t` as delimiters.
		
		af, 21.07.18
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		with open(coordfile, "w") as out_handle:
			n	= self.Y.shape[0]
			for i in range(n):
				xi		= self.Y[i,:]
				xi_str	= [str(val) for val in xi]
				string 	= ("\t").join(xi_str)
				out_handle.write(string + "\n")
			out_handle.close()
			
	# ------------------------------------------------------------------
	
	def write_components(self, coordfile, headers=True, rownames=True):
		"""Writes the component array `self.Y` in a file
		
		Parameters
		----------
		
		coordfile : string 
					the name of the file
					
		Returns
		-------
		
		Nothing
		
		
		Notes
		-----
		
		It writes a file in ascii format with `\t` as delimiters.
		
		af, 21.07.18
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		#
		(n,p)	= self.Y.shape
		headers	= ['F'+str(i+1) for i in range(p)]
		#
		with open(coordfile, "w") as out_handle:
			if headers:
				if rownames:
					headers		= ["SEQ_ID"] + headers
				item	= ('\t').join(headers)
				out_handle.write(item + "\n")
			for i in range(n):
				xi		= self.Y[i,:]
				xi_str	= [str(val) for val in xi]
				if rownames:
					xi_str 	= [self.seq_id[i]] + xi_str
				string 	= ("\t").join(xi_str)
				out_handle.write(string + "\n")
			out_handle.close()			
	
	# ------------------------------------------------------------------
		
	def heatmap(self, items_order, cmap = 'ocean', title=None, x11=True, imfile=None, fmt="png"):
		"""Draws a heatmap
		
		Parameters
		----------
		
		item_order : list of integers
			order of the rows/columns of matrix `Dis` in heatmap
			
		cmap : string
			colormap in matplotlib to be used
			
		title : string
			title of the plot
			
		Returns
		-------
		
		None
		
		Notes
		-----
		
		* Draws a heat map of distance matrix `Dis` with rows/column orders as indicated in `item_orders`
		* see https://matplotlib.org/stable/gallery/color/colormap_reference.html for available color maps in matplotlib; default is `viridis`
		
		
		Examples
		--------
		
		
		>>> disfile = "datasets/lauraceae.dis"          # selects a distance file (ascii)
		>>> dis = yap.distances(disfile=disfile)        # reads the distance file
		>>> tr = dis.to_tree()                          # builds a tree
		>>> tr.draw()                                   # drawing the tree to get the leaves order
		>>> dis.heatmap(items_order=tr.nodes_order)     # draws the heatmap with that order
		
		
		It is possible to select the method for clustering by
		
		>>> tr = dis.to_tree(meth="single")             # see scipy.cluster.hierarchy.linkage() for accepted methods
		
		
		af, 21.09.20
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		D	= self.Dis[np.ix_(items_order,items_order)]
		plt.imshow(D, cmap=cmap, interpolation='nearest')
		plt.colorbar()
		if title:
			plt.title(title)
		#
		if imfile:
			plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
		if x11:
			plt.show()
		else:
			plt.close()			

	# ------------------------------------------------------------------
	
	def clus_heatmap(self, cmap="viridis", labels=None, title=None, x11=True, imfile=None, fmt='png'):
		"""
		heapmap with rows/column orders accrding to clustering
		
		made with seaborn
		af, 23.02.09, 23.12.07
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		#print("[yap_classes]:[distances]:[clus_heatmap()]")
		#
		# labels
		#
		if labels:
			df	= pd.DataFrame(self.Dis, index=labels, columns=labels)
		else:
			df	= pd.DataFrame(self.Dis, index=self.seq_id, columns=self.seq_id)
		#
		# building heatmap
		#
		sns.clustermap(df, method="ward", cmap=cmap)
		#
		# displaying and/or saving
		#
		plt.title(title)
		#
		if imfile:
			print("saving plot in file", imfile + "." + fmt)
			plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
		if x11:
			plt.show()

	# --------------- sub-matrix
	
	def xtract_submatrix(self, which=[], seqids=[], subdisfile=None, sep="\t"):
		"""
		Parameters
		----------
		
		which  : which indices
		
		seqids : list of strings
			list of the sequence identifiers to extract
			
		subdisfile : string
			name of the file where to write the extracted distance matrix
			
		
		af, 22.01.10
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		num	= len(seqids) + len(which) 
		if num == 0:
			print_col("!! in <xtract_submatrix()>, you must select an extraction by indices or seqids !!", color="red")
		#
		if len(seqids) > 0:
			which 	= [i for i, seq in enumerate(self.seq_id) if seq in seqids]
		#
		if len(which) > 0:
			seqids	= [self.seq_id[w] for w in which]
		#
		subDis	= self.Dis[np.ix_(which,which)]
		n		= len(seqids)
		#
		if subdisfile:
			with open(subdisfile, "w") as out_handle:
				pl 		= [" "] + seqids
				item 	= sep.join(pl) + '\n'
				out_handle.write(item)
				for i in range(n):
					val		 	= subDis[i,:]
					val_string  = [str(x) for x in val]
					pl		  	= [seqids[i]] + val_string
					item		= sep.join(pl) + '\n'
					out_handle.write(item)
			out_handle.close()
		
	# --------------- extract random
	
	def xtract_random_submatrix(self, n_seq, subdisfile):
		"""extract a random submatrix of size n_seq x n_seq
		
		Parameters
		----------
		
		n_seq	: integer
			size of extracted difile
			
		subdisfile : string
			name of the file where to write the extracted distance matrix
			
		af, 24.03.09
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		which	= np.random.choice(range(self.n), n_seq, replace=False).tolist()
		which.sort()
		#seqids	= [self.seq_id[w] for w in which]
		#which=[], seqids=[], 
		self.xtract_submatrix(which=which, seqids=[], subdisfile=subdisfile)
		
	# --------------- Writes -------------------------------------------

	def write(self, disoutfile, seq_id, sep = '\t', verbose=True):
		""" writes a distance array as a file
		
		
		Parameters
		----------
		
		disoutfile : string
			name of the file to be written
			
			
		seq_id : list of strings
			rownames and column names (sequence identifiers)
			
		sep : string
			delimiter between values
			
		
		Notes
		-----
		
		Writes array `Dis` with row and column names `seq_id` with delimiters `sep` as file `disoutfile`.
		
		
		af, 21.09.21		
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		#
		print_col("\nwrites distance array as file", disoutfile, color="cyan")
		with open(disoutfile, "w") as out_handle:
			pl = [" "] + seq_id
			item = sep.join(pl) + '\n'
			out_handle.write(item)
			if verbose:
				print("headers written ...")
			for i in range(self.n):
				val		 = self.Dis[i,:]
				val_string  = [str(x) for x in val]
				pl		  = [seq_id[i]] + val_string
				item		= sep.join(pl) + '\n'
				out_handle.write(item)
				if verbose:
					print("row ", i, "/", self.n, "written", end="\r")
		out_handle.close()
		if verbose:
			print_col("distance matrix written in file <" + disoutfile + "> ...", color="cyan")	
			
			
	# ------------------------------------------------------------------
	
	def write_fasta(self, fasfile):
		"""
		"""
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		n	= len(self.seq_id)
		with open(fasfile,"w") as f_fas:
			for i in range(n):
				seqid	= ">" + self.seq_id[i]
				f_fas.write(seqid + "\n")
				f_fas.write(self.words[i] + "\n")
		
		
# ----------------------------------------------------------------------
#
#						Point clouds
#
# ----------------------------------------------------------------------

class point_cloud():
	"""Different ways to display a point cloud (graphical)
	
	Attributes
	----------
	Y : numpy array
	    array of coordinates of the components
	    
	n_item : integer
	         number of items
	         
	n_coord : integer
			  number of axis
	
	Notes
	-----
	- The coordinates can be loaded as a textfile (delimiter as "\t") of h5 file
	
	todo
	discuss the dataset for coordinates in hdf5 format
	
	"""
	def __init__(self, Y=0, coordfile=None, filetype="ascii", headers=True, rownames=True):
		"""Initialisation of a point cloud object
		
		Parameters
		----------
		
		coordfile : string
					the name of the file to be loaded
					
		Returns
		-------
		
		self.Y : numpy array
					the array of coordinates
					
		af, 21.07.18
		"""
		self.c_name	= "point_cloud"
		yut.print_through_init(self.c_name)
		
		#
		if coordfile:
			print("loading coordfile: ", coordfile)
			if filetype=="ascii":
				#
				if (headers and rownames):
					Y, cn, rn	= load_ascii(coordfile, delimiter="\t", headers=headers, rownames=rownames)
				if ((headers==False) and (rownames==False)):
					Y			= load_ascii(coordfile, delimiter="\t", headers=headers, rownames=rownames)
				if ((headers==False) and (rownames==True)):
					Y, cn		= load_ascii(coordfile, delimiter="\t", headers=headers, rownames=rownames)
				#
				self.Y			= Y
				self.seqids		= rn
				#	
			if filetype=="h5":
				exit('point_cloud on h5file no yet implemented')
			#
		else:
			print("point_cloud class initilized with Y")
			self.Y	= Y
		#
		self.n_item		= self.Y.shape[0]
		self.n_coord	= self.Y.shape[1]
		
		print("coordfile has", self.n_item, "rows and", self.n_coord, "columns")		

	# ------------------------------------------------------------------
	
	def set_Y(self,Y):
		"""sets the value of coordinate array
		
		Parameters
		----------
		
		Y : numpy array
			the coordinate array to set
			
		Returns
		-------
		
		self.Y : numpy array
				 the attribute `self.Y` set with `Y`
				 
		af, 21.07.18 
		
		"""
		print("[yap_classes]:[point cloud]:[set_Y()]")
		self.Y	= Y.copy()
		self.n_item		= self.Y.shape[0]
		self.n_coord	= self.Y.shape[1]
		print("matrix Y set as attribute of point_cloud")
	# ------------------------------------------------------------------
	
	def def_colors_deprecated(self, charfile, varname, colorfile):
		"""deprecated
		"""
		print("point_cloud.def_colors() probably deprecated")
		char	= charmat(charfile)
		v_col	= charmat.get_colors(varname, colorfile)
		#
		return v_col
	
	# ------------------------------------------------------------------


	def scatter(self, i=0, j=1,
				v_col="red", dot_size=5, fontlabel=10, labels=[], info=None, 
				legend=False, dicol=None, color_range="discrete", cmap=None, 
				title=None, x11=True, imfile=None, fmt="png"):
		"""2D scatter plot of a point cloud
		
		Parameters
		----------
		
		i : integer
			axis 1
			
		j : integer
			axis 2
			
		v_col : string or list of strings
			specifying the colors for all dots or per dot
			
		dot_size : integer or list of integer
			specifying dot size for all dots, or per dot
			
		fontlabel : integer
			font size for labelling axes
			
		legend : Boolean
			whether to add a legend for colors
			
		dicol : dictionary
			dictionary with
				key : color
				value : corresponding variable
		
		Notes
		-----
		
		Returns a plot.
		
		* the axes are numbered from 0 as in python: axis 1 is `i=O`, and so on
		* dicol is compulsory if `legend == True`
		
		Examples
		--------
		
		Let us first build a point cloud:
		
		>>> disfile = "datasets/lauraceae.dis"
		>>> dis = yap.distances(disfile=disfile)
		>>> pc = dis.mds()
		
		and here are different ways to plot the point cloud
		
		>>> pc.scatter(i=1,j=2)                   # plots the point cloud, axis 2 & 3
		>>> pc.scatter(v_col="red")               # with color "red"
		>>> pc.scatter(dot_size=10)               # with dot size = 10
		>>> pc.scatter(fontlabel)                 # font size for labelling the axis
		
		af, 21.09.21
		"""
		print_col("[yap_classes]:[point cloud]:[scatter()]", color="magenta")
		
		if dicol==True and color_range=="continuous":
			print_col("!!! you must chose between discrete and continuous colors !!!", color="red")
			print_col("up date the arguments <dicol> and <continuous>", color="red")
			print_col("program terminated", color="red")
			exit()
		
		F1	= self.Y[:,i]
		F2	= self.Y[:,j]
		#plt.scatter(F1,F2,c=v_col,s=dot_size, edgecolor='black')
		plt.scatter(F1,F2,c=v_col,s=dot_size)
		plt.xlabel("axis " + str(1+i), fontsize=fontlabel)
		plt.ylabel("axis " + str(1+j), fontsize=fontlabel)
		#
		if len(labels) > 0:
			for i, item in enumerate(labels):
				plt.text(F1[i], F2[i], labels[i])
		if color_range=="discrete":
			if dicol:
				if legend:
					p, keys = get_legend(dicol)
					plt.legend(p, keys, loc='upper center', bbox_to_anchor=(0, 1.02, 1, 0.1),
					ncol=3, fontsize=8, fancybox=True, shadow=True)
		if color_range=="continuous":
			plt.colorbar()
		if title:
			plt.title(title)
		if imfile:
			plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
			print("plot saved in file", imfile + "." + fmt)			
		if x11:
			if cmap:
				getattr(plt, cmap)()
			plt.show()
			
	# ------------------------------------------------------------------	
		
	def hovering(self, i=0, j=1, label="indices", prefix=None, c="b", s=20):
		"""
		Notes
		-----
		
		A scatter plot with F1 on axis 1 and F2 on axis 2 is displayed.
		When the mouse clicks on a point, a comment is displayed on the console
		with the index of the item cliked, and the value of the label for this item
		
		
		This is event based, i.e. cliking with the mouse can be repeated as many
		times as wished.
		
		Adapted from http://matplotlib.sourceforge.net/examples/event_handling/pick_event_demo.html
		moved to
		https://matplotlib.org/2.0.2/examples/event_handling/pick_event_demo.html
		
		*af, 25/10/2017*
		"""
		print("[yap_classes]:[point cloud]:[hovering()]")
		F1	= self.Y[:,i]
		F2	= self.Y[:,j]
		if label=="indices":
			label	= list(range(self.n_item))

		if 1: # picking on a scatter plot (matplotlib.collections.RegularPolyCollection)
			#
			def onpick(event):
				ind = event.ind
				print(prefix, np.take(label, ind))

			fig = plt.figure()
			ax1 = fig.add_subplot(111)
			col = ax1.scatter(F1, F2, c=c, s=s, picker=True)
			fig.canvas.mpl_connect('pick_event', onpick)

		plt.show() 
		
	# ------------------------------------------------------------------
	
	def points_123(self, col="red", dot_size=2, mode="2D", x11=True, imfile=None, fmt="png"):
		"""
		"""
		print("[yap_classes]:[point cloud]:[points_123()]")
		F1	= self.Y[:,0]
		F2	= self.Y[:,1]
		F3	= self.Y[:,2]
		#
		if mode=="2D":
			print("mode = 2D")
			fig, axes = plt.subplots(nrows=2, ncols=2, sharex="all", sharey="all")
			axes[0,0].scatter(F1,F3,c=col,s=dot_size)
			axes[1,0].scatter(F1,F2,c=col,s=dot_size)
			axes[1,1].scatter(F3,F2,c=col,s=dot_size)
			#
			axes[0,0].set(title='axis (1,3)')
			axes[1,0].set(title='axis (1,2)')
			axes[1,1].set(title='axis (3,2)')
			#
			plt.plotsave(plt, x11=x11, imfile=imfile, fmt="png")
		#
		if mode=="3D":
			print("mode = 3D")
			fig = plt.figure()
			ax = fig.add_subplot(111, projection='3d')
			ax.scatter(F1, F2, F3, c=col, s=dot_size)
			ax.set(xlabel="axis 1")
			ax.set(ylabel="axis 2")
			ax.set(zlabel="axis 3")
			plt.plotsave(plt, x11=x11, imfile=imfile, fmt="png")
			
	# ------------------------------------------------------------------
		

	def splines(self, v_col, n_axis=-1, legend=False, dicol=None, def_color="grey", title=None, x11=True, imfile=None, fmt="png"):
		"""
		lines smoothed with cublic splines
		
		arguments
		---------
		
		@ v_col		a color or a list f colors
		"""
		print("[yap_classes]:[point cloud]:[plot_splines()]")
		# if one color obly, translate it as a uniform color vector
		if type(v_col)==str:
			v_col 	= [v_col]*self.n_item
		#
		if n_axis==-1:
			n_axis	= self.n_coord
		else:
			axis_depth	= min(n_axis, self.n_coord)
		#
		x   = np.arange(axis_depth)
		xs  = np.linspace(0, axis_depth-1, 1000)
		#
		for i in range(self.n_item):
			if v_col[i] != def_color:
				y   = self.Y[i,list(range(axis_depth))]
				spl = sin.UnivariateSpline(x, y, s=0)
				ys  =  spl(xs)
				plt.plot(xs, ys, color=v_col[i])
		#
		if legend:
			p, keys = get_legend(dicol)
			plt.legend(p, keys, loc='upper center', bbox_to_anchor=(0, 1.02, 1, 0.1),
			  ncol=3, fontsize=8, fancybox=True, shadow=True)
		#
		plt.xlabel("Principal axis")
		plt.ylabel("Projections")
		if title:
			plt.title(title)
		plt.plotsave(plt, x11=x11, imfile=imfile, fmt="png")
	
	# ------------------------------------------------------------------
	
	def heatmatrix(self, i=0, j=1, bins=256, range=None, log=True, scale=False):
		"""
		builds the count matrix of number of items projected on a same pixel 
		in a 2D image (for plotting a heatmap)
		
		arguments
		---------
		@ F1	vector		first axis of the plot
		@ F2	vector		second axis of the plot
		@ bins	integer		number of pixel per edge of the image
		@ range				see help of numpy.histogram2d
		@ log	boolean		if True, logarithm of nb of items per pixel
		@ scale	boolean		if True, 
		
		outputs
		-------
		H			numpy array		matrix 
		xedges		list			the bin edges along the first dimension.
		yedges		list			the bin edges along the second dimension.
		
		note
		----
		it is a wrapper of function numpy.histogram2d()
		H[i,j] is the (log of the) number of items projected on pixel (i,j)
		
		af, @ Gazinet, 25/01/2020 - revised @ Aillevillers 23/07
		"""
		print("[yap_classes]:[point cloud]:[heat_matrix()]")
		F1					= self.Y[:,i]
		F2					= self.Y[:,j]
		H, xedges, yedges	= np.histogram2d(F1, F2, bins=bins, range=range)
		if log:
			H	= np.log(1+H)
		if scale:
			M	= np.max(H)
			H	= np.round((H/float(M))*255)
			H 	= np.array(H, dtype=int)
		#
		self.H		= H
		self.xedges	= xedges
		self.yedges	= yedges 
		
	# ------------------------------------------------------------------

	def heatmap(self, i=0, j=1, cmap='viridis', range=None, log=True, scale=False, title=None, x11=True, imfile=None, fmt="png"):
		"""  
		wid:	plots the cooordfile as a heatmap
		
		argument
		--------
		@ i			integer			first axis
		@ j			integer			second axis
		@ bins		integer			number of pixel per edge of the image
		@ range						see help of numpy.histogram2d
		@ log		boolean			if True, logarithm of nb of items per pixel
		@ scale		boolean			if True, 		
		@ fmt		string			format for saving plot in a file
		@ title		string			title of the plot
		@ x11		boolean			if True, plot displayed on the screen
		@ imfile	string			name of the file where to write the plot
		
		output
		------
		none	(a plot displayed on the screen and/or written in a file)
		
		Notes
		-----
		H is a count matrix produced by a call to <build_count_matrix>
		it counts the number of reads projected on a same pixel, by pixel
		it is displayed with a color according to theis count
		it is a wrapper of function numpy.histogram2d()
		H[i,j] is the (log of the) number of items projected on pixel (i,j)
		
		if x11 is true, the plot is displayed on the screen
		if imfile is a string (and not 'none'), the plot is written in file 'imfile'
		with format 'fmt'
		
		af, @ Gazinet, 25/01/2020 - revised Aillevillers 23/07
		"""		
		#F1	= self.Y[:,i]
		#F2	= self.Y[:,j]
		#self.build_count_matrix(i, j, bins=256, range=None, log=True, scale=False)
		#plot_count_matrix(self.H, i, j, fmt=fmt, title=title, x11=x11, imfile=imfile)
		#
		print("[yap_classes]:[point_cloud]:[heatmap()]")
		plt.imshow(self.H, cmap=cmap)
		plt.xlabel("axis " + str(1+i))
		plt.ylabel("axis " + str(1+j))
		if title:
			plt.title(title)
		plt.colorbar()
		#
		if imfile:
			plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
		if x11:
			plt.show()
		#plt.plotsave(plt, x11=x11, imfile=imfile, fmt=fmt)
		
	# ------------------------------------------------------------------
	
	def spikeshow(self, i=0, j=1, bins=128, log=True, title=None, x11=True, imfile=None, fmt="png"):
		"""
		see https://matplotlib.org/3.1.1/gallery/mplot3d/surface3d.html
		"""
		print("[yap_classes]:[point cloud]:[spike_show()]")
		fig		= plt.figure()
		ax		= fig.gca(projection='3d')

		# Make data
		self.heatmatrix(i, j, bins=bins, range=None, log=log, scale=False)
		Z		= self.H
		bins	= Z.shape[0]
		X 		= np.arange(bins)
		Y 		= np.arange(bins)
		X, Y	= np.meshgrid(X, Y)

		# Plot the surface.
		surf 	= ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
								linewidth=0, antialiased=False)

		# Add a color bar which maps values to colors.
		fig.colorbar(surf, shrink=0.5, aspect=5)

		#plt.show()
		if title:
			plt.title(title)
		#
		if x11:
			plt.show()
	
	# ------------------------------------------------------------------
	
	def islands(self, level, seq_id):
		"""
		
		Notes
		-----
		
		- pc.heatmatrix() must have been run before
		- Spotu stands for Otu as spot ...
		
		af, 23.08.15
		"""
		xedges	= self.xedges 
		yedges	= self.yedges 
		h 		= np.ravel(self.H)
		#print(h)
		plt.hist(h, bins=10)
		plt.xlabel("density per pixel (bins)")
		plt.ylabel("nb of pixels")
		plt.show()
		Lev	= 1*(self.H >= level)
		Lev.astype(int)
		plt.imshow(Lev)
		plt.show()
		Spotu, n_otus	= skim.measure.label(Lev, return_num=True)
		print(n_otus, "otus have been found")
		#print(Spotu)
		plt.imshow(Spotu)
		plt.show()
		
		### ----------- finds the pixel on which the read has been projected
		
		n	= Spotu.shape[0]
		dic	= {}
		for i, item in enumerate(seq_id):
			x		= self.Y[i,0]
			y		= self.Y[i,1]
			wx		= [1 for val in xedges if x >= val]
			wy		= [1 for val in yedges if y >= val]
			ix		= len(wx) - 1
			iy		= len(wy) - 1
			ix		= min([n-1,ix])
			iy		= min([n-1,iy])
			spot	= Spotu[ix, iy] 
			dic[i]	= spot
		
		### ------------ writing OTUs as a clus data structure
		
		# list of otus per index of seq_ids
		pl		= [dic[i] for i, seqid in enumerate(seq_id)]
		# initialize clus data structure
		clus 	= []
		# list of otus
		otus 	= list(set(pl))
		# builds clus
		for otu in otus:
			cc	= [i for i, item in enumerate(pl) if item==otu]
			clus.append(cc)
		#
		# sorts lists in clus
		clus 	= [sorted(xx) for xx in clus]
		clus.sort(key=len, reverse = True)
		#
		part 		= partition(clus=clus)
		#
		return part
				
		
		
	# ------------------------------------------------------------------
	
	def sns_plot(self, axis_1=1, axis_2=2, plot_type=None, charfile=None, varname=None):
		"""
		af, 23.02.09
		"""
	# ------------------------------------------------------------------
	
	def heatmap_deprecated(self, cmap="viridis", labels=None, x11=True, imfile=None, fmt="png"):
		"""
		"""
		print("[yap_classes]:[point_cloud]:[heatmap()]")
		#
		if labels:
			df	= pd.DataFrame(self.Dis, index=labels, columns=labels)
		else:
			df	= pd.DataFrame(self.Dis, index=self.seq_id, columns=self.seq_id)
		sns.clustermap(df, cmap=cmap)
		#
		if imfile:
			print("saving plot in file", imfile)
			plt.savefig("%s.%s" % (imfile, fmt), format=fmt)
		if x11:
			plt.show()		

# ----------------------------------------------------------------------
#
#						Hierarchical clustering
#
# ----------------------------------------------------------------------

class tree():
	"""
	
	Attributes
	----------
	
	height : float
		height at which the tree is cut

	meth : string
		method for aggregating in hierarchical clustering
	
	Z : array
		linkage matrix
	
	Methods
	-------
	
	draw_tree()
		plots a dendrogram
		
	get_clusters_byheight()
		getting clusters by cutting the tree at a given height
		
	get_clusters_bynumber()
		getting clusters with a prescribed number
		
	set_height()
		sets attribute height
	
	Notes
	-----
	
	Main operations are
	
	* getting clusters from Z					get_clusters_byheight()
												get_clusters_bynumber()
	* plotting a dendrogram from Z				draw()
	
	
	af, July 11, 2020 ; revised 20.07.14
	"""
	
	def __init__(self, Z=None, meth="ward", linkfile=None):
		"""Initialization for a HAC
		
		Parameters
		----------
		
		Z : numpy array
			The linkage matrix
			
		meth : string
			The method used for building the linkage matrix
			
		Returns
		-------
		
		self.Z : numpy array
			the linkage matrix
			
		self.meth : string
			the method for building the linkage matrix
			
		
		Notes
		-----
		
		Sets the height by default at `h = 100`
			
			
		af, 21.07.18; 21.08.28, 24.01.17
		"""
		#
		self.c_name	= "tree"
		yut.print_through_init(self.c_name)
		#
		if linkfile:
			Z	= np.loadtxt(Z, delimiter="\t", dtype=float)
		self.Z		= Z
		self.meth	= meth
		self.height = 100
		
	# ------------------------------------------------------------------
	
	def set_height(self, height):
		"""setting the height for cutting the tree
		
		Parameters
		----------
		
		height : float
			the height at which to cut the tree
			
		Notes
		-----
		
		Sets the height as an attribute.
		
		"""
		self.height = height
		
	# ------------------------------------------------------------------
	
	def get_clusters_byheight(self, height):
		""" getting clusters by cutting the tree at a given height
		
		Parameters
		----------
		
		height : float
			the height at which to cut the tree
			
		Returns
		-------
		
		part : an instance of class `partition`
			the partition provided by cutting the tree at selected height
			
		
		Notes
		-----
		
		* see notes on class `partition` for the format of the partition
		
		
		
		af, 21.08.28
		
		"""
		#print(self.Z)
		clusters 	= sch.cut_tree(self.Z, height=height).tolist()
		#
		clusters_pl 	= [a[0] for a in clusters]
		n_clus			= 1+max(clusters_pl)
		print(n_clus, "clusters found")
		clus			= [] 
		for i in range(n_clus):
			which 		= [ii for ii, val in enumerate(clusters_pl) if val==i]
			clus.append(which)
		#
		clus.sort(key=len, reverse = True)
		clus	= [c for c in clus] 
		#
		part 	= partition(clus)
		#
		return part
		

	# ------------------------------------------------------------------
	
	def get_clusters_bynumber(self, n_clusters):
		"""getting a partition with a given number of clusters
		
		Parameters
		----------
		
		n_clusters : integer
			the number of clusters in the partition
			
		Returns
		-------
		
		clus : a list of list
		
		
		Notes
		-----
		
		Computes the height at which the cut gives the desired number of clusters, and applies the cut at this hieght.
		
		af, 21.07.18
		"""
		n_nodes	= self.Z.shape[0]
		i_range	= list(range(n_nodes))
		i_inf	= i_range[-n_clusters]
		i_sup	= i_range[-n_clusters+1]
		z_inf	= self.Z[i_inf,2]
		z_sup	= self.Z[i_sup,2]
		height	= (z_inf + z_sup)/2
		self.set_height(height)
		print("height =", height)
		#
		part	= self.get_clusters_byheight(height)
		#
		return part
		
	# ------------------------------------------------------------------

	def draw(self, orientation="left", height=None, truncate=-1, labels=None, leaf_font_size=None, title=None, x11=True, imfile=None, fmt="png"):
		"""Draws a tree
		
		Parameters
		----------
		
		orientation : string
			orientaton of the tree
			
		labels : list of strings
			labels of the leaves
			
		title : string
			title of the plot
			
			
		Notes
		-----
		
		Uses function `scipy.cluster.hierarchy.dendrogram`, with som of its arguments.
		
		* orientation : as in `sch.dedrogram`: among "top", "right", "bottom", "left" 
		* the call to `sch.dendrogram` returns an object `dend`. The node order in the dendrogram is given in `dend["ivl"]. It is stored as an attribute. 
		
		"""
		print('[yap_classes]:[tree]:[draw()]')
		#f = plt.figure()
		#color_threshold			= self.height
		#color_above_threshold	= 'grey'
		#dend					= sch.dendrogram(Z=self.Z, orientation=orientation, labels=labels, get_leaves=True)
		if truncate==-1:
			dend					= sch.dendrogram(Z=self.Z, 
													orientation=orientation,
													labels=labels,
													leaf_font_size=leaf_font_size, 
													get_leaves=True
													)
			nodes_order				= dend["ivl"]
			if not labels:
				self.nodes_order	= [int(s) for s in nodes_order]
		else:
			dend					= sch.dendrogram(Z=self.Z, 
													orientation=orientation, 
													p=truncate, 
													truncate_mode='lastp', 
													show_contracted=True
													)
		plt.title(title)
		#
		if title:
			plt.title(title)
		if imfile:
			#f.savefig(imfile + "." + fmt)
			plt.savefig(imfile + "." + fmt)
			print("tree saved in file", imfile + "." + fmt)
		if x11:			
			plt.show()
		else:
			plt.close()
	# ------------------------------------------------------------------
	
	def Z_heights(self, Log, col="red"):
		"""
		"""
		x	= self.Z[:,2]
		y	= x[::-1]
		if Log==True:
			y	= y[y>0]
			y	= np.log(y)
			mode	= "(Log)"
		else:
			mode = ""
		plt.plot(y, c=col)
		plt.xlabel("rank of inner nodes from top ")
		plt.ylabel("distance from leaves " + mode)
		plt.title("Heights of nodes in linkage matrix")
		plt.show()

	# ------------------------------------------------------------------

	def draw_tree_coloredlabels(self, labels, dic_lab_colors, orientation = "left", title=None):
		"""Draws a tree with colored labels
		"""
		color_threshold			= self.height
		color_above_threshold	= 'grey'				
		dend			= sch.dendrogram(Z=self.Z, orientation=orientation, color_threshold=color_threshold, above_threshold_color=color_above_threshold, labels=labels)
		
		# Apply the right color to each label
		ax		= plt.gca()
		xlbls	= ax.get_ymajorticklabels()
		for lbl in xlbls:
			lbl.set_color(dic_lab_colors[lbl.get_text()])
		#
		if title:
			plt.title(title)
		plt.show()
		
# ======================================================================
#
#						phyloTree()
#
# ======================================================================

class phyloTree():
	"""class for phylogenetic trees
	
	Methods
	-------
	- evol_distances()		commputes a pairwise distance matrix with evolutionary dstances
	- phyML()				builds a newick file of a phylogenetic tree with ML
	- show_dendro()			tree visualisation with dendroscope
	- show_ete3()			tree visualisation with ete3
	
	af, 24.03.28
	"""
	def __init__(self, fasfile=None, disfile=None):
		"""
		A phylogeny can be built fom a fasta file (phyML) or a distance file (NJ)
		
		- if with a fasta file, the file is given as an attribute (as well as the fasta class)
		
		af, 24.03.29
		"""
		self.c_name	= "phyloTree"
		yut.print_through_init(self.c_name)
		
		#
		if fasfile:
			self.fasfile	= fasfile
			self.fas		= fasta(fasfile)
			self.n_seq		= self.fas.n_seq
			self.seqids		= self.fas.seq_id
			self.basename	= self.fas.basename
		
		
# ------------------ phyML -----------------------------------------
    
	def phyML(self, charfile=None, varname=None):
		"""builds a newick file with phyML
		
		Parameters
		----------
		
		charfile : string
			name of the character file to label the leaves (or None)
		
		varname : string
			header in the character file for labelling leaves
			
		
		Notes
		-----
		
		Writes a newick file named <basename>.newick
		
		There are several possibilities for labelling nodes
			- by seq_id
			- by values of a character 
		
		Intermediate files are	
		- phylfile	 	= fas.basename + ".phy"			phylip file of sequences
		- <basename_new.fas	: fasta file with new seq_ids from seq_0 to seq_(n-1)
		
		phyML does not accept seq_ids with columns ':'
		hence the fasta file is rewritten with new seq_ids
		
		what it does to better handle the names
			- renames the seq_ids from seq_0 to n_seq_(n-1) 
			- writes for this a file denoted <basename>_new.fas on the disk for phyML
			- aligns it with output in phylip format
			- runs phyMl on this phylip file (seq_ids have less the 10 characters)
			- updates the names on leaves of the tree
		
		development ongoing ...   
		
		af & jmf, 23.04.25, 23.04.28, 23.07.07, 24.03.28
		"""
		# --------------------------------------------------------------
	   
		# sequences are indexed from 0 to n_seq-1 (denoted here n)
		#	dic_seqid:	key = index (as a string)
		#				value = seq_id
		#
		#	dic_taxo:	key ) index (as a string)
		#				value = taxon
		
		
		print_col("\n[yap_classes]:[phyloTree]:[phyML()]\n", color="magenta")
		
		### ------------ names ...
		
		basename	= self.fas.basename
		phylfile	= basename + ".phy"
		#
		dic_seqid	= {}
		dic_taxo	= {}		# useful for changing leaf labels
		print_col("There are", self.n_seq, "sequences", color="cyan")
		print("name of input fasta file", self.fasfile)
		print("name of file in format Phylip", phylfile)
		print("name of character file", charfile)


		### ------------- if labels of leaves with characters
		
		if charfile:
			#
			print("\nloading character file", charfile)
			#
			char			 = charmat(charfile=charfile)
			#
			if not varname:
				print_col("\nin fasta.phylotree() ...\n", color="red")
				print_col("\n!!! warning: you must provide a varname!!!\n", color="red")
				exit("program terminated")
			else:
				v_tax			 = char.get_tax(varname)
				for i in range(self.n_seq):
					dic_taxo['seq_' + str(i)]	= v_tax[i]
			   
		###  ---- writing fastafile with seq_ids from seq_0 to seq_(n-1)
		
		print_col("preparing phyML ...", color="green")
		new_fastafile		= basename + "_new.fas"
		print("new fasta file (new seqids) is ", new_fastafile)
	   
		# writing new file
	   
		with open(new_fastafile, "w") as f_fas:
			for i in range(self.n_seq):
				item = ">seq_" + str(i)
				f_fas.write(item + "\n")
				item = self.fas.seq_words[i]
				f_fas.write(item + "\n")
	   
		### ---------------- alignement with phylip format
		
		print("aligning fastafile ...")
		cmd		= "mafft --phylipout  " + new_fastafile  + " > " + phylfile + ' 2> /dev/null'
		print(cmd)
		os.system(cmd)
	   
		### ------------------ running phyML (finally ...)
		
		print_col("\nrunning phyML ...", color="green")
		cmd		= "phyml -i " + phylfile + ' > /dev/null 2>&1'
		print(cmd)
		os.system(cmd)
		treefile		= phylfile + '_phyml_tree.txt'
		newickfile	= basename + ".newick"
		print_col("newick file written in <" + newickfile + ">", color="cyan")
		
		### ------------------- changes tree labels with charaters
		
		if charfile:
			with open(treefile) as phy:
				nwk = re.sub(r'(?P<init>[\(|,])(?P<label>seq_\d+):',
						lambda x: f'{x.group(1)}{dic_taxo[x.group(2)]}:',
						phy.readlines()[0])

			nwfile	= basename + ".newick"
			with open(nwfile, 'w') as phy:
				print(nwk, file=phy)
		

	# ------------------------------------------------------------------

	def show_dendro(self, nwfile):
		"""tree visualisation with dendroscope
		af, 24.03.28
		""" 
		print_col("\n[yap_classes]:[phyloTree]:[show_dendro()]\n", color="magenta")
		cmd 	= f'dendroscope --hideMessageWindow --hideSplash -x "open file={nwfile}; set drawer=RectangularPhylogram" 2> /dev/null'
		print(cmd)
		os.system(cmd)

	# ------------------------------------------------------------------

	def show_ete3(self, nwfile):
		"""tree visualisation with ete3
		af, 24.03.28
		"""
		print_col("\n[yap_classes]:[phyloTree]:[show_ete3()]\n", color="magenta")
		eteTree	= ete3.Tree(nwfile)
		eteTree.show()
		
	# ------------------------------------------------------------------
	
	def evol_distances(self, nwfile, disfile):
		"""Computes pairwise evolutionary distances
		af, 24.03.28
		"""
		
		print_col("[yap_classes]:[phyloTree]:[evol_distances()]", color="magenta")
		
		### ------------- parameters
		
		basename	= self.basename
		n			= self.fas.n_seq	# number of sequences
		phyloDis	= np.zeros((n,n))
		
		### ---------- loads the newick file as a ete3 tree(), and computes distances
		
		print("commputes the distances ...")
		eteTree	= ete3.Tree(nwfile)
		for i in range(n):
			for j in range(i+1,n):
				d				= eteTree.get_distance("seq_" +str(i) , "seq_"+str(j)) 
				phyloDis[i,j]	= d
				phyloDis[j,i]	= d
		### -------------- writing the distance array
		
		rnames	= [self.seqids[i] for i in range(n)]
		cnames	= [self.seqids[i] for i in range(n)]
		#
		print("writes the distance array in a file ...")
		with open(disfile, "w") as f_dis:
			headers		= ["SEQ_ID"] + cnames
			item 		= ('\t').join(headers)
			f_dis.write(item + '\n')
			for i in range(n):
				vals 	= phyloDis[i,:].tolist()
				vpl 	= [str(x) for x in vals]
				pl		= [rnames[i]] + vpl
				item 	= ('\t').join(pl)
				f_dis.write(item + '\n')
		#
		print_col("distance array of evolutuonary distances written in <" + disfile + ">", color="cyan")
		
		
# ======================================================================
#
#						mapping()
#
# ======================================================================


class mapping():
	"""class for mapping reads on a reference database and annotating
	
	Required files
	--------------
	- a fasta file of queries to be mapped and annotated
	- a reference database (subject id)
	- a character file of the reference db
	
	How it works
	------------
	
	Step 1: mapping
	---------------
	blast_queries()		-> builds an 11 column blast file by mapping queries on a reference database with blast
	mmseq_queries()		-> the same, by mapping with mmseq
	
		output: a blastfile, or a mmseqfile, referred to here as mapfile 
	
	Step 2: getting best hit
	------------------------
	get_besthit()		-> builds a dictionary with queries as keys, and refid of the besthit as values
	
		output: dic_subject, a dictionary, written as a yaml file named besthit_file 
	
	
	Step 3: annotating
	------------------
	annotates_with_besthit()	builds a dictionary with queries as keys and the taxon of the besthit as value
	
		output: dic_annot, a dictionary,  written as a yaml file named annot_file 
		
		
	
	Sequence of operations
	----------------------
	data structure --(method)--> data structure
	
	fastafile --(blast,mmseq)--> mapfile --(besthit)--> dic_subject --(annotates)--> dic_taxo  
	
	af, 24.09.11
	"""
	
	def __init__(self, basename=None, reference=None, mapping="blastn"):
		"""
		"""
		self.c_name	= "mapping"
		yut.print_through_init(self.c_name)
		#
		self.mapping			= mapping
		self.queries_fasfile	= f'{basename}.fas'
		#
		self.reference			= reference
		self.ref_charfile		= f'{reference}.char'
		
	# ------------------------------------------------------------------

	def blast_queries(self, blastfile, num_threads, num_alignments=50, verbose=True):
		"""blasts a set of queries on a blast database
		
		Notes
		-----
		- 
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		#### ----------- building the blast command
		
		cmd	= "blastn -num_threads "  + str(num_threads) + " -db " + self.reference + " -query " + self.queries_fasfile
		cmd	= cmd + " -outfmt '6 qaccver saccver pident length mismatch gapopen qstart qend sstart send evalue' " 
		cmd	= cmd + "-num_alignments " + str(num_alignments) + " -out " + blastfile 
		#
		if verbose:
			print("blast command line is")
			print(cmd)
		os.system(cmd)
		if verbose:
			print_col("blast outfile is written in", blastfile, color="green")
			
	# ------------------------------------------------------------------
	
	def get_besthit(self, mapfile=None, besthit_file=None):
		"""
		gets the best hit of each query from a blastfile or a mmseqfile
		
		@ returns
		- dic_subject	dictionary		key:	each query
										value:	the id of the best hit
		
		Notes
		-----
		- bastfile or mmseq file are referred to here as mapfile
		
		af, 24.09.11
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		dic_subject	= {}
		
		### ------------ loads the 11 columns mm file generated by mmseq with module map or search
		
		Tab			= np.loadtxt(mapfile, delimiter="\t", dtype=object)
		queries		= Tab[:,0].astype(str).tolist()
		subjects	= Tab[:,1].astype(str).tolist()
		n			= Tab.shape[0]
		print(f'there are {str(n)} queries (repeated)')

		### ---------- finds the best hit for each query

		# initialisation
		q				= queries[0]
		s				= subjects[0]
		dic_subject[q]	= s
		set_queries		= [q]

		# loop over all queries
		for i in range(1,n):
			#
			q_next	=  queries[i]
			if q_next!=q:
				s					= subjects[i]
				dic_subject[q_next]	= s
				q					= q_next
				#print(q_next, "->", s)
				set_queries.append(q_next)

		print_col("dictionary dic_subject built ...", color="green")
		
		if besthit_file:
			#self.besthit_file	= besthit_file
			print("writing dic_subject as a yaml file ...")
			with open(besthit_file, "w") as f_bhit:
				for key in dic_subject.keys():
					item	= key + ": " + dic_subject[key] + "\n" 
					f_bhit.write(item)
			print_col(f'dic_subject dictionary written in yaml file {besthit_file}', color="cyan")
		
		#return dic_subject
		
	# ------------------------------------------------------------------
	
	def annotates_with_besthit(self, varname, besthit_file, annot_file=None):
		"""
		
		arguments
		---------
		
		@ ref_charfile		string		character file of references
		@ varname			string		selected taxonomic level
		@ dic_subject		dictionary	key:	a query
										value:	refid of the best_hit
		@ dic_taxo_file		string		name of yaml file for saving annotations
		
		returns
		-------
		
		@ dic_taxo			dictionary		key:	a query
											value	taxoomic annotaton with varname
											
											
		Notes
		-----
		
		- a taxonomic level is a header of the reference character file
		- dic_subject must have been built upstream by <get_besthit>
		- the annotation per query is built as a dictionary, and can be saved as a yaml file 
		
		af, 24.09.11
		"""
		
		# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		### ------------- builds the vector of values of the varname in reference character file
		 
		Ref				= np.loadtxt(self.ref_charfile, delimiter="\t", comments='&', dtype=object)		# loads reference character file
		headers			= Ref[0,:].astype(str).tolist()
		print_col(f'varnames are: {headers}', color="cyan")
		k				= headers.index(varname)
		#
		refid			= Ref[1:,0].astype(str).tolist()			# subject id 
		v_tax			= Ref[1:,k].astype(str).tolist()			# names
		
		### --------------- loading dic_subject
		
		try:
			dic_subject	= yaml.load(open(besthit_file), Loader=yaml.SafeLoader)
			print("besthit_file file loaded")
		except Exception as e:
			traceback.print_exc()
		
		### -------------- annotates each query
		
		dic_annot		= {}
		n				= len(dic_subject.keys())

		for i, key in enumerate(dic_subject.keys()):
			#
			if i % 1000 == 0:
				print('row ',i," / ",n, end='\r')
			sub	= dic_subject[key]
			try:
				k 				= refid.index(sub)
				dic_annot[key]	= v_tax[k]
			except:
				print_col(f'subject {sub} is not in character file', color=blue)

		### -------------- writing dic_taxo as a yaml file
		
		if annot_file:
			#self.annot_file	= annot_file
			print("writing dic_annot as a yaml file ...")
			#dic_taxo_file	= f'{basename}.{varname}.dic_taxo.yaml')
			with open(annot_file, "w") as f_annot:
				for key in dic_annot.keys():
					item	= key + ": " + dic_annot[key] + "\n" 
					f_annot.write(item)
		#
		#return dic_annot
		
	# ------------------------------------------------------------------
	
	def build_inventory(self, annot_file, invfile):
		""" builds the taxonomic inventory of a sample
		
		arguments
		---------
		@ dic_taxo_file		string		name of yaml file for saving annotations
		@ invfile			string		name of the file for saving inventory
		
		returns
		-------
		- writes a file
		
		Notes
		-----
		- invfile is 	an ascii file
						with two columns	col 1	a taxon
											col 2	the number of reads in the sample
													annotated with this taxon
						delimiters	tabulations
		"""
		
				# where it is ...
		f_name	= sys._getframe().f_code.co_name 		# name of the method
		yut.print_through_meth(self.c_name, f_name)
		
		print(annot_file)
		"""
		try:
			dic_annot	= yaml.load(open(annot_file), Loader=yaml.SafeLoader)
			print("annot_file file loaded")
		except:
			print_col(f'annot_file {annot_file} not found', color="red") 
		"""
		dic_annot	= yaml.load(open(annot_file), Loader=yaml.SafeLoader)
		inv_pl	= []
		for key in dic_annot.keys():
			inv_pl.append(dic_annot[key])
		#
		res	= count_and_order(inv_pl)
		with open(invfile,"w") as f_inv:
			for item in res:
				f_inv.write(item[0] + "\t" + str(item[1]) + "\n")



# ======================================================================
#
#				That's all folks
#
# ======================================================================
